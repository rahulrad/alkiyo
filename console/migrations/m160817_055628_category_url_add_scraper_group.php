<?php

use yii\db\Migration;

class m160817_055628_category_url_add_scraper_group extends Migration
{
    public function up()
    {
		$this->addColumn('category_url', 'scraper_group', 'INT(11) NULL AFTER store_id');
    }

    public function down()
    {
        echo "m160817_055628_category_url_add_scraper_group cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
