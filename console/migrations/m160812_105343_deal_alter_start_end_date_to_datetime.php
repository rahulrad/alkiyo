<?php

use yii\db\Migration;

class m160812_105343_deal_alter_start_end_date_to_datetime extends Migration
{
    public function up()
    {
		$this->alterColumn('deals', 'start_date', "DATETIME");
		$this->alterColumn('deals', 'end_date', "DATETIME");
		
    }

    public function down()
    {
       echo 'Deal `start_date` and `end_date` cannot be reverted.';
    }

}
