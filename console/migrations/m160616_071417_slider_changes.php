<?php

use yii\db\Migration;

class m160616_071417_slider_changes extends Migration
{
    public function up()
    {
		$this->addColumn('sliders', 'slider_type', "ENUM('home', 'category') AFTER image");
		$this->addColumn('sliders', 'category_id', 'INT(11) NULL AFTER slider_type');
    }

    public function down()
    {
        echo "m160616_071417_slider_changes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
