<?php

use yii\db\Migration;

class m160712_054148_deals_table_drop_and_add extends Migration
{
    public function up()
    {
		
		$this->dropTable('deals');
		
			$this->createTable('deals', [
            'id' => $this->primaryKey(),
			'store_id' => $this->integer(11),
			'title' => $this->string(255),
			'description' => $this->text(),
			'brand' => $this->string(255),
			'link' => $this->string(255),
			'image' => $this->string(255),
			'price' => $this->string(20),
			'offer_price' => $this->string(20),
			'availability' => $this->string(255),
			'sizes' => $this->string(255),
			'status' => $this->string(10)->defaultValue('active'),
			'created' => $this->dateTime(),
        ]);
    }

    public function down()
    {
         
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
