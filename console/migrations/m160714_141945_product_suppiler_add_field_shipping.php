<?php

use yii\db\Migration;

class m160714_141945_product_suppiler_add_field_shipping extends Migration
{
    public function up()
    {
		$this->addColumn('products_suppliers', 'shipping', 'VARCHAR(255) NULL AFTER delivery');
    }

    public function down()
    {
        echo "m160714_141945_product_suppiler_add_field_shipping cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
