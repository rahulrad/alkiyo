<?php

use yii\db\Migration;

class m160827_051352_filter_groups_add_table extends Migration
{
    public function up()
    {
			$this->createTable('filter_groups', [
					'id' => $this->primaryKey(),
					'category_id' => $this->integer(11),
					'title' => $this->string(255),
					'status' => $this->integer()->defaultValue(1),	
					'is_delete' => $this->integer()->defaultValue(0),					
					'created' => $this->dateTime(),
					]);
				  
    }

    public function down()
    {
        echo "m160827_051352_filter_groups_add_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
