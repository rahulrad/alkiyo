<?php

use yii\db\Migration;

class m160624_070112_create_ads_table extends Migration
{
    public function up()
    {
        $this->createTable('ads', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
			'url' => $this->string(255),
			'description' => $this->text(),
			'image' => $this->string(255),
			'meta_title' => $this->string(255),
			'meta_keyword' => $this->text(),
			'meta_desc' => $this->text(),
			'status' => $this->string(10)->defaultValue('active'),
			'created' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        $this->dropTable('ads');
    }
}
