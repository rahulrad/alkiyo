<?php

use yii\db\Migration;

/**
 * Class m200103_130546_brand_history
 */
class m200103_130546_brand_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200103_130546_brand_history cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200103_130546_brand_history cannot be reverted.\n";

        return false;
    }
    */
}
