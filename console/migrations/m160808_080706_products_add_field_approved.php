<?php

use yii\db\Migration;

class m160808_080706_products_add_field_approved extends Migration
{
    public function up()
    {
		$this->addColumn('products', 'is_approved', 'INT(1) NOT NULL DEFAULT 0 AFTER active');
    }

    public function down()
    {
        echo "m160808_080706_products_add_field_approved cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
