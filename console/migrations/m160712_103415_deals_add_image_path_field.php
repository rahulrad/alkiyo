<?php

use yii\db\Migration;

class m160712_103415_deals_add_image_path_field extends Migration
{
    public function up()
    {
		$this->addColumn('deals', 'image_path', 'VARCHAR(11) NULL AFTER image');
    }

    public function down()
    {
        echo "m160712_103415_deals_add_image_path_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
