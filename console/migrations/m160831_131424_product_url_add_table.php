<?php


use yii\db\Migration;

class m160831_131424_product_url_add_table extends Migration
{
    public function up()
    {
		$this->createTable('product_urls', [
							
					'id' => $this->primaryKey(),
							
					'store_id' => $this->integer(11),
					
					'cateogry_url_id' => $this->integer(11),
							
					'url' => $this->text(),		
					
					'status' => $this->integer()->defaultValue(0),
						
					]);
					
    }

    public function down()
    {
        echo "m160831_131424_product_url_add_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
