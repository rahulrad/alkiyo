<?php

use yii\db\Migration;

class m160822_090429_product_alter_updated_field_name extends Migration
{
    public function up()
    {
			$this->renameColumn('products', 'updated_price_date','updated');
    }

    public function down()
    {
        echo "m160822_090429_product_alter_updated_field_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
