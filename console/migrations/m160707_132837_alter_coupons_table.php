<?php

use yii\db\Migration;

class m160707_132837_alter_coupons_table extends Migration
{
    public function up()
    {
		$this->alterColumn('coupons', 'promo_id','VARCHAR(20) NULL DEFAULT NULL');
    }

    public function down()
    {
        echo "m160707_132837_alter_coupons_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
