<?php

use yii\db\Migration;

class m160705_083316_add_product_tags_table extends Migration
{
    public function up()
    {
		$this->createTable('products_tag', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(11),
			'tag_id' => $this->integer(11),
			'status' => $this->string(10)->defaultValue('active'),
			'created' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        echo "m160705_083316_add_product_tags_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
