<?php

use yii\db\Migration;

class m160620_145834_category_data extends Migration
{
    public function up()
    {
$this->addColumn('categories_data', 'store_id','INT(11) NOT NULL DEFAULT 0 AFTER store_name');
    }

    public function down()
    {
        echo "m160620_145834_category_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
