<?php

use yii\db\Migration;

class m160811_142109_scraper_logs_add_new extends Migration
{
    public function up()
    {
		$this->createTable('scraper_logs', [
            'id' => $this->primaryKey(),
            'file_name' => $this->string(255),
			'created' => $this->string(20),
        ]);
    }

    public function down()
    {
        echo "m160811_142109_scraper_logs_add_new cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
