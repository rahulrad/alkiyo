<?php

use yii\db\Migration;

class m160702_082254_add_new_fields_slider extends Migration
{
    public function up()
    {
		$this->addColumn('sliders', 'start_date', 'VARCHAR(25) NULL AFTER link');
		$this->addColumn('sliders', 'end_date', 'VARCHAR(25) NULL AFTER start_date');
    }

    public function down()
    {
        echo "m160702_082254_add_new_fields_slider cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
