<?php

use yii\db\Migration;

class m160702_082317_add_new_fields_ads extends Migration
{
    public function up()
    {
		$this->addColumn('ads', 'position', 'VARCHAR(255) NULL AFTER image');
		$this->addColumn('ads', 'start_date', 'VARCHAR(25) NULL AFTER position');
		$this->addColumn('ads', 'end_date', 'VARCHAR(25) NULL AFTER start_date');
    }

    public function down()
    {
        echo "m160702_082317_add_new_fields_ads cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
