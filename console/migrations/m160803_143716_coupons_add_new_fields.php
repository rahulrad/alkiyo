<?php
use yii\db\Migration;

class m160803_143716_coupons_add_new_fields extends Migration
{
    public function up()
    {
		$this->addColumn('coupons', 'api_name', 'VARCHAR(255) NULL AFTER expiry_date');
		$this->addColumn('coupons', 'merchant', 'VARCHAR(255) NULL AFTER api_name');
		$this->addColumn('coupons', 'merchant_logo_url', 'VARCHAR(255) NULL AFTER merchant');
    }

    public function down()
    {
        echo "m160803_143716_coupons_add_new_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
