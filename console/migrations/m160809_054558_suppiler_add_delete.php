<?php

use yii\db\Migration;

class m160809_054558_suppiler_add_delete extends Migration
{
    public function up()
    {
		$this->addColumn('suppliers', 'is_delete', 'INT(1) NOT NULL DEFAULT 0 AFTER status');
    }

    public function down()
    {
        echo "m160809_054558_suppiler_add_delete cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
