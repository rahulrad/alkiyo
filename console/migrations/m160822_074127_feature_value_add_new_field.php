<?php

use yii\db\Migration;

class m160822_074127_feature_value_add_new_field extends Migration
{
    public function up()
    {
		$this->addColumn('feature_values', 'display_name', 'VARCHAR(255) NULL AFTER value');
    }

    public function down()
    {
        echo "m160822_074127_feature_value_add_new_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
