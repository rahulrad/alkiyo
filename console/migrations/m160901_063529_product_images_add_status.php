<?php

use yii\db\Migration;

class m160901_063529_product_images_add_status extends Migration
{
    public function up()
    {
			$this->addColumn('product_image', 'scraper_image_status', 'INT(1) NOT NULL DEFAULT 0 AFTER image_path');
    }

    public function down()
    {
        echo "m160901_063529_product_images_add_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
