<?php

use yii\db\Migration;

class m160827_051405_filter_group_values_add_table extends Migration
{
    public function up()
    {
			$this->createTable('filter_group_values', [
					'id' => $this->primaryKey(),
					'group_item_id' => $this->integer(11),
					'value_id' => $this->integer(11),
					'status' => $this->integer()->defaultValue(1),	
					'is_delete' => $this->integer()->defaultValue(0),					
					'created' => $this->dateTime(),
					]);
    }

    public function down()
    {
        echo "m160827_051405_filter_group_values_add_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
