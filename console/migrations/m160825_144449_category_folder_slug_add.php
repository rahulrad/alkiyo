<?php

use yii\db\Migration;

class m160825_144449_category_folder_slug_add extends Migration
{
    public function up()
    {
			$this->addColumn('brands', 'folder_slug', 'VARCHAR(255) NULL AFTER slug');
    }

    public function down()
    {
        echo "m160825_144449_category_folder_slug_add cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
