<?php

use yii\db\Migration;

class m160622_132315_products_store_id extends Migration
{
    public function up()
    {
		$this->alterColumn('products', 'store_id','INT(11) NULL DEFAULT NULL');
    }

    public function down()
    {
        echo "m160622_132315_products_store_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
