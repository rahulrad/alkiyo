<?php

use yii\db\Migration;

class m160720_101934_slider_image_add_table extends Migration
{
    public function up()
    {
		$this->createTable('slider_images', [
            'id' => $this->primaryKey(),
            'slider_id' => $this->integer(11),
			'image' => $this->string(255),
			'description' => $this->text(),
			'link' => $this->string(255),
			'created' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        echo "m160720_101934_slider_image_add_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
