<?php

use yii\db\Migration;

class m160805_114554_features_add_field_display_name extends Migration
{
    public function up()
    {
		$this->addColumn('features', 'display_name', 'VARCHAR(255) NULL AFTER name');
    }

    public function down()
    {
        echo "m160805_114554_features_add_field_display_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
