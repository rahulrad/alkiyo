<?php

use yii\db\Migration;

class m160916_095958_product_add_field extends Migration
{
    public function up()
    {
			$this->addColumn('products', 'store_product_name', 'VARCHAR(255) NULL AFTER slug');
    }

    public function down()
    {
        echo "m160916_095958_product_add_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
