<?php

use yii\db\Migration;

class m160704_061901_add_image_field_menus extends Migration
{
    public function up()
    {
		$this->addColumn('menus', 'image', 'VARCHAR(255) NULL AFTER type');
    }

    public function down()
    {
        echo "m160704_061901_add_image_field_menus cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
