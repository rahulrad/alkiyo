<?php

use yii\db\Migration;

class m160903_042254_add_index extends Migration
{
    public function up()
    {
        $this->createIndex('idx_group_item_id', \backend\models\FilterGroupValues::tableName(), 'group_item_id');
        $this->createIndex('idx_value_id', \backend\models\FilterGroupValues::tableName(), 'value_id');
        
        $this->createIndex('idx_group_id', \backend\models\FilterGroupItems::tableName(), 'group_id');
        $this->createIndex('idx_feature_id', \backend\models\FilterGroupItems::tableName(), 'feature_id');
        
        $this->createIndex('idx_category_id', \backend\models\FilterGroups::tableName(), 'category_id');
        
    }

    public function down()
    {
        echo "m160903_042254_add_index cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
