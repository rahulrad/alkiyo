<?php

use yii\db\Migration;

class m160722_053229_category_field_alter extends Migration
{
    public function up()
    {
		$this->alterColumn('categories', 'show_home_page','INT(1)');
		$this->alterColumn('categories', 'sort_order','INT(11)');
    }

    public function down()
    {
        echo "m160722_053229_category_field_alter cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
