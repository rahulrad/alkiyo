<?php

use yii\db\Migration;

class m160712_120220_deals_add_discount_fields extends Migration
{
    public function up()
    {
		$this->addColumn('deals', 'discount', 'VARCHAR(50) NULL AFTER offer_price');
		$this->addColumn('deals', 'start_date', 'VARCHAR(50) NULL AFTER discount');
		$this->addColumn('deals', 'end_date', 'VARCHAR(50) NULL AFTER start_date');
    }

    public function down()
    {
        echo "m160712_120220_deals_add_discount_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
