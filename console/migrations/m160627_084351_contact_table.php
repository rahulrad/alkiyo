<?php

use yii\db\Migration;

class m160627_084351_contact_table extends Migration
{
    public function up()
    {
		$this->createTable('contacts', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
			'email' => $this->string(100),
			'message' => $this->text(),
			'phone' => $this->string(15),
			'status' => $this->string(10)->defaultValue('active'),
			'created' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        echo "m160627_084351_contact_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
