<?php

use yii\db\Migration;

class m160817_152109_recently_viewed_product extends Migration
{
    public function up()
    {
		$this->createTable('viewed_products_by_user', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(11),
			'user_id' => $this->integer(11),
			'user_ip' => $this->string(50),
			'created_at' => $this->dateTime(),
			'updated_at' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        echo "viewed_products_by_user tabled cannot be dropped.\n";

        return false;
    }


}
