<?php

use yii\db\Migration;

class m160727_113811_product_reviews_rank_add_field extends Migration
{
    public function up()
    {
	$this->addColumn('product_reviews_rank', 'review_id', ' INT(11) NOT NULL DEFAULT 0 AFTER product_id');
    }

    public function down()
    {
        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
