<?php


use yii\db\Migration;

class m160927_082344_category_add_new_field extends Migration
{
    public function up()
    {
			$this->addColumn('categories', 'compare', 'INT(1) NULL AFTER api_icon');
    }

    public function down()
    {
        echo "m160927_082344_category_add_new_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
