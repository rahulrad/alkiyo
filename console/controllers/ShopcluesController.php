<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use common\models\Products;
use backend\models\CategoriesData;//
use backend\models\CategoryUrl;
use backend\models\Mobile;
use backend\models\Logs;


class ShopcluesController extends Controller {

    public function actionIndex() {
		$mobile = new Mobile();
		$mobile->test();
        echo "Hello world.";
    }
    
    public function actionHello() {
        echo "eeeee";exit;
    }
    
	
	public function actionSaveCatUrl(){
		
		$categories_data_model = new CategoriesData;
		$categoriesDatas = CategoriesData::find()->where(['type' => 0,'store_id'=>4])->all();
		 //echo '<pre>'; print_r($categoriesDatas); die;
		 if(!empty($categoriesDatas)){
				foreach($categoriesDatas as $categoriesData){
					
					// $model = CategoriesData::find($categoriesData->id)->one();
					 $model = CategoriesData::find()->where(['id' => $categoriesData->id])->one();
					 $model->type = 1;
					$model->save();
					 
					$url = parse_url($categoriesData->url); 
					parse_str($url['query'], $query);
					//echo '<pre>'; print_r($query); die;
									
					$count_products = $model->count_products;
					$perpage = 36;
					$list_page = $count_products / $perpage;
					$pages = round($list_page);
					
					$x  = 1;
					$y = 1;
				
					for($i = 1; $i<=$pages; $i++){
						$page_number = $i;
						
						
						$category_urls = 'http://www.shopclues.com/apicall.php?object_id='.$query['object_id'].'&pageType=c&page='.$page_number;
						
						 $category_url_model = new CategoryUrl;
						 $category_url_model->status = 0;
						 $category_url_model->store_name = 'shopclues';
						 $category_url_model->store_id = 4;
						 $category_url_model->category_name = $categoriesData->category_name;
						 $category_url_model->category_type = $categoriesData->category_type;
						 $category_url_model->site_url = $category_urls;
						$category_url_model->scraper_group =$y;
						if($x==10)
						{
							$y++;
							$x=0;
						}
						
						 $category_url_model->save();
						// echo 'pass'. $category_url_model->category_name.'<br>';
						$x++;	
						
					}   //die;
				  
			}	die('save category url for shopclues'); 
		 }else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('shopclues scraper','ShopcluesController','No Any Category Data Url for shopclues','Output',__LINE__);
				echo 'No Any Category Data for Save category Url for shopclues'; die;
		}
		 
	}
	
  public function actionScraper($latest_product=0) {
			
		$categories_url_model = new CategoryUrl;
		$categories_urls= CategoryUrl::find()->where(['status' => 0,'store_id'=>4,'scraper_group' => 1])->all();
		
		if(!empty($categories_urls)){
			foreach($categories_urls as $categories_url){
		 		
				$objShopcluesProduct = new \backend\models\ShopcluesProductScrapper();
                                
				//echo '<pre>'; print_r($categories_url); die;
				// $tools_componant = new \common\components\Tools();
				// $tools_componant->alertMail("Create a new brand named", "Create Brand", 'vinayverma158@gmail.com'); die;
				//Tools::killOtherProcess("php console.php jungleeproductscrapper new");
				//$siteName = 'http://www.snapdeal.com';
				// for men shoes category
				//$url = $siteName . '/lc/pr/pv1/spotList1/spot1/productList?sid=tyy%2C4io&filterNone=true&start=1&ajax=true&_=1460746573007';
				$url = $categories_url->site_url;
				$categoryName = $categories_url->category_name;
				$categoryType = $categories_url->category_type;
				//echo $categoryName.'<br>';
					$objShopcluesProduct->saveProduct($categoryName, '', $categoryType, $latest_product,'shopclues');
		
			
			}
			echo 'save products for shopclues';  die;
		}else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('shopclues scraper','SnapdealController','No any category url for scrapping','Output',__LINE__);
				echo 'No Any URLs for scurpping'; die;
		}
		 		
		//echo '<pre>'; print_r($objflipkartProduct); die;
    
	
	} 
	
	/**public function actionScraper($latest_product=0) {
			
            $objShopcluesProduct = new \backend\models\ShopcluesProductScrapper();
			
            //Tools::killOtherProcess("php console.php jungleeproductscrapper new");
            $siteName = 'http://www.snapdeal.com';
            // for men shoes category
            $url = $siteName . '/apicall.php?object_id=455&pageType=c&page=1';
            
            $categoryName = 'Mobile';
            $categoryType = 'Mobile';
			
			//echo '<pre>'; print_r($url); die;

    //        $arr_url = $objflipkartProduct->getAllPagingUrl($url);
    //        $objflipkartProduct->saveAllPagingUrl($arr_url, $categoryName, $categoryType);

    //        $arr_department_type = array('For' => 'Men');
            $objShopcluesProduct->saveProduct($categoryName, '', $categoryType, $latest_product,'shopclues');
            //echo '<pre>'; print_r($objSnapDealProduct); die;
    } **/

}
