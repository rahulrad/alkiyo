<?php

namespace console\controllers;


use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use common\models\Coupons;
use backend\models\CouponsSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Logs;
use yii\web\UploadedFile;
use common\models\CouponCategories;


class CouponsController extends Controller {

 public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post','scraper'],
                ],
            ],
        ];
    }
    public function actionIndex() {
        echo "Hello world.";
    }
    
    
	
		
	public function actionScraper(){
			
			//http://www.optimise.co.in
			//Usr: kishore@nayashoppy.com
			//Pwd : Raditya2014
			//api : 0791e488-ad84-4999-826f-5f5824846b7e
			//private key : 92e7698f8d784acfb4221dd35b2c3834
			//http://api.omgpm.com/network/OMGNetworkApi.svc/v1.2/Affiliate/GetVoucherCodes?Key={Key}&Sig={Signature}&SigData={SignatureData}&AID={AID}&AgencyID={AgencyID}&Status={Status}&StartDate={StartDate}&EndDate={EndDate}
			//$optimise_apiurl = 'http://api.omgpm.com/network/OMGNetworkApi.svc/v1.2/Affiliate/GetVoucherCodes?Key=0791e488-ad84-4999-826f-5f5824846b7e&Sig=92e7698f8d784acfb4221dd35b2c3834&SigData='.$sig_data.'&AID=965781&AgencyID=95';//
			
			
			/***********************************************************/
	
			
			// get data form optomise api for coupons
			
			// Delete All coupons before save
			
			
			Coupons::deleteAll();  // delete coupouns
			//CouponCategories::deleteAll();
			
			
			$optomiseCouponsData = $this->getOptimiseCouponsData();
			if(!empty($optomiseCouponsData)){
				
				foreach($optomiseCouponsData['GetVoucherCodesForAffiliateResult'] as $data){
					
					$couponsModel = new Coupons();
					
					$data['Code'] = str_replace('No Voucher Code','',$data['Code']);
					
					$checkResult = $couponsModel::find()->where(['offer_id' => $data['VoucherCodeId'],'code' => $data['Code']])->one();
						
						if(empty($checkResult)){
							
							$couponsModel->title = $data['Title'];
							$couponsModel->offer = $data['Product'];
							$couponsModel->code = $data['Code'];
							$couponsModel->link = $data['TrackingURL'];
							$couponsModel->promo_id = $data['PID'];
							$couponsModel->offer_id = $data['VoucherCodeId'];
							$couponsModel->offer_url = $data['TrackingURL'];
							$couponsModel->description = $data['Description'];
							$couponsModel->api_name = 'optimisemedia';
							$couponsModel->merchant = $data['Merchant'];
							$couponsModel->merchant_logo_url = $data['MerchantLogoURL'];
							
							
							//$couponsModel->start_date = $data['added'];
							//$couponsModel->expiry_date = $data['coupon_expiry'];
							$couponCategoriesModel = new CouponCategories();
							$categoryResult = $couponCategoriesModel->saveCategoryFromApi($data['CategoryName']);
							
							if(!empty($categoryResult)){
									$couponsModel->category = $categoryResult->id;
							}
							
							
							$couponsModel->created = date('Y-m-d h:i:s');
							
							$couponsModel->save(false);
							
						}
					
				}
			} 


			
			
			$payoom_apiurl = 'http://payoom.in/deeplinking/coupons-json.php?affid=20917';//

			$payoomCouponsData = json_decode(file_get_contents($payoom_apiurl), true);
			
			if(!empty($payoomCouponsData)){
				
				foreach($payoomCouponsData as $data){
					
					$couponsModel = new Coupons();
					
					$checkResult = $couponsModel::find()->where(['offer' => $data['campaign'],'code' => $data['coupon']])->one();
						
						if(empty($checkResult)){
							
							$couponsModel->title = $data['title'];
							$couponsModel->offer = $data['campaign'];
							$couponsModel->code = $data['coupon'];
							$couponsModel->start_date = $data['start_date'];
							$couponsModel->expiry_date = $data['end_date'];
							$couponsModel->link = $data['link'];
							$couponsModel->created = date('Y-m-d h:i:s');
							$couponsModel->api_name = 'payoom';
							
							$couponsModel->save(false);
							
						}
					
				}
			} 
				
			
			$vcommission_apiurl = 'https://tools.vcommission.com/api/coupons.php?apikey=7588c21a58502cb238a908dd68cb03bc2f83d8884ab03b98e0e3ba861224d7ff';//

			$couponsData = json_decode(file_get_contents($vcommission_apiurl), true);
			
			if(!empty($couponsData)){
				
				foreach($couponsData as $data){
					
					$couponsModel = new Coupons();
					$checkResult = $couponsModel::find()->where(['promo_id' => $data['promo_id']])->one();
						
						if(empty($checkResult)){
							$couponsModel->promo_id = $data['promo_id'];
							$couponsModel->offer_id = $data['offer_id'];
							$couponsModel->offer = $data['offer_name'];
							$couponsModel->offer_type = $data['coupon_type'];
							$couponsModel->code = $data['coupon_code'];
							$couponsModel->title = $data['coupon_title'];
							$couponsModel->description = $data['coupon_description'];
							
							$couponCategoriesModel = new CouponCategories();
							$categoryResult = $couponCategoriesModel->saveCategoryFromApi($data['category']);
							
							if(!empty($categoryResult)){
									$couponsModel->category = $categoryResult->id;
							}
							
							$couponsModel->offer_url = $data['preview_url'];
							$couponsModel->start_date = $data['added'];
							$couponsModel->expiry_date = $data['coupon_expiry'];
							
							$couponsModel->featured = $data['featured'];
							$couponsModel->exclusive = $data['exclusive'];
							$couponsModel->ref_id = $data['ref_id'];
							$couponsModel->link = $data['link'];
							$couponsModel->store_link = $data['store_link'];
							$couponsModel->created = date('Y-m-d h:i:s');
							$couponsModel->api_name = 'vcommission';
							$couponsModel->save(false);
							
							//echo '<pre>'; print_r($couponsModel); die;
						}
					
				}
			
			echo 'Saved Coupons'; die; 			
			//Yii::$app->session->setFlash('success','Copouns Saved Success');
			//return $this->redirect(['index']);
				
			}
		 
			
	}
	
	
	public function getOptimiseCouponsData(){
			
			error_reporting(E_ALL ^ E_WARNING); 

			date_default_timezone_set("UTC");
			$t = microtime(true);
			$micro = sprintf("%03d",($t - floor($t)) * 1000);
			$utc = gmdate('Y-m-d H:i:s.', $t).$micro;

			$sig_data= $utc;
			//########################Please Add your API Key & Private Key here to test##################################################
			$api_key='0791e488-ad84-4999-826f-5f5824846b7e';
			$private_key='92e7698f8d784acfb4221dd35b2c3834';
			//############################################################################################################################

			$concateData = $private_key.$sig_data;
			$sig = md5($concateData);

			//############# This is a test url. You have to change the parameters according to your need #######################
			$url="http://api.omgpm.com/network/OMGNetworkApi.svc/v1.2/Affiliate/GetVoucherCodes?". http_build_query(array(
			 'AgencyID' => 95, 
			 'AID'=>965781,
			 'Key' => $api_key,
			 'Sig' => $sig,
			 'SigData' => $sig_data,
			 'Status'=> 'Active'
			));

			//echo "URL: ".$url."<br><br>";

			$headers = array( 
			 "Content-Type: application/json",
			 "Accept: application/json",
			 "Access-Control-Request-Method: GET" 
			 );

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			 
			$result = curl_exec($ch);
			$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			 
			$optimiseCouponsData = json_decode($result, true);
			
			return $optimiseCouponsData;
		
	}
	
	
	

}
