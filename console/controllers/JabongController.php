<?php
namespace console\controllers;
use yii;

use yii\console\Controller;
use yii\helpers\Console;
use common\models\Products;
use backend\models\CategoriesData;//
use backend\models\CategoryUrl;
use backend\models\Mobile;
use backend\models\Logs;
use backend\models\ProductUrls;
use common\models\Categories;


class JabongController extends Controller {

    public function actionIndex() {
		$mobile = new Mobile();
		$mobile->test();
        echo "Hello world.";
    }
    
    public function actionHello() {
        echo "eeeee";exit;
    }
    
	
	public function actionSaveCatUrl(){
		
		$categories_data_model = new CategoriesData;
		$categoriesDatas = CategoriesData::find()->where(['type' => 0,'store_id'=>6])->all();
		  //echo '<pre>'; print_r($categoriesDatas); die;
		  if(!empty($categoriesDatas)){
				foreach($categoriesDatas as $categoriesData){
				
						// $model = CategoriesData::find($categoriesData->id)->one();
						 $model = CategoriesData::find()->where(['id' => $categoriesData->id])->one();
						
						 $model->type = 1;
						 $model->save();
						 
						 $url = parse_url($categoriesData->url); 

						parse_str($url['query'], $query);

						$count_products = $model->count_products;

						$perpage = 20;

						$list_page = $count_products / $perpage;

						$pages = round($list_page);

						$x  = 1;
						$y = 1;
						
						$categoryUrlModel = new CategoryUrl;
						$countLastInsertUrls = $categoryUrlModel->getLastInsertCategoryUrl($categoriesData);
						$pages = $pages + $countLastInsertUrls; // count pages aur last insert url for same category url
						$i_value = ($countLastInsertUrls != 0) ? $countLastInsertUrls + 1 : 1;
						
						for($i = $i_value; $i<=$pages; $i++){
							
							//www.jabong.com/men/accessories/watches/?source=topnav_men&ax=1&page=1&limit=5&sortBy=desc;
							
							$category_urls = 'http://www.jabong.com/'.$categoriesData->path.'&ax=1&page='.$i.'&limit='.$perpage.'&sortBy=desc';
						
							  
							 $category_url_model = new CategoryUrl;
							 $category_url_model->status = 0;
							 $category_url_model->store_name = 'jabong';
							 $category_url_model->store_id = 6;
							 $category_url_model->category_name = $categoriesData->category_name;
							 $category_url_model->category_type = $categoriesData->category_type;
							 $category_url_model->site_url = $category_urls;
							
							$category_url_model->scraper_group =$y;
							 if($x==10)
								{
									$y++;
									$x=0;
								}
								
								 $category_url_model->save();
								// echo 'pass'. $category_url_model->category_name.'<br>';
								$x++;	
							// echo 'pass'. $category_url_model->category_name.'<br>';
							
						}  
					  
				}	die('save category url for jabong');
		  }else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('jabong scraper','JabongController','No Any Category Data Url for jabong','Output',__LINE__);
				echo 'No Any Category Data for Save category Url for jabong'; die;
		}
		 
	}
	
	
	

  public function actionScraperProductUrls($latest_product=0) {
		
		$max_scraper_group = CategoryUrl::find()->where(['store_id' => 6])->orderBy(['scraper_group' => SORT_DESC])->one();
		
		if(!empty($max_scraper_group)){
				
				for($i = 1; $i <=  $max_scraper_group->scraper_group; $i++){
					
					$categories_url_model = new CategoryUrl;
					$categories_urls= CategoryUrl::find()->where(['status' => 0,'store_id' => 6,'scraper_group' => $i])->all();
					
					if(!empty($categories_urls)){
						 foreach($categories_urls as $categories_url){
								
								$objjabongProduct = new \backend\models\JabongProductScrapper();
								
								//Tools::killOtherProcess("php console.php jungleeproductscrapper new");
								$siteName = 'http://www.jabong.com';
								// for men shoes category
								//$url = $siteName . '/lc/pr/pv1/spotList1/spot1/productList?sid=tyy%2C4io&filterNone=true&start=1&ajax=true&_=1460746573007';
								$url = $categories_url->site_url;
								$categoryName = $categories_url->category_name;
								$categoryType = $categories_url->category_type;
								$scraper_group = $i;
									
								//$objsnapdealProduct->saveProductUrls($categoryName, '', $categoryType, $latest_product,'snapdeal',$scraper_group);
								
								$objjabongProduct->saveProductUrls($categoryName, '', $categoryType, $latest_product,'jabong',$scraper_group);
							
						}
						//echo 'save products for flipkart'; die;
					}
				} //die('sadfasd');
		}else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('jabong scraper','JabongController','No any category url for scrapping','Output',__LINE__);
				echo 'No Any URLs for scurpping'; die;
		}
		
  }

 
 
 
 
 
 
  public function actionScraper($latest_product=0) {
	  
	  $product_urls = ProductUrls::find()->where(['store_id' => 6,'status' => 0])->all();
		
		if(!empty($product_urls)){
				
				foreach($product_urls as $product_url){
					
					/**$categories_url_model = new CategoryUrl;
					$categories_urls= CategoryUrl::find()->where(['id_category_url' => $product_url->cateogry_url_id])->one(); **/
					$categories_urls= Categories::find()->where(['category_id' => $product_url->category_id])->one();
					$category_name = !empty($categories_urls) ? $categories_urls->category_name : '';
					$objjabongProduct = new \backend\models\JabongProductScrapper();
					$objjabongProduct->saveProductData($product_url,$category_name);
					
				}
				
		}else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('jabong scraper','JabongController','No any category url for scrapping','Output',__LINE__);
				echo 'No Any URLs for scurpping'; die;
		}
		
	} 
 

	/***public function actionScraper($latest_product=0) {
		
		$objJabongProduct = new \backend\models\JabongProductScrapper();
        //Tools::killOtherProcess("php console.php jungleeproductscrapper new");
        $siteName = 'http://www.jabong.com';
        // for men shoes category
        $url = $siteName . '/men/accessories/watches/?source=topnav_men&ax=1&page=1&limit=10&sortBy=desc';
        $categoryName = 'Mens';
        $categoryType = 'Mens';
		
//        $arr_url = $objflipkartProduct->getAllPagingUrl($url);
//        $objflipkartProduct->saveAllPagingUrl($arr_url, $categoryName, $categoryType);

//        $arr_department_type = array('For' => 'Men');
        $objJabongProduct->saveProduct($categoryName, '', $categoryType, $latest_product,'jabong');
		echo '<pre>'; print_r($objJabongProduct); die;
    } **/

}
