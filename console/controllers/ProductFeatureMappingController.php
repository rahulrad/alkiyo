<?php
/**
 * Created by PhpStorm.
 * User: alliancetec.
 * Date: 10/05/17
 * Time: 10:54 AM
 */

namespace console\controllers;


use common\models\FeatureGroups;
use common\models\Features;
use common\models\FeatureValues;
use common\models\ProductFeature;
use common\models\ProductFeatureMappings;
use yii\console\Controller;

class ProductFeatureMappingController extends Controller
{
    public $category = [];

    public function options()
    {
        return ['category'];
    }

    public function actionFilterDuplicateFeatures()
    {
        $productFeatures = ProductFeature::find()
            ->joinWith('feature', true)
            ->andWhere('id_feature <= :id_feature', [
                ':id_feature' => Features::FEATURE_LAST_ID,
            ])
//            ->andWhere(['in', 'features.category_id', $this->category])
            ->limit(5000)
            ->orderBy('features.category_id desc')
            ->all();
        print_r(count($productFeatures));
        foreach ($productFeatures as $productFeature) {
            $oldFeature = $productFeature->feature;
            $oldFeatureValue = $productFeature->featureValue;
            if (!is_null($oldFeature) && !is_null($oldFeatureValue)) {
                print_r($productFeature->attributes);
                print_r($oldFeature->attributes);
                $this->updateProductFeature($oldFeature, $oldFeatureValue, $productFeature);
            } else {
                $productFeature->delete();
            }
        }
    }

    private function updateProductFeature(Features $oldFeature, FeatureValues $oldFeatureValue, ProductFeature $productFeature)
    {
        $productFeatureMapping = ProductFeatureMappings::find()->where([
            'id_category' => $oldFeature->category_id,
            'store_feature_name' => trim($oldFeature->name),
        ])->one();
        if (!is_null($productFeatureMapping)) {
            print_r($productFeatureMapping->attributes);
            $productFeature->id_feature = $productFeatureMapping->id_feature;
            $productFeature->save();
            $oldFeatureValue->feature_id = $productFeatureMapping->id_feature;
            $oldFeatureValue->save();
        } else {
            $productFeature->delete();
        }
    }
}