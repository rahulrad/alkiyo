<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use common\models\Deals;
use backend\models\DealsSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Logs;
use backend\models\DealUrls;
use yii\web\UploadedFile;


/**
 * DealsController implements the CRUD actions for Deals model.
 */
class DealsController extends Controller
{
	
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Deals models.
     * @return mixed
     */
    public function actionIndex()
    {
        echo 'hello'; die;
    }

    
	// funcation for save deals urls form api
	public function actionScraper(){
			
			// delete all deals before save new deals
			Deals::deleteAll();
			
			$path = Yii::getAlias('@frontend') .'/web/';
			array_map('unlink', glob($path."uploads/deals/*"));
			
			
			$dealsModel = new Deals();
			
			$dealsModel->getAmazonDealUrls();
			
			$dealsModel->getFlipkartDealUrls('nayashopp','2b64692f37a5493c97d0003b4f4a9543');
			
			$dealsModel->getSnapdealDealUrls('46884','4f47e6cc3ea375329136a5b3d3f125');
			
			$dealsModel->getEbayDealUrls();
			
			$dealsModel->getShopcluesDealUrls();
			
			echo 'Saved Deals..'; die;
			
	}


	
		
		
}