<?php
namespace console\controllers;
use yii;
use yii\console\Controller;
use yii\helpers\Console;
use common\models\Products;
use backend\models\CategoriesData;
use backend\models\CategoryUrl;
use common\models\ProductsSuppliers;
use backend\models\Logs;
use backend\models\ProductUrls;
use common\models\Categories;
use common\components\HttpService;
use common\components\Aws;

class FlipkartController extends Controller {

    public function actionIndex() {
        echo "Hello world.";
		
    }
    
    public function actionHello() {
							$objHttpService = new \common\components\HttpService();
							$proxy = $objHttpService->getProxyIps();
							
							$curl = curl_init();
							curl_setopt_array($curl, array(
							CURLOPT_URL => 'http://staging.nayashoppy.com/amazon_api/test.php',
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_ENCODING => "",
							CURLOPT_MAXREDIRS => 10,
							CURLOPT_TIMEOUT => 30,
							CURLOPT_SSL_VERIFYPEER => false,
							CURLOPT_SSL_VERIFYHOST => false,
							CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							CURLOPT_PROXY => $proxy['proxy_url'],
							CURLOPT_PROXYUSERPWD =>  $proxy['proxy_user'].':'.$proxy['proxy_pwd'],
							//CURLOPT_PROXYTYPE=> CURLPROXY_SOCKS5,
							CURLOPT_HTTPHEADER => array(
								"accept: */*",
								"accept-language: en-US,en;q=0.8",
								"Accept-Encoding: gzip, deflate, sdch, br",
								"cache-control: no-cache",
								"content-type: application/json",
								"postman-token: eddf1792-5624-cc13-585b-d9ec26718c9b",
								"referer: https://www.snapdeal.com/products/mobiles-mobile-phones?sort=plrty&q=Connectivity_s%3A4G%20LTE%7CScreensize_s%3A4.0%20-%204.5%5E5.0%20-%205.5%7C",
								"user-agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36",
							  ),
							));

							
							$response = curl_exec($curl);
							$err = curl_error($curl);
							echo '<pre>curl_getinfo==>';print_r(curl_getinfo($curl));
							echo '<pre>curl_error==>';print_r(curl_error($curl)); 
							
							curl_close($curl);
							
							echo '<pre>proxyDetail=>'; print_r($proxy);
							echo '<pre>Server=>'; print_r($_SERVER);
							echo '<pre>ssss'; print_r($response); die;
							
							
    }
    
/**	
	public function actionSave_cat_url(){
		$categories_data_model = new CategoriesData;
		$categoriesDatas = CategoriesData::find()->where(['type' => 0,'store_id'=>1])->all();
		
		if(!empty($categoriesDatas)){
				foreach($categoriesDatas as $categoriesData){
				
					// $model = CategoriesData::find($categoriesData->id)->one();
					 $model = CategoriesData::find()->where(['id' => $categoriesData->id])->one();
					  $model->type = 1;
					 $model->save();
					 
					 $url = parse_url($categoriesData->url); 
					parse_str($url['query'], $query);
					
					$count_products = $model->count_products;
					$perpage = 20;
					$list_page = $count_products / $perpage;
					$pages = round($list_page);
					$x  = 1;
					$y = 1;
					for($i = 0; $i<=$pages; $i++){
							$page_data = ($i * $perpage)+ 1;
							//6bo%2Cb5g
							//http://www.flipkart.com/lc/pr/pv1/spotList1/spot1/productList?sid=tyy,4io&filterNone=true&start=641&ajax=true&_=1460746573007
							
							$category_urls = 'http://www.flipkart.com/lc/pr/pv1/spotList1/spot1/productList?sid='.$query['sid'].'&filterNone=true&start='.$page_data.'&ajax=true&_=1460746573007';
							 
							 $category_url_model = new CategoryUrl;
							 $category_url_model->status = 0;
							 $category_url_model->store_name = 'flipkart';
							 $category_url_model->store_id = 1;
							 
							 $category_url_model->category_name = $categoriesData->category_name;
							 $category_url_model->category_type = $categoriesData->category_type;
							 $category_url_model->site_url = $category_urls;
							 
							 $category_url_model->scraper_group =$y;
							 if($x==10)
							 {
								$y++;
								$x=0;
							 }
							 
							 $category_url_model->save();
							// echo 'pass'. $category_url_model->category_name.'<br>';
						
						
					$x++;	
					}  
			
			}
			echo 'save category url for flipkart'; die;
		}else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('flipkart scraper','FlipkartController','No Any Category Data Url for flipkart','Output',__LINE__);
				echo 'No Any Category Data for Save category Url for flipkart'; die;
		}
	}



  public function actionScraper_product_urls($latest_product=0) {
		
		$max_scraper_group = CategoryUrl::find()->where(['store_id' => 1])->orderBy(['scraper_group' => SORT_DESC])->one();
		
		if(!empty($max_scraper_group)){
				
				for($i = 1; $i <=  $max_scraper_group->scraper_group; $i++){
					
					$categories_url_model = new CategoryUrl;
					$categories_urls= CategoryUrl::find()->where(['status' => 0,'store_id' => 1,'scraper_group'=> $i])->all();
					
					if(!empty($categories_urls)){
						 foreach($categories_urls as $categories_url){
								
								
								$objflipkartProduct = new \backend\models\FlipKartProductScrapper();
								
								//Tools::killOtherProcess("php console.php jungleeproductscrapper new");
								$siteName = 'http://www.flipkart.com';
								// for men shoes category
								//$url = $siteName . '/lc/pr/pv1/spotList1/spot1/productList?sid=tyy%2C4io&filterNone=true&start=1&ajax=true&_=1460746573007';
								$url = $categories_url->site_url;
								$categoryName = $categories_url->category_name;
								$categoryType = $categories_url->category_type;
								$scraper_group = $i;
								
						//        $arr_url = $objflipkartProduct->getAllPagingUrl($url);
						//        $objflipkartProduct->saveAllPagingUrl($arr_url, $categoryName, $categoryType);
						
						//        $arr_department_type = array('For' => 'Men');
							   
							   
									//$objflipkartProduct->saveProduct($categoryName, '', $categoryType, $latest_product,'flipkart',$scraper_group);
									
									$objflipkartProduct->saveProductUrls($categoryName, '', $categoryType, $latest_product,'flipkart',$scraper_group);
							
						} 
						//echo 'save products for flipkart'; die;
					}
				} //die('sadfasd');
		}else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('flipkart scraper','FlipkartController','No any category url for scrapping','Output',__LINE__);
				echo 'No Any URLs for scurpping'; die;
		}
		
  }  **/

  
		
public function actionScraper($latest_product=0) {
		$product_urls = ProductUrls::find()->where(['store_id' => 1,'status' => 0])->all();
		
		if(!empty($product_urls)){
				
				foreach($product_urls as $product_url){
					
					$categories_urls= Categories::find()->where(['category_id' => $product_url->category_id])->one();
					$category_name = !empty($categories_urls) ? $categories_urls->category_name : '';
					$objflipkartProduct = new \backend\models\FlipKartProductScrapper();
					$objflipkartProduct->saveProductData($product_url,$category_name);
					
				}
				
		}else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('flipkart scraper','FlipkartController','No any category url for scrapping','Output',__LINE__);
				echo 'No Any URLs for scurpping'; die;
		}
					
		
    
	
	}
	
	
	
	/*
	public function actionScraper($latest_product=0) {
		
        $objflipkartProduct = new \backend\models\FlipKartProductScrapper();
        //Tools::killOtherProcess("php console.php jungleeproductscrapper new");
        $siteName = 'http://www.flipkart.com';
        // for men shoes category
        $url = $siteName . '/lc/pr/pv1/spotList1/spot1/productList?sid=tyy%2C4io&filterNone=true&start=1&ajax=true&_=1460746573007';
        $categoryName = 'Mobile';
        $categoryType = 'Mobile';
//        $arr_url = $objflipkartProduct->getAllPagingUrl($url);
//        $objflipkartProduct->saveAllPagingUrl($arr_url, $categoryName, $categoryType);

//        $arr_department_type = array('For' => 'Men');
        $objflipkartProduct->saveProduct($categoryName, '', $categoryType, $latest_product,'flipkart');
		echo '<pre>'; print_r($objflipkartProduct); die;
    }*/
	

public function actionSaveCatUrl(){
		$categories_data_model = new CategoriesData;
		$categoriesDatas = CategoriesData::find()->where(['type' => 0,'store_id'=>1])->all();
		
		if(!empty($categoriesDatas)){
				foreach($categoriesDatas as $categoriesData){
					// $model = CategoriesData::find($categoriesData->id)->one();
					 $model = CategoriesData::find()->where(['id' => $categoriesData->id])->one();
					 $model->type = 1;
					 $model->save();
					 
					$url = parse_url($categoriesData->url); 
					parse_str($url['query'], $query);
					$count_products = $model->count_products;
					$perpage = 60;
					$list_page = $count_products / $perpage;
					$pages = round($list_page);
					//echo $pages; die;
					$x  = 1;
					$y = 1;
					
					$categoryUrlModel = new CategoryUrl;
					$countLastInsertUrls = $categoryUrlModel->getLastInsertCategoryUrl($categoriesData);
					$pages = $pages + $countLastInsertUrls; // count pages aur last insert url for same category url
					
					for($i = $countLastInsertUrls; $i< $pages; $i++){
							$page_data = ($i * $perpage)+ 1;
							
							//6bo%2Cb5g
							//http://www.flipkart.com/lc/pr/pv1/spotList1/spot1/productList?sid=tyy,4io&filterNone=true&start=641&ajax=true&_=1460746573007
							
							$category_urls = 'http://www.flipkart.com/lc/pr/pv1/spotList1/spot1/productList?sid='.$query['sid'].'&filterNone=true&start='.$page_data.'&ajax=true&_=1460746573007';
							 
							 $category_url_model = new CategoryUrl;
							 $category_url_model->status = 0;
							 $category_url_model->store_name = 'flipkart';
							 $category_url_model->store_id = 1;
							 
							 $category_url_model->category_name = $categoriesData->category_name;
							 $category_url_model->category_type = $categoriesData->category_type;
							 $category_url_model->site_url = $category_urls;
							 
							 $category_url_model->scraper_group =$y;
							 if($x==10)
							 {
								$y++;
								$x=0;
							 }
							 
							 $category_url_model->save();
							// echo 'pass'. $category_url_model->site_url.'<br>';
						
						
					$x++;	
					}  
			}
			echo 'save category url for flipkart'; die;
		}else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('flipkart scraper','FlipkartController','No Any Category Data Url for flipkart','Output',__LINE__);
				echo 'No Any Category Data for Save category Url for flipkart'; die;
		}
	}
	
	
	
public function actionScraperProductUrls($latest_product=0) {
		
		$max_scraper_group = CategoryUrl::find()->where(['store_id' => 1])->orderBy(['scraper_group' => SORT_DESC])->one();
		if(!empty($max_scraper_group)){
				
				for($i = 1; $i <=  $max_scraper_group->scraper_group; $i++){
					
					$categories_url_model = new CategoryUrl;
					$categories_urls= CategoryUrl::find()->where(['status' => 0,'store_id' => 1])->all();
					
					if(!empty($categories_urls)){
						 foreach($categories_urls as $categories_url){
							
							$url = parse_url($categories_url->site_url); 
							parse_str($url['query'], $query);
							
							$start = $query['start'];
							$sid = trim($query['sid']);
							
							
							$categoryData = Categories::find()->where(['category_name' => $categories_url->category_name])->one();
							$category_id = !empty($categoryData) ? $categoryData->category_id : 0;
							
							$this->getProductFromCurl($sid,$start,$categories_url->id_category_url,$category_id);
								
							
						} 
						//echo 'save products for flipkart'; die;
					}
				} //die('sadfasd');
		}else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('flipkart scraper','FlipkartController','No any category url for scrapping','Output',__LINE__);
				echo 'No Any URLs for scurpping'; die;
		}
		
  }
  
  
  
 public function getProductFromCurl($s_id, $start,$id_category_url,$category_id){
	 
						
						//echo $s_id;
						$s_id = str_replace(',','/',$s_id);
						//$s_id = 't06,fy9,gjk';
						//echo $s_id;
						$objHttpService = new \common\components\HttpService();
						$proxy = $objHttpService->getProxyIps();
						
						$curl = curl_init();
							curl_setopt_array($curl, array(
							CURLOPT_URL => "https://www.flipkart.com/api/1/product/smart-browse",
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_ENCODING => "",
							CURLOPT_MAXREDIRS => 10,
							CURLOPT_TIMEOUT => 30,
							CURLOPT_SSL_VERIFYPEER => false,
							CURLOPT_SSL_VERIFYHOST => false,
							CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							CURLOPT_CUSTOMREQUEST => "POST",
							CURLOPT_PROXY => $proxy['proxy_url'],
							CURLOPT_PROXYUSERPWD =>  $proxy['proxy_user'].':'.$proxy['proxy_pwd'],
							//CURLOPT_PROXYTYPE=> CURLPROXY_SOCKS5,
							CURLOPT_POSTFIELDS => "{\"requestContext\":{\"store\":\"$s_id\",\"start\":\"$start\",\"disableProductData\":\"true\",\"count\":\"60\",\"ssid\":\"sfevl1cif4ws80oo\"}}",
							
							CURLOPT_HTTPHEADER => array(
								"accept: */*",
								"accept-language: en-GB,en-US;q=0.8,en;q=0.6",
								"cache-control: no-cache",
								"content-type: application/json",
								"postman-token: eddf1792-5624-cc13-585b-d9ec26718c9b",
								"referer: https://www.flipkart.com/mobiles/pr?sid=$s_id&otracker=categorytree",
								"user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36",
								"x-user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36 FKUA/website/41/website/Desktop"
							  ),
							));

							
							
							$response = curl_exec($curl);
							$err = curl_error($curl);
							
							
							//echo '<pre>curl_getinfo==>';print_r(curl_getinfo($curl));
							//echo '<pre>curl_error==>';print_r(curl_error($curl)); 
							curl_close($curl);
							//echo $s_id; 
							//echo '<pre>proxyDetail=>'; print_r($proxy);
							//echo '<pre>Server=>'; print_r($_SERVER);
							//echo '<pre>ssss'; print_r(json_decode($response)); die;
							
							
							if ($err) {
							  return 'error';
							} else {
								$productData = json_decode($response);
								
								if(!empty($productData)){
									$sid = str_replace(',','/',$s_id);
									
									if(isset($productData->RESPONSE->pageContext->searchMetaData->storeSearchResult->$sid->productList)){
										foreach($productData->RESPONSE->pageContext->searchMetaData->storeSearchResult->$sid->productList as $val){
											
											if(!empty($val)){
												
												$val = trim($val);
												$product_url = 'http://www.flipkart.com?pid='.$val;
												$exitProductUrl = ProductUrls::find()->where(['url' => $product_url, 'store_id' => 1])->one();
													if (empty($exitProductUrl)) {
																
																$productUrlsModel = new ProductUrls();
																$productUrlsModel->store_id = 1;
																$productUrlsModel->cateogry_url_id = $id_category_url;
																$productUrlsModel->category_id = $category_id;
																$productUrlsModel->status = 0;
																$productUrlsModel->url = $product_url;
																$productUrlsModel->save();
																$exitProductUrl = ProductUrls::find()->where(['url' => $product_url, 'store_id' => 1])->one();
															
																$aws = new \common\components\Aws();
																$aws = $aws->sendMessage($exitProductUrl,'product_scraper');															
													}else{
														//echo '<pre>'; print_r($exitProductUrl); die;
													}
											}
											
										}
										
									}else{
										echo '<pre>'; print_r($productData->RESPONSE);
									}
									
									
										
										$CategoryUrlModel = CategoryUrl::findOne($id_category_url);
										$CategoryUrlModel->status = 1;
										$CategoryUrlModel->save(false);
										echo 'save category url id=>'.$CategoryUrlModel->id_category_url.'<br>';
								}
							}
	 
 }
 
	
	
public function actionDelete(){
	$CategoryUrlData = CategoryUrl::find()->all();
	
	foreach($CategoryUrlData as $category_urls){
			$product_urls =  ProductUrls::find()->where(['cateogry_url_id' => $category_urls->id_category_url, 'store_id' => 1,'status'=>0])->orderBy(['id' => SORT_DESC])->all();
			
			if(!empty($product_urls)){
				
				if(count($product_urls) > 200){
					$i = 1;
					foreach($product_urls as $product_url){
						if($i <= 80){
							
							ProductUrls::deleteAll(['id' =>$product_url->id]);
							
						}
						
					$i++;
					}
				}
				
			} 
	}
}
	
}
