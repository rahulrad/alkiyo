<?php

namespace common\models;

use Yii;
use frontend\helpers\NoImage;
use common\components\CImage;

/**
 * This is the model class for table "slider_images".
 *
 * @property integer $id
 * @property integer $slider_id
 * @property string $image
 * @property string $description
 * @property string $link
 * @property string $created
 */
class SliderImages extends \yii\db\ActiveRecord
{
	public $file;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slider_id'], 'integer'],
            [['description'], 'string'],
            [['created','title','status'], 'safe'],
            [['image', 'link'], 'string', 'max' => 255],
			[['file'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slider_id' => 'Slider ID',
            'image' => 'Image',
			'title' => 'Title',
            'description' => 'Description',
			'status' => 'Status',
            'link' => 'Link',
            'created' => 'Created',
			'h1' =>'H1',
			'h2' => 'H2',
			'h3' => 'H3',
			'brand_id' =>'Brand ID',
        ];
    }
	
	public function saveSliderAddMoreData($sliderImagesData,$slider_id){
		
			//echo '<pre>'; print_r($sliderImagesData); die;
			$i=1;
				foreach($sliderImagesData['content'] as $slider_image_data){
						
						 $imageData = $sliderImagesData['images']['image_'.$slider_image_data['s_no']];
						 
						 if(!empty($imageData) && isset($imageData['name']) && !empty($imageData['name'])){
							 
							 $ext = explode('/',$imageData['type']);
							 $image_name = time().uniqid().'.jpg';
							 
							 $SliderImagesModel = new SliderImages;
							 $SliderImagesModel->slider_id = $slider_id;
							 $SliderImagesModel->title = trim($slider_image_data['title']);
							 $SliderImagesModel->description = trim($slider_image_data['description']);
							 $SliderImagesModel->link = $slider_image_data['link'];
							 $SliderImagesModel->image = 'uploads/sliders/'.$image_name;
							 $SliderImagesModel->status = isset($slider_image_data['status']) ? $slider_image_data['status'] : 0;
							 $SliderImagesModel->brand_id = $slider_image_data['brand_id'];
							  $SliderImagesModel->h1 = $slider_image_data['h1'];
							   $SliderImagesModel->h2 = $slider_image_data['h2'];
							    $SliderImagesModel->h3 = $slider_image_data['h3'];
							 $path = Yii::getAlias('@frontend') .'/web/uploads/sliders/';
							 
							 $tmp_name = $imageData["tmp_name"];
							 move_uploaded_file($tmp_name, "$path/$image_name");
							 
							 // code for resize image
								$thumImageSizes = array('500x1200');
								foreach($thumImageSizes as $size){
									$sizeArr = explode('x',$size);
									$image = $path.$image_name;
									//echo $image.'<br>';
									$destFile = $path.$size.'/'.$image_name;
									//echo $destFile.'<br>';
									//echo $sizeArr[1].'<br>'.$sizeArr[0]; die;
									//$objImageResize = new \common\components\CImage();
									$objImageResize = new \common\components\CImage();
									$response = $objImageResize->imageResize($image,$destFile,$sizeArr[1],$sizeArr[0],'jpg');
								}
							 
							 $SliderImagesModel->created = date('Y-m-d h:i:s');
							 $SliderImagesModel->save();
							 
						 }else{
							 //echo $slider_image_data['status']; die;
							 $SliderImagesModel = new SliderImages;
							 $SliderImagesModel->slider_id = $slider_id;
							 $SliderImagesModel->title = trim($slider_image_data['title']);
							 $SliderImagesModel->description = trim($slider_image_data['description']);
							 $SliderImagesModel->link = $slider_image_data['link'];
							 $SliderImagesModel->status = isset($slider_image_data['status']) ? $slider_image_data['status'] : 0;
							 
							 if(isset($slider_image_data['old_image']) && !empty($slider_image_data['old_image'])){
								 $SliderImagesModel->image = $slider_image_data['old_image'];
							 }
							  $SliderImagesModel->brand_id = $slider_image_data['brand_id'];
							   $SliderImagesModel->h1 = $slider_image_data['h1'];
							   $SliderImagesModel->h2 = $slider_image_data['h2'];
							    $SliderImagesModel->h3 = $slider_image_data['h3'];
							 $SliderImagesModel->created = date('Y-m-d h:i:s');
							 $SliderImagesModel->save(); 
						 }
						 
						 
				$i++;
				}
	}
	
	public static function getFieldsArray(){
		return [
			'id',
			'slider_id',
			'image',
			'description',
			'link',
			'created',
			'brand_id',
		];
	}
	
	public static function convertNullFieldsToEmptyString(array $array){
		$config = \HTMLPurifier_Config::createDefault();
		$config->set('HTML.AllowedElements', []);
		$purifyHtml = new \HTMLPurifier($config);
		$array['image'] = empty($array['image']) ? \yii::$app->params['parentDomain'].NoImage::getNoImageIfNoImageExists( $array['image'], NoImage::noImageSlider ) : \yii::$app->params['parentDomain'] . $array['image'];
		$array['link'] = empty($array['link']) ? '' : $array['link'];
		$array['description'] = empty($array['description']) ? '' : $purifyHtml->purify( $array['description'] );
		$array['created'] = empty($array['created']) ? '' : $array['created'];
		unset ($config, $purifyHtml);
		return $array;
	}
}
