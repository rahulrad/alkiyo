<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_feature_mappings".
 *
 * @property integer $id
 * @property integer $id_category
 * @property integer $id_feature
 * @property string $store_feature_name
 * @property string $category
 * @property string $feature
 * @property string $uploadFile
 */
class ProductFeatureMappings extends \yii\db\ActiveRecord
{

    public $uploadFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_feature_mappings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_category', 'id_feature', 'store_feature_name'], 'required'],
            [['id_category', 'id_feature'], 'integer'],
            [['store_feature_name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_category' => 'Category',
            'id_feature' => 'Feature',
            'store_feature_name' => 'Store Feature Name',
        ];
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this::hasOne(Categories::className(), ['category_id' => 'id_category']);
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getFeature()
    {
        return $this::hasOne(Features::className(), ['id' => 'id_feature']);
    }
}
