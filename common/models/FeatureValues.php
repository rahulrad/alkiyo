<?php

namespace common\models;

use Yii;


/**
 * This is the model class for table "feature_values".
 *
 * @property integer $id
 * @property integer $feature_id
 * @property string $value
 * @property string $created
 * @property string $modified
 *
 * @property Features $feature
 */
class FeatureValues extends \yii\db\ActiveRecord
{
	public $features_csv_file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feature_values';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['feature_id'], 'required'],
            [['feature_id'], 'integer'],
            [['created', 'modified','is_delete','display_name'], 'safe'],
            [['value'], 'string', 'max' => 150],
            [['feature_id'], 'exist', 'skipOnError' => true, 'targetClass' => Features::className(), 'targetAttribute' => ['feature_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'feature_id' => 'Feature ',
            'value' => 'Value',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeature()
    {
        return $this->hasOne(Features::className(), ['id' => 'feature_id']);
    }
    
    public static function getFieldsArray(){
    	return [
    		'id',
    		'feature_id',
    		'value',
    		'created',
    		'modified',
    	];
    }
}
