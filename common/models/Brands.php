<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "brands".
 *
 * @property integer $brand_id
 * @property string $brand_name
 * @property string $brand_description
 * @property string $created
 */
class Brands extends \yii\db\ActiveRecord {

    public $file;

    const PERMISSIONS_PRIVATE = 10;
    const PERMISSIONS_PUBLIC = 20;

    public function behaviors() {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'brand_name',
            // 'slugAttribute' => 'slug',
            ],
        ];
    }

	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'brands';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['brand_description','brand_h2'], 'required'],
            [['brand_description', 'status', 'meta_title', 'meta_keyword', 'meta_desc'], 'string'],
            [['created','is_delete','folder_slug'], 'safe'],
            [['file'], 'file'],
            [['brand_name', 'image','brand_h2'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'brand_id' => 'Brand',
            'brand_name' => 'Brand Name',
            'brand_description' => 'Brand Description',
			'brand_h2' => 'Brand h2',
            'status' => 'Status',
            'created' => 'Created',
            'file' => 'Image'
        ];
    }

    public function getProduct() {
        return $this->hasMany(Product::className(), ['brand_id' => 'brands_brand_id']);
    }
    
    public static function loadAll(){
        $query = new \yii\db\Query();
        $query->select('*')
            ->from(self::tableName(). ' s')
            ->indexBy('brand_id');
        
        return $query->all();
    }

 }
