<?php

namespace common\models;

/**
 * This is the model class for table "viewed_products_by_user".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $user_id
 * @property string $user_ip
 * @property string $created_at
 * @property string $updated_at
 */

class RecentlyViewedProducts extends \yii\db\ActiveRecord{
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'viewed_products_by_user';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['user_id', 'product_id'], 'integer'],
			[['user_ip'], 'string', 'max' => 100],
			[['created_at','updated_at'], 'safe'],
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'product_id' => 'Product',
			'user_id' => 'User',
			'user_ip' => 'Ip Address',
			'id' => 'Recently Viewed',
		];
	}
	
	public static function getRecentlyViewedProducts(array $productIds = []){
		$query = self::find();
		if(\Yii::$app->user->isGuest){
			$query->where( ['user_ip' => \Yii::$app->request->userIP] );
		}else{
			$query->where( ['user_id' => \Yii::$app->user->id] );
		}
		
		if(!empty($productIds)){
			$query->andWhere(['not in', 'product_id' , $productIds]);
		}
		
		
		
		return $query->limit(20)->all();
	}
	
	public function getProduct(){
		return $this->hasOne(Products::className(), ['product_id' => 'product_id']);
	}
}
