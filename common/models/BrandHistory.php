<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "categories".
 *
 * @property integer $category_id
 * @property string $category_name
 * @property integer $parent_category_id
 * @property string $category_description
 * @property string $category_status
 * @property string $created
 * @property int $is_index
 */
class BrandHistory extends \yii\db\ActiveRecord {

    const PERMISSIONS_PRIVATE = 10;
    const PERMISSIONS_PUBLIC = 20;

   /*  public function behaviors() {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'category_name',
            ],
        ];
    } */

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'brand_history';
    }

	
    /**
     * @inheritdoc
     */
  /*   public function rules() {
        return [
            [['category_name'], 'required'],
            [['category_description', 'status', 'meta_title', 'meta_keyword', 'meta_desc'], 'string'],
            [['created', 'parent_category_id', 'show_home_page', 'sort_order', 'is_delete', 'folder_slug', 'compare', 'image_scrap_preset', 'display_name','is_index'], 'safe'],
            [['file'], 'file'],
			 [['api_icon_file'], 'file'],
            [['category_name', 'image'], 'string', 'max' => 100],
			[['api_icon'], 'string', 'max' => 255],
			[['category_csv_file'],'file','extensions'=>'csv','maxSize'=>1024 * 1024 * 5]
        ];
    }  */
    /**
     * @inheritdoc
     */
   /* public function attributeLabels() {
        return [
            'category_id' => 'Category ID',
            'category_name' => 'Category Name',
            'parent_category_id' => 'Parent Category',
            'category_description' => 'Category Description',
            'status' => 'Category Status',
            'created' => 'Created',
            'file' => 'Image',
            'image_scrap_preset' => "Product Image Scrap Preset",
            'is_index' => 'Is Index'
        ];
    }  */
	


}
