<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProductFeatureMappings;

/**
 * ProductFeatureMappingsSearch represents the model behind the search form about `common\models\ProductFeatureMappings`.
 */
class ProductFeatureMappingsSearch extends ProductFeatureMappings
{

    public $featureName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_category', 'id_feature'], 'integer'],
            [['store_feature_name','featureName'], 'safe'],
        ];
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getFeature()
    {
        return $this::hasOne(Features::className(), ['id' => 'id_feature']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductFeatureMappings::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_category' => $this->id_category,
            'id_feature' => $this->id_feature,
        ]);

        $query->andFilterWhere(['like', 'store_feature_name', $this->store_feature_name]);
        $query->joinWith('feature');
        $query->andFilterWhere(['like', 'features.name', $this->featureName]);

        return $dataProvider;
    }
}
