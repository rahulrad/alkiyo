<?php
namespace common\models;


use Yii;
use backend\models\CouponsVendors;
use yii\helpers\Url;

/**
 * This is the model class for table "coupons".
 *
 * @property integer $id
 * @property string $title
 * @property string $sub_title
 * @property string $offer
 * @property string $description
 * @property string $valid
 * @property string $status
 * @property string $created
 * @property integer $moved
 * @property integer discount
 * @property integer type
 */
class Coupons extends \yii\db\ActiveRecord
{
	public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coupons';
    }


    public function fields()
    {
        $fields = parent::fields();
        $extraFields= [
           

            'redirectLink' => function ($model) {

                return \Yii::$app->params['frontend'].$model->track_link;
            },

          
       
        ];
        
        return array_merge( $fields,$extraFields);
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['merchant','title','link'],'required'],
            [[ 'discount', 'moved', 'type', 'created','offer_id','promo_id','offer_type','code','category','offer_url','start_date','expiry_date','featured','exclusive','ref_id','link','store_link','meta_title','meta_description','meta_keywords','verified_on','call_to_action_button','track_link','track_id'], 'safe'],
            [['title', 'sub_title', 'offer','api_name','merchant_logo_url'], 'string', 'max' => 255],
            [['valid'], 'string', 'max' => 25],
            [['status'], 'string', 'max' => 10],
			[['file'],'file','extensions'=>'csv','maxSize'=>1024 * 1024 * 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'sub_title' => 'Sub Title',
            'offer' => 'Offer',
            'description' => 'Description',
            'valid' => 'Valid',
            'status' => 'Status',
            'created' => 'Created',
            'track_link'=>'Share Link'
        ];
    }
	
    /*
     * Get Coupon category
     */
      public function getCategoryobj() {
        return $this->hasOne(CouponCategories::className(), ['id' => 'category']);
    }

    public static function getFieldsArray(){
    	return [
	    	'id',
	    	'offer_id',
	    	'promo_id',
	    	'title',
	    	'sub_title',
	    	'offer',
	    	'offer_type',
	    	'code',
	    	'category',
	    	'offer_url',
	    	'start_date',
	    	'expiry_date',
	    	'featured',
	    	'exclusive',
	    	'ref_id',
	    	'link',
	    	'store_link',
	    	'description',
	    	'valid',
	    	'status',
	    	'created',
    	];
    }



public function getVendors()
{
    return $this->hasOne(CouponsVendors::className(), ['id' => 'merchant']);
}

public static function getCategoryList($supplier,$type){
    
$where="";
if(isset($supplier)){
     
    $where=" and merchant  = ".$supplier." ";
}

if(!empty($type) && count($type)>0 && !empty($type[0])){
 
  
    $where.=" and type  IN (". implode(',',$type).") ";
}


$connection = Yii::$app->getDb();
$command = $connection->createCommand("
    SELECT count(*) as count, d.category, cc.name FROM `coupons` as d  left join coupon_categories cc on cc.id=d.category where d.status='active'  ".$where." group by category order by count(*) desc");

$result = $command->queryAll();
	
		return $result;
}
	

public static function getStoreList($cat,$type){
$where="";

if(!empty($cat)  && count($cat)>0 && !empty($cat[0])){
 
    $where=" and category  IN (".implode(',',$cat).") ";
}


if(!empty($type) && count($type)>0 && !empty($type[0])){
 
  
    $where=" and type  IN (". implode(',',$type).") ";
}


$connection = Yii::$app->getDb();
$command = $connection->createCommand("
     SELECT count(*) as count, d.merchant FROM `coupons` as d  where status='active'  ".$where." group by merchant order by count(*) desc;");

$result = $command->queryAll();
	
		return $result;
}

public static function getOfferRange(){

    $sql="select discount div 10 as discountList, count(*) as count from coupons where discount>0 group by discountList;";

$connection = Yii::$app->getDb();
$command = $connection->createCommand("
     $sql");

$result = $command->queryAll();
	
		return $result;

}

public static function getOfferType(){

    $sql="select type, count(*) as count from coupons   group by type;";

$connection = Yii::$app->getDb();
$command = $connection->createCommand("
     $sql");

$result = $command->queryAll();
	
		return $result;

}


public static function deleteOldDeals(){

      $sql="delete from coupons where expiry_date=".date('Y-m-d');

$connection = Yii::$app->getDb();
$command = $connection->createCommand("
     $sql");

$result = $command->execute();
	
		return $result;

}

    public function getStoreLink()
    {
        
        if (empty($this->track_id)) {
            $this->track_id = md5($this->id);
            $this->save(false);
        }
        return Url::toRoute(['/deals/store-out', 'id' => $this->track_id]);
    }



    public static function getLastestCoupons(){

        $sql = " select cv.slug,c.call_to_action_button, c.type, cv.image,cv.name,c.title, discount, code, link,c.id, c.description from coupons c left join coupons_vendors cv on cv.id=c.merchant  group by name order by verified_on desc limit 8";
        
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();

      
	    return $result;

    }

}
