<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "categories".
 *
 * @property integer $category_id
 * @property string $category_name
 * @property integer $parent_category_id
 * @property string $category_description
 * @property string $category_status
 * @property string $created
 * @property int $is_index
 */
class Categories extends \yii\db\ActiveRecord {

    public $file;
	public $api_icon_file;
	public $category_csv_file;

    const PERMISSIONS_PRIVATE = 10;
    const PERMISSIONS_PUBLIC = 20;

    public function behaviors() {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'category_name',
            // 'slugAttribute' => 'slug',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'categories';
    }

	
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['category_name'], 'required'],
            [['category_description', 'status', 'meta_title', 'meta_keyword', 'meta_desc'], 'string'],
            [['created', 'parent_category_id', 'show_home_page', 'sort_order', 'is_delete', 'folder_slug', 'compare', 'image_scrap_preset', 'display_name','is_index'], 'safe'],
            [['file'], 'file'],
			 [['api_icon_file'], 'file'],
            [['category_name', 'image'], 'string', 'max' => 100],
			[['api_icon'], 'string', 'max' => 255],
			[['category_csv_file'],'file','extensions'=>'csv','maxSize'=>1024 * 1024 * 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'category_id' => 'Category ID',
            'category_name' => 'Category Name',
            'parent_category_id' => 'Parent Category',
            'category_description' => 'Category Description',
            'status' => 'Category Status',
            'created' => 'Created',
            'file' => 'Image',
            'image_scrap_preset' => "Product Image Scrap Preset",
            'is_index' => 'Is Index'
        ];
    }
	
	// vinay 2 aug 2016
	public function getCategoryLists(){
			$categoryData = Categories::find()->select(['category_id','category_name','parent_category_id'])->where(['status'=>'active'])->andWhere(['!=', 'is_delete' ,1])->all();
			$categories = array();
			if(!empty($categoryData)){
				foreach($categoryData as $category){
					//echo '<pre>'; print_r($category); die;
					$categoryParent = Categories::find()->select(['category_name'])->where(['category_id'=>$category->parent_category_id])->andWhere(['!=', 'is_delete' ,1])->one();
					
					$cat_name = $category->category_name;
					if(!empty($categoryParent)){
						$cat_name = $category->category_name.' ('.$categoryParent->category_name.')';
					}
					$categories[$category->category_id] = $cat_name;
				}
			}
		return $categories;
	}
	
	// vinay 2 aug 2016
	public function getCategoryListsName(){
			$categoryData = Categories::find()->select(['category_id','category_name','parent_category_id'])->where(['status'=>'active'])->andWhere(['!=', 'is_delete' ,1])->all();
			$categories = array();
			if(!empty($categoryData)){
				foreach($categoryData as $category){
					//echo '<pre>'; print_r($category); die;
					$categoryParent = Categories::find()->select(['category_name'])->where(['category_id'=>$category->parent_category_id])->andWhere(['!=', 'is_delete' ,1])->one();
					
					$cat_name = $category->category_name;
					if(!empty($categoryParent)){
						$cat_name = $category->category_name.' ('.$categoryParent->category_name.')';
					}
					$categories[$category->category_name] = $cat_name;
				}
			}
		return $categories;
	}

	
    public function getParent() {
        return $this->hasOne(self::className(), ['category_id' => 'parent_category_id']);
    }

    public function getParentName() {
        $model = $this->parent;

        return $model ? $model->category_name : '';
    }

    public function getProducts() {
        return $this->hasMany(Products::className(), ['categories_category_id' => 'category_id'])->andWhere(['product_status' => 'active','is_delete'=>0]);
    }
	
	// vinay 15 july
	 public function getSortorder() {

        $last_sort_order = Categories::find()->select(['sort_order'])->orderBy('sort_order desc')->one();

        //echo '<pre>'; print_r($last_sort_order); 
        if (!empty($last_sort_order)) {
            $next_sort_order = $last_sort_order->sort_order + 1;
        } else {
            $next_sort_order = 1;
        }

        return $next_sort_order;
    }

 
    public static function createCategoryBreadcrumb($id) {

         $category = Categories::findOne(['category_id' => $id]);
        
        if($category->parent_category_id == 0) {
            return $category->category_name.'|'.$category->slug;
        } else {
            return Categories::createCategoryBreadcrumb($category->parent_category_id).'/'.$category->category_name.'|'.$category->slug;
        }
    }
    
    public static function getHomePageCategories(){
       return self::find()->where(['show_home_page' => '1'])->orderBy('sort_order ASC')->all();
    }
    
    public function getProductsForHomePage(){
    	return $this->getProducts()
    				->Where(['categories_category_id'=> $this->category_id, 'is_approved' => 1 ,'is_delete'=>0, 'instock'=>1])
    				->orderBy('product_id DESC')
    				->limit(6*2)->all();
    }
    
    /*
     * Gets top rated brand according to category id
     * @params $catId as category id
     * @return result
     */
 
      public static function getTopRatedBrandsByCategory($catId){

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand('
            SELECT SUM(rating) as total_rating ,brands_brand_id
            FROM `products`
            WHERE categories_category_id = :id and is_delete=0 and stock>0 
            GROUP BY brands_brand_id 
            HAVING sum(rating) >0 
            ORDER BY total_rating DESC LIMIT 0,7
                ', [':id' => $catId]) ;
 
        $topBrandIDs = $command->queryAll();
        $topBrandArr = [];
	$topBrands = [];
       foreach($topBrandIDs as $topBrand){
            $topBrandArr[]= $topBrand['brands_brand_id'] ;
        }
        
        if(count($topBrandArr) > 0)
            $topBrands=  Brands::find()->where(['brand_id'=> $topBrandArr])->all();
       
       
        return $topBrands;
        
    }

    public static function getAvailableBrandsforProductsbyCategory($id_cat)
    {
// select b.brand_id,brand_name from products p inner join brands b on p.brands_brand_id=b.brand_id where p.is_delete=0 group by b.brand_id;
 $connection = Yii::$app->getDb();
        $command = $connection->createCommand('
            select b.brand_id,brand_name from products p inner join brands b on p.brands_brand_id=b.brand_id 
            where p.is_delete=0  and categories_category_id=:id_cat group by b.brand_id;
                ', [':id_cat' => $id_cat]) ;
        $topBrandIDs = $command->queryAll();
        $topBrandArr = [];
         foreach($topBrandIDs as $topBrand){
            $topBrandArr[$topBrand['brand_id']]= $topBrand['brand_name'] ;
        }
        return $topBrandArr;

    }
    
    
    public function searchCategoryProducts(){
        $request = Yii::$app->request;
        $category = Categories::findOne(['category_id' => $request->get('category_id') ]);
        $products = $category->getProducts()->orderBy('rating DESC')->all();
     }
    
    public function getFeatures() {
        return $this->hasMany(Features::className(), ['category_id' => 'category_id'])->andWhere(['status' => 1])->orderBy(['sort_order' => SORT_ASC]);
    }
    
    
     /**
     * The function will search and filter product in a category
     * @return type
     */
     public static function filterCategoryProducts($request){
        $currentFilterTab  = $request->get('currentFilterTab');
        $category = Categories::findOne(['slug' => $request->get('slug')], ['status' => 'active']);
       // $brand = Brands::findOne(['slug' => $request->get('brand_slug')], ['status' => 'active']);

        $order=$currentFilterTab=="lowest_price" ? " ASC" : " DESC";
        
        $feature_ids_arr=[];
        $feature_ids_arr= $request->get('feature_ids');
        
        //$brands_ids_arr=[];
        $brands_ids_arr = $request->get('brands') !="" ? explode(",",$request->get('brands')) : "";
         
        
        $min_price = $request->get('min_price');
        $max_price = $request->get('max_price');
            
        $qry = (new \yii\db\Query())
    	->select('distinct(products.product_id),products.product_name,products.slug,'
                . 'products.product_description,products.model_number,'
                . 'products.price,products.discount,products.rating,products.url,'
                . 'products.lowest_price,products.highest_price')
    	->from('products')
        ->join('INNER JOIN', 'product_feature','products.product_id= product_feature.id_product') 
        ->join('INNER JOIN', 'features','product_feature.id_feature = features.id ')
        ->join('INNER JOIN', 'feature_values','product_feature.id_feature_value = feature_values.id')
    	->where(['between' ,'lowest_price', $min_price,$max_price])
    	->andWhere(['categories_category_id' => $category->category_id]);
       
        if(count($feature_ids_arr)>0){
         $qry->andWhere(['in' ,'id_feature_value', $feature_ids_arr]);
        }
        
        if(count($brands_ids_arr)>0 && is_array($brands_ids_arr)){
          $qry->andWhere(['in' ,'brands_brand_id', $brands_ids_arr]);
        }
        $countQry = clone $qry;
        $totalRecords = $countQry->count();
        $pages = new \yii\data\Pagination(['totalCount' => $totalRecords, 'defaultPageSize' => 10]);
        
        $qry = $qry->offset($pages->offset)
        ->orderBy($currentFilterTab. $order)
    	->limit($pages->limit)
    	->all();
        
                
    	//$qry = $qry->orderBy($currentFilterTab. $order)->limit(10)->all();
        
         return $qry;
     }
    
     /*
      * Shows category brands
      */
     public static function categoryBrands($catId){
     
         $connection = Yii::$app->getDb();
        $command = $connection->createCommand('
            SELECT brand_id,brand_name,slug
            FROM `brands`
            WHERE brand_id IN (SELECT distinct(brands_brand_id)
                        From products Where categories_category_id = :id)
            ORDER BY brand_name ASC            
                ', [':id' => $catId]) ;
 
        $brands = $command->queryAll();
        
           return $brands;
     }
     
     
    public static function loadAll(){
        $query = new \yii\db\Query();
        $query->select('*')
            ->from(self::tableName(). ' s')
            ->indexBy('category_id');
        
        return $query->all();
    }
 
	public function getSiblings(){
		return self::find()->where(['parent_category_id' => $this->parent_category_id, 'is_delete' => 0, 'status' => 'active'])
		->andWhere(['not in', 'category_id', [$this->category_id]])->all();
	}
}
