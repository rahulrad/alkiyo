<?php
namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "features".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category_id
 * @property integer $feature_group_id
 * @property string $is_filter
 * @property integer $is_required
 * @property string $slug
 * @property string $type
 * @property string $unit
 * @property string $display_text
 * @property integer $sort_order
 * @property integer $status
 * @property string $created
 * @property string $modified
 */
class Features extends \yii\db\ActiveRecord {

    const FEATURE_LAST_ID = 11938;
	public $features_csv_file;
	
    const PERMISSIONS_PRIVATE = 10;
    const PERMISSIONS_PUBLIC = 20;

    public function behaviors() {
        return [

            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            // 'slugAttribute' => 'slug',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'features';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'category_id', 'feature_group_id'], 'required'],
            [['category_id', 'feature_group_id', 'is_filter', 'is_required', 'sort_order','show_feature'], 'integer'],
            [['display_text'], 'string'],
            [['created', 'modified','is_delete','new'], 'safe'],
            [['name', 'slug', 'unit','display_name'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'category_id' => 'Category ',
            'feature_group_id' => 'Feature Group ',
            'is_filter' => 'Is Filter',
            'is_required' => 'Is Required',
            'slug' => 'Slug',
            'type' => 'Type',
            'unit' => 'Unit',
            'display_text' => 'Display Text',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'created' => 'Created',
            'modified' => 'Modified',
	        'new'=>'New',
			'display_name'=>'Display Name',
            'show_feature'=>'Show Feature On Page'
        ];
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getCategoriesCategory() {
        return $this::hasOne(Categories::className(), ['category_id' => 'category_id']);
    }

    public function getFeatureGroup() {
        return $this::hasOne(FeatureGroups::className(), ['id' => 'feature_group_id']);
    }

    public function getFeatureValues() {
        return $this->hasMany(FeatureValues::className(), ['feature_id' => 'id']);
    }

    public function findFeatureValuesToFeatureId($feature_id) {

        $FeatureValuesModel = new FeatureValues();

        $FeatureValues = $FeatureValuesModel::find()->where(['feature_id' => (int) $feature_id])->all();

        $featuresValueArr = array();

        foreach ($FeatureValues as $FeatureVal) {

            $featuresValueArr[] = array('id' => $FeatureVal->id, 'value' => $FeatureVal->value);
        }

        return $featuresValueArr;
    }

    public function getProductFeatures() {
        return $this->hasOne(ProductFeature::className(), ['id_feature' => 'id']);
    }
    
    public static function getFeaturesListByCategory($categoryId){
    	if(empty($categoryId) || !is_int($categoryId)){
    		return [];
    	}
    	
    	$features = Features::find()
    	->where(['category_id' => $categoryId])
    	->andWhere(['is_filter' => 1])
    	->all();
    	
    	$featuresArray = [];
    	
    	foreach ($features as $feature){
    		
    		foreach ($feature->featureValues as $featureKeyValue){
    			$featuresArray[$feature->name][] = \yii\helpers\ArrayHelper::toArray($featureKeyValue,[
    				'common\models\FeatureValues' => FeatureValues::getFieldsArray(),
    			]);
    		}
    	}
    	
    	return $featuresArray;
    }
    
    public static function getIsFilterFeatureOfProduct($categoryId, $productId){
    	if(empty($categoryId) || !is_int($categoryId)){
    		return [];
    	}
    	 
    	$features = Features::find()
    	->where(['category_id' => $categoryId])
    	->andWhere(['show_feature' => 1])
    	->all();
    	 
    	$featuresArray = [];
    	 
    	foreach ($features as $feature){
    
//     		foreach ($feature->productFeatures as $featureKeyValue){
//     			echo get_class($featureKeyValue);die;
    			$productValue = ProductFeature::findOne(['id_feature' => $feature->id, 'id_product' => $productId]);
    			if(!empty($productValue))
    				$featuresArray[$feature->name] = $productValue->featureValue->value;
//     		}
    	}
    	 
    	return $featuresArray;
    }

}
