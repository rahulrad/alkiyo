<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ActivityTracker;

/**
 * ActivityTrackerSearch represents the model behind the search form about `common\models\ActivityTracker`.
 */
class ActivityTrackerSearch extends ActivityTracker
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['url', 'session_id', 'type', 'store_name', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ActivityTracker::find()->orderBy("id desc");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        if ($this->created_at) {
            $query->andFilterWhere(['>=', 'created_at', date("y-m-d 00:00:00", strtotime($this->created_at))])
                ->andFilterWhere(['<=', 'created_at', date("y-m-d 23:59:59", strtotime($this->created_at))]);
        }

        $query->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'session_id', $this->session_id])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'store_name', $this->store_name]);

        return $dataProvider;
    }
}
