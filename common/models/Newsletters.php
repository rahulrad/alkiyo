<?php

namespace common\models;

/**
 * This is the model class for table "newsletters".
 *
 * @property integer $id
 * @property string $email
 * @property integer $status
 * @property string $created
 */
class Newsletters extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'newsletters';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'],'required'],
        	['email', 'validateEmailExists'],
        	['email', 'email'],
            [['status'], 'integer'],
            [['created','is_delete'], 'safe'],
            [['email'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'status' => 'Status',
            'created' => 'Created',
        ];
    }
    
    public function validateEmailExists($attribute, $params){
    	$exsistingEmail = $this->findOne(['email' => $this->$attribute]);
    	if(count($exsistingEmail))
    		$this->addError($attribute, 'Your\'re already registered with us. Thanks for showing your interest.');
    }
}
