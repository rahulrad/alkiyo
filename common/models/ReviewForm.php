<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Review form
 */
class ReviewForm extends Model
{
    public $title;
    public $description;
    public $user_name;
    public $review;
    public $product_id;
    public $rating;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['title', 'description'], 'required'],
            
        ];
    }

    /**
     * Save user reviews
     * @return boolean
     */
    public function save()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

}