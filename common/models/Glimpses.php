<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property integer $category_id
 * @property string $category_name
 * @property integer $parent_category_id
 * @property string $category_description
 * @property string $category_status
 * @property string $created
 * @property int $is_index
 */
class Glimpses extends \yii\db\ActiveRecord {

     public $file;
    const PERMISSIONS_PRIVATE = 10;
    const PERMISSIONS_PUBLIC = 20;
	
    public static function tableName() {
        return 'glimpses';
    }
	
	 public function rules() {
        return [
            [['name','description'], 'required'],
            [['name', 'description'], 'string'],
            [['created','is_delete','folder_slug'], 'safe'],
            [['file'], 'file'],
            
        ];
    }
	
	public function attributeLabels() {
        return [
            'name' => 'name',
            'brand_id' => 'Brand',
            'description' => 'Description',
            'status' => 'Status',
            'file' => 'Image'
        ];
    }

}
