<?php

namespace common\models;

use Yii;


/**
 * This is the model class for table "sliders".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $status
 * @property string $created
 * @property integer $is_mobile
 */
class Sliders extends \yii\db\ActiveRecord
{
	public $file;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sliders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['title'], 'required'],
            [['created','slider_type','category_id','start_date','end_date','is_delete', 'is_mobile'], 'safe'],
            [['title', 'image','link'], 'string', 'max' => 255],
			[['file'], 'file'],
            [['status'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
            'status' => 'Status',
            'created' => 'Created',
			'file' => 'Image',
            'category_id' =>'Category',
            'slider_type' => 'Slider Type',
			'link' => 'link',
			'is_mobile' => 'Is Mobile Banner',
        ];
    }
	
	public function getImageurl()
	{
		return \Yii::$app->request->BaseUrl.'/'.$this->image;
	}
	
	// vinay 20 july 2016
	public function getExitSliderAcordingDateRange($data){
		
		$sliderModel = new Sliders();
		
		if(!empty($data)){
			
			$exitResult = array();
			
			if(empty($data['category_id'])){
				$data['category_id'] = NULL;
			}
			
			if($data['type'] == 'update'){				
				$exitResult = $sliderModel::find()
					->where(['between','start_date',$data['start_date'],$data['end_date']])
					->orWhere(['between','end_date',$data['start_date'],$data['end_date']])
					->andWhere(['slider_type' => $data['slider_type']])
					->andWhere(['category_id' => $data['category_id']])
					->andWhere(['is_delete' => 0])
					->andWhere(['!=','id',$data['id']])
					->one();
			}else{
				$exitResult = $sliderModel::find()
					->where(['between','start_date',$data['start_date'],$data['end_date']])
					->orWhere(['between','end_date',$data['start_date'],$data['end_date']])
					->andWhere(['slider_type' => $data['slider_type']])
					->andWhere(['category_id' => $data['category_id']])
					->andWhere(['is_delete' => 0])
					->one();
			}
			
			return $exitResult;
			
		}
	}

	public static function getHomePageSlider(){
	/* 	$sql = 'SELECT * FROM `'. self::tableName().'` as u 
				WHERE Date(NOW()) between u.`start_date` 
				AND u.end_date AND u.`status` = :status 
				AND u.`slider_type` = :sliderType 
				AND u.is_delete = :isDelelted and is_mobile=0 
				UNION (SELECT * FROM `'. self::tableName().'` as u 
				WHERE u.`status` = :status 
				AND u.`start_date` IS NULL 
				AND u.`end_date` IS NULL 
				AND u.`slider_type` = :sliderType and is_mobile=0
				AND u.is_delete = :isDelelted )';
		
        $slider = self::findBySql($sql, [':status' => 'active', ':sliderType' => 'home', ':isDelelted' => 0]);
 		//var_dump($slider->createCommand()->rawSql);die;
		
        $slider = $slider->all();
        if(count($slider) > 0){
         	return $slider;
        }else{
         	return self::find()->where(['status' => 'active'])
         		->andWhere(['slider_type' => 'default'])
         		->andWhere(['is_delete' => 0])
         		->all();
        } */
		
		
		$query = new  yii\db\Query;
		$query	->select(['sliders.*'])  
        ->from('sliders')
        ->where(['status' => 'active'])
         		->andWhere(['slider_type' => 'home'])
         		->andWhere(['is_delete' => 0]);
				
		$command = $query->createCommand();
		$data = $command->queryAll();
	
		
		$arr = array();			
		if(!empty($data))
		{
			foreach($data as $slider)
			{
				array_push($arr,$slider['id']);
			}
		}
		
	 	$query1 = new  yii\db\Query;
		$query1	->select(['slider_images.*', 'brands.slug as bransslug','brands.image as brandimage'])  
        ->from('slider_images')
        ->leftJoin('brands', 'slider_images.brand_id = brands.brand_id')
        ->where(['IN', 'slider_images.slider_id', $arr ]);
		$command1 = $query1->createCommand(); 
		$data1 = $command1->queryAll();
		
		return $data1;
		
		
    }
	
	
	public static function getHomePageSecondSlider(){
		/* $sql = 'SELECT * FROM `'. self::tableName().'` as u 
				WHERE Date(NOW()) between u.`start_date` 
				AND u.end_date AND u.`status` = :status 
				AND u.`slider_type` = :sliderType 
				AND u.is_delete = :isDelelted and is_mobile=0 
				UNION (SELECT * FROM `'. self::tableName().'` as u 
				WHERE u.`status` = :status 
				AND u.`start_date` IS NULL 
				AND u.`end_date` IS NULL 
				AND u.`slider_type` = :sliderType and is_mobile=0
				AND u.is_delete = :isDelelted )';
		
        $slider = self::findBySql($sql, [':status' => 'active', ':sliderType' => 'homesecond', ':isDelelted' => 0]);
 		//var_dump($slider->createCommand()->rawSql);die;
		
        $slider = $slider->all();
        if(count($slider) > 0){
         	return $slider;
        }else{
         	return self::find()->where(['status' => 'active'])
         		->andWhere(['slider_type' => 'homesecond'])
         		->andWhere(['is_delete' => 0])
         		->all();
        } */
		
		$query = new  yii\db\Query;
		$query	->select(['sliders.*'])  
        ->from('sliders')
        ->where(['status' => 'active'])
         		->andWhere(['slider_type' => 'homesecond']);
         	
				
		$command = $query->createCommand();
		$data = $command->queryAll();
		$arr = array();
			
		if(!empty($data))
		{
			foreach($data as $slider)
			{
				array_push($arr,$slider['id']);
			}
		}

	 	$query1 = new  yii\db\Query;
		$query1	->select(['slider_images.*', 'brands.slug as bransslug','brands.image as brandimage'])  
        ->from('slider_images')
        ->leftJoin('brands', 'slider_images.brand_id = brands.brand_id')
        ->where(['IN', 'slider_images.slider_id', $arr ]);
		$command1 = $query1->createCommand();
	
		$data1 = $command1->queryAll();
		
		return $data1;
		
    }

    public static function getHomePageSliderImages($is_mobile=0)
    {
        $and="";
		if($is_mobile){
			$and.=" and u.is_mobile=1 ";
		}
        $sql = "SELECT si.title, si.image, si.link FROM  slider_images si 
                inner join `sliders` as u on si.slider_id=u.id  
                WHERE u.`status` = :status  AND u.`start_date` IS NULL  
                AND u.`end_date` IS NULL  AND u.`slider_type` = :sliderType   AND u.is_delete = :isDelelted" .$and;
		
        $slider = self::findBySql($sql, [':status' => 'active', ':sliderType' => 'home', ':isDelelted' => 0]);
// 		var_dump($slider->createCommand()->rawSql);die;
        $slider = $slider->all();
        if (count($slider) > 0) {
            return $slider;
        } else {
            return self::find()->where(['status' => 'active'])
                ->andWhere(['slider_type' => 'default'])
                ->andWhere(['is_delete' => 0])
                ->all();
        }
	}
	




	public static function getCategorySlider($id){
		$sql = 'SELECT * FROM `'. self::tableName().'` as u 
				WHERE Date(NOW()) between u.`start_date`
				AND u.`end_date` AND u.`status` = :status
				AND u.`slider_type` = :sliderType
				AND u.`is_delete` = :isDelelted
				AND u.`category_id` = :categoryId
				UNION (SELECT * FROM `'. self::tableName().'` as u 
				WHERE u.`status` = :status 
				AND u.`start_date` IS NULL 
				AND u.`end_date` IS NULL 
				AND u.`category_id` = :categoryId 
				AND u.`slider_type` = :sliderType 
				AND u.`is_delete` = :isDelelted )';

    	$slider = self::findBySql($sql, [
    			':status' => 'active', 
    			':sliderType' => 'category',
    			':isDelelted' => 0,
    			':categoryId' => $id,
    	])
    	->all();
    	
    	if(count($slider) > 0){
    		return $slider;
    	}else{
    		return self::find()->where(['status' => 'active'])
    				->andWhere(['slider_type' => 'default'])
    				->andWhere(['is_delete' => 0])
    				->all();
    	}
	}
	
	public static function getSliderForAPI($category_id = 0){
		$category_id = (int) $category_id;
		$data = [];
		if(empty($category_id))
			$sliders = self::getHomePageSlider(0);
		else 
			$sliders = self::getCategorySlider($category_id);
		
		$index = 0;
		foreach($sliders as $slider){
			$data[$index] = self::convertNullFieldsToEmptyString( \yii\helpers\ArrayHelper::toArray($slider,[
	    		'common\models\Sliders' => self::getFieldsArray(),
	    	]) );
			$images = isset($slider->sliderImages) ? $slider->sliderImages : [];
			if(!empty($images)){
				foreach ($images as $sliderImage){
					$data[$index]['images'][] = SliderImages::convertNullFieldsToEmptyString(
						\yii\helpers\ArrayHelper::toArray($sliderImage,[
							'common\models\SliderImages' => SliderImages::getFieldsArray(),
						])
					);
				}
			}else
				$data[$index]['images'] = [];
			
			$index++;
		}
		unset($sliders);
		return $data;
	}
	
	public static function convertNullFieldsToEmptyString(array $array){
		$config = \HTMLPurifier_Config::createDefault();
		$config->set('HTML.AllowedElements', []);
		$purifyHtml = new \HTMLPurifier($config);
		$array['image'] = empty($array['image']) ? \yii::$app->params['parentDomain'].'images/api/no-image.jpg' : \yii::$app->params['parentDomain'] . $array['image'];
		$array['link'] = empty($array['link']) ? '' : $array['link'];
		$array['description'] = empty($array['description']) ? '' : $purifyHtml->purify( $array['description'] );
		$array['start_date'] = empty($array['start_date']) ? '' : $array['start_date'];
		$array['end_date'] = empty($array['end_date']) ? '' : $array['end_date'];
		$array['category_id'] = empty($array['category_id']) ? 0 : $array['category_id'];
		$array['title'] = empty($array['title']) ? '' : $array['title'];
		unset ($config, $purifyHtml);
		return $array;
	}
	
	public static function getFieldsArray(){
    	return [
	    	'id',
	    	'title',
	    	'category_id',
	    	'slider_type',
	    	'image',
	    	'description',
    		'start_date',
    		'end_date',
    		'link',
	    	'status',
			'created',
			'is_mobile',
    	];
    }
    
    public function getSliderImages() {
        return $this->hasMany(SliderImages::className(), ['slider_id' => 'id'])->Where(['status' => 1]);
    }
}
