<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "home_image_slider".
 *
 * @property integer $id
 * @property string $url
 * @property string $file
 */
class HomeImageSlider extends \yii\db\ActiveRecord
{
    public $uploadFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'home_image_slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'file'], 'required'],
            [['url', 'file'], 'string'],
            [['uploadFile'], 'file', 'extensions' => 'jpg, jpeg, gif, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'file' => 'Image',
            'uploadFile' => 'Image',
        ];
    }

    /**
     * fetch stored image file name with complete path
     * @return string
     */
    public function getImageFile()
    {
        return Yii::getAlias('@frontend/web/img/slider/') . $this->file;
    }

    /**
     * fetch stored image url
     * @return string
     */
    public function getImageUrl()
    {
        return Yii::$app->getModule('articles')->frontPageUrl . 'img/slider/' . $this->file;
    }

    /**
     * Process upload of image
     *
     * @return mixed the uploaded image instance
     */
    public function uploadImage()
    {
        // get the uploaded file instance. for multiple file uploads
        // the following data will return an array (you may need to use
        // getInstances method)
        $image = UploadedFile::getInstance($this, 'uploadFile');

        // if no image was uploaded abort the upload
        if (empty($image)) {
            return false;
        }
        error_log(var_export($image->name, true));

        // store the source file name
//        $this->filename = $image->name;
        $ext = end((explode(".", $image->name)));

        // generate a unique file name
        $this->file = Yii::$app->security->generateRandomString() . ".{$ext}";

        // the uploaded image instance
        return $image;
    }

    /**
     * Process deletion of image
     *
     * @return boolean the status of deletion
     */
    public function deleteImage()
    {
        $file = $this->getImageFile();

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        // if deletion successful, reset your file attributes
        $this->avatar = null;
        $this->filename = null;

        return true;
    }
}
