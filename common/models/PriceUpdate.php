<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_reviews".
 *
 * @property integer $id
 * @property string $user_name
 * @property string $email
 * @property integer $product_id
 * @property string $review
 * @property string $created
 * @property string $updated
 */
class PriceUpdate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price_update';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id','send_mail'], 'integer'],
            [['user_id'], 'integer'],
            [['created','alert_price'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
            'created' => 'Created',
        ];
    }

    public function getUpdate($userId, $productId){
        $PriceUpdate = self::find()->where(['user_id' => (int) $userId, 'product_id' =>  $productId ])->all();

        return $PriceUpdate;
    }
}
