<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product-supplier-feature-mappings".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $store_id
 * @property string $store_name
 * @property string $emi
 * @property string $cod
 * @property string $delivery
 * @property string $return_policy
 * @property string $uploadFile
 */
class ProductSupplierFeatureMappings extends \yii\db\ActiveRecord
{

    public $uploadFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_supplier_feature_mappings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'store_id', 'store_name'], 'required'],
            [['category_id', 'store_id'], 'integer'],
            [['emi', 'cod', 'delivery', 'return_policy'], 'string'],
            [['store_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category',
            'store_id' => 'Store ID',
            'store_name' => 'Store Name',
            'emi' => 'Emi',
            'cod' => 'Cash On Delivery',
            'delivery' => 'Delivery',
            'return_policy' => 'Return Policy',
        ];
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this::hasOne(Categories::className(), ['category_id' => 'category_id']);
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this::hasOne(Suppliers::className(), ['id' => 'store_id']);
    }
}
