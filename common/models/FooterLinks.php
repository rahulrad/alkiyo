<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "footer_links".
 *
 * @property integer $id
 * @property string $link_name
 * @property string $link
 */
class FooterLinks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'footer_links';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['link_name', 'link'], 'required'],
            [['link_name'], 'string', 'max' => 255],
            [['link'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link_name' => 'Link Name',
            'link' => 'Link',
        ];
    }
}
