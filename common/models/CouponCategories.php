<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "cupon_categories".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $meta_title
 * @property string $meta_keyword
 * @property string $meta_descrption
 * @property integer $show_home
 * @property string $created
 */
class CouponCategories extends \yii\db\ActiveRecord
{
	
	 public $file;
	 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coupon_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['name'], 'required'],
            [['show_home'], 'integer'],
            [['created','is_delete'], 'safe'],
			[['file'], 'file'],
            [['name'], 'string', 'max' => 100],
            [['meta_title', 'meta_keyword', 'meta_descrption','image','image_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_descrption' => 'Meta Descrption',
            'show_home' => 'Show Home',
            'created' => 'Created',
        ];
    }
	
	public function saveCategoryFromApi($categoryName){
		
		$category = CouponCategories::findOne(['name'=>  $categoryName]);
			
        if (empty($category)) {
		    $couponCategoriesModel = new CouponCategories();
			$couponCategoriesModel->name = $categoryName;
			$couponCategoriesModel->description = $categoryName;
			$couponCategoriesModel->meta_title = $categoryName;
			$couponCategoriesModel->meta_keyword = $categoryName;
			$couponCategoriesModel->meta_descrption = $categoryName;
			$couponCategoriesModel->status = 'active';
			$couponCategoriesModel->show_home = 0;
			$couponCategoriesModel->created = date('Y-m-d h:i:s');
			$couponCategoriesModel->save();
			$category = CouponCategories::findOne(['name'=>  $categoryName]);
			
        }else{
			
			$couponCategoriesModel = $category;
			$couponCategoriesModel->is_delete = 0;
			$couponCategoriesModel->save(false);
			
		}
       
		
        return $category;
	}
	
	public static function getCouponsAndCashBackForAPI(){
		$couponCategories = self::find()->where(['show_home' => '1'])->limit(6)->all();
		$couponsArray = [];
		$couponsIndex = 0;
		 
		foreach($couponCategories as $couponCategory){
			$couponData = \common\models\Coupons::find()->Where([ 'category' => $couponCategory->id ])->orderBy('id DESC')->limit(6)->all();

			$couponsArray[$couponsIndex] = \yii\helpers\ArrayHelper::toArray($couponCategory,[
				'common\models\CouponCategories' => self::getFieldsArray(),
			]);
			$couponsArray[$couponsIndex]['image'] = empty($couponsArray[$couponsIndex]['image']) ? '' : $couponsArray[$couponsIndex]['image'];
			
			if(count($couponData) > 0){
				
				$couponsDataArray =  [];
				$couponDataIndex = 0;
				foreach($couponData as $deal){
					$couponsDataArray = \yii\helpers\ArrayHelper::toArray($deal,[
						'common\models\Coupons' => \common\models\Coupons::getFieldsArray(),
					]);
					$couponsArray[$couponsIndex]['children'][$couponDataIndex] = $couponsDataArray;
					$couponDataIndex++;
				}
			}

			$couponsIndex++;
		}
		
		return Products::createFormatedArrayForAPI( $couponsArray );
	}
	
	public static function getFieldsArray(){
		return [
			'id',
			'name',
			'description',
			'meta_title',
			'meta_keyword',
			'meta_descrption',
			'show_home',
			'status',
			'created',
		];
	}
        
    /**
     * Gets category coupons
     * @return type
     */
    public function getCoupons() {
        return $this->hasMany(Coupons::className(), ['category' => 'id']);
    }


	   	
	public function getCategoryLists(){
			$categoryData = Self::find()->select(['id','name'])->where(['status'=>'active'])->andWhere(['!=', 'is_delete' ,1])->all();
			$categories = array();
			if(!empty($categoryData)){
				foreach($categoryData as $category){
				
					$categories[$category->id] = $category->name;
				}
			}
		return $categories;
	}
	
    
}
