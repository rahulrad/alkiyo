<?php

namespace common\modules\importerEs\components;

use yii\base\Component;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ElasticIndex extends Component {

    const REPLICAS = 0; // default number of replicas for index
    const SHARDS = 5; // default number of replicas for index
    
    const INDEX_PRODUCTS = 'ns_products';

    static function getSettings($index = null) {

        $settings = [
            'number_of_replicas' => self::REPLICAS,
            'number_of_shards' => self::SHARDS,
        ];
        
        switch ($index):
            case self::INDEX_PRODUCTS:
                $settings['number_of_replicas'] = 0;
                $settings['number_of_shards'] = 5;
                $settings['translog'] = ['sync_interval' => '15s', 'durability' => 'async'];
                $settings['analysis'] = [
                    "analyzer" => [
                        "autocomplete_analyzer" => [
                            "type" => "custom",
                            "tokenizer" => "keyword",
                            "filter" => ["lowercase", "autocomplete_filter"]
                        ]
                    ],
                    'filter' => [
                        'autocomplete_filter' => [
                            "type" => 'edge_ngram',
                            "min_gram" => 1,
                            "max_gram" => 20
                        ]
                    ],
                ];
                break;
        endswitch;
        
        
        return $settings;
    }

    static function getMappings($index) {

        $mapping = null;

        switch ($index):
            case 'ns_products':
                $mapping = [
                    'product' => [ // type
                        '_source' => ['enabled' => true],
                        '_all' => ['enabled' => false],
                        'properties' => [
                            'product_id' => [
                                'type' => 'integer',
                                'doc_values' => true,
                                'index' => 'not_analyzed',
                            ],
                            'product_name'=>[
                                'type'=>"string",
                                'index'=>"analyzed",
                                'copy_to'=>'product_name_autocomplete',
                            ],
                            'product_name_autocomplete' => [
                                'type' => 'string',
                                'index' => 'analyzed',
                                'analyzer' => 'autocomplete_analyzer',
                                'search_analyzer' => 'autocomplete_analyzer'
                            ],
                            'product_description'=>[
                                'type'=>"string",
                                'index'=>"analyzed",
                            ],
                            'model_number'=>[
                                'type'=>"string",
                                'index'=>"analyzed",
                            ],
                            'meta_title'=>[
                                'type'=>"string",
                                'index'=>"analyzed",
                            ],
                            'meta_keyword'=>[
                                'type'=>"string",
                                'index'=>"analyzed",
                            ],
                            'meta_desc'=>[
                                'type'=>"string",
                                'index'=>"analyzed",
                            ],
                            'slug'=>[
                                'type'=>"string",
                                'doc_values' => true,
                                'index'=>"not_analyzed",
                            ],
                            'supplier_name'=>[
                                'type'=>"string",
                                'doc_values' => true,
                                'index'=>"not_analyzed",
                            ],
                            'supplier_image'=>[
                                'type'=>"string",
                                'doc_values' => true,
                                'index'=>"not_analyzed",
                            ],
                            'active'=>[
                                'type'=>"string",
                                'doc_values' => true,
                                'index'=>"not_analyzed",
                            ],
                            'url'=>[
                                'type'=>"string",
                                'doc_values' => true,
                                'index'=>"not_analyzed",
                            ],
                            'product_status'=>[
                                'type'=>"string",
                                'doc_values' => true,
                                'index'=>"not_analyzed",
                            ],
                            'image'=>[
                                'type'=>"string",
                                'doc_values' => true,
                                'index'=>"not_analyzed",
                            ],
                            'product_image'=>[
                                'type'=>"string",
                                'doc_values' => true,
                                'index'=>"not_analyzed",
                            ],
                            'categories_category_id' => [
                                'type' => 'integer',
                                'doc_values' => true,
                                'index' => 'not_analyzed',
                            ],
                            'categories' =>[
                                'type'=>"nested",
                                'include_in_parent' => true,
                                'properties' => [
                                    'name' =>[
                                        'type'=>'string',
                                        'doc_values' => true,
                                        'index'=>'not_analyzed'
                                    ],
                                    'id' =>[
                                        'type'=>'integer',
                                        'doc_values' => true,
                                        'index'=>'not_analyzed'
                                    ],
                                    'folder_slug' =>[
                                        'type'=>'string',
                                        'doc_values' => true,
                                        'index'=>'not_analyzed'
                                    ],
                                    'slug' =>[
                                        'type'=>'string',
                                        'doc_values' => true,
                                        'index'=>'not_analyzed'
                                    ]
                                ]
                            ],
                            'lowest_price' => [
                                'type' => 'float',
                                'doc_values' => true,
                                'index' => 'not_analyzed',
                            ],
                            'original_price' => [
                                'type' => 'float',
                                'doc_values' => true,
                                'index' => 'not_analyzed',
                            ],
                            'highest_price' => [
                                'type' => 'float',
                                'doc_values' => true,
                                'index' => 'not_analyzed',
                            ],
                            'rating' => [
                                'type' => 'float',
                                'doc_values' => true,
                                'index' => 'not_analyzed',
                            ],
                            'discount' => [
                                'type' => 'float',
                                'doc_values' => true,
                                'index' => 'not_analyzed',
                            ],
                            'brands_brand_id' => [
                                'type' => 'integer',
                                'doc_values' => true,
                                'index' => 'not_analyzed',
                            ],
                            'brands_folder_slug' => [
                                'type' => 'string',
                                'doc_values' => true,
                                'index' => 'not_analyzed',
                            ],
                            'supplier_count' => [
                                'type' => 'integer',
                                'doc_values' => true,
                                'index' => 'not_analyzed',
                            ],
                            'store_id' => [
                                'type' => 'integer',
                                'doc_values' => true,
                                'index' => 'not_analyzed',
                            ],
                            'date_add' => [
                                'type' => 'date',
                                'doc_values' => true,
                                "format"=> "yyy-MM-dd HH:mm:ss",
                                'index' => 'not_analyzed',
                                'null_value' => '0000-00-00 00:00:00',
                                'ignore_malformed' => true,
                            ],
                            'date_update' => [
                                'type' => 'date',
                                'doc_values' => true,
                                "format"=> "yyy-MM-dd HH:mm:ss",
                                'index' => 'not_analyzed',
                                'null_value' => '0000-00-00 00:00:00',
                                'ignore_malformed' => true,
                            ],
                            'date_launch' => [
                                'type' => 'date',
                                'doc_values' => true,
                                "format"=> "yyy-MM-dd",
                                'index' => 'not_analyzed',
                                'null_value' => '0000-00-00',
                                'ignore_malformed' => true,
                            ],
//                            'features' => [
////                                'type'=>'nested',
////                                'include_in_parent'=>true,
//                                'properties'=>[
//                                    'feature_id'=>[
//                                        'type' => 'integer',
//                                        'doc_values' => true,
//                                        'index' => 'not_analyzed',
//                                    ],
//                                    'value_id'=>[
//                                        'type' => 'integer',
//                                        'doc_values' => true,
//                                        'index' => 'not_analyzed',
//                                    ],
//                        ]
//                    ],
//                            'filters' => 
                        ]
                        ]
                ];
                
                $filters = \backend\models\FilterGroups::loadAll();
                
                $properties = &$mapping['product']['properties'];
                foreach($filters as $filter):
                    $id = $filter['id'];
                    $properties['filter_'.$id] = [
//                            'type'=>'nested',
//                            'include_in_parent'=>true,
                            'properties'=>[
                                'group_id'=>[
                                    'type' => 'integer',
                                    'doc_values' => true,
                                    'index' => 'not_analyzed',
                                ],
//                                'group_name'=>[
//                                    'type' => 'string',
//                                    'doc_values' => true,
//                                    'index' => 'not_analyzed',
//                                ],
                                "items"=>[
//                                    'type'=>'nested',
//                                    'include_in_parent'=>true,
                                    'properties'=>[
                                        'item_id'=>[
                                            'type' => 'integer',
                                            'doc_values' => true,
                                            'index' => 'not_analyzed',
                                        ],
//                                        'item_name'=>[
//                                            'type' => 'string',
//                                            'doc_values' => true,
//                                            'index' => 'not_analyzed',
//                                        ]
                                    ]
                                ]
                            ]
                    ];
//                    \common\components\Utils::d($mapping,1);
                endforeach;

                break;
        endswitch;
        return $mapping;
    }

}