<?php

namespace common\modules\searchEs;

use Yii;
use yii\base\BootstrapInterface;
use yii\base\Module as BaseModule;
 
class Module extends BaseModule implements BootstrapInterface
{

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\searchEs\controllers';

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        // initialize the module with the configuration loaded from config.php
        \Yii::configure($this, require(__DIR__ . '/config/config.php'));
//        if (Yii::$app instanceof \yii\console\Application) {
        
//            $this->controllerNamespace = 'common\modules\searchEs\commands';
                
//        }
    }
    
    public function bootstrap($app) {
        
    }

}
