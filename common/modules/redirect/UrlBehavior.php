<?php

namespace common\modules\redirect;

use Yii;
use yii\base\Behavior;
use yii\web\Response;
use common\models\Redirect;


class UrlBehavior extends Behavior
{
    /**
     * @var string
     */
    public $redirect = 'redirect';

    public function events()
    {
        return [
            Response::EVENT_BEFORE_SEND => 'beforeSend'
        ];
    }

    public function beforeSend()
    {
        if (Yii::$app->response->statusCode == 404) {
            $pathInfo = Yii::$app->request->pathInfo;
            if (!empty($pathInfo)) {
                $newURL=Redirect::getNewUrl($pathInfo);
               
                if ( $newURL) {
                       header('Location: ' . Yii::$app->params['baseurl'].$newURL, true, 301);
                        exit;
                }
            }
        }
    }
}