<?php
use common\modules\articles\assets\CmsAsset;

/* @var $this \yii\web\View */
/* @var $content string */

CmsAsset::register ( $this );
?>
<?php $this->beginPage()?>
<!DOCTYPE HTML>
<!-- BEGIN html -->
<html lang="en">
<!-- BEGIN head -->
<head>
<title><?php echo $this->title ?> | NayaShopi.in</title>

<!-- Meta Tags -->
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1" />

<!-- Favicon -->
<link rel="shortcut icon" href="http://nayashopi.in/images/favicon.ico" type="image/x-icon" />


<!--[if lte IE 8]>
<link type="text/css" rel="stylesheet" href="css/ie-ancient.css" />
<![endif]-->
<!--[if lte IE 8]>
<link type="text/css" rel="stylesheet" href="css/ie-ancient.css" />
<![endif]-->


<?php $this->head()?>

<!-- END head -->
</head>

<!-- BEGIN body -->
<body>
<?php $this->beginBody()?>
	<!-- BEGIN .boxed -->
	<div class="boxed">


		<!-- BEGIN .header -->
		<header class="header">

<!--			<div class="top-line">
<!--				<div class="wrapper">
<!-- 					<nav> -->
<!-- 						<ul class="top-menu load-responsive" rel="Top Menu"> -->
<!-- 							<li><a href="index.html"><span>Homepage</span></a> -->
<!-- 								<ul> -->
<!-- 									<li><a href="index-sizes.html">Homepage With Split Blocks</a></li> -->
<!-- 								</ul></li> -->
<!-- 							<li><a href="category.html">Gadgets</a></li> -->
<!-- 							<li><a href="category.html"><span>Video</span></a> -->
<!-- 								<ul> -->
<!-- 									<li><a href="#">Sub-Category link</a></li> -->
<!-- 									<li><a href="#">Another Sub-Category</a></li> -->
<!-- 								</ul></li> -->
<!-- 							<li><a href="category.html">People</a></li> -->
<!-- 							<li><a href="category.html">Software</a></li> -->
<!-- 							<li><a href="photo-gallery.html">Gallery</a></li> -->
<!-- 							<li><a href="contact-us.html">Contact Us</a></li> -->
<!-- 						</ul> -->
<!-- 					</nav> -->
<!--				</div>
<!--			</div>

			<!-- BEGIN .wrapper -->
			<div class="wrapper">

				<!-- BEGIN .header-block -->
				<div class="header-block">

					<div class="header-logo">
						<!-- <h1><a href="/engage">Gadgetine</a></h1> -->
						<a href="/engage"><img src="http://nayashopi.in/images/engage-logo.png" alt="" /></a>
					</div>

					<div class="header-banner">
					</div>

					<!-- END .header-block -->
				</div>

				<!-- END .wrapper -->
			</div>

			<?php $this->beginContent(__DIR__.'/menu.php'); ?>
    			<!-- You may need to put some content here -->
			<?php $this->endContent(); ?>
			<!-- END .header -->
		</header>

		<!-- BEGIN .content -->
		<section class="content">

			<!-- BEGIN .wrapper -->
			<div class="wrapper">

				<!-- BEGIN .full-block -->
				<?= $content?>
			</div>

			<!-- BEGIN .content -->
		</section>

		<!-- BEGIN .footer -->
		<footer class="footer">

			<!-- BEGIN .footer-widgets -->
			<div class="footer-widgets">

				<!-- BEGIN .wrapper -->
				<div class="wrapper">

					<div class="widget">
						<h3>Contact Information</h3>
						<div class="contact-widget">
							<div class="large-icon">
							</div>
							<div class="large-icon">
								<i class="fa fa-envelope"></i> <span><a href="#">support@nayashopi.in</a></span>
							</div>
							<p>Phasellus semper nisi vel mi sollicitudin commodo congue risus
								imperdiet. Cras massa lacus, volutpat bibendum ornare ut,
								pharetra non quam. Donec nec neque lectus.</p>
							<p>
								<strong>Phasellus semper nisi vel mi sollicitudin commodo congue
									risus imperdiet</strong>
							</p>
						</div>
					</div>

					<div class="widget">
						<h3>Popular Articles</h3>
						<div class="small-article-list">

							<div class="item">
								<div class="item-header">
									<a href="post.html" class="hover-image"><img
										src="/images/photos/image-1.jpg" alt="" /></a>
								</div>
								<div class="item-content">
									<h4>
										<a href="post.html">Southwest France's best for mountains: The
											Pyrenees</a><a href="post.html#comments" class="comment-link"><i
											class="fa fa-comment-o"></i><span>21</span></a>
									</h4>
								</div>
							</div>

							<div class="item">
								<div class="item-header">
									<a href="post.html" class="hover-image"><img
										src="/images/photos/image-2.jpg" alt="" /></a>
								</div>
								<div class="item-content">
									<h4>
										<a href="post.html">Shot of Aspirin: Potential New Treatment
											for Migraines?</a><a href="post.html#comments"
											class="comment-link"><i class="fa fa-comment-o"></i><span>50</span></a>
									</h4>
								</div>
							</div>

							<div class="item">
								<div class="item-header">
									<a href="post.html" class="hover-image"><img
										src="/images/photos/image-3.jpg" alt="" /></a>
								</div>
								<div class="item-content">
									<h4>
										<a href="post.html">More working families use food banks for
											help</a><a href="post.html#comments" class="comment-link"><i
											class="fa fa-comment-o"></i><span>98</span></a>
									</h4>
								</div>
							</div>

							<a href="blog.html" class="more-articles-button">show all
								articles</a>
						</div>
					</div>

					<div class="widget">
						<h3>Subscribe Newsletter</h3>
						<div class="subscribe-widget">
							<p>Te lorem libris iracundia eos. Ne eam liber veritus, eos eu
								agam recteque, exerci reformidans sea no</p>
							<!-- <div class="alert-block">
										<a href="#" class="close-alert-block"><i class="fa fa-times"></i></a>
										<strong>ERROR:</strong><span>Ad zril putent nusquam vix</span>
										</div> -->
							<form name="aweber-form" action="#" class="subscribe-form">
								<p class="sub-name">
									<input type="text" value="" placeholder="Name.." name="email"
										class="name">
								</p>
								<p class="sub-email">
									<input type="text" value="" placeholder="E-mail address.."
										name="email" class="email">
								</p>
								<p class="sub-submit">
									<input type="submit" value="Subscribe"
										class="button aweber-submit">
								</p>
							</form>
						</div>
					</div>

					<div class="clear-float"></div>

					<!-- END .wrapper -->
				</div>

				<!-- END .footer-widgets -->

			</div>


			<!-- END .footer -->
		</footer>

		<!-- END .boxed -->
	</div>
<div id="fb-root"></div>


<?php $this->endBody()?>
	<!-- END body -->
</body>
<!-- END html -->
</html>
<?php $this->endPage()?>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=979626842115905";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</script>

<script>
jQuery(document).ready(function() {
	jQuery('.ot-slider').owlCarousel({
		items : 1,
		loop: true,
		autoplay : true,
		nav : true,
		lazyload : false,
		dots : false,
		margin : 15
	}).on('custominit', function(event) {
		//var thisel = jQuery(this);
		console.log("asd");
	});

	jQuery('.owl-carousel').owlCarousel({
        center: true,
		items : 1,
		loop: true,
		autoplay : true,
		nav : false,
		lazyload : false,
        dots : false,
        margin : 15
	});
});
</script>

