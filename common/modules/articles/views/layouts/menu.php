<?php
use common\modules\articles\models\Categories;
use yii\helpers\Url;

$articleTypes = Categories::getParentCategories();

?>
<div class="header-menu">

    <nav class="main-menu">

        <a href="#dat-menu" class="main-menu-reposnive-button"><i
                    class="fa fa-bars"></i>Show Menu</a>
        <ul class="load-responsive" rel="Main Menu">
            <li><a href="<?= Url::to(['/articles']) ?>"><span>Home</span></a>
            </li>
            <?php foreach ($articleTypes as $id => $type): ?>
                <li>
                    <a href="<?= Url::to(['/articles/items/category', 'slug' => $type->alias]) ?>"><span><?= ucwords($type->name) ?></span></a>
                    <ul class="sub-menu">
                        <?php foreach (Categories::getChildCategories($type->id) as $child): ?>
                            <li><a
                                        href="<?= Url::to(['/articles/items/category', 'type' => $child->parent->alias, 'slug' => $child->alias]) ?>"><?= ucwords($child->name) ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endforeach; ?>
        </ul>
    </nav>
</div>
