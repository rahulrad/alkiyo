<?php
use yii\helpers\Url;
$this->title = "Engage";
?>
<!-- BEGIN .full-block -->
<div class="full-block" style="display:none;">

    <!-- BEGIN .ot-slider -->
    <div class="ot-slider owl-carousel">
        <?php
$sliderItems=array();
 foreach ($sliderItems as $item) : ?>
            <!-- BEGIN .ot-slide -->
            <div class="ot-slide">
                <div class="ot-slider-layer first">
                    <a
                            href="<?= Url::to(['/articles/items/category', 'type' => $item[0]->articleType->alias, 'slug' => $item[0]->category->alias]) ?>">
                        <strong><span style="color: #ed2d00;"
                                      class="category-tag"><?= ucwords($item[0]->category->name) ?></span><?= ucwords($item[0]->title) ?>
                           </span></strong>
                        <img src="<?= $item[0]->getImageThumbUrl("552x426") ?>" alt=""/>
                    </a>
                </div>
                <div class="ot-slider-layer second">
                    <a
                            href="<?= Url::to(['/articles/items/category', 'type' => $item[1]->articleType->alias, 'slug' => $item[1]->category->alias]) ?>">
                        <strong><span style="color: #2793b6;"
                                      class="category-tag"><?= ucwords($item[1]->category->name) ?></span><?= ucwords($item[1]->title) ?>
                       </span></strong>
                        <img src="<?= $item[1]->getImageThumbUrl("264x426") ?>" alt=""/>
                    </a>
                </div>
                <div class="ot-slider-layer third">
                    <a
                            href="<?= Url::to(['/articles/items/category', 'type' => $item[2]->articleType->alias, 'slug' => $item[2]->category->alias]) ?>">
                        <strong><span style="color: #86a10b;"
                                      class="category-tag"><?= ucwords($item[2]->category->name) ?></span><?= ucwords($item[2]->title) ?>
                      </span></strong>
                        <img src="<?= $item[2]->getImageThumbUrl("360x207") ?>" alt=""/>
                    </a>
                </div>
                <div class="ot-slider-layer fourth">
                    <a
                            href="<?= Url::to(['/articles/items/category', 'type' => $item[3]->articleType->alias, 'slug' => $item[3]->category->alias]) ?>">
                        <strong><span style="color: #a10b4b;"
                                      class="category-tag"><?= ucwords($item[3]->category->name) ?></span><?= ucwords($item[3]->title) ?>
                       </span></strong>
                        <img src="<?= $item[2]->getImageThumbUrl("360x207") ?>" alt=""/>
                    </a>
                </div>
                <!-- END .ot-slide -->
            </div>
        <?php endforeach; ?>

        <!-- END .ot-slider -->
    </div>

    <!-- END .full-block -->
</div>

<div class="main-content-wrapper">


    <!-- BEGIN .main-content -->
    <div class="main-content">

        <!-- BEGIN .split-blocks -->
        <div class="paragraph-row">

            <div class="column6">
                <?php foreach ($cmsTypesInfo as $keys=> $cmsTypesData) : ?>
                    <!-- BEGIN .def-panel -->
                    <div class="def-panel">
                        <div class="panel-title">
                            <a
                                    href="<?= Url::to(['/articles/items/category', 'slug' => $cmsTypesData['data']->alias]) ?>"
                                    class="right">show all <?= ucwords($cmsTypesData['data']->name) ?></a>
                            <h2>Recent <?= ucwords($cmsTypesData['data']->name) ?></h2>
                        </div>
                        <div class="panel-content article-list-big">

                            <!-- BEGIN .item -->
                            <div class="item item-the-huge">
                                <div class="item-header">
                                    <div class="image-overlay-icons"
                                         onclick="javascript:location.href = '<?= Url::to(['/articles/items/view', 'type' => $cmsTypesData['mostRecentItem']->articleType->alias, 'alias' => $cmsTypesData['mostRecentItem']->alias]) ?>';">
                                        <a href="<?= Url::to(['/articles/items/view', 'type' => $cmsTypesData['mostRecentItem']->articleType->alias, 'alias' => $cmsTypesData['mostRecentItem']->alias]) ?>"
                                           title="Read article"><i
                                                    class="fa fa-search"></i></a>
                                    </div>
                                    <a href="<?= Url::to(['/articles/items/view', 'type' => $cmsTypesData['mostRecentItem']->articleType->alias, 'alias' => $cmsTypesData['mostRecentItem']->alias]) ?>"
                                       class="hover-image"><img
                                                src="<?= $cmsTypesData['mostRecentItem']->getImageThumbUrl("430x240") ?>"
                                                alt=""/></a>
                                </div>
                                <div class="item-content">
                                    <h3>
                                        <a
                                                href="<?= Url::to(['/articles/items/view', 'type' => $cmsTypesData['mostRecentItem']->articleType->alias, 'alias' => $cmsTypesData['mostRecentItem']->alias]) ?>"><?= '' . ucwords($cmsTypesData['mostRecentItem']->title) ?></a>
                                    </h3>
                                    <p><?= substr(strip_tags($cmsTypesData['mostRecentItem']->fulltext), 0, 200) ?></p>
                                    <a
                                            href="<?= Url::to(['/articles/items/view', 'type' => $cmsTypesData['mostRecentItem']->articleType->alias, 'alias' => $cmsTypesData['mostRecentItem']->alias]) ?>"
                                            class="read-more-link">Read More<i class="fa fa-chevron-right"></i></a>
                                </div>
                                <!-- END .item -->
                            </div>

                        </div>

                        <div class="small-article-list">
                            <?php foreach ($cmsTypesData['mostRecentItemList'] as $item) : ?>

                                <div class="item">
                                    <div class="item-header">
                                        <a href="<?= Url::to(['/articles/items/view', 'type' => $item->articleType->alias, 'alias' => $item->alias]) ?>"
                                           class="hover-image"><img
                                                    src="<?= $item->getImageThumbUrl("57x37") ?>" alt=""/></a>
                                    </div>
                                    <div class="item-content">
                                        <h4>
                                            <a
							    href="<?= Url::to(['/articles/items/view', 'type' => $item->articleType->alias, 'alias' => $item->alias]) ?>"><?= ucwords($item->title) ?></a>
                                        </h4>
                                    </div>
                                </div>

                            <?php endforeach; ?>

                            <a href="<?= Url::to(['/articles/items/category', 'slug' => $cmsTypesData['data']->alias]) ?>"
                               class="more-articles-button">show all <?= ucwords($cmsTypesData['data']->name) ?></a>
                        </div>

                        <!-- END .def-panel -->
                    </div>

                    <!-- BEGIN .def-panel -->
			<?php if($keys>0 && $keys<2){ ?>
                    <div class="def-panel banner">


                        <!-- END .def-panel -->
                    </div>
		<?php } ?>
                <?php endforeach; ?>

            </div>

            <div class="column6">

                <!-- BEGIN .def-panel -->
                <?php foreach ($cmsTypesInfo as $key=> $mostHits) : ?>
                    <div class="def-panel featured-color">
                        <div class="medium-article-list">
                            <?php foreach ($mostHits['mostHitItemList'] as $item) : ?>
                                <div class="item">
                                    <div class="item-header">
                                        <div class="image-overlay-icons"
                                             onclick="javascript:location.href = '<?= Url::to(['/articles/items/view', 'type' => $item->articleType->alias, 'alias' => $item->alias]) ?>';">
                                            <a href="<?= Url::to(['/articles/items/view', 'type' => $cmsTypesData['mostRecentItem']->articleType->alias, 'alias' => $item->alias]) ?>"
                                               title="Read article"><i
                                                        class="fa fa-search"></i></a>
                                        </div>
                                        <a href="<?= Url::to(['/articles/items/view', 'type' => $item->articleType->alias, 'alias' => $item->alias]) ?>"
                                           class="hover-image"><img
                                                    src="<?= $item->getImageThumbUrl("114x85") ?>" alt=""/></a>
                                    </div>
                                    <div class="item-content">
                                        <h4>
                                            <a
                                                    href="<?= Url::to(['/articles/items/view', 'type' => $item->articleType->alias, 'alias' => $item->alias]) ?>"><?= ucwords($item->title) ?></a>
                                        </h4>
                                        <p><?= substr(strip_tags($item->fulltext), 0, 100) ?>...</p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <a href="<?= Url::to(['/articles/items/category', 'slug' => $mostHits['data']->alias]) ?>"
                               class="more-articles-button">Read More <?= ucwords($mostHits['data']->name) ?></a>
                        </div>
                        <!-- END .def-panel -->
                    </div>

                    <!-- BEGIN .def-panel -->
		  <?php if($key<1){  ?>
                    <div class="def-panel banner">
                        <!-- END .def-panel -->
                    </div>	
		<?php } ?>
                <?php endforeach; ?>


            </div>

            <!-- END .split-blocks -->
        </div>

        <!-- END .main-content -->
    </div>

    <aside id="sidebar">

        <!-- BEGIN .widget -->
        <div class="widget">
            <div class="banner">
            
</div>
            <!-- END .widget -->
        </div>


        <!-- BEGIN .widget -->
        <div class="widget">
            <h3>Popular Content</h3>
            <div class="small-article-list">
                <?php foreach ($topHitsData as $item): ?>
                    <div class="item">
                        <div class="item-header">
                            <a href="<?= Url::to(['/articles/items/view', 'type' => $item->articleType->alias, 'alias' => $item->alias]) ?>"><img
                                        src="<?= $item->getImageThumbUrl("114x85") ?>" alt=""/></a>
                        </div>
                        <div class="item-content">
                            <h4>
                                <a
                                        href="<?= Url::to(['/articles/items/view', 'type' => $item->articleType->alias, 'alias' => $item->alias]) ?>"><?= ucfirst($item->title) ?></a>
                            </h4>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <!-- END .widget -->
        </div>



    </aside>

</div>

<!-- END .wrapper -->
