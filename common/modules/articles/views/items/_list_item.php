<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>

<!-- BEGIN .item -->
<div class="item">
    <div class="item-header">
        <div class="relative-element">
            <div class="image-overlay-icons"
                 onclick="javascript:location.href = '<?= Url::to(['/articles/items/view', 'type' => $model->articleType->alias, 'alias' => $model->alias]) ?>';">
                <a href="<?= Url::to(['/articles/items/view', 'type' => $model->articleType->alias, 'alias' => $model->alias]) ?>"
                   title="Read article"><i class="fa fa-search"></i></a>
            </div>
            <a href="<?= Url::to(['/articles/items/view', 'type' => $model->articleType->alias, 'alias' => $model->alias]) ?>"
               class="hover-image"><?= Html::img($model->getImageThumbUrl("272x180")) ?></a>
        </div>
    </div>
    <div class="item-content">
        <h3>
            <a href="<?= Url::to(['/articles/items/view', 'type' => $model->articleType->alias, 'alias' => $model->alias]) ?>"><?= $model->title ?></a>
        </h3>
        <p><?= substr(strip_tags($model->fulltext), 0, 400); ?></p>
        <a href="<?= Url::to(['/articles/items/view', 'type' => $model->articleType->alias, 'alias' => $model->alias]) ?>"
           class="read-more-link">Read More<i
                    class="fa fa-chevron-right"></i></a>
    </div>
    <!-- END .item -->
</div>
