<?php

/**
 *
 * @copyright Copyright &copy; Gogodigital Srls
 *            @company Gogodigital Srls - Wide ICT Solutions
 *            @website http://www.gogodigital.it
 *            @github https://github.com/cinghie/yii2-articles
 * @license GNU GENERAL PUBLIC LICENSE VERSION 3
 * @package articles
 * @version 0.6.3
 *         
 */

use common\modules\articles\assets\ArticlesAsset;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

// Load Articles Assets
ArticlesAsset::register ( $this );
$asset = $this->assetBundles ['common\modules\articles\assets\ArticlesAsset'];

// Set Title and Breadcrumbs
$this->title = Yii::t ( 'articles', 'Tags' );
$this->params ['breadcrumbs'] [] = $this->title;

// Render Yii2-Articles Menu
echo Yii::$app->view->renderFile ('@common/modules/articles/views/default/_menu.php' );

// Register action buttons js
$this->registerJs ( '
    $(document).ready(function()
    {
        $("a.btn-update").click(function() {
            var selectedId = $("#w2").yiiGridView("getSelectedRows");

            if(selectedId.length == 0) {
                alert("' . Yii::t ( "articles", "Select at least one item" ) . '");
            } else if(selectedId.length>1){
                alert("' . Yii::t ( "articles", "Select only 1 item" ) . '");
            } else {
                var url = "' . Url::to ( [ 
		'/articles/tags/update' 
] ) . '&id="+selectedId[0];
                window.location.href= url;
            }
        });
        $("a.btn-active").click(function() {
            var selectedId = $("#w2").yiiGridView("getSelectedRows");

            if(selectedId.length == 0) {
                alert("' . Yii::t ( "articles", "Select at least one item" ) . '");
            } else {
                $.ajax({
                    type: \'POST\',
                    url : "' . Url::to ( [ 
		'/articles/tags/activemultiple' 
] ) . '?id="+selectedId,
                    data : {ids: selectedId},
                    success : function() {
                        $.pjax.reload({container:"#w2"});
                    }
                });
            }
        });
        $("a.btn-deactive").click(function() {
            var selectedId = $("#w2").yiiGridView("getSelectedRows");

            if(selectedId.length == 0) {
                alert("' . Yii::t ( "articles", "Select at least one item" ) . '");
            } else {
                $.ajax({
                    type: \'POST\',
                    url : "' . Url::to ( [ 
		'/articles/tags/deactivemultiple' 
] ) . '?id="+selectedId,
                    data : {ids: selectedId},
                    success : function() {
                        $.pjax.reload({container:"#w2"});
                    }
                });
            }
        });
        $("a.btn-delete").click(function() {
            var selectedId = $("#w2").yiiGridView("getSelectedRows");

            if(selectedId.length == 0) {
                alert("' . Yii::t ( "articles", "Select at least one item" ) . '");
            } else {
                var choose = confirm("' . Yii::t ( "articles", "Do you want delete selected items?" ) . '");

                if (choose == true) {
                    $.ajax({
                        type: \'POST\',
                        url : "' . Url::to ( [ 
		'/articles/tags/deletemultiple' 
] ) . '&id="+selectedId,
                        data : {ids: selectedId},
                        success : function() {
                            $.pjax.reload({container:"#w1"});
                        }
                    });
                }
            }
        });
        $("a.btn-preview").click(function() {
            var selectedId = $("#w2").yiiGridView("getSelectedRows");

            if(selectedId.length == 0) {
                alert("' . Yii::t ( "articles", "Select at least one item" ) . '");
            } else if(selectedId.length>1){
                alert("' . Yii::t ( "articles", "Select only 1 item" ) . '");
            } else {
                var url = "' . Url::to ( [ 
		'/articles/tags/view' 
] ) . '&id="+selectedId[0];
                window.open(url,"_blank");
            }
        });
    });
' );

?>

<div class="tags-index">

	<?php if(Yii::$app->getModule('articles')->showTitles): ?>
		<div class="page-header">
			<h1><?= Html::encode($this->title) ?></h1>
		</div>
	<?php endif ?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <!-- tags Grid -->
    <div class="tags-grid">

		<?php Pjax::begin() ?>

        <?= GridView::widget([
            'dataProvider'=> $dataProvider,
//             'filterModel' => $searchModel,
            'pjaxSettings'=>[
                'neverTimeout' => true,
            ],
            'columns' => [
				[
					'class' => '\kartik\grid\CheckboxColumn'
				],
            		[
            		'attribute' => 'id',
            		'hAlign' => 'center',
            		'width' => '5%',
            		],
				[
					'attribute' => 'name',
					'format' => 'html',
					'hAlign' => 'center',
                    'value' => function ($model) {
                        $url = urldecode(Url::toRoute(['tags/update', 'id' => $model->id]));
                        return Html::a($model->name,$url);
                    }
				],

			],
            'responsive' => true,
            'hover' => true,
			'panel' => [
				'heading'    => '<h3 class="panel-title"><i class="fa fa-folder-open"></i></h3>',
				'type'       => 'success',
                'before'     => '<span style="margin-right: 5px;">'.
                    Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t('articles', 'New'),
                        ['create'], ['class' => 'btn btn-success']
                    ).'</span><span style="margin-right: 5px;">'.
                    Html::a('<i class="glyphicon glyphicon-pencil"></i> '.Yii::t('articles', 'Update'),
                        '#', ['class' => 'btn btn-update btn-warning']
                    ).'</span><span style="margin-right: 5px;">'.
                    Html::a('<i class="fa fa-eye"></i> '.Yii::t('articles', 'Preview'),
                        '#', ['class' => 'btn btn-preview btn-info']
                    ).'</span><span style="float: right; margin-right: 5px;">',
				'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> '.
                    Yii::t('articles', 'Reset Grid'), ['index'], ['class' => 'btn btn-info']
				),
				'showFooter' => false
			],
        ]); ?>

		<?php Pjax::end() ?>

	</div>

</div>
