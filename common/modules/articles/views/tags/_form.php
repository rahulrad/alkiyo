<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model cinghie\articles\models\ArticleTags */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tags-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">

        <div class="col-lg-12">

            <div class="col-lg-5">

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            </div>

            <div class="col-lg-12">

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

            </div>

        </div>

    </div>

    

    

    <?php ActiveForm::end(); ?>

</div>



