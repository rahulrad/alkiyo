<?php

/**
* @copyright Copyright &copy; Gogodigital Srls
* @company Gogodigital Srls - Wide ICT Solutions 
* @website http://www.gogodigital.it
* @github https://github.com/cinghie/yii2-articles
* @license GNU GENERAL PUBLIC LICENSE VERSION 3
* @package yii2-articles
* @version 0.6.3
*/

use common\modules\articles\assets\ArticlesAsset;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use yii\helpers\Html;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

// Load Articles Assets
ArticlesAsset::register($this);
$asset = $this->assetBundles['common\modules\articles\assets\ArticlesAsset'];

// Load info
$select2videotype = $model->getVideoTypeSelect2();
$select2articles = $model->getItemsSelect2();
$articleType 	  = \common\modules\articles\models\Categories::getArticleTypes();


?>

<div class="attachments-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype'=>'multipart/form-data'
        ],
    ]); ?>

    <div class="row">

        <div class="col-lg-12">
        
<div class="col-lg-6">
                        
                        	<?= $form->field($model, 'video', [
									'addon' => [
										'prepend' => [
											'content'=>'<i class="glyphicon glyphicon-film"></i>'
										]
									]
							])->textInput(['maxlength' => true]) ?>
                        
                        	<?= $form->field($model, 'type')->widget(Select2::classname(), [
                                    'data' => $select2videotype,
                                    'addon' => [
										'prepend' => [
											'content'=>'<i class="glyphicon glyphicon-film"></i>'
										]
									],
                            ]); ?>   
                            
                        </div> <!-- end col-lg-6 -->
                        
                        <div class="col-lg-6">
                            
                            <?= $form->field($model, 'caption', [
									'addon' => [
										'prepend' => [
											'content'=>'<i class="glyphicon glyphicon-facetime-video"></i>'
										]
									 ]
							])->textarea(['maxlength' => 255,'rows' => 6]) ?>
                            
                            <?= 
	                        	$form->field($model, 'category_id')->widget(Select2::classname(), [
	                        			'data' => $articleType,
	                        			'language' => 'eg',
	                        			'options' => ['placeholder' => 'Select Article Type'],
	                        			'pluginOptions' => [
	                        					'allowClear' => true
	                        			],
	                        	]);
                        	
                        	?>
                        	<?=
                        		Html::hiddenInput('input-article_type', $model->category_id, ['id'=>'input-article_type']);
                        	?>
                            <?=
	                            $form->field($model, 'item_id')->widget(DepDrop::classname(), [
	                            		'options' => ['id'=>'itemvideogallery-item_id'],
	                            		'pluginOptions'=>[
	                            				'initialize' => true,
	                            				'depends'=>['itemvideogallery-category_id'],
	                            				'placeholder' => 'Select Cagtegory...',
	                            				'url' => Url::to(['/articles/item-image-gallery/category-articles']),
	                            				'params'=>['input-article_type']
	                            		]
	                            ]);
                            ?>
            
                        
                        
                    
         </div> <!-- #image -->

            <div class="col-lg-12">

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

            </div>

        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
