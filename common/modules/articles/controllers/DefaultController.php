<?php

/**
 * @copyright Copyright &copy; Gogodigital Srls
 * @company Gogodigital Srls - Wide ICT Solutions
 * @website http://www.gogodigital.it
 * @github https://github.com/cinghie/yii2-articles
 * @license GNU GENERAL PUBLIC LICENSE VERSION 3
 * @package yii2-articles
 * @version 0.6.3
 */
namespace common\modules\articles\controllers;

use yii\filters\AccessControl;
use yii\web\Controller;
use common\modules\articles\models\Items;
use yii\rbac\Item;
use common\modules\articles\models\Categories;

class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index'
                        ],
                        'roles' => [
                            '?', '@'
                        ]
                    ]
                ],
                'denyCallback' => function () {
                    throw new \Exception ('You are not allowed to access this page');
                }
            ]
        ];
    }

    public function actionIndex()
    {
        $this->layout = "cms";
$sliderItems = Items::getHomePageSliderItems();
        $sliderItems = array_chunk($sliderItems, 4);
        $cmsTypesData = [];
        $categories = Categories::getParentCategories();
        $counter = 0;
        foreach ($categories as $categorie) {
            $mostRecentItem = Categories::getMostRecentItemsByCmsType($categorie->id, 1);
            $mostRecentItemList = Categories::getMostRecentItemsByCmsType($categorie->id, 10, 'created desc', 1);
            $mostHitsItemList = Categories::getMostRecentItemsByCmsType($categorie->id, 10, 'hits desc');
            $cmsTypesData[$counter]['data'] = $categorie;
            $cmsTypesData[$counter]['mostRecentItem'] = isset ($mostRecentItem[0]) ? $mostRecentItem[0] : null;
            $cmsTypesData[$counter]['mostRecentItemList'] = $mostRecentItemList;
            $cmsTypesData[$counter]['mostHitItemList'] = $mostHitsItemList;
            $counter++;
        }

        $topHitsData = Items::getTopHitItems();

        return $this->render('index', [
            'sliderItems' => $sliderItems,
            'cmsTypesInfo' => $cmsTypesData,
            'topHitsData' => $topHitsData
        ]);
    }
}
