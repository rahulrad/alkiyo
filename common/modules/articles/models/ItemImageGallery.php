<?php

namespace common\modules\articles\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "article_item_image_gallery".
 *
 * @property integer $id
 * @property string $image
 * @property string $catption
 * @property integer $item_id
 */
class ItemImageGallery extends Articles
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_item_image_gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'item_id','category_id'], 'required'],
            [['image'], 'string'],
            [['item_id','category_id'], 'integer'],
            [['catption'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'catption' => 'Catption',
            'item_id' => 'Article',
        	'category_id' => 'Category'	
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
    	return $this->hasOne(Items::className(), ['id' => 'item_id'])->from(Items::tableName() . ' AS item');;
    }
    
    /**
     * fetch stored file url
     * @return string
     */
    public function getImageUrl()
    {
    	// return a default image placeholder if your source avatar is not found
    	$file = isset($this->image) ? $this->image : 'default.jpg';
    	return Yii::getAlias(Yii::$app->controller->module->itemImageURL).$file;
    }
    
    /**
     * fetch stored image url
     * @param $size
     * @return string
     */
    public function getImageThumbUrl($size)
    {
    	// return a default image placeholder if your source avatar is not found
    	$file = isset($this->image) ? $this->image : 'default.jpg';
    	return Yii::getAlias(Yii::$app->controller->module->itemImageURL)."thumb/".$size."/".$file;
    }
    
    public function search($params)
    {
    	$query = ItemImageGallery::find();
    	$query->joinWith('item');
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    			'sort' => [
    					'defaultOrder' => [
    							'id' => SORT_DESC
    					],
    			],
    	]);
    	$this->load($params);
    
//     	if (!$this->validate()) {
//     		return $dataProvider;
//     	}
    
    	$query->andFilterWhere(['like', 'item.title', $this->item_id]);
    	return $dataProvider;
    }
}
