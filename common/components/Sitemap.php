<?php

namespace common\components;

use Yii;
use common\models\Menus;


class Sitemap {

     public static $siteUrl = "https://www.nayashopi.in/";

    public function getSiteMapIndex() {
      $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
    SELECT  slug FROM menus where parent_id=0 and is_delete=0");
    $result = $command->queryAll();


        $sitemaps = array();
        // $link = new Link();

        foreach ($result as $category) {
             $sitemaps[] = self::getSitemapLink($category['slug']);
            $sitemaps[] =  self::getSitemapLink($category['slug'], 'product');
         }
        $sitemap_string = "";
        foreach ($sitemaps as $k => $sitemap) {
            $sitemap_string .= "\n<sitemap>\n";
            $sitemap_string .= "<loc>" . self::entityEscape($sitemap) . "</loc>\n";
            $sitemap_string .= "<lastmod>" . date('c', time()) . "</lastmod>\n";
            $sitemap_string .= "</sitemap>";
        }

     

        $sitemap_string = '<?xml version="1.0" encoding="UTF-8"?>
                            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' .
                $sitemap_string .
                '</sitemapindex>';
        return $sitemap_string;
    }

    public static function getSitemapLink($slug,$type="listing"){

        return self::$siteUrl.$slug.'-'.$type."-sitemap.xml";
    }



    public static function getListPageSiteMap($slug){


$connection = Yii::$app->getDb();
        $command = $connection->createCommand('
            SELECT id 
            FROM `menus`
            WHERE slug = :slug and is_delete=0 
           
                ', [':slug' => $slug]) ;
 
        $result = $command->queryAll();
        $listid=array();
        if(!empty($result)){
                       array_push($listid, $result[0]['id']);
                       $command = $connection->createCommand('
            SELECT id 
            FROM `menus`
            WHERE parent_id = :parent and is_delete=0 
           
                ', [':parent' => $result[0]['id']]) ;
 
              $resultchild = $command->queryAll();


              if(!empty( $resultchild)){

                    foreach( $resultchild as $subchild){

                         array_push($listid, $subchild['id']);

                           $comman= $connection->createCommand('
                                    SELECT id 
                                FROM `menus`
                                WHERE parent_id = :parent and is_delete=0 
                            
                                    ', [':parent' =>$subchild['id']]) ;
                    
                                $resultchilds = $comman->queryAll();
                                if(!empty( $resultchilds)){
                        foreach( $resultchilds as $subchilds){
                                    array_push($listid, $subchilds['id']);

                                                    }

                                }



                    }

              }

        }

      
 $commandexecute= $connection->createCommand('
                                    SELECT slug 
                                FROM `menus`
                                WHERE id  in ('.implode(",",$listid).') and is_delete=0') ;
                    
                                $resultSitemap = $commandexecute->queryAll();
                        if(!empty( $resultSitemap)){

                        
    


     $sitemap_string = "";
        foreach ($resultSitemap as $k => $sitemap) {
          $sitemap_string .=self::getSingleUrlEntry(self::$siteUrl.$sitemap['slug']) ;
        }

                        }

        $sitemap_string =  self::getFullSiteMap($sitemap_string);
        return $sitemap_string;
       

    }



    public static function getProductSiteMap($slug){


$connection = Yii::$app->getDb();
        $command = $connection->createCommand('
            SELECT id 
            FROM `menus`
            WHERE slug = :slug and is_delete=0 
           
                ', [':slug' => $slug]) ;
 
        $result = $command->queryAll();
        $listid=array();
        if(!empty($result)){
                       array_push($listid, $result[0]['id']);
                       $command = $connection->createCommand('
            SELECT category_id as id 
            FROM `menus`
            WHERE parent_id = :parent and is_delete=0 
           
                ', [':parent' => $result[0]['id']]) ;
 
              $resultchild = $command->queryAll();


              if(!empty( $resultchild)){

                    foreach( $resultchild as $subchild){

                         array_push($listid, $subchild['id']);

                           $comman= $connection->createCommand('
                                    SELECT category_id as id
                                FROM `menus`
                                WHERE parent_id = :parent and is_delete=0 
                            
                                    ', [':parent' =>$subchild['id']]) ;
                    
                                $resultchilds = $comman->queryAll();
                                if(!empty( $resultchilds)){
                        foreach( $resultchilds as $subchilds){
                                    array_push($listid, $subchilds['id']);

                                                    }

                                }



                    }

              }

        }

      

 $commandexecute= $connection->createCommand('
                                   select p.slug  as pslug, c.slug as cslug from products p inner join categories c on c.category_id=p.categories_category_id where p.categories_category_id  in ('.implode(",",$listid).') and p.is_delete=0') ;
                    
                                $resultSitemap = $commandexecute->queryAll();
                        if(!empty( $resultSitemap)){

                        
    


     $sitemap_string = "";
        foreach ($resultSitemap as $k => $sitemap) {
          $sitemap_string .=self::getSingleUrlEntry(self::$siteUrl.$sitemap['cslug'].'/'.$sitemap['pslug']) ;
        }

                        }

        $sitemap_string =  self::getFullSiteMap($sitemap_string);
        return $sitemap_string;
       

    }

    // public function getProductSiteMap() {
    //     $all_entries = $this->getProductEntries();
    //     $site_map_part = implode("\n", $all_entries);
    //     return $this->getFullSiteMap($site_map_part);
    // }

    // public function getListingSiteMap() {
    //     $all_entries = $this->getListingEntries();
    //     $site_map_part = implode("\n", $all_entries);
    //     return $this->getFullSiteMap($site_map_part);
    // }

    // public function getNewsSiteMap() {
    //     $all_entries = $this->getNewsEnteries();
    //     $site_map_part = implode("\n", $all_entries);
    //     return $this->getFullSiteMap($site_map_part);
    // }

    // public function getModelSiteMap() {
    //     $model_entries = $this->getModelEnteries();
    //     $site_map_part = implode("\n", $model_entries);
    //     return $this->getFullSiteMap($site_map_part);
    // }

    private static function getFullSiteMap($submap) {
        return '<?xml version="1.0" encoding="UTF-8"?>
                    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
                            xmlns:image="http://www.sitemaps.org/schemas/sitemap-image/1.1"
                            xmlns:video="http://www.sitemaps.org/schemas/sitemap-video/1.1">' .
                $submap .
                '</urlset>';
    }

    private static function getSingleUrlEntry($url, $frequency = 'daily', $last_modify = false, $priority = false) {
        $xml = "<url>";
        $xml.= "<loc>" . self::entityEscape($url) . "</loc>";

        if ($frequency)
            $xml.= "<changefreq>$frequency</changefreq>";

        if ($last_modify)
            $xml.= "<lastmod>$last_modify</lastmod>";

        if ($priority)
            $xml.= "<priority>$priority</priority>";

        $xml.= "</url>";

        return $xml;
    }

    // private function getProductEntries() {
    //     $product_entries = array();
    //     $category = Category::getCategoryBylinkRewrite($this->_linkrewrite);
    //     $link = new Link();
    //     $products = $category->products;
    //     if($products && is_array($products)){
    //         foreach ($products as $product) {
    //             $price_url = $link->getProductLink($product);
    //             $product_specs_link = $link->getProductSpecsLink($product);
    //             $product_review_link = $link->getProductReviewLink($product);
    //             $product_video_url = $link->getProductVideoViewLink($product);
    //             $product_picture_url = $link->getProductImageViewLink($product);

    //             $expertreveiws = $product->getallExpertreview($product->id_product);
    //             $all_videos = UtilityModel::getVideos($product->id_product);
    //             if ($category->link_rewrite == 'books')
    //                 $all_images = array();
    //             else
    //                 $all_images = CImage::getImages($product->id_product);
                
    //             //$product->
    //             $modfilydate = date('c', strtotime($product->date_update));
    //             $product_entries[] = $this->getSingleUrlEntry($price_url, 'daily', $modfilydate);
    //             $product_entries[] = $this->getSingleUrlEntry($product_specs_link, 'daily', $modfilydate);
    //             if (count($expertreveiws)):
    //                 $product_entries[] = $this->getSingleUrlEntry($product_review_link, 'daily', $modfilydate);
    //             endif;
    //             if (count($all_videos)):
    //                 $product_entries[] = $this->getSingleUrlEntry($product_video_url, 'daily', $modfilydate);
    //             endif;
    //             if (count($all_images)):
    //                 $product_entries[] = $this->getSingleUrlEntry($product_picture_url, 'daily', $modfilydate);
    //             endif;
    //         }
    //     }
    //     return $product_entries;
    // }

    // private function getListingEntries() {
    //     $listing_entries = array();
    //     $category = Category::getCategoryBylinkRewrite($this->_linkrewrite);
    //     $link = new Link();

    //     $all_filters = FilterManager::getCatalogFilter($category->id_category);
    //     $temp_filter = array();
    //     foreach ($all_filters as $afilter) {
    //         $group = $afilter['groupname'];
    //         if (!isset($temp_filter[$group]))
    //             $temp_filter[$group] = array();
    //         $count1 = FilterManager::getProductCount($category->id_category, $afilter, array());
    //         if ($count1 > 0)
    //             $listing_entries[] = $this->getSingleUrlEntry($link->getCatalogLink($category->id_category, array($afilter), array()), 'weekly');
    //         $temp_filter[$group][] = $afilter;
    //     }
    //     array_reverse($temp_filter);
    //     while (count($temp_filter) > 0) {
    //         $selectedfilters = array_pop($temp_filter);
    //         foreach ($selectedfilters as $k => $selected_filter) {

    //             foreach ($temp_filter as $k => $v) {
    //                 foreach ($v as $filtertag) {
    //                     $count = FilterManager::getProductCount($category->id_category, $filtertag, array($selected_filter));
    //                     if ($count > 0)
    //                         $listing_entries[] = $this->getSingleUrlEntry($link->getCatalogLink($category->id_category, array($selected_filter), $filtertag), 'weekly');
    //                 }
    //             }
    //         }
    //     }

    //     return $listing_entries;
    // }

    // private function getNewsEnteries() {
    //     Yii::import('application.modules.news.models.*');
    //     $news_entries = array();
    //     $news = NwsNews::model()->getNewsByCategory();
    //     $link = new Link();
    //     foreach ($news as $k => $v) {
    //         $news_entries[] = $this->getSingleUrlEntry(
    //                 $link->getNewsPageLink($v['id_news'], $v['link_rewrite']), 'weekly'
    //         );
    //     }

    //     return $news_entries;
    // }

    // private function getModelEnteries() {
    //     $model_entries = array();
    //     $model = ProductModel::getAll();
    //     $link = new Link();
    //     foreach ($model as $k => $v) {
    //         $model_entries[] = $this->getSingleUrlEntry(
    //                 $link->getModelLink($v['link_rewrite'], $v['domain']), 'weekly'
    //         );
    //     }
    //     return $model_entries;
    // }

  
    private static function entityEscape($string) {
        return str_replace('&', '&amp;', $string);
    }
    

}
