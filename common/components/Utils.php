<?php

namespace common\components;

class Utils {

    const CACHE_EXPIRY_DAY = 86400;
    const CACHE_EXPIRY_DAY_HALF = 43200;
    const CACHE_EXPIRY_HOUR = 3600;

    public static function d($content, $die = false) {

        if (!YII_DEBUG):
            return false;
        endif;

        if (is_array($content) || is_object($content)):
            $content = print_r($content, true);
        endif;
        $isCli = php_sapi_name() == 'cli' ? true : false;
        echo $isCli ? PHP_EOL : "<pre>";
        echo $content;
        echo $isCli ? PHP_EOL : "</pre>";
        if ($die):
            \yii::$app->end();
        endif;
    }
    
//    public static function d($content, $die = false){
//        self::debug($content, $die);
//    }
    /*
    public static function getCreateTableSql($tableName){
        
        $row = \Yii::$app->db->createCommand('SHOW CREATE TABLE ' . $tableName )->queryOne();
        
        if (isset($row['Create Table'])) {
            $sql = $row['Create Table'];
        } else {
            $row = array_values($row);
            $sql = $row[1];
        }

        return $sql;
    }
    
    public static function copyTable($tableName){
        
        $sql = self::getCreateTableSql($tableName);
        
        $connectionLocal = \Yii::$app->dblocal;
        $source = \Yii::$app->db;
        
        // drop table
        $connectionLocal->createCommand('DROP TABLE IF EXISTS '.$tableName)->execute();
        
        // create table
        $connectionLocal->createCommand($sql)->execute();
        
        $rs = $source->createCommand('SELECT * FROM '.$tableName)->queryAll();
        
        $chunks = array_chunk($rs, 10);
        
        foreach($chunks as $records){
            
            $keys = array_keys($records[0]);
            
            $connectionLocal->createCommand()->batchInsert($tableName,$keys, $records)->execute();
        }
        return true;
    }*/
    

}
