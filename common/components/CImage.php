<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Image
 *
 * @author root
 */
namespace common\components;

use Yii;
class CImage extends \common\models\Image
{
    //put your code here
    private static $imagesTypes = array(
            'Product' => array(
                        0 => array('name' => 'small', 'height' => 45, 'width' => 45),
                        1 => array('name' => 'home', 'height' => 115, 'width' => 115),
                        2 => array('name' => 'medium', 'height' => 150, 'width' => 150),
                        3 => array('name' => 'big', 'height' => 200, 'width' => 200),
                        4 => array('name' => 'large', 'height' => 600, 'width' => 600),
                    ),

             'News' => array(
                        0 => array('name' => 'small', 'height' => 200, 'width' => 200),
                        4 => array('name' => 'large', 'height' => 260, 'width' => 260),
                    ),

             'Deal' => array(
                        0 => array('name' => 'tiny', 'height' => 55, 'width' => 35),
                        1 => array('name' => 'small', 'height' => 40, 'width' => 69),
                        2 => array('name' => 'medium', 'height' => 63, 'width' => 150),
                        3 => array('name' => 'home', 'height' => 180, 'width' => 180),
                        4 => array('name' => 'big', 'height' => 98, 'width' => 194),
                        5 => array('name' => 'large', 'height' => 217, 'width' => 242),
                    ),

    );

    public static function getConverImage($id_product)
    {
    }

    public static function copyImg($id_category, $id_entity, $id_image, $url, $variant='Product')
    {
        $url = trim(preg_replace("/[\t\n ]+/", " ", trim($url)));

        $tmpfile = tempnam(Yii::app()->getBasePath() . '../p/tmp', 'original');

        if ($variant == 'Product') {
            $category_path = _PD_IMG_PATH_ . $id_category;
            //$path = _PD_IMG_PATH_ . $id_entity;
            $path = $category_path.'/' . $id_entity;
        } elseif ($variant == 'News') {
            $category_path='';
            $path = _PD_NEWS_IMG_PATH_ . $id_entity;
            $tmpfile = tempnam(Yii::app()->getBasePath() . '../n/tmp', 'original');
        } elseif ($variant == 'Deal') {
            $category_path='';
            $path = Yii::getPathOfAlias('webroot') . '/deal/' . $id_entity . "/";
            $tmpfile = tempnam(Yii::app()->getBasePath() . '../deal/tmp', 'original');
        } else {
            $category_path = _PD_IMG_PATH_ . $id_entity . '/' . $variant;
            //$path = _PD_IMG_PATH_ . $id_entity . '/' . $variant;
            $path = $category_path.'/' . $id_entity . '/' . $variant;
        }

        if ($category_path && !file_exists($category_path))
            //mkdir($category_path);
            mkdir ($category_path, 0770, 1);

        if (!file_exists($path)){
            //$oldmask = umask(022);
            mkdir ($path);            
            //umask($oldmask);
        }
        if (@copy($url, $tmpfile)) {
            //imageResize($tmpfile, $path.'.jpg');
            $isOK = self::removeWhiteSpace($tmpfile, $path . '/' . $id_image . '.jpg');
            if (!$isOK)
                return false;
            $newimage = $path . '/' . $id_image . '.jpg';
            $imagesTypes = self::$imagesTypes;
            foreach ($imagesTypes[$variant] AS $k => $imageType) {
                self::imageResize($newimage, $path . '/' . $id_image . '-' . stripslashes($imageType['name']) . '.jpg', $imageType['width'], $imageType['height']);
            }
        } else {
            unlink($tmpfile);
            return false;
        }
        unlink($tmpfile);
        return true;
    }

    public static function imageResize($sourceFile, $destFile, $destHeight = NULL, $destWidth = NULL, $fileType = 'jpg')
    {
		
        list($sourceWidth, $sourceHeight, $type, $attr) = getimagesize($sourceFile);
        if (!$sourceWidth)
            return false;
        if ($destWidth == NULL)
            $destWidth = $sourceWidth;
        if ($destHeight == NULL)
            $destHeight = $sourceHeight;

        $sourceImage = self::createSrcImage($type, $sourceFile);

        $widthDiff = $destWidth / $sourceWidth;
        $heightDiff = $destHeight / $sourceHeight;

        if ($widthDiff > 1 AND $heightDiff > 1) {
            $nextWidth = $sourceWidth;
            $nextHeight = $sourceHeight;
        } else {

            if ($widthDiff > $heightDiff) {
                $nextHeight = $destHeight;
                $nextWidth = round(($sourceWidth * $nextHeight) / $sourceHeight);
                $destWidth = $destWidth;
            } else {
                $nextWidth = $destWidth;
                $nextHeight = round($sourceHeight * $destWidth / $sourceWidth);
                $destHeight = $destHeight;
            }
        }

        $destImage = imagecreatetruecolor($destWidth, $destHeight);

        $white = imagecolorallocate($destImage, 255, 255, 255);
        imagefill($destImage, 0, 0, $white);
		//echo $destImage.'===>'.$sourceImage; die;
		try{
			//echo 'sourceFile=>'.$sourceFile.'==>'.$destFile.'<br>';
			//echo 'destImage=>'.$destImage.'=>'.$sourceImage;
			imagecopyresampled($destImage, $sourceImage, (int) (($destWidth - $nextWidth) / 2), (int) (($destHeight - $nextHeight) / 2), 0, 0, $nextWidth, $nextHeight, $sourceWidth, $sourceHeight);
			imagecolortransparent($destImage, $white);
			return (self::returnDestImage($fileType, $destImage, $destFile));
		}catch(Exception $e) {
													
					$error = "wwwwwwwwwww".$e->getMessage().'::'.$e->getLine().'::'.$e->getTraceAsString();
					echo 'vinay error=> '.$error;
//					die;
			
		}
        
    }

    public static function createSrcImage($type, $filename)
    {
        switch ($type) {
            case 1:
                return imagecreatefromgif($filename);
                break;
            case 3:
                return imagecreatefrompng($filename);
                break;
            case 6:
                return self::imagecreatefrombmp($filename);
                break;
            case 2:
            default:
                return @imagecreatefromjpeg($filename);
                break;
        }
    }

    public static function createDestImage($width, $height)
    {
        $image = imagecreatetruecolor($width, $height);
        $white = imagecolorallocate($image, 255, 255, 255);
        imagefill($image, 0, 0, $white);
        return $image;
    }

    public static function returnDestImage($type, $ressource, $filename)
    {
        $flag = false;
        switch ($type) {
            case 'gif':
                $flag = imagegif($ressource, $filename);
                break;
            case 'png':
                $flag = imagepng($ressource, $filename, 7);
                break;
            case 'jpeg':
            default:
                $flag = imagejpeg($ressource, $filename, 90);
                break;
        }
        imagedestroy($ressource);
        return $flag;
    }

    public static function removeWhiteSpace($from, $to)
    {
        list($sourceWidth, $sourceHeight, $type, $attr) = getimagesize($from);

        $img = self::createSrcImage($type, $from);
        if (!$img)
            return false;
        //find the size of the borders
        $b_top = 0;
        $b_btm = 0;
        $b_lft = 0;
        $b_rt = 0;

        //top
        for (; $b_top < imagesy($img); ++$b_top) {
            for ($x = 0; $x < imagesx($img); ++$x) {
                if (imagecolorat($img, $x, $b_top) != 0xFFFFFF) {
                    break 2; //out of the 'top' loop
                }
            }
        }

        //bottom
        for (; $b_btm < imagesy($img); ++$b_btm) {
            for ($x = 0; $x < imagesx($img); ++$x) {
                if (imagecolorat($img, $x, imagesy($img) - $b_btm - 1) != 0xFFFFFF) {
                    break 2; //out of the 'bottom' loop
                }
            }
        }

        //left
        for (; $b_lft < imagesx($img); ++$b_lft) {
            for ($y = 0; $y < imagesy($img); ++$y) {
                if (imagecolorat($img, $b_lft, $y) != 0xFFFFFF) {
                    break 2; //out of the 'left' loop
                }
            }
        }

        //right
        for (; $b_rt < imagesx($img); ++$b_rt) {
            for ($y = 0; $y < imagesy($img); ++$y) {
                if (imagecolorat($img, imagesx($img) - $b_rt - 1, $y) != 0xFFFFFF) {
                    break 2; //out of the 'right' loop
                }
            }
        }

        //copy the contents, excluding the border
        $newimg = imagecreatetruecolor(
                imagesx($img) - ($b_lft + $b_rt), imagesy($img) - ($b_top + $b_btm));

        imagecopy($newimg, $img, 0, 0, $b_lft, $b_top, imagesx($newimg), imagesy($newimg));
        imagejpeg($newimg, $to);

        return true;
    }

    public static function imagecreatefrombmp($p_sFile)
    {
        $file = fopen($p_sFile, "rb");
        $read = fread($file, 10);
        while (!feof($file) && ($read <> ""))
            $read .= fread($file, 1024);
        $temp = unpack("H*", $read);
        $hex = $temp[1];
        $header = substr($hex, 0, 108);
        if (substr($header, 0, 4) == "424d") {
            $header_parts = str_split($header, 2);
            $width = hexdec($header_parts[19] . $header_parts[18]);
            $height = hexdec($header_parts[23] . $header_parts[22]);
            unset($header_parts);
        }
        $x = 0;
        $y = 1;
        $image = imagecreatetruecolor($width, $height);
        $body = substr($hex, 108);
        $body_size = (strlen($body) / 2);
        $header_size = ($width * $height);
        $usePadding = ($body_size > ($header_size * 3) + 4);
        for ($i = 0; $i < $body_size; $i+=3) {
            if ($x >= $width) {
                if ($usePadding)
                    $i += $width % 4;
                $x = 0;
                $y++;
                if ($y > $height)
                    break;
            }
            $i_pos = $i * 2;
            $r = hexdec($body[$i_pos + 4] . $body[$i_pos + 5]);
            $g = hexdec($body[$i_pos + 2] . $body[$i_pos + 3]);
            $b = hexdec($body[$i_pos] . $body[$i_pos + 1]);
            $color = imagecolorallocate($image, $r, $g, $b);
            imagesetpixel($image, $x, $height - $y, $color);
            $x++;
        }
        unset($body);
        return $image;
    }

    public static function getImages($id_product, $limit=0)
    {
        $query = 'SELECT product_image.id_image,cover,product_image.name,product_image.id_product,products.categories_category_id FROM product_image
                  inner join products on(product_image.id_product=products.product_id)
                  where product_image.id_product=:product_id';
		
        if ($limit)
            $query.=' LIMIT ' . $limit;

        $command = Yii::$app->db->createCommand($query);
		
        $command->bindParam(':product_id', $id_product);
        $images = $command->queryAll(true);
		//echo '<pre>'; print_r($command); die;

        return $images;
    }

    public static function getImageTypes()
    {
        return self::$imagesTypes;
    }

    public static function deleteImages($id_product, $id_image="",$variant='Product')
    {
        $product = Product::model()->findByPk($id_product);
        $id_category = $product->id_category;
        if ($id_image) {

            $delete_command = Yii::app()->db->createCommand("DELETE FROM product_image WHERE id_image=:id_image AND id_product=:id_product");
            $delete_command->bindParam(':id_product', $id_product);
            $delete_command->bindParam(':id_image', $id_image);
            $delete_command->execute();
            @unlink(_PD_ROOT_DIR_ . "/p/$id_category/$id_product/$id_image.jpg");

            $image_types = self::$imagesTypes;
            foreach ($image_types[$variant] as $imagetype) {
                $image_name = "-".$imagetype['name'];
                if (file_exists(_PD_ROOT_DIR_ . "/p/$id_category/$id_product/$id_image$image_name.jpg"))
                    @unlink(_PD_ROOT_DIR_ . "/p/$id_category/$id_product/$id_image$image_name.jpg");
            }

        } else {
            
            $delete_query = "DELETE FROM product_image WHERE id_product=:id_product";
            $cmd = Yii::app()->db->createCommand($delete_query);
            $cmd->bindParam(":id_product", $id_product);

            if ($cmd->execute()) {
                $physical_path = _PD_ROOT_DIR_ . "/p/$id_category/$id_product";
                $dir_handle = @opendir($physical_path);
                while ($dir = @readdir($dir_handle)) {
                    if ($dir != '..' && $dir != '.' && $dir != '') {
                        @unlink(_PD_ROOT_DIR_ . "/p/$id_category/$id_product" . "/" . $dir);
                    }
                }
                @closedir($dir_handle);
                $dir = _PD_ROOT_DIR_ . "/p/$id_category/$id_product";
                if (file_exists($dir) && is_dir($dir))
                    rmdir($dir);
            }
        }
        
    }

}
