<?php
// make cache to use memcache on production else use filecache

if(YII_ENV_PROD):
    $cache = [
        //'class' => 'yii\caching\FileCache',
    	//'cachePath' => realpath(__DIR__.'/../../frontend/runtime/cache')
	'class' => 'yii\caching\MemCache',
            'servers' => [
                [
                    'host' => 'localhost',
                    'port' => 11211,
                    'weight' => 60,
                ],
            ],
    ];
else:
    $cache = [
        'class' => 'yii\caching\FileCache',
    ];
endif;

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => $cache,
        'elasticsearch' => [
            'class' => 'yii\elasticsearch\Connection',
            'nodes' => [
                ['http_address' => '127.0.0.1:9200'],
            // configure more hosts if you have a cluster
            ],
        ],
        'awssdk' => [
            'class' => 'fedemotta\awssdk\AwsSdk',
            'credentials' => [ //you can use a different method to grant access
                'key' => 'AKIAJRXDJBWKPXAJTEMQ',
                'secret' => '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',
            ],
            'region' => 'ap-south-1', //i.e.: 'us-east-1'
            'version' => 'latest', //i.e.: 'latest
            'extra' => [
                    'queueUrl' => YII_ENV_PROD ? "http://sqs.ap-south-1.amazonaws.com/999711653057/sqs_production_scrape_urls" : "http://sqs.ap-south-1.amazonaws.com/999711653057/sqs_dev_scrape_urls",'queuePriceUrl' => YII_ENV_PROD ? "http://sqs.ap-south-1.amazonaws.com/999711653057/sqs_production_price_scraper_urls" : "http://sqs.ap-south-1.amazonaws.com/999711653057/sqs_dev_price_scraper_urls"]

        ],
        'rabbit' => [
            'class' => 'common\components\Rabbit',
            'host' => '205.147.103.62',
            'amqpPort' => 5672,
            'login' => 'root',
            'pass' => 'nsroot@123',
            'vhost' => '/',
            'receiveQueueName' => 'queue_supplier',
            'receiveWaitTimeout' => '10',
            'sendQueueName' => 'queue_supplier',
            'prefetchCount' => 1,
            'heartbeat' => 30,
            'read_write_timeout' => 60,
            'connection_timeout' => 3,
            'useSSL' => false,
            'autoACK' => false,
        ],
    ],
    'modules' => [
        // ...
        'gii' => [
            'class' => 'yii\gii\Module',
            'generators' => [
                'mongoDbModel' => [
                    'class' => 'yii\mongodb\gii\model\Generator'
                ]
            ],
        ],
        'importeres' => [
            'class' => 'common\modules\importerEs\Module',
        ],
        'searches' => [
            'class' => 'common\modules\searchEs\Module',
        ],
			
	'articles' => [
            'class' => 'common\modules\articles\Articles',
            'userClass' => 'dektrium\user\models\User',

            // Select Languages allowed
            'languages' => [
                "it-IT" => "it-IT",
                "en-GB" => "en-GB"
            ],

            // Select Date Format
            'dateFormat' => 'd F Y',

            // Select Editor: no-editor, ckeditor, imperavi, tinymce, markdown
            'editor' => 'ckeditor',
			'Productpath' => '@backend/web/upload/',
		
            // Select Path To Upload Category Image
            'categoryImagePath' => '@frontend/web/img/articles/categories/',
            // Select URL To Upload Category Image
            'categoryImageURL' => '@web/img/articles/categories/',
            // Select Path To Upload Category Thumb
            'categoryThumbPath' => '@frontend/web/img/articles/categories/thumb/',
            // Select URL To Upload Category Image
            'categoryThumbURL' => '@web/img/articles/categories/thumb/',

            // Select Path To Upload Item Image
            'itemImagePath' => '@frontend/web/img/articles/items/',
            // Select URL To Upload Item Image
            'itemImageURL' => '@web/img/articles/items/',

            'itemUplaodImageURL' => 'img/articles/items/',
            // Select Path To Upload Item Thumb
            'itemThumbPath' => '@frontend/web/img/articles/items/thumb/',
            // Select URL To Upload Item Thumb
            'itemThumbURL' => '@web/img/articles/items/thumb/',

            // Select Path To Upload Attachments
            'attachPath' => '@frontend/web/attachments/',
            // Select URL To Upload Attachment
            'attachURL' => '@web/img/articles/items/',
            // Select Image Types allowed
            'attachType' => 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, .csv, .pdf, text/plain, .jpg, .jpeg, .gif, .png',

            // Select Image Name: categoryname, original, casual
            'imageNameType' => 'casual',
            // Select Image Types allowed
            'imageType' => 'jpg,jpeg,gif,png',
            // Thumbnails Options
            'thumbOptions' => [

                '552x426' => [
                    'quality' => 100,
                    'width' => 552,
                    'height' => 426
                ],

                '264x426' => [
                    'quality' => 100,
                    'width' => 264,
                    'height' => 426
                ],

                '360x207' => [
                    'quality' => 100,
                    'width' => 360,
                    'height' => 207
                ],

                '430x240' => [
                    'quality' => 100,
                    'width' => 430,
                    'height' => 240
                ],

                '57x37' => [
                    'quality' => 100,
                    'width' => 57,
                    'height' => 37
                ],

                '114x85' => [
                    'quality' => 100,
                    'width' => 114,
                    'height' => 85
                ],

                '272x180' => [
                    'quality' => 100,
                    'width' => 272,
                    'height' => 180
                ],

                '700x400' => [
                    'quality' => 100,
                    'width' => 700,
                    'height' => 400
                ],

                'small' => [
                    'quality' => 100,
                    'width' => 150,
                    'height' => 100
                ],
                'medium' => [
                    'quality' => 100,
                    'width' => 200,
                    'height' => 150
                ],
                'large' => [
                    'quality' => 100,
                    'width' => 300,
                    'height' => 250
                ],
                'extra' => [
                    'quality' => 100,
                    'width' => 400,
                    'height' => 350
                ]
            ],

            // Show Titles in the views
            'showTitles' => true,

            // front page url
            'frontPageUrl' => YII_ENV_PROD ? 'http://www.nayashopi.in/' : 'http://front.nayashoppy.com/'
        ],

        // Module Kartik-v Grid
        'gridview' => [
            'class' => '\kartik\grid\Module'
        ],

        // Module Kartik-v Markdown Editor
        'markdown' => [
            'class' => 'kartik\markdown\Module'
        ]
    ]
];
