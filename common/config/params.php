<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
	'backendUrl' => 'http://alkiyobacknew',
	'site-cache' => [
		'ftop_menu' => 'top_menu',
		'fmobile_top_menu' => 'mobile_top_menu',
	],
	
];
