<?php
if (YII_ENV_PROD):
    $db = ['class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=205.147.100.232;dbname=nayashoppy',
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8',];

    $mongodb = [
        'class' => '\yii\mongodb\Connection',
        //'dsn' => 'mongodb://vijay:admin@localhost:27017/nayashoppy',
        'dsn' => 'mongodb://localhost/database?connectTimeoutMS=300000',
        'defaultDatabaseName' => 'nayashoppy', // Avoid autodetection of default database name
    ];
    $mailer = [
        'class' => 'yii\swiftmailer\Mailer',
        'viewPath' => '@common/mail',
        // send all mails to a file by default. You have to set
        // 'useFileTransport' to false and configure a transport
        // for the mailer to send real emails.
        'useFileTransport' => true,
    ];
elseif (YII_ENV_TEST):
    $db = ['class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=localhost;dbname=nayashoppy',
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8',];
    $mongodb = [
        'class' => '\yii\mongodb\Connection',
        //'dsn' => 'mongodb://vijay:admin@localhost:27017/nayashoppy',
        'dsn' => 'mongodb://localhost/database?connectTimeoutMS=300000',
        'defaultDatabaseName' => 'nayashoppy', // Avoid autodetection of default database name
    ];
      $mailer = [
        'class' => 'yii\swiftmailer\Mailer',
        'viewPath' => '@common/mail',
        // send all mails to a file by default. You have to set
        // 'useFileTransport' to false and configure a transport
        // for the mailer to send real emails.
        'useFileTransport' => false,
		
		 'transport' => [
           'class' => 'Swift_SmtpTransport',
			'host' => 'mail.radtechnoservices.com',  
			'username' => 'developer@radtechnoservices.com',
			'password' => 'developer#123',
			'port' => '25',
			'encryption' => 'tls'
			
            ],
    ];
elseif (YII_ENV_DEV):
    $db = ['class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=localhost;dbname=nayashoppy',
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8',];
    $mongodb = [
        'class' => '\yii\mongodb\Connection',
        //'dsn' => 'mongodb://vijay:admin@localhost:27017/nayashoppy',
        'dsn' => 'mongodb://localhost/database?connectTimeoutMS=300000',
        'defaultDatabaseName' => 'nayashoppy', // Avoid autodetection of default database name
    ];
    $mailer = [
        'class' => 'yii\swiftmailer\Mailer',
        'viewPath' => '@common/mail',
        // send all mails to a file by default. You have to set
        // 'useFileTransport' to false and configure a transport
        // for the mailer to send real emails.
        'useFileTransport' => false,
		
		 'transport' => [
           'class' => 'Swift_SmtpTransport',
			'host' => 'mail.radtechnoservices.com',  
			'username' => 'developer@radtechnoservices.com',
			'password' => 'developer#123',
			'port' => '25',
			'encryption' => 'ssl'
            ],
    ];
endif;




return [
    'components' => [
        'db' => $db,
//        'mongodb' => $mongodb,
        'mailer' => $mailer,
    ],
];
