<?php
/**
 * Created by PhpStorm.
 * User: alliancetec.
 * Date: 04/12/17
 * Time: 1:03 PM
 */

namespace api\modules\v1\controllers;


use common\helpers\CorsCustom;
use common\models\Categories;
use common\models\Menus;
use common\models\Products;
use common\models\Suppliers;
use common\modules\searchEs\components\ElasticSearchProducts;
use yii\rest\Controller;
use yii;
use common\models\FeatureGroups;
use common\models\ProductFeature;
use common\models\ProductsSuppliers;
use common\models\ProductReviews;

class ProductController extends Controller
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => yii\filters\ContentNegotiator::className(),
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ],
        ];
        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);
        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => CorsCustom::className(),
        ];
        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    public function actionCategoryWiseList()
    {
        try {
            $categoriesData = [];
            $categories = Categories::find()
                ->where(['show_home_page' => '1'])
                ->orderBy('sort_order ASC')->all();
            foreach ($categories as &$category) {
                $categoryData = $category->attributes;
                $categoryData['products'] = $category->productsForHomePage;
                $categoriesData[] = $categoryData;
            }
            return [
                "status" => true,
                "data" => $categoryData,
                "error" => null
            ];
        } catch (\Exception $exception) {
            return ["status" => false,
                "data" => null,
                "error" => "Somthing went wrong, Please Try again later."];
        }

    }

    public function actionAddreview()
    {
        $request_body = file_get_contents('php://input');
    $data=json_decode($request_body);
     
        try {

           $productReview=new ProductReviews();
           $productReview->product_id=$data->product_id;
           $productReview->title=$data->title;
           $productReview->description=$data->description;
           $productReview->rating=$data->rating;


           
            if ($productReview->save()) {
                return [
                    "status" => true,
                    "data" => "User Review added Successfully!",
                    "error" => null
                ];
            } else {
                $registerErrors = [];
                foreach ($productReview->errors as $errors) {
                    foreach ($errors as $error) {
                        $registerErrors[] = $error;
                    }
                }
                return [
                    "status" => false,
                    "data" => implode("<br>", $registerErrors),
                    "error" => "Please fix errors:"
                ];
            }
        } catch (\Exception $exception) {
            return [
                "status" => false,
                "data" => null,
                "error" => $exception->getMessage(),
            ];
        }


    }

    public function actionDetail($slug = null, $expand = null)
    {
        $_GET['expand'] = $expand;
        if (!is_null($slug)) {
            $product = Products::findOne(['slug' => $slug, 'is_delete' => 0], ['product_status' => 'active']);

            if (!is_null($product)) {
                $featureGroups = FeatureGroups::getFeatureGroupandItemsByCategory($product->categories_category_id, $product->product_id);
                $groupwithfeatures = [];
                $keyFeature = [];
                foreach ($featureGroups as $keys => $valuesList) {
                    $featureHeading = str_replace('&', 'and', str_replace(' ', '-', $valuesList['name']));
                    if (array_key_exists($valuesList['name'], $groupwithfeatures)) {
                        $groupwithfeatures[0][$featureHeading][] = $valuesList;
                    } else {
                        $groupwithfeatures[0][$featureHeading][] = $valuesList;
                    }
                    if ($valuesList['show_feature']) {
                        $keyFeature[] = $valuesList;
                    }
                }

                $similarProducts = Products::getProductsInTwoPriceRanges($product->lowest_price, $product->original_price, $product->categories_category_id);


                $data['product'] = $product;
                $data['featureGroups'] = $groupwithfeatures;
                $data['suppliers'] = $product->suppliersProducts;
                $data['keyfeature'] = $keyFeature;
                $data['similar'] = $similarProducts;

                return [
                    "status" => true,
                    "data" => $data,
                    "error" => null,
                ];
            }
        }
        return ["status" => false,
            "data" => null,
            "error" => "Product not found!"];
    }

    /**
     * @param null $categoryId
     * @param string $sort new|popular
     * @return array
     */
    public function actionCategoryWiseProducts($categoryId = null, $sort = "new", $page = 1)
    {
        if (!is_null($categoryId)) {
            $search = new ElasticSearchProducts();
            $param['term'] = null;
            $param['category'] = [$categoryId];
            $param['sort'] = $sort;
            $param['page'] = $page;
            $param['perpage'] = 5;
            $param['facets'] = true;
            $result = $search->find($param);
            $products = $result['items'];
            $this->setProductImage($products);
            return [
                "status" => true,
                "data" => array_values($products),
                "error" => null
            ];
        }
        return [
            "status" => false,
            "data" => null,
            "error" => "Products not found!"
        ];
    }

    public function actionMenuWiseProducts()
    {
        $request = Yii::$app->request;
        $slug = $request->get('slug', null);
        if (!is_null($slug)) {

            $search = new ElasticSearchProducts();
            $supplierArr = $request->get('store', []);
            $sort = $request->get('sort', 'new');
            $featureIds = $request->get('feature_ids', []);
            $arrBrands = $request->get('brands', []);
            $price = $request->get('price', null);
            $page = $request->get('page', 1);
            $perpage = $request->get('perpage', 10);
            $menuItem = Menus::findOne(['slug' => $slug, 'is_delete' => 0]);

            if (empty($arrBrands)) {
                $brandIdOfMenu = $menuItem->brand_id;
                if (!empty($brandIdOfMenu)) {
                    $arrBrands = [$brandIdOfMenu];
                }
            }

            if ($menuItem->price_min || $menuItem->price_max) {
                $defaultMinPrice = $menuItem->price_min;
                if ($menuItem->price_max) {
                    $defaultMaxPrice = $menuItem->price_max;
                } else {
                    $defaultMaxPrice = 100000;
                }
            } else {
                $defaultMinPrice = 100;
                $defaultMaxPrice = 300000;
            }


            if (!is_null($price)) {
                $priceArr = explode("_", $price);
                if (($priceArr[1] > 0 && $priceArr[0] >= 0) && ($priceArr[0] <= $priceArr[1])) {
                    $param['price_max'] = $priceArr[1];
                    $param['price_min'] = $priceArr[0];
                }
            } else {
                $param['price_min'] = $defaultMinPrice;
                $param['price_max'] = $defaultMaxPrice;
            }


            $param['term'] = null;
            $param['category'] = [$menuItem->category_id];
            $param['brand'] = $arrBrands;
            $param['filter_group_item'] = $featureIds;
            $param['sort'] = $sort;
            $param['page'] = $page;
            $param['perpage'] = $perpage;
            $param['facets'] = true;
            $param['store'] = $supplierArr;

            $result = $search->find($param);
            $products = $result['items'];
            $this->setProductImage($products);
            return [
                "status" => true,
                "data" => array_values($products),
                "error" => null
            ];
        }
        return [
            "status" => false,
            "data" => null,
            "error" => "Products not found!"
        ];
    }

    private function setProductImage(&$products)
    {
        foreach ($products as &$product) {
            $product["product_img"] = \frontend\helpers\NoImage::getProductImageFullPathFromElasticResult($product);
        }
    }

    public function actionSearch()
    {
        return [
            "status" => true,
            "data" => $this->getSearchData(),
            "error" => null
        ];

    }

    private function getSearchData()
    {
        $param = [];
        $search = new ElasticSearchProducts();
        $param['term'] = Yii::$app->request->get('term', null);
        $param['page'] = Yii::$app->request->get('page', 1);
        $param['perpage'] = Yii::$app->request->get('perpage', 20);
        $param['sort'] = 'relevance';

        $result = $search->find($param);
        //$products = Menus::searchData(Yii::$app->request->get('term', null));

        foreach ($result['items'] as $product) {
            $products[] = [
                'url' => \yii\helpers\Url::to(['product/index', 'slug' => $product['slug'], 'category' => $product['categories']['slug']]),
                'icon' => \frontend\helpers\NoImage::getProductImageFullPathFromElasticResult($product),
                'label' => $product['product_name'],
                'value' => $product['product_name'],
                'slug' => $product['slug'],
                'supplierName' => $product['supplier_name'],
                'supplierImage' => $product['supplier_image'],
                'lowest_price' => $product['lowest_price'],
                'original_price' => $product['original_price'],
                "isMenu" => false,
                "isProduct" => true,
            ];
        }

        return $products;
    }

    public function actionCompareSearch()
    {
        return [
            "status" => true,
            "data" => $this->getCompareSearchData(),
            "error" => null
        ];

    }

    private function getCompareSearchData()
    {
        $param = [];
        $categoryArray=[];
        $category=Yii::$app->request->get('category', null);
        if(isset($category) && !empty($category) ){
          
            $categoryArray[]=Yii::$app->request->get('category', null);
            $param['category'] = $categoryArray;
        }
      
        $search = new ElasticSearchProducts();
        $param['term'] = Yii::$app->request->get('term', null);
      
        $param['page'] = Yii::$app->request->get('page', 1);
        $param['perpage'] = Yii::$app->request->get('perpage', 20);
        $param['sort'] = 'relevance';
     

        $result = $search->find($param);
        foreach ($result['items'] as $product) {
            $products[] = [
                'icon' => \frontend\helpers\NoImage::getProductImageFullPathFromElasticResult($product),
                'label' => $product['product_name'],
                'value' => $product['product_name'],
                'slug' => $product['slug'],
                'lowest_price' => $product['lowest_price'],
                'original_price' => $product['original_price'],
                'category' => $product['categories_category_id'],
            ];
        }

        return $products;
    }

    public function actionFilters($slug = null)
    {
        if (!is_null($slug)) {
            $menuItem = Menus::findOne(['slug' => $slug, 'is_delete' => 0]);
            $category = Categories::findOne(['category_id' => $menuItem->category_id], ['status' => 'active']);
            $featureFilters = $this->_getFeautreFilter($slug, $menuItem, $category);
            $sorts = $this->_getSorts();
            return [
                "status" => true,
                "data" => [
                    "filters" => $featureFilters,
                    "sorts" => $sorts,
                ],
                "error" => null
            ];
        }
        return [
            "status" => false,
            "data" => null,
            "error" => "Products not found!"
        ];

    }

    private function _getSorts()
    {
        return [
            [
                "name" => 'New Arrivals',
                "key" => "new",
                "role" => 'destructive',
                "checked" => true,
            ],
            [
                "name" => 'Popularity',
                "key" => "popular",
                "role" => 'destructive',
                "checked" => false,
            ],
            [
                "name" => 'Price - High to Low',
                "key" => "price_high",
                "role" => 'destructive',
                "checked" => false,
            ],
            [
                "name" => 'Price - Low to High',
                "key" => "price_low",
                "role" => 'destructive',
                "checked" => false,
            ],

            [
                "name" => 'Discount',
                "key" => "discount",
                "role" => 'destructive',
                "checked" => false,
            ],
        ];
    }

    private function _getFeautreFilter($slug, $menuItem, $category)
    {
        $search = new ElasticSearchProducts();

        if ($menuItem->price_min || $menuItem->price_max) {

            $defaultMinPrice = $menuItem->price_min;

            if ($menuItem->price_max) {

                $defaultMaxPrice = $menuItem->price_max;

            } else {
                $defaultMaxPrice = 100000;
            }

        } else {

            $defaultMinPrice = 100;
            $defaultMaxPrice = 300000;
        }

        $param['price_min'] = $defaultMinPrice;
        $param['price_max'] = $defaultMaxPrice;
        $param['term'] = null;
        $param['category'] = [$category->category_id]; // numeric array
        $param['brand'] = [];
        $param['filter_group_item'] = [];
        $param['page'] = 1;
        $param['perpage'] = 10;
        $param['sort'] = "new";
        $param['store'] = [];
        $param['facets'] = true;
        $result = $search->find($param);
        $return =
            [
                [
                    "key" => "store",
                    "name" => "Suppliers",
                    "type" => "list",
                    "scroll" => false,
                    "display" => true,
                    'selectType' => "multi",
                    'filterKey' => "store[]",
                    "values" => $this->parseSupplierFilters($result)
                ],
                [
                    "key" => "price",
                    "name" => "Price",
                    "type" => "list",
                    "display" => false,
                    'selectType' => "single",
                    'filterKey' => "price",
                    "values" => $this->getPriceFilters($param['price_min'], $param['price_max'])
                ],
                [
                    "key" => "brand",
                    "name" => "Brands",
                    "type" => "list",
                    "scroll" => true,
                    "display" => false,
                    'selectType' => "multi",
                    'filterKey' => "brands[]",
                    "values" => $this->parseBrandsFilters($result)
                ],
            ];
        return array_merge_recursive($return, $this->parseFeaturesFilters($result));
    }

    private function getPriceFilters($start, $stop, $steps = 10)
    {
        $ranges = [];
        $cmassarray = range($start, $stop, (($start - $stop) / $steps));
        for ($i = 0; $i <= count($cmassarray); $i++) {
            if (isset($cmassarray[$i + 1])) {
                $ranges[] = [
                    "name" => "Rs." . $cmassarray[$i] . " - " . "Rs." . $cmassarray[$i + 1],
                    "filterVal" => $cmassarray[$i] . "_" . $cmassarray[$i + 1],
                    "selected" => false,

                ];
            }
        }
        return $ranges;
    }

    private function parseSupplierFilters($result)
    {
        $suppliers = isset($result['facets']['facet_store']) ? array_values($result['facets']['facet_store']) : [];
        foreach ($suppliers as &$supplier) {
            $supplier['selected'] = false;
            $supplier['filterVal'] = $supplier['key'];

        }
        return $suppliers;
    }

    private function parseBrandsFilters($result)
    {
        $brands = isset($result['facets']['facet_brand_id']) ? array_values($result['facets']['facet_brand_id']) : [];
        foreach ($brands as &$brand) {
            $brand['selected'] = false;
            $brand['filterVal'] = $brand['key'];
        }
        return $brands;
    }

    private function parseFeaturesFilters($result)
    {
        $features = isset($result['facets']['filters']) ? $result['facets']['filters'] : [];
        $featuresData = [];
        usort($features, "self::CollectionSortForGroupItem");
        foreach ($features as $ky => $val) {
            usort($val[key($val)]['values'], "self::CollectionSortForItem");
            $features[$ky][key($val)]['values'] = $val[key($val)]['values'];
            foreach ($features[$ky][key($val)]['values'] as &$filterValue) {
                $filterValue['filterVal'] = $features[$ky][key($val)]["key"] . "_" . $filterValue["key"];
            }
            $feature = $features[$ky][key($val)];
            $feature['type'] = "list";
            $feature['scroll'] = false;
            $feature['display'] = false;
            $feature['selected'] = false;
            $feature['selectType'] = "multi";
            $feature['filterKey'] = "feature_ids[]";
            $featuresData[] = $feature;

        }
        return array_values($featuresData);
    }

    public static function CollectionSortForGroupItem($a, $b)
    {
        if ($a[key($a)]['sort'] == $b[key($b)]['sort']) {
            return 0;
        }
        return ($a[key($a)]['sort'] < $b[key($b)]['sort']) ? -1 : 1;
    }

    public static function CollectionSortForItem($a, $b)
    {


        if ($a['sort'] == $b['sort']) {
            return 0;
        }
        return ($a['sort'] < $b['sort']) ? -1 : 1;
    }


    public function actionCompare()
    {
        try {
            $request = Yii::$app->request;
            $products = $request->get('products', []);
            if (!empty($products)) {
                return $this->compareProducts($products);
            } else {
                return [
                    "status" => false,
                    "data" => null,
                    "error" => "Select atleat two products!"
                ];
            }
        } catch (\Exception $exception) {
            return $exception->getMessage() . $exception->getTraceAsString();
            return [
                "status" => false,
                "data" => null,
                "error" => "Somthing went wrong, Please Try again later."
            ];
        }
    }

    public function compareProducts(array $productSlugs)
    {
        $comparasionProduct = Products::find()->where(['in', 'slug', $productSlugs, 'product_status' => 'active'])->all();
        if (empty($comparasionProduct) || count($comparasionProduct) < 2) {
            return [
                "status" => false,
                "data" => null,
                "error" => "Select atleat two products!"
            ];
        } else if ($comparasionProduct[0]->categories_category_id != $comparasionProduct[1]->categories_category_id) {
            return [
                "status" => false,
                "data" => null,
                "error" => "Select products within category!"
            ];
        } else {
            return [
                "status" => true,
                "data" => $this->getFeatureGroupData($comparasionProduct[0]->categories_category_id, $comparasionProduct),
                "error" => ""
            ];
        }
    }

    private function getFeatureGroupData($categoryId, $products)
    {
        $featureGroupData = [];
        $productFeatures = [];
        $supplier=[];
        $supplierByStore=[];
        foreach ($products as $product) {
          
            $supplier[]= $product->suppliersProducts;
            $productFeatures[$product->slug] = ProductFeature::getProductFeatureValues($product->product_id);
        }

        foreach($supplier as $k => $invidualProduct){
            foreach($invidualProduct as $key => $value){
               
                    $supplierByStore[$value->store_name][$k]=$value;
               

            }
        }



        $featureGroups = FeatureGroups::getFeatureAttributeByCategory($categoryId);
        foreach ($featureGroups as $featureGroup) {
            $featureGroupDataTemp = $featureGroup->attributes;
            $featureGroupDataTemp['features'] = [];
            foreach ($featureGroup->features as $featureGroupFeature) {
                $featureGroupDataTemp['features'][] = $featureGroupFeature->attributes;
            }
            $featureGroupData[] = $featureGroupDataTemp;
        }
        return ['comparasionProducts' => $products, 'productFeatureGroup' => $featureGroupData, 'productsFeatures' => $productFeatures,'supplier'=>$supplierByStore];
    }
}