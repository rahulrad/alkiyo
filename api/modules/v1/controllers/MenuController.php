<?php
/**
 * Created by PhpStorm.
 * User: alliancetec.
 * Date: 04/12/17
 * Time: 12:28 PM
 */

namespace api\modules\v1\controllers;


use common\helpers\CorsCustom;
use common\models\Menus;
use common\models\Sliders;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;

class MenuController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ],
        ];
        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);
        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => CorsCustom::className(),
        ];
        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    public function actionSliderData()
    {
        try {
            return [
                "status" => true,
                "data" => Sliders::getHomePageSliderImages(1),
                "error" => null
            ];
        } catch (\Exception $exception) {
            return [
                "status" => false,
                "data" => null,
                "error" => "Somthing went wrong, Please Try again later."
            ];
        }
    }

    /**
     * @return array
     */
    public function actionList()
    {
        try {
            return [
                "status" => true,
                "data" => $this->getMenuList(),
                "error" => null
            ];
        } catch (\Exception $exception) {
            return [
                "status" => false,
                "data" => null,
                "error" => "Somthing went wrong, Please Try again later."
            ];
        }
    }

    private function getMenuList()
    {
        $menus = Menus::getTopMenu();

        print_r( $menus );
        $menusData = [];
        foreach ($menus as $menu) {
            $concatString = "";
            $subMenus = $menu->getChildMenus('sub_menu', $menu->id);
            $subMenusData = [];

            foreach ($subMenus  as $index => $subMenu) {

                $customMenus = $subMenu->getChildMenus('custom_menu', $subMenu->id);
                $customMenusData = [];
               

                foreach ($customMenus as $key => $customMenu) {
                    
                    if ($index < 2 && $key <2) {
                        $concatString .= $customMenu->title . ", ";
                    }
                    $customMenusData[] = [
                        "id" => $customMenu->id,
                        "title" => $customMenu->title,
                        "slug" => $customMenu->slug,
                    ];
                }

                $subMenusData[] = [
                    "id" => $subMenu->id,
                    "title" => $subMenu->title,
                    "slug" => $subMenu->slug,
                    "children" => $customMenusData
                ];
            }

            $menusData[] = [
                "id" => $menu->id,
                "title" => $menu->title,
                "slug" => $menu->slug,
                "children" => $subMenusData,
                "concat" => $concatString,
             "category" => $menu->category_id,
            ];
        }
        return $menusData;
    }

}