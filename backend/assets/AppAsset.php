<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'bootstrap/css/bootstrap.min.css',
		'//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css',
		'//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
		'dist/css/AdminLTE.min.css',
		'dist/css/skins/_all-skins.min.css',
		'plugins/iCheck/square/blue.css',
		'css/site.css',
        'css/jquery.mCustomScrollbar.css',
		'css/jquery-ui.css',
    ];
    public $js = [
		'bootstrap/js/bootstrap.min.js',
		'//code.jquery.com/ui/1.11.4/jquery-ui.min.js',
		'cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',
		'plugins/sparkline/jquery.sparkline.min.js',
		'plugins/slimScroll/jquery.slimscroll.min.js',
		'plugins/fastclick/fastclick.min.js',
		'dist/js/app.min.js',
		//'dist/js/pages/dashboard.js',
		'dist/js/demo.js',	
		//'css/jquery.2.1.3.min.js',
		'css/admin_development.js',
                'css/jquery.mCustomScrollbar.js',
                //'css/jquery-2.2.3.min.js',
		//'css/angular.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
