<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use common\models\Brands;
use common\models\Glimpses;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\Brands */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="categories-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	<?php
		echo $form->field($model, 'brand_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Brands::find()->andWhere(['status'=>'active'])->all(),'brand_id','brand_name'),
			'language' => 'eg',
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	 <?php echo  $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	<?php  echo   $form->field($model, 'description')->textarea(['rows' => 6]) ?>
	 <?php echo  $form->field($model,'file')->fileInput(); ?>
			<?php if ($model->image): ?>
	
        <div class="form-group">
		<?php 
				//$path = str_replace('admin','frontend/web/',Yii::$app->getUrlManager()->getBaseUrl());
				$path = Yii::getAlias('@frontends') .'/';
				//$path = "/";
			?>
			<?php echo Html::img($path.$model->image,['class'=>'img-responsive  img-thumbnail small-thumbnail','alt'=>'Glimpses Image']); ?>
           
        </div>
		
    <?php endif; ?>
	<?php echo  $form->field($model, 'active')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>
    <div class="form-group">
        <?php echo  Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
