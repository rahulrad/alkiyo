<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo  $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo  $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    
	<?php echo $form->field($model, 'descritption')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'standard'
    ]) ?>

    <?php echo  $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

    <?php echo  $form->field($model, 'meta_keyword')->textarea(['rows' => 6]) ?>

    <?php echo  $form->field($model, 'meta_desc')->textarea(['rows' => 6]) ?>

   <?php echo  $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>

    
    <div class="form-group">
        <?php echo  Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
