<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MongoProductFeaturesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mongo-product-features-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, '_id') ?>

    <?= $form->field($model, 'id_product_feature') ?>

    <?= $form->field($model, 'id_product') ?>

    <?= $form->field($model, 'id_feature') ?>

    <?= $form->field($model, 'id_feature_value') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
