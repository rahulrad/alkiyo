<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MongoProductFeatures */

$this->title = 'Update Mongo Product Features: ' . ' ' . $model->_id;
$this->params['breadcrumbs'][] = ['label' => 'Mongo Product Features', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->_id, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mongo-product-features-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
