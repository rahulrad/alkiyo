<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MongoProductFeatures */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mongo-product-features-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_product_feature') ?>

    <?= $form->field($model, 'id_product') ?>

    <?= $form->field($model, 'id_feature') ?>

    <?= $form->field($model, 'id_feature_value') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
