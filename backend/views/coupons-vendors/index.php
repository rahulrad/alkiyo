<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Coupons Vendors';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content">
    
    <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">

   
                    </div><!-- /.box-header -->
                <div class="box-body">

<div class="coupons-vendors-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Coupons Vendors', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'image',
  //          'image_name',
//            'description:ntext',
            // 'meta_title',
            // 'meta_keyword',
            // 'meta_descrption:ntext',
            // 'show_home',
            // 'status',
            // 'is_delete',
            // 'created',

            ['class' => 'yii\grid\ActionColumn',
			'template' => ' {view}  &nbsp;{update}&nbsp;  {delete}',
			 'buttons' => [
            	'view' => function ($url, $model) {
                   $url =  "https://www.nayashopi.in/".$model->slug.'.html';
                 return Html::a('<span class="fa fa-eye"></span>', $url, ['title' => 'view', 'target'=>'_blank']);
                          }
                        ]
            
        ],
        ],
    ]); ?>
</div>

    </div>
    </div>
    </div>

</div>
</section>
