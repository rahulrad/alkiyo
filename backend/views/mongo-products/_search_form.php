

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Brands;
use backend\models\MongoProducts;
use common\models\Categories;
use common\models\Suppliers;


/* @var $this yii\web\View */
/* @var $model backend\models\Products */

$this->title = 'Mongo Products Search';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
 

<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>

<section class="content">	
	
	  <div class="box box-info">
			 <div class="box-body">	
				
				
					
				<?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
	
	<?php 
			$category_name = '';
			$brand_name = '';
			$product_name = '';
			$created = '';
            $store_id = '';
			$rating = '';
			$discount = '';
			$auto_brand_name = '';
			$auto_product_name = '';
			if(isset($_GET['MongoProductsSearch'])){
				$category_name = $_GET['MongoProductsSearch']['category_name'];
				$brand_name = !empty($_GET['auto_brand_name']) ? $_GET['MongoProductsSearch']['brand_name'] : '';
				$auto_brand_name = $_GET['auto_brand_name'];
				//$product_name = $_GET['MongoProductsSearch']['product_name'];
				$product_name = !empty($_GET['auto_brand_name']) ? $_GET['MongoProductsSearch']['product_name'] : '';
				$auto_product_name = $_GET['auto_product_name'];
				$created = $_GET['MongoProductsSearch']['created'];
                $store_id = $_GET['MongoProductsSearch']['store_id'];
				$rating = $_GET['MongoProductsSearch']['rating'];
				$discount = $_GET['MongoProductsSearch']['discount'];
			}
	?>
	<div class="searchblock">
	
	
	<div class="col-sm-2 col-lg-2 col-md-2  col-xs-12">
	<?php
		// Normal select with ActiveForm & model
        $model->store_id = $store_id;
		echo $form->field($model, 'store_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Suppliers::find()->andWhere(['!=', 'is_delete' ,1])->all(),'id','name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Store','id' => 'store_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	</div>
	 <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12">
<?php

		
		$model->category_name = $category_name;
		// Normal select with ActiveForm & model
		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryListsName();
		echo $form->field($model, 'category_name')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category','id' => 'category_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
			
		
	?>
	</div>
	 <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12">
	 <label> Brand </label>
	<input type="text" name="auto_brand_name" value="<?php echo $auto_brand_name; ?>" class="form-control searchAutoComplete" model_name="Brands" field_name="brand_name" id_value="mongoproductssearch-brand_name">
	<?php echo $form->field($model, 'brand_name')->hiddenInput(['value'=>$brand_name])->label(false) ?>
<?php
		// Normal select with ActiveForm & model
		/**$model->brand_name = $brand_name;
		echo $form->field($model, 'brand_name')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Brands::find()->where(['status'=>'active'])->andWhere(['!=', 'is_delete' ,1])->all(),'brand_name','brand_name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Brand'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]); **/
	?>
	</div>
	
	<div class="col-sm-2 col-lg-2 col-md-2  col-xs-12">
	<?php
		// Normal select with ActiveForm & model
		/**$productRatings = MongoProducts::find()->select(['rating'])->orderBy(['rating' => SORT_DESC])->all();
		
		$ratings = array();
		if(!empty($productRatings)){
			foreach($productRatings as $productRating){
				if($productRating->rating != ''){
					//$rate = number_format($productRating->rating, 2, '.', '');
					$rate = $productRating->rating;
					$ratings[$productRating->rating] = $productRating->rating;
				}
				
			}
		} **/
		$ratings = array('5'=>'5','4'=>'4','3'=>'3','2'=>'2','1'=>'1');
	
		$model->rating = $rating;
		echo $form->field($model, 'rating')->widget(Select2::classname(), [
			'data' => $ratings,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Rating'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	</div>
	
	<div class="col-sm-2 col-lg-2 col-md-2  col-xs-12">
	<?php
		// Normal select with ActiveForm & model
		/**$productDiscounts = MongoProducts::find()->select(['discount'])->orderBy(['discount' => SORT_DESC])->all();
		$discounts = array();
		if(!empty($productDiscounts)){
			foreach($productDiscounts as $productDiscount){
				if($productDiscount->discount != ''){
					$discounts["$productDiscount->discount"] = $productDiscount->discount;
				}
				
			}
			
		} **/
		
		$discounts = array('90-100' => '90-100','80-90'=>'80-90','70-80'=>'70-80','60-70'=>'60-70','50-60'=>'50-60','40-50'=>'40-50','30-40'=>'30-40','20-30'=>'20-30','10-20'=>'10-20','1-10'=>'1-10');
		$model->discount = $discount;
		echo $form->field($model, 'discount')->widget(Select2::classname(), [
			'data' => $discounts,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Discount'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	</div>

	 <?php /**<div class="col-sm-3 col-lg-3 col-md-3  col-xs-12">

<?php 
$model->created = $created;
echo  $form->field($model, 'created')->textInput(['id' => 'start_date']) ?>
	</div>  */?>
	
	
	
	<div class="col-sm-8 col-lg-8 col-md-8 col-xs-12">
	 <label> Product </label>
	<input type="text" name="auto_product_name" value="<?php echo $auto_product_name; ?>" class="form-control searchAutoComplete" model_name="MongoProducts" field_name="product_name" id_value="mongoproductssearch-product_name">
	<?php echo $form->field($model, 'product_name')->hiddenInput(['value'=>$product_name])->label(false) ?>
<?php
		// Normal select with ActiveForm & model
		/**$model->product_name = $product_name;
		echo $form->field($model, 'product_name')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(MongoProducts::find()->where(['status'=>1])->all(),'product_name','product_name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Product'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]); **/
	?>
	</div>
	 <div class="col-sm-2 col-lg-2 col-md-2  col-xs-12">

		<?php 
		$model->created = $created;
		echo  $form->field($model, 'created')->textInput(['id' => 'start_date']) 
		?>
	</div>
	 <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12">
<div class="form-group" style="margin-top:24px;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-block searchSubmitButton']) ?>
    </div></div>
	</div>
	<?php ActiveForm::end() ?>
	
    <div class="clearfix"></div>
			</div>
			 <div class="box-footer">&nbsp;</div>
	</div>

</section>
