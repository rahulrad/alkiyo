<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\MongoProducts */

$this->title = $model->_id;
$this->params['breadcrumbs'][] = ['label' => 'Mongo Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mongo-products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => (string)$model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            '_id',
            'store_name',
            'brand_name',
            'product_name',
            'product_description',
            'model_number',
            'cod',
            'return_policy',
            'delivery',
            'price',
            'url',
            'image',
            'other_images',
            'emi',
            'unique_id',
            'status',
        ],
    ]) ?>

</div>
