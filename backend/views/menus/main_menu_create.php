<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

use yii\widgets\ActiveForm;
use backend\models\MenuTypes;
use backend\models\Categories;
use backend\models\Brands;
use backend\models\Menus;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model backend\models\Menus */

$this->title = 'Create Main Menu';
$this->params['breadcrumbs'][] = ['label' => 'Main Menu', 'url' => ['main_menu']];
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>

<section class="content">	
	
	  <div class="box box-info">
			 <div class="box-body">	
		<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

   
	<?php
		// Normal select with ActiveForm & model
		$model->type = 2;
		echo $form->field($model, 'type')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(MenuTypes::find()->where(['status'=>'active'])->andWhere(['!=', 'is_delete' ,1])->all(),'id','title'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Menu Type'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	
	<?php echo  $form->field($model, 'file')->fileInput(); ?>
	
	<?php if ($model->image): ?>
	
        <div class="form-group">
		<?php $path = str_replace('admin','frontend/web/',Yii::$app->getUrlManager()->getBaseUrl());?>
			<?php echo Html::img($path.$model->image,['class'=>'img-responsive  img-thumbnail small-thumbnail','alt'=>'User Image']); ?>
           
        </div>
		
    <?php endif; ?>

    
	<?php echo   $form->field($model, 'api_icon_file')->fileInput(); ?>
	
	
	<?php echo $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>
	
	 <?php echo  $form->field($model, 'meta_title')->textarea(['rows' => 1]) ?>
	 
	 <?php echo  $form->field($model, 'meta_keyword')->textarea(['rows' => 2]) ?>
	 
	 <?php echo  $form->field($model, 'meta_desc')->textarea(['rows' => 2]) ?>
	 
	  
	  <?php echo $form->field($model, 'menu_type')->hiddenInput(['value'=> 'main_menu'])->label(false); ?>

     <?php echo $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>
	 
	 <?php echo $form->field($model, 'image_name')->textInput() ?>
	 
	 <?php echo $form->field($model, 'show_on_app')->checkBox(['label' => 'show on app', 'uncheck' => null, 'checked' => 'checked']);  ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	
	<!--<div class="add_more_filter_list row"></div>-->
	
	<input type="hidden" id="menu_id" value="<?php echo $model->id ?>" />
	<input type="hidden" id="model_name" value="menus" />

    <?php ActiveForm::end(); ?>
			</div>
			 <div class="box-footer">&nbsp;</div>
	</div>

</section>