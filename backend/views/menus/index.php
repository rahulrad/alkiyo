<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MenusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menus';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>
<section class="content">
    
	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
				  <p><?php echo  Html::a('Create Menus', ['create'], ['class' => 'btn btn-success','style' => 'float:right']) ?></p>
                </div><!-- /.box-header -->
                <div class="box-body">
     <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'title',
            'slug',
           // 'category_id',
            //'parent_id',
			 [
				'attribute'=>'category_id',
				'value'=>'categoriesCategory.category_name',
			   ],
			    [
				'attribute'=>'brand_id',
				'value'=>'brandsBrand.brand_name',
			   ],
			  
			[
				'attribute' => 'parent_id',
				
				'label' => 'Parent Category',
				
				'format' => 'text',
				
				'content'=> function($data){
				
					return $data->getParentName();
				
				}
			],
			 [
				'attribute'=>'type',
				'value'=>'menuTypeTitle.title',
			   ],
            // 'type',
            // 'others:ntext',
             'status',
             //'created',

            [
			'class' => 'yii\grid\ActionColumn',
			'template' => '{update} {delete}',
			],
        ],
    ]); ?>
	</div>
	</div>
	</div>

</div>
</section>
