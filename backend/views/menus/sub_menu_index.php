<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Menus;
use backend\models\MenuTypes;
use common\models\Categories;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MenusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Menus';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>
<section class="content">
    
	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
				  <p><?php echo  Html::a('Create Sub Menus', ['sub_menu_create'], ['class' => 'btn btn-success','style' => 'float:right']) ?></p>
                </div><!-- /.box-header -->
				
				
				
				<?php $form = ActiveForm::begin([
        'action' => ['sub_menu'],
        'method' => 'get',
    ]); ?>
	
	<?php 
			$menu_type = '';
			$menu_id = '';
			$created = '';
			$parent_id = '';
			$category_id = '';
			if(isset($_GET['MenusSearch'])){
				$menu_type = $_GET['MenusSearch']['type'];
				$menu_id = $_GET['MenusSearch']['id'];
				$created = $_GET['MenusSearch']['created'];
				$parent_id = $_GET['MenusSearch']['parent_id'];
				$category_id = $_GET['MenusSearch']['category_id'];
			}
	?>
	<div class="searchblock">
	<div class="col-sm-2 col-lg-2 col-md-2  col-xs-12">
	<?php
		// Normal select with ActiveForm & model
		$model->type = $menu_type;
		echo $form->field($model, 'type')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(MenuTypes::find()->andWhere(['!=', 'is_delete' ,1])->all(),'id','title'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Menu Type'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	</div>
	
	<div class="col-sm-3 col-lg-3 col-md-3  col-xs-12">
	
	<?php
		// Normal select with ActiveForm & model
		$model->parent_id = $parent_id;
		echo $form->field($model, 'parent_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Menus::find()->where(['parent_id'=> 0])->andWhere(['!=', 'is_delete' ,1])->all(),'id','title'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Parent Menu'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	</div>
	<div class="col-sm-4 col-lg-4 col-md-4 col-xs-12">
<?php

		
		$model->category_id = $category_id;
		// Normal select with ActiveForm & model
		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryLists();
		echo $form->field($model, 'category_id')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
			
		
	?>
	</div>
	 <div class="col-sm-3 col-lg-3 col-md-3  col-xs-12">
	
	<?php
		// Normal select with ActiveForm & model
		$model->id = $menu_id;
		echo $form->field($model, 'id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Menus::find()->where(['menu_type'=> 'sub_menu'])->andWhere(['!=', 'is_delete' ,1])->all(),'id','title'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Menu Title'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	</div>
	 
	 <div class="col-sm-2 col-lg-2 col-md-2  col-xs-12">

<?php 
$model->created = $created;
echo  $form->field($model, 'created')->textInput(['id' => 'start_date']) ?>
	</div>
	
	 <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12">
<div class="form-group" style="margin-top:24px;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-block']) ?>
    </div></div>
	</div>
	<?php ActiveForm::end() ?>
    <div class="clearfix"></div>
	
	
                <div class="box-body">
     <?= GridView::widget([
        'dataProvider' => $dataProvider,
      //  'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'title',
            'slug',
           // 'category_id',
            //'parent_id',
			 [
				'attribute'=>'category_id',
				'value'=>'categoriesCategory.category_name',
			   ],
			    [
				'attribute'=>'brand_id',
				'value'=>'brandsBrand.brand_name',
			   ],
			  
			[
				'attribute' => 'parent_id',
				
				'label' => 'Parent Category',
				
				'format' => 'text',
				
				'content'=> function($data){
				
					return $data->getParentName();
				
				}
			],
			/* [
				'attribute'=>'type',
				'value'=>'menuTypeTitle.title',
			   ], */
            // 'type',
            // 'others:ntext',
				'sort_order',
             'status',
			  [
			 	//'label'=>'sort up',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     return Html::a('<i class="fa fa-arrow-up" aria-hidden="true"></i>', Yii::$app->homeUrl.'menus/sort_order_update?type=up&id=' . $dataProvider->id);
                 },
             ],
			 [
			 	//'label'=>'sort down',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     return Html::a('<i class="fa fa-arrow-down" aria-hidden="true"></i>', Yii::$app->homeUrl.'menus/sort_order_update?type=down&id=' . $dataProvider->id);
                 },
             ],
             //'created',
			 [
				'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->homeUrl.'menus/sub_menu_update?id=' . $dataProvider->id);
                 },
            ],

           
            [
			'class' => 'yii\grid\ActionColumn',
			'template' => ' {delete}',
			],
			
        ],
    ]); ?>
	</div>
	</div>
	</div>

</div>
</section>
