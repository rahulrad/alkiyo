<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

use yii\widgets\ActiveForm;
use backend\models\MenuTypes;
use common\models\Categories;
use common\models\Brands;
use common\models\Menus;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;
use yii\web\JsExpression;
use jlorente\remainingcharacters\RemainingCharacters;

/* @var $this yii\web\View */
/* @var $model backend\models\Menus */

$this->title = 'Update Menus: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Menus', 'url' => ['custom_menu']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>

<section class="content">	
	
	  <div class="box box-info">
			 <div class="box-body">	
			<div class="menus-form">
		<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?php //echo $form->field($model, 'category_id')->textInput() ?>
    
    <?php //echo $form->field($model, 'parent_id')->textInput() ?>
	<?php
		// Normal select with ActiveForm & model
		$parentMenus = Menus::find()->select(['id','title','parent_id'])->where(['status'=>'active','menu_type'=>'sub_menu'])->andWhere(['!=', 'is_delete' ,1])->all();
		$menus = array();
		if(!empty($parentMenus)){
			foreach($parentMenus as $parentMenu){
				$menuParent = Menus::find()->select(['title'])->where(['id'=>$parentMenu->parent_id])->andWhere(['!=', 'is_delete' ,1])->one();
				$menus[$parentMenu->id] = $parentMenu->title.' ( '.$menuParent->title.' )';
			}
		}
		
		// Normal select with ActiveForm & model
		echo $form->field($model, 'parent_id')->widget(Select2::classname(), [
			'data' => $menus,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Parent'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	
                            
	<?php
		// Normal select with ActiveForm & model
		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryLists();
		echo $form->field($model, 'category_id')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	
	<?php
		// Normal select with ActiveForm & model
		echo $form->field($model, 'brand_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Brands::find()->where(['status'=>'active'])->andWhere(['!=', 'is_delete' ,1])->all(),'brand_id','brand_name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Brand'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>

	<?php
		// Normal select with ActiveForm & model
		echo $form->field($model, 'products')->widget(Select2::classname(), [
            'initValueText' => $menuProducts,
			'language' => 'eg',
            'options' => [
                'placeholder' => 'Select Products',
                'multiple' => true
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                ],
                'ajax' => [
                    'url' => "/products/menu-list",
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {
                                                            q:params.term,
                                                            brandId:$("#menus-brand_id").val(),
                                                            categoryId:$("#menus-category_id").val(),
                                            }; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
		]);
	?>

     <?php echo $form->field($model, 'price_min')->textInput() ?>

     <?php echo $form->field($model, 'price_max')->textInput() ?>

	<?php echo  $form->field($model, 'file')->fileInput(); ?>

    <?php
    echo $form->field($model, 'banner_image_file')->widget(\kartik\file\FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
        ],
        'pluginOptions' => [
            'initialPreview' => [
                $model->getPreviewImage()
            ],
            'initialPreviewAsData' => true,
            'initialCaption' => $model->title,
            'initialPreviewConfig' => [
                [
                    'caption' => $model->title,
                    'key' => $model->id
                ]
            ],
            'overwriteInitial' => false,
            'maxFileSize' => 4000,
            'deleteUrl' => \yii\helpers\Url::to(['/menus/delete-banner-image']),
        ]
    ]); ?>
	
	<?php if ($model->image): ?>
	
        <div class="form-group">
		<?php
				//$path = str_replace('admin','frontend/web/',Yii::$app->getUrlManager()->getBaseUrl());
				$path = '';
				 $module = Yii::$app->getModule('articles');
				 $path= $module->frontPageUrl;
			?>
			<?php echo Html::img($path.$model->image,['class'=>'img-responsive  img-thumbnail small-thumbnail','alt'=>'Custom Menu Image']); ?>
           
        </div>
		
    <?php endif; ?>

    <?php //echo $form->field($model, 'type')->textInput() ?>
	<?php
		// Normal select with ActiveForm & model
		/**echo $form->field($model, 'type')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(MenuTypes::find()->where(['status'=>'active'])->all(),'id','title'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Menu Type'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]); **/
	?>

    <?php //echo $form->field($model, 'others')->textarea(['rows' => 6]) ?>
	
	<!--<a class="btn btn-success" id="AddMoreMenuFilters">Add Menu Filters</a>-->
	
	<a class="btn btn-success" id="showFilters">Show Filters</a>
	<div id="filtersListinMenu" style="display:none">
		
		<div id="featuresByCategoryId" class="row"></div>
	
	</div>	
	<!--<div id="AddMoreMenuFiltersList"></div>-->
	
<?php echo $form->field($model, 'short_description')->widget(CKEditor::className(), [
    
'clientOptions' => [
 'filebrowserUploadUrl' => \yii\helpers\Url::to(['/coupons/upload-file']),

                                                'toolbarGroups' => [
                                                    ['name' => 'undo'],
                                                    ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                                                    ['name' => 'colors'],
                                                    ['name' => 'links', 'groups' => ['links', 'insert']],
                                                    ['name' => 'others', 'groups' => ['others', 'about']],
                                                    ['name' => 'styles'],
                                                    ['name' => 'document', 'groups' => ['mode', 'document']],
                                                    ['name' => 'videodetector'] // <--- OUR NEW PLUGIN YAY!
                                                ]],
                                            'options' => ['rows' => 6],
                                            'preset' => 'custom'





]) ?>

	<?php echo $form->field($model, 'description')->widget(CKEditor::className(), [
    
'clientOptions' => [
 'filebrowserUploadUrl' => \yii\helpers\Url::to(['/coupons/upload-file']),

                                                'toolbarGroups' => [
                                                    ['name' => 'undo'],
                                                    ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                                                    ['name' => 'colors'],
                                                    ['name' => 'links', 'groups' => ['links', 'insert']],
                                                    ['name' => 'others', 'groups' => ['others', 'about']],
                                                    ['name' => 'styles'],
                                                    ['name' => 'document', 'groups' => ['mode', 'document']],
                                                    ['name' => 'videodetector'] // <--- OUR NEW PLUGIN YAY!
                                                ]],
                                            'options' => ['rows' => 6],
                                            'preset' => 'custom'




]) ?>
	
	 <?php echo  $form->field($model, 'meta_title')->textarea(['rows' => 1,'maxlength'=>70]) ?>
	 
	 <?php echo  $form->field($model, 'meta_keyword')->textarea(['rows' => 2]) ?>
	 
	 <?php  

echo $form->field($model, 'meta_desc')->widget(RemainingCharacters::classname(), [
    'type' => RemainingCharacters::INPUT_TEXTAREA,
    'text' => Yii::t('app', '{n} characters remaining'),
    'label' => [
        'tag' => 'p',
        'id' => 'my-counter',
        'class' => 'counter',
        'invalidClass' => 'error'
    ],
    'options' => [
        'rows' => '3',
        'class' => 'col-md-12',
        'maxlength' => 320,
        'placeholder' => Yii::t('app', 'Write something')
    ]
]);






?>

     <?php echo $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>
	 
	 <?php echo $form->field($model, 'show_on_app')->checkBox(['label' => 'show on app', 'uncheck' => null, 'checked' => 'checked']);  ?>
	 <?php echo $form->field($model, 'field_index')->checkBox(['label' => 'Index']);  ?>
	 <?php echo $form->field($model, 'show_on_mega_menu')->checkBox(['label' => 'Show on Mega Menu']);  ?>


 <?php echo $form->field($model, 'is_landing_page')->checkBox(['label' => 'Is Landing Page']);  ?>

<?php echo $form->field($model, 'is_popular')->checkBox(['label' => 'Show to Popular']);  ?>
    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	
	<!--<div class="add_more_filter_list row"></div>-->
	
	<input type="hidden" id="menu_id" value="<?php echo $model->id ?>" />
	<input type="hidden" id="model_name" value="menus" />

    <?php ActiveForm::end(); ?>
    

</div>
	</div>
	</div>
	</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
	
	$(document).ready(function(){
		
	
		
		
		/*$('#AddMoreMenuFilters').click(function(){
					
					$('#AddMoreMenuFiltersList').append('<div class="add_more_filter_list row"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 "><label for="menus-status" class="control-label">Feature</label><select name="Menus[status]" class="form-control" id="menus-status"><option value="">-Select Feature-</option><?php if(!empty($features)){ foreach($features as $feature){ ?><option value="<?php echo $feature->id; ?>"><?php echo $feature->name; ?></option><?php } } ?></select><div class="help-block"></div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 "><label for="menus-status" class="control-label">Feature Value</label><select name="Menus[status]" class="form-control" id="menus-status"><option value="">-Select Feature Value-</option><option selected="" value="active">Active</option><option value="inactive">Inactive</option></select><div class="help-block"></div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 "><?php echo Html::img('@web/images/remove.png',['class'=>'user-image','alt'=>'User Image','onclick'=>'$(this).parent().parent().remove();']); ?></div></div>');
	
			
		});*/
	});
	
</script>
			</div>
			 <div class="box-footer">&nbsp;</div>
	</div>

</section>
