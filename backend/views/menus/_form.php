<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\MenuTypes;
use common\models\Categories;
use common\models\Brands;
use common\models\Menus;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\models\Menus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menus-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?php //echo $form->field($model, 'category_id')->textInput() ?>
	<?php
		// Normal select with ActiveForm & model
		echo $form->field($model, 'category_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Categories::find()->where(['status'=>'active'])->all(),'category_id','category_name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	
	<?php
		// Normal select with ActiveForm & model
		echo $form->field($model, 'brand_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Brands::find()->where(['status'=>'active'])->all(),'brand_id','brand_name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Brand'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
    <?php //echo $form->field($model, 'parent_id')->textInput() ?>
	<?php
		// Normal select with ActiveForm & model
		echo $form->field($model, 'parent_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Menus::find()->where(['status'=>'active'])->all(),'id','title'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Parent'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	
	

    <?php //echo $form->field($model, 'type')->textInput() ?>
	<?php
		// Normal select with ActiveForm & model
		echo $form->field($model, 'type')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(MenuTypes::find()->where(['status'=>'active'])->all(),'id','title'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Menu Type'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>

    <?php //echo $form->field($model, 'others')->textarea(['rows' => 6]) ?>
	
	<!--<a class="btn btn-success" id="AddMoreMenuFilters">Add Menu Filters</a>-->
	
	<a class="btn btn-success" id="showFilters">Show Filters</a>
	<div id="filtersListinMenu" style="display:none">
		
		<div id="featuresByCategoryId" class="row"></div>
	
	</div>	
	<!--<div id="AddMoreMenuFiltersList"></div>-->
	
	<?php echo $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>
	
	 <?php echo  $form->field($model, 'meta_title')->textarea(['rows' => 1]) ?>
	 
	 <?php echo  $form->field($model, 'meta_keyword')->textarea(['rows' => 2]) ?>
	 
	 <?php echo  $form->field($model, 'meta_desc')->textarea(['rows' => 2]) ?>

     <?php echo $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => '-Select Status-']) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	
	<!--<div class="add_more_filter_list row"></div>-->
	
	<input type="hidden" id="menu_id" value="<?php echo $model->id ?>" />
	<input type="hidden" id="model_name" value="menus" />

    <?php ActiveForm::end(); ?>
	
</div>
