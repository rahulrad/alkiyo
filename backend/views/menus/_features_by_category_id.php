<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\widgets\Breadcrumbs;

?>
<?php 
	if(!empty($menuDetail)){
		$others = unserialize($menuDetail->others);
		
	}
	$selected_feautres_value = array();
	
	if(!empty($others)){
		foreach($others as $other){
			if(isset($other['feature_value_id']) && !empty($other['feature_value_id'])){
				foreach($other['feature_value_id'] as $f_value_id){
					$selected_feautres_value[] = $f_value_id;
				}
			}
		}
	}
	
?>
<?php if(!empty($features)){  ?>
			<?php 
				$i = 0;
				foreach($features as $feature){ ?>
			  
			  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
				<label for="menus-status" class="control-label"><?php echo $feature->name; ?></label>
				
				<input type="hidden" name="Menus[other][<?php echo $i; ?>][feature_id]" value="<?php echo $feature->id; ?>" />
				<select name="Menus[other][<?php echo $i; ?>][feature_value_id][]" class="form-control" id="" multiple="multiple">
				  <option value="">-Select Feature Value-</option>
				  <?php foreach($feature->featureValues as $feature_value){ ?>
				  <option value="<?php echo $feature_value->id; ?>" <?php if(in_array($feature_value->id,$selected_feautres_value)){ echo 'selected=selected';} ?>><?php echo $feature_value->value; ?></option>
				  <?php } ?>
				</select>
				<div class="help-block"></div>
			  </div>
			<?php $i++; } ?>
		<?php }else{  echo '<div class="col-lg-12"><h6>No Any Features for this category</h6></div>'; } ?>
<div class="clearfix"></div>
