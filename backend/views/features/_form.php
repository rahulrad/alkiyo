<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Categories;
use common\models\FeatureGroups;
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Features */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="features-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo  $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	
	<?php echo  $form->field($model, 'display_name')->textInput(['maxlength' => true]) ?>
	
	<?php echo  $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

 	<?php ///echo  $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Categories::find()->all(),'category_id','category_name'),	['prompt' => '-Select Category-'] ); ?>
	
	<?php
		// Normal select with ActiveForm & model
		echo $form->field($model, 'category_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Categories::find()->all(),'category_id','category_name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>

   <?php //echo  $form->field($model, 'feature_group_id')->dropDownList(ArrayHelper::map(FeatureGroups::find()->all(),'id','name'),	['prompt' => '-Select -'] ); ?>
   <?php
		// Normal select with ActiveForm & model
		echo $form->field($model, 'feature_group_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(FeatureGroups::find()->all(),'id','name'),
			'language' => 'de',
			'options' => ['placeholder' => 'Select FeatureGroup'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>

   <?php echo  $form->field($model, 'is_filter')->textInput(['maxlength' => true]) ?>

   <?php echo  $form->field($model, 'is_required')->textInput() ?>

   <?php echo  $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

   <?php echo  $form->field($model, 'unit')->textInput(['maxlength' => true]) ?>

   <?php echo  $form->field($model, 'display_text')->textarea(['rows' => 1]) ?>

   <?php echo  $form->field($model, 'sort_order')->textInput() ?>

   <?php echo  $form->field($model, 'show_feature')->checkbox() ?>

    <?php echo  $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>

    

    <div class="form-group">
       <?php echo  Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
