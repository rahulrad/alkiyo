<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model app\models\Adminusers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adminusers-form">
	<?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
	
	<?php echo $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php //echo $form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?>
	
	<?php //if(empty($model->password_hash)){ ?>
    <?php echo $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>
	<?php //} ?>
	
    <?php //echo $form->field($model, 'password_reset_token')->textInput(['maxlength' => true]) ?>

    <?php //echo $form->field($model, 'created_at')->textInput() ?>

    <?php //echo $form->field($model, 'updated_at')->textInput() ?>
	
	<?php echo  $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => '-Select Status-']) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
