<?php




use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use common\models\Categories;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\SliderImages;
use common\models\Brands;

$brands = ArrayHelper::map(Brands::find()->limit(10)->all(),'brand_id','brand_name');

/* @var $this yii\web\View */
/* @var $model app\models\Sliders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sliders-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'id'=>'sliderForm']); ?>
    <?php echo  $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?php echo  $form->field($model, 'slider_type')->dropDownList([ 'home' => 'Home', 'category' => 'Category', 'default' => 'Default','homesecond'=> 'HomeSecond'],['class'=>'form-control sliderType']) ?>
    <?php 
        $display = 'display_hide';
        if(!empty($model->slider_type)){
            if($model->slider_type == 'category'){
                $display = 'display_show';
            }else{
                $display = 'display_hide';
            } 
        }
    ?>
    <div id="sliderCategoryList" class="<?php echo $display; ?>">
    <?php
		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryLists();
		// Normal select with ActiveForm & model
		echo $form->field($model, 'category_id')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
    </div>
	<?php echo $form->field($model, 'is_mobile')->checkBox(['label' => 'Is mobile banner', 'uncheck' => null, 'checked' => 'checked']);  ?>
	<?php echo  $form->field($model, 'start_date')->textInput(['id' => 'start_date']) ?>
	<?php echo  $form->field($model, 'end_date')->textInput(['id' => 'end_date']) ?>
	
	<?php /**<?php echo $form->field($model, 'file')->fileInput(); ?>
	
	<?php if ($model->image): ?>
	
        <div class="form-group">
		<?php $path = str_replace('admin','frontend/web/',Yii::$app->getUrlManager()->getBaseUrl());?>
			<?php echo Html::img($path.$model->image,['class'=>'img-responsive  img-thumbnail small-thumbnail','alt'=>'User Image']); ?>
           
        </div>
		
     <?php endif; ?>
    <?php  echo  $form->field($model, 'description')->textarea(['rows' => 2]) ?>
	
	<?php echo  $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
    **/ ?>
	
	<div id="AddMoreSliders">
	<?php $slider_imagesData = SliderImages::find()->where(['slider_id' => $model->id])->all(); ?>
	<?php 
		$count_slider = 1;
		if(!empty($slider_imagesData)){ 
			$i = 1;
			$count_slider = count($slider_imagesData);
			foreach($slider_imagesData as $data){
			//echo '<pre>'; print_r($data); die;
	?> 
		<div class="add_more_filter_list box box-info">
				<h3>Slider <?php echo $i ?></h3>
				<div class="form-group field-sliders-file">
				<label for="sliders-file" class="control-label">Slider <?php echo $i ?> Image</label>
				<div class="col-lg-12 text-center">
				<div class="form-group">
					<?php 
						
						//$path = str_replace('admin','frontend/web/',Yii::$app->getUrlManager()->getBaseUrl());
						//$path = 'https://s3.ap-south-1.amazonaws.com/nayashoppy/';
						$path = Yii::getAlias('@frontends') .'/';
					?>
					<img alt="Slider Image" width="100%" src="<?php echo $path.$data->image; ?>" class="img-responsive  img-thumbnail small-thumbnail">           
					</div>
				<input type="hidden" name="SliderImage[<?php echo $i ?>][old_image]" value="<?php echo $data->image; ?>">
				<input type="file"  name="image_<?php echo $i ?>" id="sliders-file">
				<div class="help-block"></div>
				</div>
				
				</div>
				
				
			
				

				<div class="form-group field-sliders-description">
				<label for="sliders-description" class="control-label">Slider <?php echo $i ?> Title</label>
				<input type="text" maxlength="255" name="SliderImage[<?php echo $i ?>][title]" class="form-control" id="sliders-link" value="<?php echo $data->title; ?>">
				</div>
			
				<label for="sliders-description" class="control-label">Slider <?php echo $i ?> Description</label>
				<input type="text" maxlength="255" name="SliderImage[<?php echo $i ?>][description]" class="form-control" id="sliders-link" value="<?php echo $data->description; ?>">
				
				<label for="sliders-h1" class="control-label">Slider <?php echo $i ?> h1</label>
				<input type="text" maxlength="255" name="SliderImage[<?php echo $i ?>][h1]" class="form-control" id="sliders-h1" value="<?php echo $data->h1; ?>">
				
				<label for="sliders-h2" class="control-label">Slider <?php echo $i ?> h2</label>
				<input type="text" maxlength="255" name="SliderImage[<?php echo $i ?>][h2]" class="form-control" id="sliders-h2" value="<?php echo $data->h2; ?>">
				
				<label for="sliders-h3" class="control-label">Slider <?php echo $i ?> h3</label>
				<input type="text" maxlength="255" name="SliderImage[<?php echo $i ?>][h3]" class="form-control" id="sliders-h3" value="<?php echo $data->h3; ?>">
				
				<div class="form-group field-sliders-brand">
					<label for="sliders-description" class="control-label">Brand</label>
					<select name="SliderImage[<?php echo $i ?>][brand_id]" class="form-control" id="sliders-brand_id">
						<?php  foreach($brands as $key=>$bran){ ?>
							<option value="<?= $key; ?>"  <?php if($key==$data->brand_id){ echo "selected"; } ?>><?php echo $bran; ?></option>
					
						<?php } ?>
					</select>
				</div>
				
				<div class="help-block"></div>
			
				<div class="form-group field-sliders-link">
				<label for="sliders-link" class="control-label">Slider <?php echo $i ?> link</label>
				<input type="text" maxlength="255" name="SliderImage[<?php echo $i ?>][link]" class="form-control" id="sliders-link" value="<?php echo $data->link; ?>">
				<input type="hidden" name="SliderImage[<?php echo $i ?>][s_no]" class="form-control" value="<?php echo $i ?>">
				<div class="help-block"></div>
				</div>
				
				<label for="sliders-link" class="control-label">Slider <?php echo $i ?> Status</label>
				<input type="checkbox" name="SliderImage[<?php echo $i ?>][status]" <?php if($data->status == 1){ echo 'checked=checked'; } ?>  value="1"/>
				
		</div>		
		<?php $i++; } } else { ?>
		<div class="add_more_filter_list box box-info">
				<h3>Slider 1</h3>
				<div class="form-group field-sliders-file">
				<label for="sliders-file" class="control-label">Slider 1 Image</label>
				<input type="file" name="image_1" id="sliders-file">

				<div class="help-block"></div>
				</div>

				<div class="form-group field-sliders-description">
				<label for="sliders-description" class="control-label">Slider 1 Title</label>
				
				<input type="text" maxlength="255" name="SliderImage[1][title]" class="form-control" id="sliders-link">
				</div>
			
				<label for="sliders-description" class="control-label">Slider 1 Description</label>
				<input type="text" maxlength="255" name="SliderImage[1][description]" class="form-control" id="sliders-link">
				
				<label for="sliders-h1" class="control-label">Slider 1 h1</label>
				<input type="text" maxlength="255" name="SliderImage[1][h1]" class="form-control" id="sliders-h1">
				
				<label for="sliders-h2" class="control-label">Slider 1 h2</label>
				<input type="text" maxlength="255" name="SliderImage[1][h2]" class="form-control" id="sliders-h2">
				
				<label for="sliders-h3" class="control-label">Slider 1 h3</label>
				<input type="text" maxlength="255" name="SliderImage[1][h3]" class="form-control" id="sliders-h3">
			
				
				<div class="form-group field-sliders-brand">
					<label for="sliders-description" class="control-label">Brand</label>
					<select name="SliderImage[1][brand_id]" class="form-control" id="sliders-brand_id">
						<?php  foreach($brands as $key=>$bran){ ?>
							<option value="<?= $key; ?>" ><?php echo $bran; ?></option>
					
						<?php } ?>
					</select>
				</div>

				
				<div class="help-block"></div>
				<div class="form-group field-sliders-link">
				<label for="sliders-link" class="control-label">Slider 1 link</label>
				<input type="text" maxlength="255" name="SliderImage[1][link]" class="form-control" id="sliders-link">
				<input type="hidden" name="SliderImage[1][s_no]" class="form-control" value="1">
				<div class="help-block"></div>
				</div>
				
				<label for="sliders-link" class="control-label">Slider 1 Status</label>
				<input type="checkbox" name="SliderImage[1][status]" value="1"  checked="checked" />
				
				<div class="help-block"></div>
				</div>
				</div>
				
		</div>
	<?php } ?>
	
<div id="AddMoreSliderButton" class="btn btn-success">Add More Slider</div>

     <?php echo  $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>


    <div class="form-group">
        <?php echo  Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary sliderSubmitButton']) ?>
    </div>
	
	

    <?php ActiveForm::end(); ?>	
	</div>
	

</div>
<input type="hidden" class="totalAddMoreSlider" value="<?php echo $count_slider; ?>"  />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
	
	$(document).ready(function(){
		
		$('#AddMoreSliderButton').click(function(){
			var totalAddMoreSlider = $('.totalAddMoreSlider').val();
			var a = parseInt(totalAddMoreSlider) + Number(1);
			$('.totalAddMoreSlider').val(a);
			
					$('#AddMoreSliders').append('<div class="add_more_filter_list  box box-info"><h3>Slider '+a+' <?php echo Html::img('@web/images/remove.png',['class'=>'deleteImgIcon','alt'=>'User Image','onclick'=>'$(this).parent().parent().remove();']); ?></h3><div class="form-group field-sliders-file"><label for="sliders-file" class="control-label">Slider '+a+' Image</label><input type="file" name="image_'+a+'" id=""><div class="help-block"></div></div><div class="form-group field-sliders-description"><label class="control-label" for="sliders-description">Slider '+a+' Title</label><input type="text" value="" id="sliders-link" class="form-control" name="SliderImage['+a+'][title]" maxlength="255"></div><div class="form-group field-sliders-h1"><label class="control-label" for="sliders-h1">Slider '+a+' H1</label><input type="text" value="" id="sliders-h1" class="form-control" name="SliderImage['+a+'][h1]" maxlength="255"></div><div class="form-group field-sliders-h2"><label class="control-label" for="sliders-h2">Slider '+a+' H2</label><input type="text" value="" id="sliders-h2" class="form-control" name="SliderImage['+a+'][h2]" maxlength="255"></div><div class="form-group field-sliders-h3"><label class="control-label" for="sliders-h3">Slider '+a+' H3</label><input type="text" value="" id="sliders-h3" class="form-control" name="SliderImage['+a+'][h3]" maxlength="255"></div><div class="form-group field-sliders-brand"><label for="sliders-description" class="control-label">Brand '+a+'</label><select name="SliderImage['+a+'][brand_id]" class="form-control" id="sliders-brand_id"><?php  foreach($brands as $key=>$bran){ ?><option value="<?= $key; ?>" ><?php echo $bran; ?></option><?php } ?></select></div><div class="form-group field-sliders-description"><label for="sliders-description" class="control-label">Slider '+a+' Description</label><input type="text" name="SliderImage['+a+'][description]" class="form-control" id="sliders-description"><div class="help-block"></div></div><div class="form-group field-sliders-link"><label for="sliders-link" class="control-label">Slider '+a+' link</label><input type="text" maxlength="255" name="SliderImage['+a+'][link]" class="form-control" id="sliders-link"><div class="help-block"><input type="hidden" name="SliderImage['+a+'][s_no]" class="form-control" value="'+a+'"></div></div><label class="control-label" for="sliders-link">Slider '+a+' Status</label><input type="checkbox" value="1" name="SliderImage['+a+'][status]"  checked="checked" ></div>');
		});
	});
	
</script>