<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\models\CuponCategories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cupon-categories-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

     <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	 
	 <?php echo $form->field($model, 'image_name')->textInput() ?>
	 
	 <?php echo  $form->field($model, 'file')->fileInput(); ?>
	
	<?php if ($model->image): ?>
	
        <div class="form-group">
		<?php 
				//$path = str_replace('admin','frontend/web/',Yii::$app->getUrlManager()->getBaseUrl());
				$path = '/';
			?>
			<?php echo Html::img($path.$model->image,['class'=>'img-responsive  img-thumbnail small-thumbnail','alt'=>'Coupons Category Image']); ?>
           
        </div>
		
    <?php endif; ?>

  <?php //echo $form->field($model, 'description')->widget(CKEditor::className(), ['options' => ['rows' => 6],'preset' => 'basic']) ?>

    <?php // echo $form->field($model, 'meta_title')->textarea(['rows' => 1]) ?>

     <?php //echo $form->field($model, 'meta_keyword')->textarea(['rows' => 2]) ?>

     <?php //echo $form->field($model, 'meta_descrption')->textarea(['rows' => 2]) ?>
	<?php echo  $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>
     <?php //echo $form->field($model, 'show_home')->textInput() ?>
     <?php echo $form->field($model, 'show_home')->checkBox(['label' => 'show home page', 'uncheck' => null, 'checked' => 'checked']);  ?>

    <div class="form-group">
         <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
