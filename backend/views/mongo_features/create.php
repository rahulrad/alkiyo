<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\MongoFeatures */

$this->title = 'Create Mongo Features';
$this->params['breadcrumbs'][] = ['label' => 'Mongo Features', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mongo-features-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
