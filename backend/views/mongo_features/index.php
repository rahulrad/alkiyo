<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MongoFeaturesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mongo Features';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mongo-features-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Mongo Features', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            '_id',
            'id',
            'name',
            'category_id',
            'feature_group_id',
            // 'is_filter',
            // 'is_required',
            // 'slug',
            // 'type',
            // 'unit',
            // 'display_text',
            // 'status',
            // 'created',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
