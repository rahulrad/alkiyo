<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductSupplierFeatureMappings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-supplier-feature-mappings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category_id')->textInput() ?>

    <?= $form->field($model, 'store_id')->textInput() ?>

    <?= $form->field($model, 'store_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'emi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cod')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'delivery')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'return_policy')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
