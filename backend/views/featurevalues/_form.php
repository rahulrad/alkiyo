<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Features;
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\FeatureValues */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feature-values-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php //echo   $form->field($model, 'feature_id')->dropDownList(ArrayHelper::map(Features::find()->all(),'id','name')) ?>
	<?php
		// Normal select with ActiveForm & model
		echo $form->field($model, 'feature_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Features::find()->all(),'id','name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Feature'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>

    <?php echo  $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

    <?php echo  $form->field($model, 'display_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?php echo   Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
