<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use common\models\Brands;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\Brands */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
		<?php
		echo $form->field($model, 'brand_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Brands::find()->andWhere(['status'=>'active'])->all(),'brand_id','brand_name'),
			'language' => 'eg',
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
    <?php echo  $form->field($model, 'year')->textInput(['maxlength' => true]) ?>
    <?php // echo   $form->field($model, 'brand_description')->textarea(['rows' => 6]) ?>
	<?php  echo   $form->field($model, 'content')->textarea(['rows' => 6]) ?>
	<?php echo  $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>
    <div class="form-group">
        <?php echo  Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
