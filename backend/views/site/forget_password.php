<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Adminusers */

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = ['label' => 'Adminusers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-logo">
        <a href="../../index2.html"><b>Nayashopy</b>Admin</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
	  <p class="login-box-msg"><strong>Please fill out the following fields to forget password:</strong></p>
	<?php $form = ActiveForm::begin(); ?>
	<?php echo $form->field($model, 'username')->textInput(['maxlength' => true,'required' => true]) ?>
	<?php echo $form->field($model, 'email')->textInput(['maxlength' => true,'required' => true]) ?>
	
    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Forget Password' : 'Forget Password', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
