<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-logo">
        <a href="../../index2.html"><b>Nayashopy</b>Admin</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
	  	
        <p class="login-box-msg">Please fill out the following fields to login:</p>
         <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?php echo  $form->field($model, 'username',['options'=>[ 
									'tag'=>'div',
									'class'=>'form-group field-loginform-username has-feedback required'
									],
									'template'=>'{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>
									{error}{hint}'
				])->textInput(['paceholder' => 'Username','class' => 'form-control']) ?>

                <?php echo  $form->field($model, 'password',['options'=>[
									'tag'=>'div',
									'class'=>'form-group field-loginform-password has-feedback required',	
									],
									'template'=>'{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>
									{error}{hint}'
				])->passwordInput(['paceholder' => 'Password','class' => 'form-control']); ?>
		<div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <?php echo  $form->field($model, 'rememberMe')->checkbox() ?>

                    </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
                    <?php echo  Html::submitButton('Sign In', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
                </div>
			</div><!-- /.col -->
          </div>
            <?php ActiveForm::end(); ?>
		
		<?php echo Html::a('I forgot my password', ['/site/forget-password']); ?><br>

      </div><!-- /.login-box-body -->