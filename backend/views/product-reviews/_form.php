<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use common\models\ProductReviews;

/* @var $this yii\web\View */
/* @var $model backend\models\ProductReviews */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-reviews-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
	
	 <?php //echo$form->field($model, 'product_id')->textInput() ?>
	 
	  <?php echo$form->field($model, 'ip')->textInput() ?>
	  
	   <?php echo$form->field($model, 'user_id')->textInput() ?>
	   
	  <?php echo$form->field($model, 'rating')->textInput() ?>
	  
    <?php echo $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>

   

    
	
	<?php echo $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>

	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
