<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TagsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tags';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content-header">
       <h1><?php echo  Html::encode($this->title) ?></h1>
      <?php echo Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
</section>

<section class="content">
    
    <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <p><?php echo  Html::a('Create Tags', ['create'], ['class' => 'btn btn-success','style' => 'float:right']) ?></p>
                </div><!-- /.box-header -->
                <div class="box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          //  'id',
            'tag',
            'type',
            'status',
            'created',

            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}'
            ],
        ],
    ]); ?>

    </div>
    </div>
    </div>

</div>
</section>
