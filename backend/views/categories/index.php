<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Categories;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>
<section class="content">
    
	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
				<?php echo  Html::a('Download CSV Template', ['download_blank_csv_format'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
                <?php echo  Html::a('Export', ['export'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
				<?php echo  Html::a('Import', ['importdata'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
				  <p><?php echo  Html::a('Create Categories', ['create'], ['class' => 'btn btn-success','style' => 'float:right']) ?></p>
                </div><!-- /.box-header -->
				
				
				
				<?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
	
	<?php 
			$category_id = '';
			if(isset($_GET['CategoriesSearch'])){
				$category_id = $_GET['CategoriesSearch']['category_id'];
			}
	?>
	<div class="searchblock">
	
	
	 <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12">
<?php

		
		$model->category_id = $category_id;
		// Normal select with ActiveForm & model
		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryLists();
		echo $form->field($model, 'category_id')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category','id' => 'category_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
			
		
	?>
	</div>
	

	 <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12">
<div class="form-group" style="margin-top:24px;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-block']) ?>
    </div></div>
	</div>
	<?php ActiveForm::end() ?>
    <div class="clearfix"></div>
	
	
	
                <div class="box-body">
    <?php echo  GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'category_name:ntext',
			'display_name',
			[
				'attribute' => 'parent_category_id',
				
				'label' => 'Parent Category',
				
				'format' => 'text',
				
				'content'=> function($data){
				
					return $data->getParentName();
				
				}
			],
			
           // 'category_description:ntext',
			'sort_order',
		   'status',
            'created',
			

            [
			 	//'label'=>'sort up',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     return Html::a('<i class="fa fa-arrow-up" aria-hidden="true"></i>', Yii::$app->homeUrl.'categories/sort_order_update?type=up&id=' . $dataProvider->category_id);
                 },
             ],
			 [
			 	//'label'=>'sort down',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     return Html::a('<i class="fa fa-arrow-down" aria-hidden="true"></i>', Yii::$app->homeUrl.'categories/sort_order_update?type=down&id=' . $dataProvider->category_id);
                 },
             ],

            [
			'class' => 'yii\grid\ActionColumn',
			'template' => '{update} ',
			],
        ],
		'tableOptions' =>['class' => 'table table-bordered table-hover'],

    ]); ?>
	</div>
	</div>
	</div>

</div>
</section>