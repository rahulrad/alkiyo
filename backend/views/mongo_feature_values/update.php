<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MongoFeatureValues */

$this->title = 'Update Mongo Feature Values: ' . ' ' . $model->_id;
$this->params['breadcrumbs'][] = ['label' => 'Mongo Feature Values', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->_id, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mongo-feature-values-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
