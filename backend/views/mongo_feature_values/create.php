<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\MongoFeatureValues */

$this->title = 'Create Mongo Feature Values';
$this->params['breadcrumbs'][] = ['label' => 'Mongo Feature Values', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mongo-feature-values-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
