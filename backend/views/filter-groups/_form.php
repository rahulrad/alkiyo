<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Categories;
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\FilterGroups */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="filter-groups-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
		// Normal select with ActiveForm & model
		// Normal select with ActiveForm & model
		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryLists();
		echo $form->field($model, 'category_id')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category','id' => 'category_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo  $form->field($model, 'status')->dropDownList([ '1' => 'Active', '0' => 'Inactive', ]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
