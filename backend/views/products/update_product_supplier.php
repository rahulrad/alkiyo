<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\models\MongoProducts */
/* @var $form yii\widgets\ActiveForm */


$this->title = 'Update Product Supplier ';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<section class="content-header">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php echo \yii\widgets\Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
</section>

<section class="content">

    <div class="box box-info">
        <div class="box-body">


            <div class="mongo-products-form">

                <?php $form = ActiveForm::begin(); ?>

                <?php
                // Normal select with ActiveForm & model
                echo $form->field($model, 'store_id')->widget(\kartik\select2\Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\common\models\Suppliers::find()->where(['status' => 'active'])->andWhere(['!=', 'is_delete', 1])->all(), 'id', 'name'),
                    'language' => 'eg',
                    'options' => ['placeholder' => 'Select Store'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

                <?php echo $form->field($model, 'rating') ?>

                <?php echo $form->field($model, 'cod') ?>

                <?php echo $form->field($model, 'meta_title') ?>

                <?php echo $form->field($model, 'meta_desc') ?>

                <?php echo $form->field($model, 'return_policy') ?>

                <?php echo $form->field($model, 'delivery') ?>

                <?php echo $form->field($model, 'price') ?>

                <?php echo $form->field($model, 'url') ?>

                <?php echo $form->field($model, 'emi') ?>

                <?php echo $form->field($model, 'colors') ?>

                <?php echo $form->field($model, 'sizes') ?>

                <?php echo $form->field($model, 'offers') ?>

                <?php echo $form->field($model, 'instock')->checkbox() ?>

                <?php echo $form->field($model, 'unique_id') ?>


                <?php //echo $form->field($model, 'status')->dropDownList([ '1' => 'Active', '0' => 'Inactive', ], ['prompt' => '-Select Status-']) ?>

                <div class="form-group">
                    <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>

        </div>
        <div class="box-footer">&nbsp;</div>
    </div>

</section>
