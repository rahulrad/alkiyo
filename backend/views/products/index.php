<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Brands;
use common\models\Products;
use common\models\Categories;
use common\models\Suppliers;
use common\models\ProductsSuppliers;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'NS Products';

$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>
<section class="content"  >
    
	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
				
					<?php echo  Html::a('Export', ['export'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
					<?php echo  Html::a('Import', ['importdata'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
				  <p><?php echo  Html::a('Create Products', ['create'], ['class' => 'btn btn-success','style' => 'float:right']) ?></p>
				  
                </div>
				
				
				<?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
	
	<?php 
			$category_id = '';
			$brand_id = '';
			$product_id = '';
			$created = '';
			$store_id = 1;
			$rating = '';
			$discount = '';
			$auto_product_name = '';
			$auto_brand_name = '';
			if(isset($_GET['ProductsSearch'])){
				$category_id = $_GET['ProductsSearch']['categories_category_id'];
				$brand_id = !empty($_GET['auto_brand_name']) ? $_GET['ProductsSearch']['brands_brand_id'] : '';
				$auto_brand_name = $_GET['auto_brand_name'];
				$created = $_GET['ProductsSearch']['created'];
				$store_id = $_GET['ProductsSearch']['store_id'];
				$rating = $_GET['ProductsSearch']['rating'];
				$discount = $_GET['ProductsSearch']['discount'];
				
				$product_id = !empty($_GET['auto_product_name']) ? $_GET['ProductsSearch']['product_id'] : '';
				$auto_product_name = $_GET['auto_product_name'];
			}
	?>
	<div class="searchblock">
	<div class="col-sm-2 col-lg-2 col-md-2  col-xs-12">
	<?php
		// Normal select with ActiveForm & model
		$model->store_id = $store_id;
		echo $form->field($model, 'store_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Suppliers::find()->andWhere(['!=', 'is_delete' ,1])->all(),'id','name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Store','id' => 'store_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	</div>
	
	 <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12">
<?php

		
		$model->categories_category_id = $category_id;
		// Normal select with ActiveForm & model
		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryLists();
		echo $form->field($model, 'categories_category_id')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category','id' => 'category_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
			
		
	?>
	</div>
	 <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12">
	  <label> Brand </label>
	<input type="text" name="auto_brand_name" value="<?php echo $auto_brand_name; ?>" class="form-control searchAutoComplete" model_name="Brands" field_name="brand_name" id_value="productssearch-brands_brand_id">
	<?php echo $form->field($model, 'brands_brand_id')->hiddenInput(['value'=>$brand_id])->label(false) ?>
<?php
		// Normal select with ActiveForm & model
		/**$model->brands_brand_id = $brand_id;
		echo $form->field($model, 'brands_brand_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Brands::find()->where(['status'=>'active'])->andWhere(['!=', 'is_delete' ,1])->all(),'brand_id','brand_name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Brand'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]); **/
	?>
	</div>
	
	
	
	<div class="col-sm-2 col-lg-2 col-md-2  col-xs-12">
	<?php
		// Normal select with ActiveForm & model
		/**$productRatings = Products::find()->select(['rating'])->groupBy(['rating'])->orderBy(['rating' => SORT_DESC])->all();
		
		$ratings = array();
		if(!empty($productRatings)){
			foreach($productRatings as $productRating){
				if($productRating->rating != ''){
					//$rate = number_format($productRating->rating, 2, '.', '');
					$rate = $productRating->rating;
					$ratings[$productRating->rating] = $productRating->rating;
				}
				
			}
		} **/
		$ratings = array('5'=>'5','4'=>'4','3'=>'3','2'=>'2','1'=>'1');
	//echo '<pre>'; print_r($ratings); die;
		$model->rating = $rating;
		echo $form->field($model, 'rating')->widget(Select2::classname(), [
			'data' => $ratings,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Rating'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	</div>

	 <div class="col-sm-2 col-lg-2 col-md-2  col-xs-12">

<?php 
$model->created = $created;
echo  $form->field($model, 'created')->textInput(['id' => 'start_date']) ?>
	</div>
	
	
	<div class="col-sm-8 col-lg-8 col-md-8 col-xs-12">
	<label> Product </label>
	<input type="text" name="auto_product_name" value="<?php echo $auto_product_name; ?>" class="form-control searchAutoComplete" model_name="Products" field_name="product_name" id_value="productssearch-product_id">
	<?php echo $form->field($model, 'product_id')->hiddenInput(['value'=>$product_id])->label(false) ?>
<?php
		// Normal select with ActiveForm & model
		/**$model->product_id = $product_id;
		
		$productSearch = array();
		if(!empty($category_id)){
			$productSearch['categories_category_id'] = $category_id;
		}
		if(!empty($brand_id)){
			$productSearch['brands_brand_id'] = $brand_id;
		}
		$products = !empty($category_id) ? Products::find()->where($productSearch)->all() : Products::find()->all() ;
		
		echo $form->field($model, 'product_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map($products,'product_id','product_name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Product'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]); **/
	?>
	</div>
	<div class="col-sm-2 col-lg-2 col-md-2  col-xs-12">
	<?php
		// Normal select with ActiveForm & model
		/**$productDiscounts = Products::find()->select(['discount'])->groupBy(['discount'])->orderBy(['discount' => SORT_DESC])->all();
		$discounts = array();
		if(!empty($productDiscounts)){
			foreach($productDiscounts as $productDiscount){
				if($productDiscount->discount != ''){
					$discounts["$productDiscount->discount"] = $productDiscount->discount;
				}
				
			}
			
		} **/
		$discounts = array('90-100' => '90-100','80-90'=>'80-90','70-80'=>'70-80','60-70'=>'60-70','50-60'=>'50-60','40-50'=>'40-50','30-40'=>'30-40','20-30'=>'20-30','10-20'=>'10-20','1-10'=>'1-10');
		$model->discount = $discount;
		echo $form->field($model, 'discount')->widget(Select2::classname(), [
			'data' => $discounts,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Discount'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	</div>
	 <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12">
<div class="form-group" style="margin-top:24px;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-block searchSubmitButton']) ?>
    </div></div>
	</div>
	<?php ActiveForm::end() ?>
    <div class="clearfix"></div>
	
	
				
				
				<!-- /.box-header -->
                <div class="box-body">
				<?php // echo Html::beginForm(['products/change_multiproduct_is_approved'],'post');?>
<?php // echo Html::submitButton('Approved Products', ['class' => 'btn btn-primary pull-right',]);?>
     <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			 

           // 'product_id',
          	   [
				'attribute'=>'categories_category_id',
				'value'=>'categoriesCategory.category_name',
			   ],
			   [
				'attribute'=>'brands_brand_id',
				'value'=>'brandsBrand.brand_name',
			   ],
           // 'product_name',
			 
			 
			 [
			 	'attribute'=>'product_name',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($data) {
					$product_name = substr($data->product_name, 0, 50);
                      return Html::tag('div', $product_name, ['title'=>$data->product_name,'style'=>'cursor:pointer;']);
                 },
             ],
			

			'lowest_price',
			//'model_number',
			 'discount',
			 'rating',
			 
			//'unique_id',
           // 'product_description:ntext',
		   //	'date_update',
			//'product_status',
            // 'created',
			
			[
				'attribute'=>'created',
			    'value'=> function($data){
					$date = '';
					if(!empty($data->created)){ $date = date('Y-m-d', strtotime($data->created));	}
					return $date;
				}
		     ],
			
			/**[
			 	'label'=>'Status',
			 	'format' => 'raw',
				'attribute'=>'active',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     if($dataProvider->active == 'active'){
						return Html::a('<i class="fa fa-check" aria-hidden="true" title="Product Status"></i>', Yii::$app->homeUrl.'products/change_status?type=inactive&product_id=' . $dataProvider->product_id);
					 }else{
						 return Html::a('<i class="fa fa-close" aria-hidden="true" title="Product Status"></i>', Yii::$app->homeUrl.'products/change_status?type=active&product_id=' . $dataProvider->product_id); 
					 }
                 },
             ], **/
		/**	 [
			 	'label'=>'Apporved',
				'attribute'=>'is_approved',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     if($dataProvider->is_approved == 1){
						return Html::a('<i class="fa fa-check" aria-hidden="true" title="Product Apporved"></i>', Yii::$app->homeUrl.'products/change_is_approved?type=0&product_id=' . $dataProvider->product_id);
					 }else{
						 return Html::a('<i class="fa fa-close" aria-hidden="true" title="Product Apporved"></i>', Yii::$app->homeUrl.'products/change_is_approved?type=1&product_id=' . $dataProvider->product_id); 
					 }
                 },
             ],**/
			
			 
			 
			[
			 	'label'=>'Stores',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
					$products_suppilers = ProductsSuppliers::find()->select(['id'])->where(['product_id'=>$dataProvider->product_id])->all();
					$products_suppilers = count($products_suppilers);
                     return Html::a($products_suppilers.'</i>',  Yii::$app->homeUrl.'products/view_suppilers?product_id='. $dataProvider->product_id, ['target'=>'_blank']);
                 },
             ],
			/**	[
			 	//'label'=>'unlink',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     if($dataProvider->store_id != 0){
					 return Html::a('<i class="fa fa-unlink" aria-hidden="true" title="Unlink Product to Stores"></i>', Yii::$app->homeUrl.'products/unlink_product?product_id=' . $dataProvider->product_id);
					 }
                 },
             ],**/
			 [
			 	//'label'=>'Reviews',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     return Html::a('<i class="fa fa-comments" title="Reviews"></i>',  Yii::$app->homeUrl.'product-reviews/index?product_id='. $dataProvider->product_id, ['target'=>'_blank']);
                 },
             ],
			 [
			 	//'label'=>'sort up',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     return Html::a('Features', Yii::$app->homeUrl.'products/product_features?product_id=' . $dataProvider->product_id, ['target'=>'_blank']);
                 },
             ],
			 [
			 	//'label'=>'sort down',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     return Html::a('<i class="fa fa-eye" aria-hidden="true" title="View Store"></i>', $dataProvider->url, ['target'=>'_blank']);
                 },
             ],
            [
			'class' => 'yii\grid\ActionColumn',
			'template' => '{update}&nbsp; |&nbsp;  {delete}',
			],
			/**[
			'class' => 'yii\grid\CheckboxColumn',
				'checkboxOptions' => function ($dataProvider) {
					if($dataProvider->is_approved == 1){
							return ['checked' => 'checked','value'=>$dataProvider->product_id];
					}else{
						return ['value'=>$dataProvider->product_id];
					}
					
				}
			] **/
        ],
    ]); ?>
	</div>
	</div></div></div>

</section>
