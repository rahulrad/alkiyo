<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\widgets\Breadcrumbs;
use common\models\Brands;
use common\models\Products;
use common\models\Categories;
use common\models\Suppliers;

$this->title = 'Map Products User Friendly: ' . ' ' . $getFormData['Products']['store_name'];
$this->params['breadcrumbs'][] = ['label' => 'Map Products', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->product_name, 'url' => ['view', 'id' => $model->product_id]];
$this->params['breadcrumbs'][] = 'Update';

/* @var $this yii\web\View */
/* @var $model backend\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>
<?php //echo Yii::$app->request->baseUrl. '/supermarkets/sample' ?>
<?php //$this->registerJsFile(Yii::$app->request->baseUrl.'/js/admin_development.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>


<section class="content">
<div class="panel panel-primary">
<div class="panel-heading"> <?php echo ucfirst($getFormData['Products']['store_name']); ?> <span class="pull-right"> Nayashopi Product</span>  </div>
 
	
<div class="panel-body">
	<div class="row">
  <div class="col-sm-6 col-lg-6 col-md-6 col-xs-12">
  <div class="searchblock">
 <!----------- Code for Mongo Product Searching -------------->

		<?php $form = ActiveForm::begin([
        'action' => ['products_mapping_user_friendly'],
        'method' => 'get',
    ]); ?>	 
			 <?php 
			$category_id = '';
			$brand_id = '';
			$store_id = '';
			$auto_brand_name = '';
			if(isset($_GET['Products'])){
				$category_id = $_GET['Products']['categories_category_id'];
				$brand_id = $_GET['Products']['brands_brand_id'];
				$auto_brand_name = isset($_GET['auto_brand_name']) ? $_GET['auto_brand_name'] : '';
				$store_id = $_GET['Products']['store_id'];
			}
	?>
	<div class="col-sm-6 col-lg-6 col-md-6 col-xs-12">
	<?php
		// Normal select with ActiveForm & model
		$model->store_id = $store_id;
		echo $form->field($model, 'store_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Suppliers::find()->andWhere(['!=', 'is_delete' ,1])->all(),'id','name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Store','id' => 'store_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	</div>
	 <div class="col-sm-6 col-lg-6 col-md-6 col-xs-12">
			 <?php

		
		$model->categories_category_id = $category_id;
		// Normal select with ActiveForm & model
		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryLists();
		echo $form->field($model, 'categories_category_id')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category','id' => 'category_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
			
		
	?>
	</div>
	<div class="col-sm-6 col-lg-5 col-md-6 col-xs-12">
	<label> Brand </label>
	<input type="text" name="auto_brand_name" value="<?php echo $auto_brand_name; ?>" class="form-control searchAutoComplete" model_name="Brands" field_name="brand_name" id_value="products-brands_brand_id">
	<?php echo $form->field($model, 'brands_brand_id')->hiddenInput(['value'=>$brand_id])->label(false) ?>
<?php
		// Normal select with ActiveForm & model
	/**	$model->brands_brand_id = $brand_id;
		echo $form->field($model, 'brands_brand_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Brands::find()->where(['status'=>'active'])->andWhere(['!=', 'is_delete' ,1])->all(),'brand_id','brand_name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Brand'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]); **/
	?>
	</div>
	
	 <div class="col-sm-6 col-lg-4 col-md-6 col-xs-12">
	<?php
		echo  $form->field($model, 'created')->textInput(['id' => 'start_date']) ;
	?>
	</div>
	
	 <div class="col-sm-6 col-lg-3 col-md-6 col-xs-12">
<div class="form-group" style="margin-top:24px;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-block searchSubmitButton']) ?>
    </div>
	</div>
	<?php ActiveForm::end() ?>
	<!--------- End code for mongoDb Product Searching ------------>
	</div>
	
	</div>
	<div class="col-sm-6 col-lg-6 col-md-6 col-xs-12">
	 <div class="searchblock">
 <!----------- Code for Mongo Product Searching -------------->

		<?php $form = ActiveForm::begin([ 'method' => 'post',
		'id' => 'nayashoppyProductForm'
    ]); ?>	 
		
	
	 <div class="col-sm-6 col-lg-6 col-md-6 col-xs-12">
			 <?php
		$productModel->ns_category_id = $category_id;
		$categoriesModel = new Categories();
		$categories_data =  $categoriesModel->getCategoryLists();
		echo $form->field($productModel, 'ns_category_id')->widget(Select2::classname(), [
			'data' => $categories_data,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
			
		
	?>
	</div>
	<div class="col-sm-6 col-lg-5 col-md-6 col-xs-12">
	<label> NS Brand </label>
	<input type="text" class="form-control searchAutoComplete" value="<?php echo $auto_brand_name; ?>" model_name="Brands" field_name="brand_name" id_value="products-ns_brand_id">
	<?php echo $form->field($model, 'ns_brand_id')->hiddenInput(['value'=>$brand_id])->label(false) ?>
	
	
<?php
		// Normal select with ActiveForm & model
		/**echo $form->field($productModel, 'ns_brand_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Brands::find()->where(['status'=>'active'])->andWhere(['!=', 'is_delete' ,1])->all(),'brand_id','brand_name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Brand'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]); **/
	?>
	</div>
	
	 <div class="col-sm-6 col-lg-4 col-md-6 col-xs-12">
	<?php
		echo  $form->field($productModel, 'ns_created')->textInput(['id' => 'start_date']) ;
	?>
	</div>
	
	 <div class="col-sm-6 col-lg-3 col-md-6 col-xs-12">
<div class="form-group" style="margin-top:24px;">
        <input type="button" value="Search" class="btn btn-primary btn-block searchButtonForNayashoppyProduct">
    </div>
	</div>
	<?php ActiveForm::end() ?>
	<!--------- End code for mongoDb Product Searching ------------>
	</div>
	
	</div>
	
	</div>
 <div> 
<?php  $form = ActiveForm::begin(['method' => 'post','action' => ['products/product_map']]); ?>
<div class="row">

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
           
	  <div class="box box-info">
			 <div class="box-body ">
			 
		
					
	
                             <?php  if(!empty($products)){ ?>
                              <input type="text" value="" id="searchingMongoProduct" class="form-control" placeholder="Search <?php echo ucfirst($getFormData['Products']['store_name']); ?> Products"> <br>
							  <?php echo '<span>'.count($products).' products.</span>'; ?>
                             <?php }else{ echo '<h4>Not Found Product</h4>'; } ?>
<div class="products-form productLists left-scrollblock">
    
	
	<?php 
	
	$product_lists = array(); // a blank array for products list
		if(!empty($products)){
			$s_no = 1;
			foreach($products as $product) { // foreach loop of products those we filter to category name and store wise
			
				
				$product_data = array(
									'product_id'=>$product->_id,
									'category_id'=>$getFormData['Products']['categories_category_id'],
									'store_name'=>$product->store_name,
									'product_name'=>$product->product_name,
									'brand_name'=>$product->brand_name,
									'price'=>$product->price
								);
								
				$product_json_data = json_encode($product_data);
				
			
			
			echo '<p class="MongoProductLists mongo_products list_product_for_mongo_mapping" id="'.$product->_id.'" data-product="'.htmlentities($product_json_data, ENT_QUOTES, 'UTF-8').'">'.$s_no .'. ';
			echo '<input type="radio" class="radio_'.$product->_id.'" name=Products[map_product_id] value="'.$product->_id.'">';
			echo   '<span class="mongoProductName">'.$product->product_name.'</span>';
			echo ' <b>Brand:-</b> '.$product->brand_name.' <b>Price:-</b> '.$product->price.' <a href="'.$product->url.'" target="_blank">Read More</a><br>';
			echo '<a href="javascript:void(0)" class="AddMongoproduct" product_name="'.htmlentities($product->product_name, ENT_QUOTES, 'UTF-8').'"  mongo_product_id="'.$product->_id.'"  data-toggle="modal" data-target="#AddMongoproduct" data-whatever="@mdo">Add New Product</a>';
		//echo '<a href="'.Yii::$app->getUrlManager()->getBaseUrl().'/mongo-products/update_mongo_product_features?mongo_product_id='.$product->_id.'" target="_blank">Add New Product</a>';
			echo '</p>';
			$s_no++;
			}
		}
		
	?>
	

</div>
</div>


<!--<div class="box-footer">&nbsp;</div>--->
	</div>
	
	</div>
	
	
	
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
	  <div class="box box-info">
			 <div class="box-body">	
			 	<input type="text" value="" id="searchingOurProduct" class="form-control" placeholder="search products"> <br>
					
					<div class="products-form productLists left-scrollblock">
						<div class="products-form productLists" id="search_nayashoppy_products_for_mapping">
					
					
						<h4>No Product selected</h4>
					
					
					</div>	
					</div>
					
						<div class="form-group">
							<?php echo Html::submitButton($model->isNewRecord ? 'Mapp' : 'Mapp', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>
					
				</div>
			 <!--<div class="box-footer">&nbsp;</div>-->
	</div>
	
	</div>
	</div>
	
		<?php ActiveForm::end(); ?>
		
		
</div>
</div>

<div class="modal fade" id="AddMongoproduct" tabindex="-1" role="dialog" aria-labelledby="AddMongoproduct">
	<div id="mongo_product_popup_form"></div>
</div> 
</div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
	$(window).load(function(){
		$(".searchButtonForNayashoppyProduct").trigger( "click" );	
	});
</script>

