<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Categories;
use common\models\Brands;
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;
use backend\models\ProductImage;
use common\models\Suppliers;

/* @var $this yii\web\View */
/* @var $model backend\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="products-form">

    <?php //$form = ActiveForm::begin(['action' => ['products/products_mapping'],'options' => ['method' => 'get']]); ?>
	<?php $form = ActiveForm::begin([
        'method' => 'get',
        'action' => ['products/products_mapping'],
		'id'=>'productMappSearchForm'
]); ?>
	
	
	
	<?php
		echo $form->field($model, 'store_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Suppliers::find()->andWhere(['!=','id',1])->andWhere(['!=', 'is_delete' ,1])->all(),'id','name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Store','id' => 'store_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	<?php
		// Normal select with ActiveForm & model
		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryLists();
		echo $form->field($model, 'categories_category_id')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category','id' => 'category_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	
	<?php
		// Normal select with ActiveForm & model
		echo $form->field($model, 'brands_brand_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Brands::find()->where(['status'=>'active'])->andWhere(['!=', 'is_delete' ,1])->all(),'brand_id','brand_name'),
			'language' => 'de',
			'options' => ['placeholder' => 'Select Brand'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	
	
	
	<?php
		echo  $form->field($model, 'created')->textInput(['id' => 'start_date']) ;
	?>

   <?php echo $form->field($model, 'active')->dropDownList(['fixed' => 'Auto','custom' => 'Custom', 'user_friendly' => 'User Friendly'], ['class'=>'form-control productMappType'])->label('Search Type') ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Search' : 'Search', ['class' => $model->isNewRecord ? 'btn btn-success product_mapp_search_form_button searchSubmitButton' : 'btn btn-primary product_mapp_search_form_button searchSubmitButton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
