<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\FilterGroupValues */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Filter Group Values', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filter-group-values-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'group_item_id',
            'value_id',
            'status',
            'is_delete',
            'created',
        ],
    ]) ?>

</div>
