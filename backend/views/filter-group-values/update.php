<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\FilterGroupValues */

$this->title = 'Update Filter Group Values: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Filter Group Values', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="filter-group-values-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
