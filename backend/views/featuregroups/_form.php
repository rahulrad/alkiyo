<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Categories;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\FeatureGroups */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feature-groups-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php //echo  $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Categories::find()->all(),'category_id','category_name'),	['prompt' => '-Select Category-'] ); ?>
	<?php
		// Normal select with ActiveForm & model
		echo $form->field($model, 'category_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Categories::find()->all(),'category_id','category_name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>

    <?php echo  $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php echo  $form->field($model, 'sort_order')->textInput() ?>

   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
