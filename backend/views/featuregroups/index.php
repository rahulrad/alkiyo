<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FeatureGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Feature Groups';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php echo Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
</section>
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <p>
                        <?php echo Html::a('Create Feature Groups', ['create'], ['class' => 'btn btn-success', 'style' => 'float:right']) ?>
                        &nbsp;&nbsp;
                        <?php echo Html::a('Sample CSV', ['download-sample-csv'], ['class' => 'btn btn-success', 'style' => 'float:right']) ?>
                        &nbsp;&nbsp;
                        <?php echo Html::a('Import', ['import-data'], ['class' => 'btn btn-success', 'style' => 'float:right']) ?>
                    </p>

                </div><!-- /.box-header -->
                <?php $form = \kartik\form\ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
                ]); ?>

                <div class="searchblock">
                    <div class="col-sm-4 col-lg-4 col-md-4 col-xs-12">
                        <?php
                        $categoriesModel = new \common\models\Categories();
                        echo $form->field($searchModel, 'category_id')->widget(\kartik\select2\Select2::classname(), [
                            'data' => $categoriesModel->getCategoryLists(),
                            'language' => 'eg',
                            'options' => ['placeholder' => 'Select Category'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>

                    <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12">
                        <div class="form-group" style="margin-top:24px;">
                            <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-block']) ?>
                        </div>
                    </div>
                </div>
                <?php \kartik\form\ActiveForm::end() ?>
                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            // 'id',

                            [
                                'attribute' => 'category_id',
                                'value' => 'categoriesCategory.category_name',
                            ],
                            'name',
                            'sort_order',
                            'created',
                            'modified',

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update}',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>

    </div>
</section>
