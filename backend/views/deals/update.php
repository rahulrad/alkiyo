<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model backend\models\Deals */

$this->title = 'Update Deals: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Deals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="deals-update">

    <section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>
<section class="content">	
	
	  <div class="box box-info">
			 <div class="box-body">	
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
			 <div class="box-footer">&nbsp;</div>
	</div>

</section>
</div>
