<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;


$this->title = 'Import Deals Data';
$this->params['breadcrumbs'][] = ['label' => 'Deals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>

<section class="content">	
	
	  <div class="box box-info">
	  	<div class="box-body">	
			<?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
		    <?php echo  $form->field($model,'file')->fileInput()->label('Deals Import csv file') ?>
			
		   
		    <div class="form-group">
		        <?= Html::submitButton('Save',['class'=>'btn btn-primary']) ?>
		    </div>
		<?php ActiveForm::end(); ?>
		</div>
	</div>

</section>