<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Activity Trackers';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php echo Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
</section>
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">

                </div><!-- /.box-header -->
                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'type',
                            'store_name',
                            [
                                'attribute' => 'created_at',
                                'value' => function ($data) {
                                    return date("Y-m-d", strtotime($data->created_at));
                                },
                                'filter' => \kartik\date\DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'created_at',
                                    'options' => ['placeholder' => 'Select issue date ...'],
                                    'pluginOptions' => [
                                        'format' => 'yyyy-mm-dd',
                                        'todayHighlight' => true
                                    ]
                                ]),
                            ],
                            [
                                'attribute' => 'session_id',
                                'filter' => false,
                                'contentOptions' => ['style' => 'max-width: 150px;']
                            ],
                            [
                                'attribute' => 'url',
                                'filter' => false,
                                'contentOptions' => ['style' => 'max-width: 150px;']
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>

    </div>
</section>
