<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model app\models\Blogs */

$this->title = 'Update Home Image Slider: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Home Image Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<section class="content-header">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php echo Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
</section>

<section class="content">

    <div class="box box-info">
        <div class="box-body">
            <?php echo $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
        <div class="box-footer">&nbsp;</div>
    </div>

</section>

