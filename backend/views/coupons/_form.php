<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\models\Coupons */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coupons-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<?php echo  $form->field($model, 'offer')->textInput(['maxlength' => true]) ?>
	 
    <?php echo  $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo  $form->field($model, 'sub_title')->textInput(['maxlength' => true]) ?>

   
	
	<?php echo  $form->field($model, 'offer_id')->textInput() ?>
	<?php echo  $form->field($model, 'promo_id')->textInput() ?>
	<?php echo  $form->field($model, 'offer_type')->textInput() ?>
	<?php echo  $form->field($model, 'code')->textInput() ?>
	<?php echo  $form->field($model, 'category')->textInput() ?>
	<?php echo  $form->field($model, 'offer_url')->textInput() ?>
	<?php echo  $form->field($model, 'start_date')->textInput(['id' => 'start_date']) ?>
	<?php echo  $form->field($model, 'expiry_date')->textInput(['id' => 'end_date']) ?>
	
	<?php echo  $form->field($model, 'featured')->textInput() ?>
	<?php echo  $form->field($model, 'exclusive')->textInput() ?>
	<?php echo  $form->field($model, 'ref_id')->textInput() ?>
	<?php echo  $form->field($model, 'link')->textInput() ?>
	<?php echo  $form->field($model, 'store_link')->textInput() ?>
	
    <?php echo $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>


   <?php echo  $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
