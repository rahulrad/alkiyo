<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CouponsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->controller->id;
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>
<section class="content">
    
	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
					<?php echo  Html::a('Run Scrapper', ['coupons/scraper'], ['class' => 'btn btn-success','style' => 'float:right','target'=>'_blank']) ?>
					<?php echo  Html::a('Export', ['export'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
					<?php echo  Html::a('Import', ['importdata'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
				  <p><?php echo  Html::a('Create '.Yii::$app->controller->id, ['create'], ['class' => 'btn btn-success','style' => 'float:right']) ?></p>
                </div><!-- /.box-header -->
                <div class="box-body">


				<?=Html::beginForm(['coupons/movecoupons'],'post');?>
<?=Html::submitButton('Delete', ['class' => 'btn btn-primary pull-right',]);?>
     <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
           
			[
				'attribute'=>'title',
			    'content'=> function($data){
				 return substr($data->title, 0, 35);
				}
		     ],
            //'sub_title',
            'merchant',
			'category',
            //'description:ntext',
//			'start_date',
             'expiry_date',
             'status',
            'created',

            [
			'class' => 'yii\grid\ActionColumn',
			'template' => '{update} {delete}',
			],[
			'class' => 'yii\grid\CheckboxColumn',
				'checkboxOptions' => function ($dataProvider) {
					
						return ['value'=>$dataProvider->id];
					
					
				} 
			]
        ],
    ]); ?>
	<?= Html::endForm();?> 
	</div>
	</div>
	</div>

</div>
</section>


