<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\MongoFeatureGroups */

$this->title = 'Create Mongo Feature Groups';
$this->params['breadcrumbs'][] = ['label' => 'Mongo Feature Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mongo-feature-groups-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
