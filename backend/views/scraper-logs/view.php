<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;


/* @var $this yii\web\View */
/* @var $model backend\models\ScraperLogs */

$this->title = 'View Scraper Logs';
$this->params['breadcrumbs'][] = ['label' => 'Scraper Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>

<section class="content">	
	
	  <div class="box box-info">
			 <div class="box-body">	
				<?php if(!empty($content)){
						$data = explode('Time:-',$content);
						
						if(!empty($data)){
							$i = 0;
							foreach($data as $val){
								if($i != 0){
									echo '<h4>';
									echo '( '. $i.' ) '.$val.'<br>'; 
									echo '<h4>';
								}
								
							$i++;
							}
							
						}
						
				} ?>
			</div>
			 <div class="box-footer">&nbsp;</div>
	</div>

</section>
