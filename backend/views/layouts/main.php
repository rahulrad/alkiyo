<?php
/* @var $this \yii\web\View */
/* @var $content string */


use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
	<!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8">-->
    <meta charset="<?php echo Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo Html::csrfMetaTags() ?>
    <title><?php echo Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrapper">
   <header class="main-header">
        <!-- Logo -->
        <a href="<?= Yii::$app->homeUrl; ?>" class="logo">

          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>N</b>Shoppy</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Nayashopi</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->


              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <?php echo Html::img('@web/images/user2-160x160.jpg',['class'=>'user-image','alt'=>'User Image']); ?>
                  <span class="hidden-xs"><?php echo  !Yii::$app->user->isGuest ? Yii::$app->user->identity->username:"";?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <?php echo Html::img('@web/images/user2-160x160.jpg',['class'=>'img-circle','alt'=>'User Image']); ?>
                    <p>
                      <?php echo  !Yii::$app->user->isGuest ? Yii::$app->user->identity->username:"";?> - Web Developer
                      <!--<small>Member since Nov. 2012</small>-->
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <!--<li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li>-->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <!--<div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>-->
					<div class="pull-left">
					<?php echo Html::a('Change Password', ['/users/change-password'],['class'=>'btn btn-default btn-fla']); ?>
					</div>
                    <div class="pull-right">

					<?php
						echo Html::beginForm(['/site/logout'], 'post')
								. Html::submitButton(
									'Logout',
									['class' => 'btn btn-default btn-fla']
								)
								. Html::endForm()
					?>
                     <!-- <a href="#" class="btn btn-default btn-flat">Sign out</a>-->
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>

   <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <?php echo Html::img('@web/images/user2-160x160.jpg',['class'=>'img-circle','alt'=>'User Image']); ?>
            </div>
            <div class="pull-left info">
              <p><?php echo !Yii::$app->user->isGuest ?  Yii::$app->user->identity->username:"";?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->

          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">

            <li class="header">MAIN NAVIGATION</li>
              <li class="treeview">
                  <a href="#">
                      <i class="fa fa-file-text"></i>
                      <span>Access Control</span>
                      <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                      <li><?php echo Html::a('<i class="fa fa-th"></i> <span>User Assignments</span>', ['/admin/assignment']); ?></li>
                      <li><?php echo Html::a('<i class="fa fa-th"></i> <span>Roles</span>', ['/admin/role']); ?></li>
                      <li><?php echo Html::a('<i class="fa fa-th"></i> <span>Permissions</span>', ['/admin/permission']); ?></li>
                      <li><?php echo Html::a('<i class="fa fa-th"></i> <span>Routes</span>', ['/admin/route']); ?></li>
                      <li><?php echo Html::a('<i class="fa fa-th"></i> <span>Manage User</span>', ['/user/admin']); ?></li>
                      <li><?php echo Html::a('<i class="fa fa-th"></i> <span>Create User</span>', ['/user/admin/create']); ?></li>
                  </ul>
              </li>
<!--			<li>--><?php //echo Html::a('<i class="fa fa-user"></i> <span>Users</span>', ['/users']); ?><!--</li>-->
			<li><?php echo Html::a('<i class="fa fa-file-text"></i> <span>Static Pages</span>', ['/pages']); ?></li>
	<li><?php echo Html::a('<i class="fa fa-rss"></i> <span>CMS</span>', ['/articles/items']); ?></li>
<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Categories</span>', ['/categories']); ?></li>

			<li class="treeview">
              <a href="#">
                <i class="fa fa-file-text"></i>
                <span>Sliders</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><?php echo Html::a('<i class="fa fa-th"></i> <span>Home Banner</span>', ['/sliders']); ?></li>
				<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Deals Banner</span>', ['/home-image-slider']); ?></li>


              </ul>
            </li>



			<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Brands</span>', ['/brands']); ?></li>
			<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Brand History</span>', ['/brands-history']); ?></li>
			<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Glimpses</span>', ['/glimpses']); ?></li>
			
			<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Delar</span>', ['/delar/delar']); ?></li>
			<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Product Bulk Upload</span>', ['/product/product']); ?></li>
			<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Showroom</span>', ['/showroom']); ?></li>
			
            <li><?php echo Html::a('<i class="fa fa-th"></i> <span>Activity</span>', ['/activity-tracker']); ?></li>
            <li><?php echo Html::a('<i class="fa fa-th"></i> <span>Redirects</span>', ['/redirect']); ?></li>

			<li class="treeview">
              <a href="#">
                <i class="fa fa-file-text"></i>
                <span>Products & Mapping</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><?php echo Html::a('<i class="fa fa-th"></i> <span>NS Products</span>', ['/products']); ?></li>
				<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Tags</span>', ['/tags']); ?></li>

				 <li><?php echo Html::a('<i class="fa fa-th"></i> <span>Product Mapping</span>', ['/products/products-mapping']); ?></li>
                                <li><?php echo Html::a('<i class="fa fa-th"></i> <span>Mongo Products</span>', ['/mongo-products/search_form']); ?></li>

              </ul>
            </li>


			<li class="treeview">
              <a href="#">
                <i class="fa fa-file-text"></i>
                <span>Feature Attributes</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><?php echo Html::a('<i class="fa fa-th"></i> <span>Features</span>', ['/features']); ?></li>
                <li><?php echo Html::a('<i class="fa fa-th"></i> <span>Features NS Mapping</span>', ['/product-feature-mappings']); ?></li>
				<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Feature Groups</span>', ['/featuregroups']); ?></li>
				<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Feature Values</span>', ['/featurevalues']); ?></li>
				 <li><?php echo Html::a('<i class="fa fa-th"></i> <span>Filter Groups</span>', ['/filter-groups']); ?></li>
              </ul>
            </li>

			<li class="treeview">
              <a href="#">
                <i class="fa fa-file-text"></i>
                <span>Data Scrape</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><?php echo Html::a('<i class="fa fa-th"></i> <span>Categories Data</span>', ['/categories-data']); ?></li>
				<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Product Urls</span>', ['/product-urls']); ?></li>
              </ul>
            </li>

	    <li class="treeview">
              <a href="#">
                <i class="fa fa-file-text"></i>
                <span>Menus</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><?php echo Html::a('<i class="fa fa-th"></i> <span>Menu Types</span>', ['/menu-types']); ?></li>
				<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Main Menus</span>', ['/menus/main_menu']); ?></li>
				<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Sub Menus</span>', ['/menus/sub_menu']); ?></li>
				<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Custom Menus</span>', ['/menus/custom_menu']); ?></li>
				<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Clear Menus Caches</span>', ['/menus/clear_cache']); ?></li>
				<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Footer Links</span>', ['/footer-links/index']); ?></li>
              </ul>
            </li>

			<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Suppliers</span>', ['/suppliers']); ?></li>
              <li><?php echo Html::a('<i class="fa fa-th"></i> <span>Suppliers Feature Mapping</span>', ['/product-supplier-feature-mappings/index']); ?></li>

			<li class="treeview">
              <a href="#">
                <i class="fa fa-file-text"></i>
                <span>Coupons</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>

              <ul class="treeview-menu">
				<li><?php echo Html::a('<i class="fa fa-envelope"></i> <span>Coupons vendor</span>', ['/coupons-vendors']); ?></li>
    		 <li><?php echo Html::a('<i class="fa fa-envelope"></i> <span> Coupons</span>', ['/coupons/index']); ?></li>
         	 <li><?php echo Html::a('<i class="fa fa-envelope"></i> <span>Create Coupons</span>', ['/coupons/create']); ?></li>
                <li><?php echo Html::a('<i class="fa fa-envelope"></i> <span>Coupons Category</span>', ['/coupon-categories']); ?></li>
              </ul>
            </li>



			<li><?php echo Html::a('<i class="fa fa-envelope"></i> <span>Email Templates</span>', ['/emailtemplates']); ?></li>
			<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Advertistments</span>', ['/ads']); ?></li>



			<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Contacts</span>', ['/contacts']); ?></li>
			<li><?php echo Html::a('<i class="fa fa-th"></i> <span>NewsLetters</span>', ['/newsletters']); ?></li>
			<li></li>
			<li class="treeview">
              <a href="#">
                <i class="fa fa-file-text"></i>
                <span>Logs</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><?php echo Html::a('<i class="fa fa-th"></i> <span>User Logs</span>', ['/logs']); ?></li>
				<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Scraper Logs</span>', ['/scraper-logs']); ?></li>
              </ul>
            </li>

			<!---<li><?php echo Html::a('<i class="fa fa-globe"></i> <span>Countries</span>', ['/countries']); ?></li>
			<li><?php echo Html::a('<i class="fa fa-th"></i> <span>Products Reviews</span>', ['/product-reviews']); ?></li>
			<li><?php echo Html::a('<i class="fa fa-cogs"></i> <span>Logs</span>', ['/logs']); ?></li> ---->

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
    <!-- Content Wrapper. Contains page content -->
	<?php echo Alert::widget() ?>
      <div class="content-wrapper">

	   <?php echo $content ?>
    </div>
</div>

<input type="hidden" id="base_url" value="<?php echo Yii::$app->request->baseUrl; ?>">
<input type="hidden" id="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>">


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
