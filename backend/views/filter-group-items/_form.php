<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\FilterGroups;
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;

use common\models\Categories;
use common\models\FeatureGroups;
use common\models\Features;
use common\models\FeatureValues;

/* @var $this yii\web\View */
/* @var $model backend\models\FilterGroups */
/* @var $form yii\widgets\ActiveForm */
?>
<script>

</script>
<div class="filter-groups-form">

    <?php $form = ActiveForm::begin(); ?>
	
    <?php
		// Normal select with ActiveForm & model
		$group_id = $model->group_id;
		if(isset($_GET['group_id']) && !empty($_GET['group_id'])){
			$group_id = $_GET['group_id'];
		}
		//echo $_GET['group_id'];
		$model->group_id = $group_id;
		echo $form->field($model, 'group_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(FilterGroups::find()->where(['status'=>1])->andWhere(['!=', 'is_delete' ,1])->all(),'id','title'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Brand'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
	
	<?php
		// Normal select with ActiveForm & model
		// Normal select with ActiveForm & model
		$FeaturesData = Features::find()->where(['is_filter' => 1])->select(['id','name','feature_group_id','category_id'])->all();
		
		$Features = array();
			if(!empty($FeaturesData)){
				foreach($FeaturesData as $feature){
					
					$featureGroup = FeatureGroups::find()->select(['name'])->where(['id' => $feature->feature_group_id])->one();
					
					$category = Categories::find()->select(['category_name'])->where(['category_id' => $feature->category_id])->one();
					
					$Features[$feature->id] = $feature->name.' ('.$featureGroup->name.' ~~ '. $category->category_name .' )';
				}
			}
			
			
		//$model->feature_id = $feature_id;
		echo $form->field($model, 'feature_id')->widget(Select2::classname(), [
			'data' => $Features,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Feature','id' => 'groupItemFeatureId'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		/**echo $form->field($model, 'feature_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Features::find()->where(['is_filter' => 1])->all(),'id','name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Feature','id' => 'groupItemFeatureId'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]); **/
	?>
	
	<div id="featureValuesByFeatureId"></div>
	<input type="hidden" id="groupItemId" value="<?php echo isset($_GET['id']) ? $_GET['id'] : 0; ?>">
    <?php echo  $form->field($model, 'status')->dropDownList([ '1' => 'Active', '0' => 'Inactive', ]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
