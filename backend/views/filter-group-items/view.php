<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\FilterGroupItems */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Filter Group Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filter-group-items-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'group_id',
            'title',
            'status',
            'is_delete',
            'created',
        ],
    ]) ?>

</div>
