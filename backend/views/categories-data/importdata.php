<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;


$this->title = 'Import Category Data';
$this->params['breadcrumbs'][] = ['label' => 'Categories Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>

<section class="content">	
	
	  <div class="box box-info">
	  	<div class="box-body">	
			<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
		    <?php echo  $form->field($model,'category_csv_file')->fileInput()->label('Category Data Import csv file') ?>
			
		   
		    <div class="form-group">
		        <?= Html::submitButton('Save',['class'=>'btn btn-primary']) ?>
		    </div>
		<?php ActiveForm::end(); ?>
		</div>
	</div>

</section>