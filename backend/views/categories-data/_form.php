<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Categories;
use common\models\Suppliers;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\CategoriesData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-data-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<?php //$category = array('Computer' => 'Computer', 'Mobile' => 'Mobile'); ?>
    <?php //echo $form->field($model, 'category_name')->dropDownList($category, ['prompt' => '-Select Category-']); ?>
	<?php
		// Normal select with ActiveForm & model
		
		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryListsName();
		echo $form->field($model, 'category_name')->widget(Select2::classname(), [
			'data' =>$categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	

    <?php //echo $form->field($model, 'category_type')->dropDownList($category, ['prompt' => '-Select Category Type-']); ?>

    <?php echo $form->field($model, 'url')->textarea(['rows' => 1]) ?>
	
	<?php //echo $form->field($model, 'store_name')->textInput(['maxlength' => true]) ?>
	<?php
		// Normal select with ActiveForm & model
		echo $form->field($model, 'store_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Suppliers::find()->where(['status'=>'active'])->andWhere(['!=', 'is_delete' ,1])->all(),'id','name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Store'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	
	<?php echo $form->field($model, 'path')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'count_products')->textInput() ?>
	<?php echo  $form->field($model, 'type')->dropDownList([ '0' => 'pending', '1' => 'complete', ]) ?>
    <?php echo  $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>
	
	
    <?php // echo $form->field($model, 'created')->textInput() ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
