<?php
namespace backend\models;


use Yii;

/**
 * This is the model class for table "user_logs".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $model
 * @property string $action
 * @property integer $action_id
 * @property string $activity
 * @property string $created
 */
class Logs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_logs';
    }

   

   
	/**
     * @return yii\db\ActiveQuery
     */
    public function saveUserLogs($data)
    {
	
		$activity = $data['activity'].' by '.Yii::$app->user->identity->username.' ~ '.Yii::$app->user->identity->id;
  		$user_logs = new Logs();
        $user_logs->model = $data['model'];
		$user_logs->action = $data['action'];
	    $user_logs->action_id = $data['action_id'];
		$user_logs->activity = $activity;
        $user_logs->user_id = Yii::$app->user->identity->id;
		$user_logs->created = date('Y-m-d h:i:s');
        return $user_logs->save() ? $user_logs : null;
 	}

	public function getUserDetailForEnterLog(){
		
		$user = Yii::$app->user->identity->username.' ~ '.Yii::$app->user->identity->id;
		return $user;
	}

	/**
     * @return yii\db\ActiveQuery
     */
    public function getUsername()
    {
  		return $this::hasOne(Users::className(),['id'=>'user_id'] );
 	}
   
   public function saveScraperLog($scraper_name, $file_name, $error,$error_type, $line_number){
	   /**
		$date = date('d_m_Y');
		$errorTime = date('d-m-y : h:i:sa');
		$file_path = "uploads/scraper_logs/".$date."_log.txt";
		
		if(file_exists($file_path)){
			//echo 'hello1'; die;
			$readfile = fopen($file_path, "r") or die("Unable to open file!"); // read old file
			$oldContent = fread($readfile,filesize($file_path)); // read old file content and save it in a varible
			touch($file_path);
			chmod($file_path, 0755);
			$file = fopen($file_path, "w+") or die("Unable to open file!");
			$newContent = '';
			$newContent .= $oldContent . PHP_EOL . PHP_EOL;
			$newContent .= ' Time:- '.$errorTime;
			$newContent .= ', Scraper:- '.$scraper_name;
			$newContent .= ', File:- '.$file_name;
			$newContent .= ', Line Number:- '.$line_number;
			$newContent .= ', '.$error_type.':- '.$error;
			$newContent .= PHP_EOL . PHP_EOL;
					
			echo fwrite($file,$newContent);
			fclose($file);
			
			
		}else{
			
			//echo 'hello2';die;
			touch($file_path);
			chmod($file_path, 0755);
			$file = fopen($file_path, "w+") or die("Unable to open file!");
			//chmod($file_path, 0755);
			$newContent = '';
			$newContent .= ' Time:- '.$errorTime;
			$newContent .= ', Scraper:- '.$scraper_name;
			$newContent .= ', File:- '.$file_name;
			$newContent .= ', Line Number:- '.$line_number;
			$newContent .= ', '.$error_type.':- '.$error;
			$newContent .= PHP_EOL . PHP_EOL;
			
			echo fwrite($file,$newContent);
			fclose($file);
			
			$scraperLogsModel = new ScraperLogs();
			$scraperLogsModel->file_name = $file_path;
			$scraperLogsModel->created = date('Y-m-d h:i:s');
			$scraperLogsModel->save();
			
		} 
		**/
	}
	
	
	public function getSlugFromName($name){
				
			$slug_name = str_replace(array(' ','(',')'),array('_','',''),$name);
			$slug_trim  =trim($slug_name);
			$slug = strtolower($slug_trim);
			
			return $slug;
				
	}
	
	
	public function getProductSlugFromName($name){
				
			$slug_name = str_replace(array(' ','(',')'),array('-','',''),$name);
			$slug_trim  =trim($slug_name);
			$slug = strtolower($slug_trim);
			
			return $this->slugify($slug);
				
	}

	public function slugify($string, $replace = array(), $delimiter = '-') {

                                 $clean=$string;
                                // Save the old locale and set the new locale to UTF-8
                                if (!empty($replace)) {
                                        $clean = str_replace((array) $replace, ' ', $clean);
                                }
                                $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
	    	                    $clean = strtolower($clean);
    	                            $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
                                $clean = trim($clean, $delimiter);
                                return $clean;
	}
   
}
