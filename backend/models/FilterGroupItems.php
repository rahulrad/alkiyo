<?php

namespace backend\models;

use Yii;
use backend\models\FilterGroups;

/**
 * This is the model class for table "filter_group_items".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $title
 * @property integer $status
 * @property integer $is_delete
 * @property string $created
 */
class FilterGroupItems extends \yii\db\ActiveRecord
{
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filter_group_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'status', 'is_delete','feature_id','sort_order'], 'integer'],
            [['created'], 'safe'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Filter Group',
            'title' => 'Title',
            'status' => 'Status',
            'is_delete' => 'Is Delete',
            'created' => 'Created',
			'feature_id' => 'Feature',
        ];
    }
	
	/**
     * @return yii\db\ActiveQuery
     */
    public function getFilterGroupName() {
        return $this::hasOne(FilterGroups::className(), ['id' => 'group_id']);
    }
	
	
}
