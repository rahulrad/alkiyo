<?php

namespace backend\models;

use Yii;
use yii\behaviors\SluggableBehavior;


/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $descritption
 * @property string $meta_title
 * @property string $meta_keyword
 * @property string $meta_desc
 * @property string $status
 * @property string $created
 */
class Pages extends \yii\db\ActiveRecord
{
	
	const PERMISSIONS_PRIVATE = 10;
    const PERMISSIONS_PUBLIC = 20;
	
	public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                // 'slugAttribute' => 'slug',
            ],
        ];
    }
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','descritption'], 'required'],
            [['descritption', 'meta_keyword', 'meta_desc'], 'string'],
            [['created','is_delete'], 'safe'],
            [['title', 'slug', 'meta_title'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'descritption' => 'Descritption',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_desc' => 'Meta Desc',
            'status' => 'Status',
            'created' => 'Created',
        ];
    }
}
