<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\components\simple_html_dom;
use common\components\HttpService;
use backend\models\ProductUrls;
use common\components\AmazonECS;
use common\components\Aws;
use backend\models\MongoProducts;

//require_once 'simple_html_dom.php';
//require_once 'JungleeProductDetailScrapper.php';
//require_once('Net/URL2.php');

class EbayProductScrapper {

    /**
     * get All Paging url from Junglee Site and save in DB All product pages Url.
     */
    public function getAllPagingUrl($url) {
        $strurl = str_replace('{pg}', 1, $url);

//        $response = $this->fetchContentUsingProxy($strurl);
//
//        if (!$response)
//            return false;
//
//        $html = $this->LoadHTMLContent($response);
//        if ($html->find('#resultCount')) {
//            $totalProductStr = $html->find('#resultCount span', 0)->plaintext;
//            $arr_records = explode('of', $totalProductStr);
//            $productPerPage = 24;
//            $totalProduct = $this->convert_total_pages_string($arr_records[1]);
//            $noOfPages = ceil($totalProduct / $productPerPage);
//        }
        $productPerPage = 20;
        $noOfPages = ceil(100 / $productPerPage);
        $arr_url = array();
        for ($i = 1; $i <= 8360; $i+=20) {
            $strurl = str_replace('{pg}', $i, $url);
            $arr_url[] = $strurl;
        }
        return $arr_url;
    }

    /**
     * Save All Product Pages Url in Data Base.
     */
    public function saveAllPagingUrl($arr_url, $categoryName, $categoryType) {
        $connection = Yii::$app->db;
        foreach ($arr_url as $key => $value) {
            $query = "SELECT site_url from category_url where site_url='" . $value . "'";
            $command = $connection->createCommand($query);
            $arr_result = $command->queryAll();
            $no_of_records = count($arr_result);

            if ($no_of_records == 0) {
                $insert_query = "INSERT INTO category_url (category_name,site_url, category_type) VALUES(:category_name, :site_url, :category_type)";
                $insert_command = $connection->createCommand($insert_query);
                $category_name = $categoryName;
                $insert_command->bindParam(':category_name', $category_name);
                $insert_command->bindParam(':site_url', $value);
                $insert_command->bindParam(':category_type', $categoryType);
                $insert_command->execute();
            }
        }
    }

    /**
     * Save Product In DB.
     */
    public function saveProduct($categoryName, $arr_department_type, $categoryType, $latest_product = 0, $store_name) {
		//echo $categoryName; die;
        $connection = Yii::$app->db;
        /* This condition indicate that if new product add on this category then first we set all the status as 1 and after that we updated status column as 0 of top 2 pages and save new product in our DB. */
        /* if ($latest_product == 1) {

          $update_sql= "update junglee_category_url set status=1 where category_type='" . $categoryType . "' and category_name='" . $categoryName . "' order by id_category_url";
          $update_result = yii::app()->db->createCommand($update_sql);
          $update_result->execute();

          $update_query = "update junglee_category_url set status=0 where category_type='" . $categoryType . "' and category_name='" . $categoryName . "' order by id_category_url LIMIT 2";
          $update_query = yii::app()->db->createCommand($update_query);
          $update_query->execute();
          } */

        $objEbayProduct = new \backend\models\EbayProductScrapper();
        $query = "SELECT id_category_url,category_name,site_url from category_url where status=0 and category_type='" . $categoryType . "' and category_name='" . $categoryName . "' and store_name='" . $store_name . "' order by id_category_url limit 1";
        $command = $connection->createCommand($query);
        $arr_data = $command->queryAll();
//        $arr_data[] = array('site_url'=>'http://www.flipkart.com/lc/pr/pv1/spotList1/spot1/productList?sid=tyy%2C4io&filterNone=true&start=1&ajax=true&_=1460746573007');

		
		//echo '<pre>'; print_r($arr_data); die;
        if (is_array($arr_data)) {
            foreach ($arr_data as $key => $value) {
                //if ($key == 0) {
				//echo $value['site_url'].'=======>'.$categoryName; die;
                $data = $objEbayProduct->getAllProductDetailsOnPage($value['site_url'], $categoryName);
				
				//echo '<pre>'; print_r($data); die;
                try {
                    foreach ($data as $pkey => $pval) {
					
						
                        $name = $pval['name'];
//                        if (!is_array($pval['merchant_arr']))
//                            continue;
//                        else
//                            $price = isset($pval['merchant_arr'][0]['mprice']) ? $pval['merchant_arr'][0]['mprice'] : '';
                        $price = $pval['product_price'];
                        $model = isset($pval['product_detail']['model_number']) ? $pval['product_detail']['model_number'] : '';
                        $brand = isset($pval['product_detail']['brand']) ? $pval['product_detail']['brand'] : '';
						$description = isset($pval['product_detail']['descritpion']) ? $pval['product_detail']['descritpion'] : '';
						$emi = isset($pval['product_detail']['emi']) ? $pval['product_detail']['emi'] : '';
						$cash_on_delivery = isset($pval['product_detail']['cash_on_delivery']) ? $pval['product_detail']['cash_on_delivery'] : '';
						$return_policy = isset($pval['product_detail']['return_policy']) ? $pval['product_detail']['return_policy'] : '';
						$imgpath = isset($pval['imgpath']) ? $pval['imgpath'] : '';
						$murl = isset($pval['merchant_arr']['murl']) ? $pval['merchant_arr']['murl'] : '';
						$m_unique_id = isset($pval['merchant_arr']['m_unique_id']) ? $pval['merchant_arr']['m_unique_id'] : '';
						
						$discount = isset($pval['product_detail']['discount']) ? $pval['product_detail']['discount'] : '';// vinay new
						$rating = isset($pval['product_detail']['rating']) ? $pval['product_detail']['rating'] : '';// vinay new
						$product_delivery = isset($pval['product_detail']['product_delivery']) ? $pval['product_detail']['product_delivery'] : '';// vinay new
						
						$reviews = isset($pval['product_detail']['reviews']) ? $pval['product_detail']['reviews'] : '';// vinay new
						$shipping = isset($pval['product_detail']['shipping']) ? $pval['product_detail']['shipping'] : '';// vinay new
						$original_price = isset($pval['product_detail']['original_price']) ? $pval['product_detail']['original_price'] : '';// vinay new
						$rating_user_count = isset($pval['product_detail']['rating_user_count']) ? $pval['product_detail']['rating_user_count'] : '';// vinay new
						
						// $model = \common\components\Tools::removeBrand($name, $brand);

                        //$importer = new BaseProductImporter();
                        //$importer->setName($name);

                        $sandle_cat_name = 'Sandal';
                        $slipper_cat_name = 'Slipper';

                        if (stristr($name, $sandle_cat_name)) {
                            //$importer->setCategory('Sandals');
                            $importer = new BaseEbayProductImporter('Sandals', $name);
                        } elseif (stristr($name, $slipper_cat_name)) {
                            //$importer->setCategory('Slippers');
                            $importer = new BaseEbayProductImporter('Slippers', $name);
                        } else {
                            //$importer->setCategory($value['category_name']);
                            $importer = new BaseEbayProductImporter($value['category_name'], $name);
							
                        }

                        $importer->setLowestPrice($price);
                        $importer->setLinkRewrite($name);
                        $importer->setModel($model);
                        $importer->setBrand($brand);
						$importer->setdescription($description);
						$importer->setProductEMI($emi);
						$importer->setCategory($value['category_name']);
						
						$importer->setCOD($cash_on_delivery);
						$importer->setReturnPolicy($return_policy);
						$importer->setMainImages($imgpath);
						$importer->setProducturl($murl);
						$importer->setProductUniqueId($m_unique_id);
						
						$importer->setProductDiscount($discount);
						$importer->setProductRating($rating);
						$importer->setProductDelivery($product_delivery);
						
						$importer->setProductReviews($reviews);
						$importer->setProductShipping($shipping);
						$importer->setProductOriginalPrice($original_price);
						$importer->setProductRatingUserCount($rating_user_count);
						
						
						$importer->setMultipleImages($pval['product_img_arr']);
						$importer->setProductFeature($pval['product_features']);
						
                       
                        $importer->save();
                    }

                    $q = "update category_url SET status = '1' where id_category_url = '" . $value['id_category_url'] . "'";
                    echo $q;
                    $comm = $connection->createCommand($q);
                    $comm->bindParam(':id_category_url', $key);
                    $comm->execute();
                } catch (Exception $e) {
                    echo "wwwwwwwwwww".$e->getMessage().'::'.$e->getLine().'::'.$e->getTraceAsString();exit;
                    continue;
                }
            }
        }
    }

    /**
     * get HTML Data finding with XPath.
     */
    private function getProductUrlOnPage($url, $categoryName) {
		
		// get data from url in json
		$json = file_get_contents($url);
		
		// we are doing utf8_encode for json data
		$foo = utf8_encode($json);
		
		// json encode for get data form url
		$resultData = json_decode($foo, true);
		
		//echo '<pre>'; print_r($resultData); die;
		if(empty($resultData)){
			return false;
		}
			$productData = $resultData['findItemsByCategoryResponse'][0]['searchResult'][0]['item'];
			if(isset($productData) && !empty($productData)){
					$productDetail = array();
					$i = 0;
					foreach($productData as $data){
						//echo '<pre>'; print_r($data);// die;
						$cod = '';
						if(in_array('COD',$data['title'])){ 
							$cod = 'Cash On Delivery'; 
						}
						
						
						$product_shipping = '';
						if(isset($data['shippingInfo'][0]['shippingType'][0])){ 
							$product_shipping = $data['shippingInfo'][0]['shippingType'][0]; 
						}
						
						$productDetail[$i]['title'] = isset($data['title'][0]) ? $data['title'][0] : '';
						$productDetail[$i]['subtitle'] = isset($data['subtitle'][0]) ? $data['subtitle'][0] : '';
						$productDetail[$i]['price'] = isset($data['sellingStatus'][0]['currentPrice'][0]['__value__']) ? $data['sellingStatus'][0]['currentPrice'][0]['__value__'] : '';
						$productDetail[$i]['url'] = isset($data['viewItemURL'][0]) ? $data['viewItemURL'][0] : '';
						$productDetail[$i]['cod'] = $cod;
						$productDetail[$i]['product_shipping'] = $product_shipping;
					
					$i++;					
					}
				}
				
			return $productDetail;
    }

    private function getAllProductDetailsOnPage($url, $categoryName) {
        $product_urls = $this->getProductUrlOnPage($url, $categoryName);
		
        $product_details = array();
        if (is_array($product_urls)) {
            foreach ($product_urls as $key => $value) {
				//echo '<pre>value'; print_r($value); die;
                $product_details[] = $this->getProductDetail($value['url'], $categoryName,$value);
            }
        }

      
        return $product_details;
    }

    private function getProductDetail($url, $category, $productDetail) {
		
        $objEbay_product_detail = new \backend\models\EbayProductDetailScrapper($url, $category,$productDetail);
	
        return $objEbay_product_detail->StoreDataInArray();
    }
	
	

    /**
     * Return site content from a specifice site url.
     */
    private function fetchContentUsingProxy($strurl) {
        $objHttpService = new \common\components\HttpService();
        $response = $objHttpService->getResponse($strurl);
        return $response;
    }

    /**
     * Load HTML
     */
    private function LoadHTMLContent($content) {
        $html = new \common\components\simple_html_dom();
        $html->load($content);
        unset($content);
        return $html;
    }

    /**
     * Convert No of Total Pages in a string.
     */
    private function convert_total_pages_string($userString) {
        $userString = str_ireplace('rs.', '', strtolower($userString));
        $money = explode(".", $userString);

        $dollars = preg_replace("/[^0-9]/", null, $money[0]);

        $cents = '';
        if (isset($money[1])) {
            $cents = preg_replace("/[^0-9]/", null, $money[1]);
            $result = $dollars . "." . $cents;
        } else {
            $result = $dollars;
        }

        return($result);
    }
	
	
	
	/***************************** Save Product Data ***********************************/
	
	
	
	public function saveProductData($productUrlData,$categoryName,$queueMessageData=null){
		
				$exitProduct = MongoProducts::find()->where(['url' => $productUrlData->url])->one();
				
				if(!empty($exitProduct)){
					$ProductUrlsModel = ProductUrls::findOne($productUrlData->id);
					$ProductUrlsModel->status = 1;
					$ProductUrlsModel->save(false);
					
					
					if(!empty($queueMessageData)){
						$aws = new \common\components\Aws();
						$aws = $aws->deleteQueueMessage($queueMessageData,'product_scraper');
						
					}
				}
			
			
			$connection = Yii::$app->db;
			

			$objebayProduct = new \backend\models\EbayProductScrapper();
			
			if ($productUrlData) {
					//echo '<pre>'; print_r($productUrlData->other); die;
					$data = preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $productUrlData->other);
					$other = unserialize($data);
					
					$productDetail = !empty($other) ? $other: array();
					//echo '<pre>'; print_r($productDetail); die;
					$pval = $objebayProduct->getProductDetail($productUrlData->url, $categoryName,$productDetail);
				    //echo '<pre>tt'; print_r($pval); die;
				   try {
						
						if(!empty($pval)){
							//echo '<pre>'; print_r($pval); die;
							$name = $pval['name'];
							$price = $pval['product_price'];
							$model = isset($pval['product_detail']['model_number']) ? $pval['product_detail']['model_number'] : '';
							$brand = isset($pval['product_detail']['brand']) ? $pval['product_detail']['brand'] : '';
							$description = isset($pval['product_detail']['descritpion']) ? $pval['product_detail']['descritpion'] : '';
							$emi = isset($pval['product_detail']['emi']) ? $pval['product_detail']['emi'] : '';
							$cash_on_delivery = isset($pval['product_detail']['cash_on_delivery']) ? $pval['product_detail']['cash_on_delivery'] : '';
							$return_policy = isset($pval['product_detail']['return_policy']) ? $pval['product_detail']['return_policy'] : '';
							$imgpath = isset($pval['imgpath']) ? $pval['imgpath'] : '';
							$murl = isset($pval['merchant_arr']['murl']) ? $pval['merchant_arr']['murl'] : '';
							$m_unique_id = isset($pval['merchant_arr']['m_unique_id']) ? $pval['merchant_arr']['m_unique_id'] : '';
							
							$discount = isset($pval['product_detail']['discount']) ? $pval['product_detail']['discount'] : '';// vinay new
							$rating = isset($pval['product_detail']['rating']) ? $pval['product_detail']['rating'] : '';// vinay new
							$product_delivery = isset($pval['product_detail']['product_delivery']) ? $pval['product_detail']['product_delivery'] : '';// vinay new
							$reviews = isset($pval['product_detail']['reviews']) ? $pval['product_detail']['reviews'] : '';// vinay new
							
							$shipping = isset($pval['product_detail']['shipping']) ? $pval['product_detail']['shipping'] : '';// vinay new
							$original_price = isset($pval['product_detail']['original_price']) ? $pval['product_detail']['original_price'] : '';// vinay new
							$rating_user_count = isset($pval['product_detail']['rating_user_count']) ? $pval['product_detail']['rating_user_count'] : '';// vinay new
							
							// $model = \common\components\Tools::removeBrand($name, $brand);

							//$importer = new BaseProductImporter();
							//$importer->setName($name);

							$importer = new BaseEbayProductImporter($categoryName, $name);
							
							//echo $price; die;
							$importer->setLowestPrice($price);
							$importer->setLinkRewrite($name);
							$importer->setModel($model);
							$importer->setBrand($brand);
							$importer->setdescription($description);
							$importer->setProductEMI($emi);
							$importer->setCategory($categoryName);
							
							$importer->setCOD($cash_on_delivery);
							$importer->setReturnPolicy($return_policy);
							$importer->setMainImages($imgpath);
							$importer->setProducturl($murl);
							$importer->setProductUniqueId($m_unique_id);
							
							$importer->setProductDiscount($discount);
							$importer->setProductRating($rating);
							$importer->setProductDelivery($product_delivery);
							$importer->setProductReviews($reviews);
							$importer->setProductShipping($shipping);
							$importer->setProductOriginalPrice($original_price);
							$importer->setProductRatingUserCount($rating_user_count);
							
							$importer->setMultipleImages($pval['product_img_arr']);
							$importer->setProductFeature($pval['product_features']);
							
							$importer->setProductUrlId($productUrlData->id);
							
							$importer->setQueueMessageData($queueMessageData);// vinay 13 oct
							
							
						   // $importer->setReferenceNumber($ref_no);

							$front_image = array();
							$product_images = array();
							
							$importer->save();
							
							/**$q = "update product_urls SET status = '1' where id = '" . $productUrlData->id . "'";
							echo $q.'</br>';
							$comm = $connection->createCommand($q);
							$comm->bindParam(':id_category_url', $key);
							$comm->execute(); **/
						}
						
						

						
					} catch (Exception $e) {
						echo "wwwwwwwwwww".$e->getMessage().'::'.$e->getLine().'::'.$e->getTraceAsString();exit;
						continue;
					} 
				   
			}
		
	}
	
	

}
