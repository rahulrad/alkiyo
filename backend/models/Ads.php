<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ads".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $description
 * @property string $image
 * @property string $meta_title
 * @property string $meta_keyword
 * @property string $meta_desc
 * @property string $status
 * @property string $created
 */
class Ads extends \yii\db\ActiveRecord
{
    
    public $file;
    
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'ads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'meta_keyword', 'meta_desc'], 'string'],
            [['created','start_date','end_date',], 'safe'],
            [['title', 'url', 'image', 'meta_title', 'status'], 'string', 'max' => 255],
            [['file'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
            'description' => 'Description',
            'image' => 'Image',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_desc' => 'Meta Desc',
            'status' => 'Status',
            'created' => 'Created',
            'file' => 'Image'
        ];
    }

    public function getImageurl()
    {
        return \Yii::$app->request->BaseUrl.'/'.$this->image;
    }
    
}
