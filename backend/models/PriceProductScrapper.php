<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\components\simple_html_dom;
use common\components\HttpService;
use common\models\Products;
use common\models\ProductsSuppliers;
use common\components\Aws;

//require_once 'simple_html_dom.php';
//require_once 'JungleeProductDetailScrapper.php';
//require_once('Net/URL2.php');

class PriceProductScrapper {

    
	/**
	 *	update product price from product url
	 */
	 
	 public function update_product_prices($productDetail,$messageData=null){
			//echo '<pre>'; print_r($messageData); die;
		 
			if(!empty($productDetail)){
				$url = $productDetail->url;
				$store_id = $productDetail->store_id;
				$price = $productDetail->price;
				$original_price = $productDetail->price;
				
				$newPriceData = $this->getProductPriceDetail($url,$store_id,$price,$original_price);
				
				$ProdSuppliersData = ProductsSuppliers::find()->where(['id'=>$productDetail->id])->one();
				$ProdSuppliersData = $ProdSuppliersData;
				$ProdSuppliersData->price_status = 1;
				$ProdSuppliersData->created = date('Y-m-d h:i:s');
				$ProdSuppliersData->save(false);
				
				
				$aws = new \common\components\Aws();
				$aws = $aws->deleteQueueMessage($messageData,'price_scraper');
				
				if($newPriceData['price'] < $price){
						//echo 'newprice=>'.$newPriceData['price'].'====== old price==>'.$price;
						//echo '<pre>'; print_r($productDetail); 
						
						$ProductsSuppliersData = ProductsSuppliers::find()->where(['id'=>$productDetail->id])->one();
					
						$ProductsSuppliersModel = $ProductsSuppliersData;
						$ProductsSuppliersModel->price = $newPriceData['price'];
						$ProductsSuppliersModel->original_price = $newPriceData['original_price'];
						$ProductsSuppliersModel->save(false);

						//echo '<pre>'; print_r($ProductsSuppliersModel); die;
						
						$productModel = new Products();
						$productModel->productSave($ProductsSuppliersModel);
						
				}
				
				
			}
	 }
	 
	  private function getProductPriceDetail($url,$store_id,$price,$original_price) {
        $objPrice_product_price_detail = new \backend\models\PriceProductDetailScrapper($url, $store_id,$price,$original_price);
        return $objPrice_product_price_detail->StoreDataInArray();
    }

    
}
