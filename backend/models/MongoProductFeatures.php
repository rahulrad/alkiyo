<?php

namespace backend\models;

use Yii;
use common\models\Features;
use common\models\FeatureValues;
use common\models\FeatureGroups;
use common\models\ProductFeature;

/**
 * This is the model class for collection "MongoProductFeatures".
 *
 * @property \MongoId|string $_id
 * @property mixed $id_product_feature
 * @property mixed $id_product
 * @property mixed $id_feature
 * @property mixed $id_feature_value
 */
class MongoProductFeatures extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['nayashoppy', 'MongoProductFeatures'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'id_product_feature',
            'id_product',
            'id_feature',
            'id_feature_value',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product_feature', 'id_product', 'id_feature', 'id_feature_value'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'id_product_feature' => 'Id Product Feature',
            'id_product' => 'Id Product',
            'id_feature' => 'Id Feature',
            'id_feature_value' => 'Id Feature Value',
        ];
    }
	
	
	
    public function getMongoProductFeatures($mongo_product_id) {
		//$mongo_product_id = 1;
		
        $productFeaturesWithValues = array();
		
		

        $mongProductFeatureModel = new MongoProductFeatures();
		$mongoProductFeatures = $mongProductFeatureModel::find()->where(['id_product' => (int) $mongo_product_id])->all();
		//echo '<pre>'; print_r($mongoProductFeatures); die;
        //$mongoProductFeatures = $mongProductFeatureModel::find()->where(['id_product' => (int) $mongo_product_id])->groupBy(['id_feature'])->all();
		//echo '<pre>'; print_r($mongoProductFeatures); die;
		
		$mongoFeatureArr = array();
		foreach($mongoProductFeatures as $mongo_pro){
			
			$mongoFeatureArr[$mongo_pro->id_feature] = $mongo_pro->id_feature;
			
		
		}
		
        if (!empty($mongoFeatureArr)) {

            $i = 1;
            foreach ($mongoFeatureArr as $mongoProductFeatureID) {

                $MongoFeaturesModel = new MongoFeatures();
                $MongoFeatureValuesModel = new MongoFeatureValues();

                //$productFeaturesWithValues[$i]['Product']['features'] = $FeaturesModel::find()->where(['id' => (int)$productFeature->id_feature])->one();
                //$productFeaturesWithValues[$i]['Product']['feature_values'] = $FeaturesModel::find()->where(['id' => (int)$productFeature->id_feature_value])->one();
                // get feature to product id_feature
                $features = $MongoFeaturesModel::find()->where(['id' => (int) $mongoProductFeatureID])->one();

                $features = array('id' => $features->id, 'name' => $features->name);
                $productFeaturesWithValues[$i]['features'] = $features;

                // get product feature values 
                $featurevalues = $MongoFeaturesModel->findFeatureValuesToFeatureId($features['id']);
                $productFeaturesWithValues[$i]['features']['feature_value'] = $featurevalues;

                // get feature value to product id_feature_value
                //$featureValues = $FeatureValuesModel::find()->where(['id' => (int)$productFeature->id_feature_value])->one();
                //$feature_values = array('id' => $featureValues->id,'value'=>$featureValues->value);
                //$productFeaturesWithValues[$i]['Product']['feature_values']= $feature_values;


                $i++;
            }
			//echo '<pre>'; print_r($productFeaturesWithValues); die;
            return $productFeaturesWithValues;
        }
        return $productFeaturesWithValues;
    }

    public function getSelectedProductFeatures($mongo_product_id) {
		
		//$mongo_product_id = 1;
        $MongoProductFeaturesModel = new MongoProductFeatures();

        $SelectedFeatureValues = $MongoProductFeaturesModel::find()->where(['id_product' => (int) $mongo_product_id])->all();
		//echo '<pre>'; print_r($mongo_product_id); die;
        $featureValueArr = array();

        foreach ($SelectedFeatureValues as $feature_val) {
            $featureValueArr[] = $feature_val->id_feature_value;
        }

        return $featureValueArr;
    }
	
	
	
	public function saveMongoProductFeatureData($product_id, $category_id, $mongoProductFeature){
		
		$mongoFeatureGroupsModel = new MongoFeatureGroups();
		$mongoFeatureGroupsModel->saveProductFeatureGroup($product_id, $category_id,$mongoProductFeature);
		
	}
	
	
	
	public function saveProductFeatureFromMongoDB($product_id, $feature_id,$feature_value_id){
				
				if(!empty($product_id) && !empty($feature_id) && !empty($feature_value_id)){
						
						$product_feature = ProductFeature::findOne(['id_product'=>  $product_id, 'id_feature' => $feature_id,'id_feature_value'=> $feature_value_id]);
						
						if ($product_feature == null) {
						  
						//   Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');   
									$product_feature_model = new ProductFeature();
									$product_feature_model->id_product = $product_id;
									$product_feature_model->id_feature = $feature_id;
									$product_feature_model->id_feature_value = $feature_value_id;
									$product_feature_model->save();
									
									$product_feature = ProductFeature::findOne(['id_product'=>  $product_id, 'id_feature' => $feature_id,'id_feature_value'=> $feature_value_id]);
						}  
						//$this->saveProductFeature($product_id, $feature_id,$feature_value_data->id);
				return $product_feature;
				}	
		}
	
	
	
	
}
