<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "scraper_logs".
 *
 * @property integer $id
 * @property string $file_name
 * @property string $created
 */
class ScraperLogs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scraper_logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_name'], 'string', 'max' => 255],
            [['created'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_name' => 'File Name',
            'created' => 'Created',
        ];
    }
}
