<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class PriceProductDetailScrapper
{
    public $arr;

    public function __construct($url, $store_id,$price,$original_price)
    {
		
        $response = $this->fetchContentUsingProxy($url,$store_id);
		
        if (!$response)
            return false;

		if($store_id == 1){ // flipkart store
			$html = $response;
		}else{ // all other  store
			$html = $this->LoadHTMLContent($response);
		}
		
		
        $retrnarr = $this->getHTMLUsingXPath($html, $store_id, $url,$price,$original_price);
        $this->arr = $retrnarr;
       
	   if($store_id != 1){
			$html->clear();
			unset($html);
	   }
    }

    /**
     * Return site content from a specifice site url.
     */
    private function fetchContentUsingProxy($strurl,$store_id)
    {
        $objHttpService = new \common\components\HttpService();
		
		if($store_id == 1){ // flipkart store
			 $response = $objHttpService->getFlipkartProductDetail($strurl);
		}elseif($store_id == 5){ // amazon store
			 $response = $objHttpService->getAmazonProductDetail($strurl);
		}else{ // all other stores
			 $response = $objHttpService->getResponse($strurl);
		}
		
        return $response;
    }

    /**
     * Load HTML
     */
    private function LoadHTMLContent($content)
    {
        $html = new \common\components\simple_html_dom();
        $html->load($content);
        unset($content);
        return $html;
    }

    /**
     * Sotre Data in Array Format.
     */
    public function StoreDataInArray()
    {
        return $this->arr;
    }
	
	private function getHTMLUsingXPath($html, $store_id, $url,$price,$original_price)
    {
		
		
		$productPriceData = array();
		$productPriceData['price'] = $price;
		$productPriceData['original_price'] = $original_price;
		
		if($store_id == 1){ // flipkart product price
			// product price
			if(isset($html->RESPONSE->data->product_summary_1->data[0]->value->pricing->finalPrice->value)){
				$productPriceData['price'] = $html->RESPONSE->data->product_summary_1->data[0]->value->pricing->finalPrice->value;
			}else{
				$productPriceData['price'] = $price;
			}
			
			// original price
			if(isset($html->RESPONSE->data->product_summary_1->data[0]->value->pricing->prices[0]->value)){
				$productPriceData['original_price'] = $html->RESPONSE->data->product_summary_1->data[0]->value->pricing->prices[0]->value;
			}else{
				$productPriceData['original_price'] = $original_price;
			}
			
		}elseif($store_id == 2){ // paytm product price
			
			$productPriceData['price'] = $price;
			$productPriceData['original_price'] = $original_price;
		
		}elseif($store_id == 3){ // snapdeal product price
			
			if ($html->find('.payBlkBig', 0)) {
                $productPrice = $html->find('.payBlkBig', 0)->plaintext;
                $productPriceData['price'] = $this->convet_price_string($productPrice);
            }else{
				$productPriceData['price'] = $price;
			}
			
			// original price
			 if ($html->find('.pdp-e-i-PAY-r', 0)) {
				 if ($html->find('.pdpCutPrice', 0)) {
					$productOriginalPrice = $html->find('.pdpCutPrice', 0)->plaintext;
					$productPriceData['original_price'] = $this->convet_price_string($productOriginalPrice);
				}
			}else{
				$productPriceData['original_price'] = $original_price;
			}
			
		}elseif($store_id == 4){ // shopclues price
			
			 if ($html->find('.product-pricing', 0)) {
				if($html->find('.price', 0)){
					$productPrice = $html->find('.price', 0)->plaintext;
					$productPriceData['price'] = $this->convet_price_string($productPrice);
				}
            }else{
				$productPriceData['price'] = $price;
			}
			
			// original price
			$productPriceData['original_price'] = $original_price;
			
			
		}elseif($store_id == 5){ // amazon price
			
			if ($html->find('#priceblock_ourprice', 0)) {
					$productPrice = $html->find('#priceblock_ourprice', 0)->plaintext;
					$productPriceData['price'] = $this->convet_price_string($productPrice);
			}else{
				$productPriceData['price'] = $price;
			}
			
			// original price
			if ($html->find('#price', 0)) {
				 $priceDiv = $html->find('#price', 0);
				 if ($priceDiv->find('.a-text-strike', 0)) {
					$productOriginalPrice = $priceDiv->find('.a-text-strike', 0)->plaintext;
					$productPriceData['original_price'] = $this->convet_price_string($productOriginalPrice);
				}
			}else{
				$productPriceData['original_price'] = $original_price;
			}
			
		}elseif($store_id == 6){ // jabong price
			
			if ($html->find('.content', 0)) {
				if ($html->find('.price', 0)) {
					if($html->find('.actual-price', 0)){
						$productPrice = $html->find('.actual-price', 0)->plaintext;
						$productPrice = $this->convet_price_string($productPrice);		
					}
					
				}
			}else{
				$productPriceData['price'] = $price;
			}
			
			// original price
			if ($html->find('.content', 0)) {
				if ($html->find('.price', 0)) {
					if($html->find('.standard-price', 0)){
						$productOriginalPrice = $html->find('.standard-price', 0)->plaintext;
						$productPriceData['original_price'] = $this->convet_price_string($productOriginalPrice);		
					}
					
				}
			}else{
				$productPriceData['original_price'] = $original_price;
			}
			
		}elseif($store_id == 7){ // ebay price
			
			if ($html->find('#prcIsum', 0)) {
					$productPrice = $html->find('#prcIsum', 0)->plaintext;
					$productPriceData['price'] = $this->convet_price_string($productPrice);
			}else{
				$productPriceData['price'] = $price;
			}
			
			// original price
			$productPriceData['original_price'] = $original_price;
		}
		
		//echo '<pre>'; print_r($productPriceData); die;
		//echo $productPrice; die;
        return $productPriceData;
    } 

    /**
     * get HTML Data finding with XPath.
     */
    /**private function getHTMLUsingXPath($html, $store_id, $url,$price,$original_price)
    {
	//echo 'hello'; die;
		$productPriceData = array();
		$productPriceData['price'] = $price;
		$productPriceData['original_price'] = $price;
		
		if($store_id == 1){ // flipkart product price

			if ($html->find('.pricing', 0)) {
				if ($html->find('.selling-price', 0)) {
					$productPrice = $html->find('.selling-price', 0)->plaintext;
					$productPrice = $this->convet_price_string($productPrice);
				}
			}else{
				$productPrice = $price;
			}
		
		}elseif($store_id == 2){ // paytm product price
			
			$productPrice = $price;
		
		}elseif($store_id == 3){ // snapdeal product price
			
			if ($html->find('.payBlkBig', 0)) {
					$productPrice = $html->find('.payBlkBig', 0)->plaintext;
					$productPrice = $this->convet_price_string($productPrice);
			}else{
				$productPrice = $price;
			}
			
		}elseif($store_id == 4){ // shopclues price
			
			 if ($html->find('.product-pricing', 0)) {
				if($html->find('.price', 0)){
					$productPrice = $html->find('.price', 0)->plaintext;
					$productPrice = $this->convet_price_string($productPrice);
				}
            }else{
				$productPrice = $price;
			}
		}elseif($store_id == 6){ // jabong price
			
			 if ($html->find('.content', 0)) {
				if ($html->find('.price ', 0)) {
					if($html->find('.productDiscount', 0)){
						$productDiscount = $html->find('.productDiscount', 0)->plaintext;
						preg_match_all('!\d+!', $productDiscount, $matches);
						$product_detail['discount'] = $matches[0][0];
					}
					
				}
			}else{
				$productPrice = $price;
			}
		}elseif($store_id == 7){ // ebay price
			if ($html->find('#prcIsum', 0)) {
					$productPrice = $html->find('#prcIsum', 0)->plaintext;
					$productPrice = $this->convet_price_string($productPrice);
			}else{
				$productPrice = $price;
			}
		}
		//echo $productPrice; die;
        return $productPrice;
    } **/
	

    private function convet_price_string($userString)
    {
        $userString = str_ireplace('rs.', '', strtolower($userString));
        $money = explode(".", $userString);

        $dollars = preg_replace("/[^0-9]/", null, $money[0]);

        $cents = '';
        if (isset($money[1])) {
            $cents = preg_replace("/[^0-9]/", null, $money[1]);
            $result = $dollars . "." . $cents;
        } else {
            $result = $dollars;
        }

        return($result);
    }
	
	
}
