<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "category_url".
 *
 * @property integer $id_category_url
 * @property string $category_name
 * @property string $site_url
 * @property string $category_type
 * @property integer $status
 * @property string $createdate
 */
class CategoryUrl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category_url';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['status','store_id','scraper_group'], 'integer'],
            [['createdate','scraper_group_type'], 'safe'],
            [['category_name', 'category_type'], 'string', 'max' => 45],
            [['site_url'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_category_url' => 'Id Category Url',
            'category_name' => 'Category Name',
            'site_url' => 'Site Url',
            'category_type' => 'Category Type',
            'status' => 'Status',
            'createdate' => 'Createdate',
			'store_id' => 'store_id',
        ];
    }
	
	
	public function changeCategoryName($oldCategoryName, $newCategoryName){
		
		$categoryUrlIds = CategoryUrl::find()->select(['id_category_url','category_name'])->where(['category_name'=>$oldCategoryName])->all();
		
		if(!empty($categoryUrlIds)){
			
			foreach($categoryUrlIds as $categoryUrl){
				
				$model = CategoryUrl::findOne($categoryUrl->id_category_url);
				$model->category_name = $newCategoryName;
				$model->category_type = $newCategoryName;
				$model->save(false);
				
			}
		}
	}
 
 public function getLastInsertCategoryUrl($data){
		
		$categoryUrls = CategoryUrl::find()->where(['category_name'=>$data->category_name,'store_id'=>$data->store_id])->count();
		
		return $categoryUrls;
		
		
 }
 
}
