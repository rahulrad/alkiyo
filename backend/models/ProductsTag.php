<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "products_tag".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $tag_id
 * @property string $status
 * @property string $created
 */
class ProductsTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'tag_id'], 'integer'],
            [['created'], 'safe'],
            [['status'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'tag_id' => 'Tag ID',
            'status' => 'Status',
            'created' => 'Created',
        ];
    }
	
	public function saveProductsTag($product_tags, $product_id){
		
		foreach($product_tags as $tag){
				
				$ProductsTagModel = new ProductsTag();
				
				$ProductsTagModel->tag_id = $tag;
				
				$ProductsTagModel->product_id = $product_id;
				
				$ProductsTagModel->created = date('Y-m-d h:i:s');
				
				$ProductsTagModel->save();
				
		}
		
	}
	
	
}
