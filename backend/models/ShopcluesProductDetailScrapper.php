<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class ShopcluesProductDetailScrapper
{
    public $arr;

    public function __construct($url, $categoryName="")
    {
        $response = $this->fetchContentUsingProxy($url);
        if (!$response)
            return false;

        $html = $this->LoadHTMLContent($response);
		
        $retrnarr = $this->getHTMLUsingXPath($html, $categoryName, $url);
        $this->arr = $retrnarr;
        $html->clear();
		
        unset($html);
    }

    /**
     * Return site content from a specifice site url.
     */
    private function fetchContentUsingProxy($strurl)
    {
        $objHttpService = new \common\components\HttpService();
        $response = $objHttpService->getResponse($strurl);
        return $response;
    }

    /**
     * Load HTML
     */
    private function LoadHTMLContent($content)
    {
        $html = new \common\components\simple_html_dom();
        $html->load($content);
        unset($content);
        return $html;
    }

    /**
     * Sotre Data in Array Format.
     */
    public function StoreDataInArray()
    {
        return $this->arr;
    }

    /**
     * get HTML Data finding with XPath.
     */
    private function getHTMLUsingXPath($html, $categoryName, $url)
    {
		$scraperLog = new Logs();
		//echo $url;
        $siteName = 'http://www.shopclues.com';
        $returnarray = array();
        $returnarray['name'] = '';
        $returnarray['imgpath'] = '';
        $returnarray['product_detail'] = '';
        $returnarray['merchant_arr'] = '';

        $productNamehtml = '';
        $productPrice = '';
        $productImgSrchtml = '';
        $productDetailhtml = '';
        $merchantNamehtml = 'BabyOye';
		
		$product_unique_id = '';
		$productDeliverd = '';
		$productReturnPolicy = '';
		$productFeatures = '';
		$productCOD = '';

	//$url = 'http://www.shopclues.com/panasonic-eluga-i2-2gb-ram-metallic-grey.html';	
        $arr_merchant = '';
		// product title
        if ($html->find('.product-about', 0)) {
			if($html->find('.name h1', 0)){
				$productNamehtml = $html->find('.name h1', 0)->plaintext;
				$productNamehtml = trim($productNamehtml);
			}else{
				$scraperLog->saveScraperLog('shopclues scraper','ShopcluesProductDetailScrapper','.name h1 class not found','Not Found',__LINE__);
			}
        }else{
				$scraperLog->saveScraperLog('snapdeal scraper','ShopcluesProductDetailScrapper','.product-about class not found','Not Found',__LINE__);
		}
		
		
		// product price
         if ($html->find('.product-pricing', 0)) {
				if($html->find('.price', 0)){
					$productPrice = $html->find('.price', 0)->plaintext;
					$productPrice = $this->convet_price_string($productPrice);
				}else{
					$scraperLog->saveScraperLog('snapdeal scraper','ShopcluesProductDetailScrapper','.price class not found','Not Found',__LINE__);
				}
            }else{
				$scraperLog->saveScraperLog('snapdeal scraper','ShopcluesProductDetailScrapper','.product-pricing class not found','Not Found',__LINE__);
		}
		
            
        $productImgSrchtml = '';  
	$arr_product_images = array();
		if ($html->find('.slides', 0)) {
			$im = 1;
            foreach ($html->find(".slides li") as $skey => $sval) {
                    if($im <= 4){
                        $image = 'http://cdn.shopclues.net'.$sval->find('input', 0)->getAttribute('value');
                        if($im == 1){
                            $productImgSrchtml = $image;
                        }
			$arr_product_images[] = $image;
                    }  
          	 $im++;
		    }
        }
         
	
		
		
		// product unique id
                
        if ($html->find('.product-about', 0)) {
                $product_unique_id = $html->find('.name span span', 0)->plaintext;
                $product_unique_id = $this->convet_price_string($product_unique_id);
            }
		
		
		
		$arr_merchant = array('mname' => $merchantNamehtml, 'mprice' => $productPrice, 'murl' => $url, 'm_unique_id' => $product_unique_id);
		
		
       // $arr_merchant[] = array();

       /* if ($html->find('.proDescript', 0)) {
             $productDetailhtml = $html->find('.proDescript', 0)->plaintext;
        }*/
		//echo $url; 
		
		// product details 
		$product_detail = array();
		 
		 // product category
		 $product_detail['Category'] = $categoryName;
		 $product_detail['shipping'] = 'Free Shipping';
		 
		 
		 
		// product original price
		
		
		
		$product_detail['original_price'] = $productPrice;
		
		if ($html->find('.product-pricing', 0)) {
				$original_price_class = '#sec_list_price_'.$product_unique_id;
				if($html->find($original_price_class, 0)){
					$productOriginalPrice = $html->find($original_price_class, 0)->plaintext;
					 $product_detail['original_price'] = $this->convet_price_string($productOriginalPrice);
				}
                
            }
				// product count users for product rating
				$product_detail['rating_user_count'] = '';
				 if ($html->find('.reviews', 0)) {
					 if ($html->find('.review span', 0)) {
						$productReviews = trim($html->find('.review span', 0)->plaintext);
						$product_detail['rating_user_count'] = (int)$productReviews;
					 }
					
				}
		
                  $product_detail['cash_on_delivery'] = '';
                 if ($html->find('.product-discounts span', 5)) {
                      $product_detail['cash_on_delivery'] = trim($html->find('.product-discounts span', 5)->plaintext);
                }
               // echo $product_detail['cash_on_delivery']; die;
                $product_detail['discount'] = '';
                if ($html->find('#product_save .off', 0)) {
                      $discount = $html->find('#product_save .off', 0)->plaintext;
					  $product_detail['discount'] = filter_var($discount, FILTER_SANITIZE_NUMBER_INT);
                }
                
			 // product reviews
				$product_detail['reviews'] = '';
				 if ($html->find('.reviews', 0)) {
					 if ($html->find('.review span', 1)) {
						$productReviews = trim($html->find('.review span', 1)->plaintext);
						$product_detail['reviews'] = (int)$productReviews;
					 }
					
				}
				
				
					
				
                // product EMI details
				$product_detail['emi'] = '';
                if ($html->find('.emi_sec', 0)) {
                    if ($html->find('.emi_strt', 0)) {
                        $product_detail['emi']  = trim($html->find('.emi_strt', 0)->plaintext);

                    }
                }
		
		
		 $pro_title = explode(' ',$productNamehtml);
		 
		 $product_detail['brand'] = '';
		 $product_detail['model_number'] = '';
		 if($pro_title[0]){
			 $product_detail['brand'] = $pro_title[0];
			 $product_detail['model_number'] = trim(str_replace($pro_title[0],'',$productNamehtml));
		 }
		 
		 
		
		
		
		
		// product description
		$product_detail['descritpion'] = '';
		$desc_box = $html->find('#content_block_description', 0);
		if ($desc_box->find('.product-details-list div', 0)) {
                    $productDetailhtml = $desc_box->find('.product-details-list div', 0)->outertext;
                    $product_detail['descritpion'] = trim($productDetailhtml);
               }
			   
		$product_detail['rating'] = '';
		 if ($html->find('.product-about', 0)) {
			 if ($html->find('.rating meta', 0)) {
				$product_detail['rating'] = $html->find('.rating meta', 0)->getAttribute('content');
			 }else{
					$scraperLog->saveScraperLog('snapdeal scraper','ShopcluesProductDetailScrapper','.rating meta class not found','Not Found',__LINE__);
			}
        }
		
		$product_detail['product_delivery'] = 'order is shipped within hours';
		
		
		
		// product feature details		
		$arr_product_features = array();
	
		 if ($html->find('.specTable', 0)) {
			//$g = 0;
            for($g =0; $g<=20;$g++) {
				
				$table = $html->find(".specTable",$g); // table record

				// initialize empty array to store the data array from each row
				$theData = array();
				
				// loop over rows
				$n = 0;
				if(!empty($html->find('.groupHead', $g)->plaintext)){
				
						foreach($table->find('tr') as $row) { // loop for tr for get product group head or features
						
							$arr_product_features[$g]['group'] = trim($html->find('.groupHead', $g)->plaintext);
							// initialize array to store the cell data from each row
							$f = 0;
							foreach($row->find('td.specsKey') as $cell) {  // loop for get product feature name
								 $feature_name = trim($cell->innertext);
								 $arr_product_features[$g]['feature'][$f][$n]['feature'] = $feature_name;
							
							$f++;
							}
							
							$v= 0;
							foreach($row->find('td.specsValue') as $cell) { // loop for get product feature value
						
								// push the cell's text to the array
								$feature_value = trim($cell->innertext);
								$arr_product_features[$g]['feature'][$v][$n]['feature_value'] = $feature_value;
							
							$v++;
							}
						
						$n++;	
						} 
					}
			// $g++;	
				}
           
			}
	
	
        
		
        $returnarray['name'] = $productNamehtml;
        $returnarray['imgpath'] = $productImgSrchtml;
        $returnarray['merchant_arr'] = $arr_merchant;
        $returnarray['product_detail'] = $product_detail;
        $returnarray['product_img_arr'] = $arr_product_images;
        $returnarray['product_price'] = $productPrice;
        $returnarray['product_features'] = $arr_product_features;
       //echo '<pre>'; print_r($returnarray); die;
        return $returnarray;
    }

    private function convet_price_string($userString)
    {
        $userString = str_ireplace('rs.', '', strtolower($userString));
        $money = explode(".", $userString);

        $dollars = preg_replace("/[^0-9]/", null, $money[0]);

        $cents = '';
        if (isset($money[1])) {
            $cents = preg_replace("/[^0-9]/", null, $money[1]);
            $result = $dollars . "." . $cents;
        } else {
            $result = $dollars;
        }

        return($result);
    }
	
	
}
