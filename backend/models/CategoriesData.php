<?php

namespace backend\models;
use common\models\Suppliers;
use Yii;

/**
 * This is the model class for table "categories_data".
 *
 * @property integer $id
 * @property string $category_name
 * @property string $category_type
 * @property string $url
 * @property integer $count_products
 * @property string $status
 * @property string $created
 */
class CategoriesData extends \yii\db\ActiveRecord
{
	public $category_csv_file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_name','count_products'], 'required'],
            [['url'], 'string'],
            [['count_products','store_id'], 'integer'],
            [['created'], 'safe'],
            [['category_name', 'category_type','store_name','path'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 15],
			[['category_csv_file'],'file','extensions'=>'csv','maxSize'=>1024 * 1024 * 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_name' => 'Category Name',
            'category_type' => 'Category Type',
           // 'url' => 'Url',
		   'store_name' => 'Store Name',
            'count_products' => 'Count Products',
            'status' => 'Status',
            'created' => 'Created',
			'store_id' => 'Store',
        ];
    }
	
	public function getStoreName()
    {
	  return $this::hasOne(Suppliers::className(),['id'=>'store_id'] );
	 }
	 
	public function changeCategoryName($oldCategoryName, $newCategoryName){
		
		$categoryDataIds = CategoriesData::find()->select(['id','category_name'])->where(['category_name'=>$oldCategoryName])->all();
		
		if(!empty($categoryDataIds)){
			
			foreach($categoryDataIds as $categoryData){
				
				$model = CategoriesData::findOne($categoryData->id);
				$model->category_name = $newCategoryName;
				$model->category_type = $newCategoryName;
				$model->save(false);
				
			}
		}
	}
 
}
