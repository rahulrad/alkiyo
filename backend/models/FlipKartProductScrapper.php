<?php

namespace backend\models;

use common\models\ProductsSuppliers;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\components\simple_html_dom;
use common\components\HttpService;
use common\models\Products;
use backend\models\ProductUrls;
use common\components\AmazonECS;
use common\components\Aws;

//require_once 'simple_html_dom.php';
//require_once 'JungleeProductDetailScrapper.php';
//require_once('Net/URL2.php');

class FlipKartProductScrapper {

    /**
     * get All Paging url from Junglee Site and save in DB All product pages Url.
     */
    public function getAllPagingUrl($url) {
        $strurl = str_replace('{pg}', 1, $url);

//        $response = $this->fetchContentUsingProxy($strurl);
//
//        if (!$response)
//            return false;
//
//        $html = $this->LoadHTMLContent($response);
//        if ($html->find('#resultCount')) {
//            $totalProductStr = $html->find('#resultCount span', 0)->plaintext;
//            $arr_records = explode('of', $totalProductStr);
//            $productPerPage = 24;
//            $totalProduct = $this->convert_total_pages_string($arr_records[1]);
//            $noOfPages = ceil($totalProduct / $productPerPage);
//        }
        $productPerPage = 20;
        $noOfPages = ceil(100 / $productPerPage);
        $arr_url = array();
        for ($i = 1; $i <= 8360; $i+=20) {
            $strurl = str_replace('{pg}', $i, $url);
            $arr_url[] = $strurl;
        }
        return $arr_url;
    }

    /**
     * Save All Product Pages Url in Data Base.
     */
    public function saveAllPagingUrl($arr_url, $categoryName, $categoryType) {
        $connection = Yii::$app->db;
        foreach ($arr_url as $key => $value) {
            $query = "SELECT site_url from category_url where site_url='" . $value . "'";
            $command = $connection->createCommand($query);
            $arr_result = $command->queryAll();
            $no_of_records = count($arr_result);

            if ($no_of_records == 0) {
                $insert_query = "INSERT INTO category_url (category_name,site_url, category_type) VALUES(:category_name, :site_url, :category_type)";
                $insert_command = $connection->createCommand($insert_query);
                $category_name = $categoryName;
                $insert_command->bindParam(':category_name', $category_name);
                $insert_command->bindParam(':site_url', $value);
                $insert_command->bindParam(':category_type', $categoryType);
                $insert_command->execute();
            }
        }
    }

    /**
     * Save Product In DB.
     */
    public function saveProduct($categoryName, $arr_department_type, $categoryType, $latest_product = 0, $store_name) {
		//echo $categoryName; die;
        $connection = Yii::$app->db;
        /* This condition indicate that if new product add on this category then first we set all the status as 1 and after that we updated status column as 0 of top 2 pages and save new product in our DB. */
        /* if ($latest_product == 1) {

          $update_sql= "update junglee_category_url set status=1 where category_type='" . $categoryType . "' and category_name='" . $categoryName . "' order by id_category_url";
          $update_result = yii::app()->db->createCommand($update_sql);
          $update_result->execute();

          $update_query = "update junglee_category_url set status=0 where category_type='" . $categoryType . "' and category_name='" . $categoryName . "' order by id_category_url LIMIT 2";
          $update_query = yii::app()->db->createCommand($update_query);
          $update_query->execute();
          } */

        $objflipkartProduct = new \backend\models\FlipKartProductScrapper();
        $query = "SELECT id_category_url,category_name,site_url from category_url where status=0 and category_type='" . $categoryType . "' and category_name='" . $categoryName . "' and store_name='" . $store_name . "' order by id_category_url limit 1";
        $command = $connection->createCommand($query);
        $arr_data = $command->queryAll();
		//echo '<pre>'; print_r($arr_data); die;
//        $arr_data[] = array('site_url'=>'http://www.flipkart.com/lc/pr/pv1/spotList1/spot1/productList?sid=tyy%2C4io&filterNone=true&start=1&ajax=true&_=1460746573007');

		
        if (is_array($arr_data)) {
            foreach ($arr_data as $key => $value) {
                //if ($key == 0) {
				
				
			    $data = $objflipkartProduct->getAllProductDetailsOnPage($value['site_url'], $categoryName);
			   
				//echo '<pre>'; print_r($data);
                try {
				
                    foreach ($data as $pkey => $pval) {
					
						
                        $name = $pval['name'];
						$price = $pval['product_price'];
                        $model = isset($pval['product_detail']['model_number']) ? $pval['product_detail']['model_number'] : '';
                        $brand = isset($pval['product_detail']['brand']) ? $pval['product_detail']['brand'] : '';
						$description = isset($pval['product_detail']['descritpion']) ? $pval['product_detail']['descritpion'] : '';
						$emi = isset($pval['product_detail']['emi']) ? $pval['product_detail']['emi'] : '';
						$cash_on_delivery = isset($pval['product_detail']['cash_on_delivery']) ? $pval['product_detail']['cash_on_delivery'] : '';
						$return_policy = isset($pval['product_detail']['return_policy']) ? $pval['product_detail']['return_policy'] : '';
						$imgpath = isset($pval['imgpath']) ? $pval['imgpath'] : '';
						$murl = isset($pval['merchant_arr']['murl']) ? $pval['merchant_arr']['murl'] : '';
						$m_unique_id = isset($pval['merchant_arr']['m_unique_id']) ? $pval['merchant_arr']['m_unique_id'] : '';
						
						$discount = isset($pval['product_detail']['discount']) ? $pval['product_detail']['discount'] : '';// vinay new
						$rating = isset($pval['product_detail']['rating']) ? $pval['product_detail']['rating'] : '';// vinay new
						$product_delivery = isset($pval['product_detail']['product_delivery']) ? $pval['product_detail']['product_delivery'] : '';// vinay new
						$reviews = isset($pval['product_detail']['reviews']) ? $pval['product_detail']['reviews'] : '';// vinay new
						
						$shipping = isset($pval['product_detail']['shipping']) ? $pval['product_detail']['shipping'] : '';// vinay new
						$original_price = isset($pval['product_detail']['original_price']) ? $pval['product_detail']['original_price'] : '';// vinay new
						$rating_user_count = isset($pval['product_detail']['rating_user_count']) ? $pval['product_detail']['rating_user_count'] : '';// vinay new
						
						// $model = \common\components\Tools::removeBrand($name, $brand);

                        //$importer = new BaseProductImporter();
                        //$importer->setName($name);

                        $sandle_cat_name = 'Sandal';
                        $slipper_cat_name = 'Slipper';

                        if (stristr($name, $sandle_cat_name)) {
                            //$importer->setCategory('Sandals');
                            $importer = new BaseProductImporter('Sandals', $name);
                        } elseif (stristr($name, $slipper_cat_name)) {
                            //$importer->setCategory('Slippers');
                            $importer = new BaseProductImporter('Slippers', $name);
                        } else {
                            //$importer->setCategory($value['category_name']);
                            $importer = new BaseProductImporter($value['category_name'], $name);
                        }
						//echo $price; die;
                        $importer->setLowestPrice($price);
                        $importer->setLinkRewrite($name);
                        $importer->setModel($model);
                        $importer->setBrand($brand);
						$importer->setdescription($description);
						$importer->setProductEMI($emi);
						$importer->setCategory($value['category_name']);
						
						$importer->setCOD($cash_on_delivery);
						$importer->setReturnPolicy($return_policy);
						$importer->setMainImages($imgpath);
						$importer->setProducturl($murl);
						$importer->setProductUniqueId($m_unique_id);
						
						$importer->setProductDiscount($discount);
						$importer->setProductRating($rating);
						$importer->setProductDelivery($product_delivery);
						$importer->setProductReviews($reviews);
						$importer->setProductShipping($shipping);
						$importer->setProductOriginalPrice($original_price);
						$importer->setProductRatingUserCount($rating_user_count);
						
						$importer->setMultipleImages($pval['product_img_arr']);
						$importer->setProductFeature($pval['product_features']);
						
						
                       // $importer->setReferenceNumber($ref_no);

                        $front_image = array();
                        $product_images = array();
                        if (is_array($pval['product_img_arr']) && count($pval['product_img_arr'])) {
                            foreach ($pval['product_img_arr'] as $ikey => $ival) {
                                if ($ikey == 0)
                                    $front_image['name'] = "Front View";
                                else
                                    $front_image['name'] = "";
                                $front_image['url'] = $ival;
                                $product_images[] = $front_image;
                                $importer->setImages($product_images);
                            }
                        } else {
                            $front_image['name'] = "Front View";
                            $front_image['url'] = $pval['imgpath'];
                            $product_images[] = $front_image;
                            $importer->setImages($product_images);
                        }


                        $importer->save();
                    }
					

                    $q = "update category_url SET status = '1' where id_category_url = '" . $value['id_category_url'] . "'";
                    echo $q;
                    $comm = $connection->createCommand($q);
                    $comm->bindParam(':id_category_url', $key);
                    $comm->execute();
                } catch (Exception $e) {
                    echo "wwwwwwwwwww".$e->getMessage().'::'.$e->getLine().'::'.$e->getTraceAsString();exit;
                    continue;
                }
            }
        }
    }
	
	

    /**
     * get HTML Data finding with XPath.
     */
    private function getProductUrlOnPage($url, $categoryName) {
		
		$response = $this->fetchContentUsingProxy($url);
        if (!$response)
            return false;
		
        $html = $this->LoadHTMLContent($response);
		$arr_product = array();
        if ($html->find('.old-grid', 0)) {
            $siteName = 'http://www.flipkart.com';
            foreach ($html->find(".gd-col") as $key => $pval) {
                if ($pval->find('.pu-visual-section a', 0)) {
                    $productUrl = $pval->find('.pu-visual-section a', 0)->getAttribute('href');
                    $arr_product[] = $siteName . $productUrl;
				   // $arr_product[] = 'http://www.flipkart.com/samsung-galaxy-j5-6-new-2016-edition/p/itmegmrnzqjcpfg9?pid=MOBEG4XWHJDWMQDF&al=AFeXV5TI2vmgbXxFFowfmsldugMWZuE7Qdj0IGOOVqsRNF2afT3H%2FHf%2B2Qg%2FDquScKH0PmIWQZU%3D&ref=L%3A2954885076302187605&srno=b_4';
                }
            }
        }
        $html->clear();
        unset($html);
		//echo '<pre>'; print_r($arr_product); die;
        return $arr_product;
    }

    private function getAllProductDetailsOnPage($url, $categoryName) {
        $product_urls = $this->getOnlyProductUrlOnPage($url, $categoryName);
		
        $product_details = array();
        if (is_array($product_urls)) {
            foreach ($product_urls as $key => $value) {
                $product_details[] = $this->getProductDetail($value, $categoryName);
            }
        }

        return $product_details;
    }

    private function getProductDetail($url, $category) {
		
		 $objflipkart_product_detail = new \backend\models\FlipKartProductDetailScrapper($url, $category);
        return $objflipkart_product_detail->StoreDataInArray();
    }

    


    /**
     * Return site content from a specifice site url.
     */
    private function fetchContentUsingProxy($strurl) {
        $objHttpService = new \common\components\HttpService();
        $response = $objHttpService->getResponse($strurl);
        return $response;
    }

    /**
     * Load HTML
     */
    private function LoadHTMLContent($content) {
        $html = new \common\components\simple_html_dom();
        $html->load($content);
        unset($content);
        return $html;
    }

    /**
     * Convert No of Total Pages in a string.
     */
    private function convert_total_pages_string($userString) {
        $userString = str_ireplace('rs.', '', strtolower($userString));
        $money = explode(".", $userString);

        $dollars = preg_replace("/[^0-9]/", null, $money[0]);

        $cents = '';
        if (isset($money[1])) {
            $cents = preg_replace("/[^0-9]/", null, $money[1]);
            $result = $dollars . "." . $cents;
        } else {
            $result = $dollars;
        }

        return($result);
    }


    public function saveProductData($productUrlData, $categoryName, $queueMessageData = null, $saveScrapDateAtMongo = true)
    {
        if (!$saveScrapDateAtMongo) {
            $product_url = parse_url($productUrlData->url);
            parse_str($product_url['query'], $query);
            if (isset($query['pid']) && !empty($query['pid'])) {
                $product_id = trim($query['pid']);
                $exitProduct = Products::find()->where(['unique_id' => $product_id, 'store_id' => 1])->one();

                if (!empty($exitProduct)) {
                    $ProductUrlsModel = ProductUrls::findOne($productUrlData->id);
                    $ProductUrlsModel->status = 1;
                    $ProductUrlsModel->save(false);
                }


                if (!empty($queueMessageData)) {
                    $aws = new \common\components\Aws();
                    $aws = $aws->deleteQueueMessage($queueMessageData, 'product_scraper');
                }

            }

            $connection = Yii::$app->db;

            $objflipkartProduct = new \backend\models\FlipKartProductScrapper();

            if (!empty($productUrlData) && empty($exitProduct)) {

                $pval = $objflipkartProduct->getProductDetail($productUrlData->url, $categoryName);
                // echo '<pre>pval'; print_r($pval); die;
                try {

                    if (!empty($pval)) {
                        //echo '<pre>'; print_r($pval); die;
                        $name = $pval['name'];
                        $price = $pval['product_price'];
                        $model = isset($pval['product_detail']['model_number']) ? $pval['product_detail']['model_number'] : '';
                        $brand = isset($pval['product_detail']['brand']) ? $pval['product_detail']['brand'] : '';
                        $description = isset($pval['product_detail']['descritpion']) ? $pval['product_detail']['descritpion'] : '';
                        $emi = isset($pval['product_detail']['emi']) ? $pval['product_detail']['emi'] : '';
                        $cash_on_delivery = isset($pval['product_detail']['cash_on_delivery']) ? $pval['product_detail']['cash_on_delivery'] : '';
                        $return_policy = isset($pval['product_detail']['return_policy']) ? $pval['product_detail']['return_policy'] : '';
                        $imgpath = isset($pval['imgpath']) ? $pval['imgpath'] : '';
                        $murl = isset($pval['merchant_arr']['murl']) ? $pval['merchant_arr']['murl'] : '';
                        $m_unique_id = isset($pval['merchant_arr']['m_unique_id']) ? $pval['merchant_arr']['m_unique_id'] : '';

                        $discount = isset($pval['product_detail']['discount']) ? $pval['product_detail']['discount'] : '';// vinay new
                        $rating = isset($pval['product_detail']['rating']) ? $pval['product_detail']['rating'] : '';// vinay new
                        $product_delivery = isset($pval['product_detail']['product_delivery']) ? $pval['product_detail']['product_delivery'] : '';// vinay new
                        $reviews = isset($pval['product_detail']['reviews']) ? $pval['product_detail']['reviews'] : '';// vinay new

                        $shipping = isset($pval['product_detail']['shipping']) ? $pval['product_detail']['shipping'] : '';// vinay new
                        $original_price = isset($pval['product_detail']['original_price']) ? $pval['product_detail']['original_price'] : '';// vinay new
                        $rating_user_count = isset($pval['product_detail']['rating_user_count']) ? $pval['product_detail']['rating_user_count'] : '';// vinay new

                        // $model = \common\components\Tools::removeBrand($name, $brand);

                        //$importer = new BaseProductImporter();
                        //$importer->setName($name);

                        $importer = new BaseProductImporter($categoryName, $name);

                        //echo $price; die;
                        $importer->setLowestPrice($price);
                        $importer->setLinkRewrite($name);
                        $importer->setModel($model);
                        $importer->setBrand($brand);
                        $importer->setdescription($description);
                        $importer->setProductEMI($emi);
                        $importer->setCategory($categoryName);

                        $importer->setCOD($cash_on_delivery);
                        $importer->setReturnPolicy($return_policy);
                        $importer->setMainImages($imgpath);
                        $importer->setProducturl($murl);
                        $importer->setProductUniqueId($m_unique_id);

                        $importer->setProductDiscount($discount);
                        $importer->setProductRating($rating);
                        $importer->setProductDelivery($product_delivery);
                        $importer->setProductReviews($reviews);
                        $importer->setProductShipping($shipping);
                        $importer->setProductOriginalPrice($original_price);
                        $importer->setProductRatingUserCount($rating_user_count);

                        $importer->setMultipleImages($pval['product_img_arr']);
                        $importer->setProductFeature($pval['product_features']);

                        // $importer->setReferenceNumber($ref_no);
                        // echo $productUrlData->id; die;
                        $importer->setProductUrlId($productUrlData->id);

                        $importer->setQueueMessageData($queueMessageData);// vinay 13 oct

                        $front_image = array();
                        $product_images = array();

                        $importer->save();

                        /**$q = "update product_urls SET status = '1' where id = '" . $productUrlData->id . "'";
                         * echo $q.'</br>';
                         * $comm = $connection->createCommand($q);
                         * $comm->bindParam(':id_category_url', $key);
                         * $comm->execute(); **/
                    }


                } catch (Exception $e) {
                    echo "wwwwwwwwwww" . $e->getMessage() . '::' . $e->getLine() . '::' . $e->getTraceAsString();
                    exit;
//                    continue;
                }

            }

        } else {
            $this->saveProductMongoData($productUrlData, $categoryName, $queueMessageData);
        }

    }
	
	/*********************************Save product Url ***********************************/

	/**
     * Save Product In DB.
     */
 /**   public function saveProductUrls($categoryName, $arr_department_type, $categoryType, $latest_product = 0, $store_name,$scraper_group) {
		//echo $categoryName; die;
        $connection = Yii::$app->db;
        
        $objflipkartProduct = new \backend\models\FlipKartProductScrapper();
        $query = "SELECT id_category_url,category_name,site_url,scraper_group,scraper_group_type,status from category_url where status=0 and category_type='" . $categoryType . "' and category_name='" . $categoryName . "' and store_name='" . $store_name . "' and scraper_group='" . $scraper_group . "'  order by id_category_url limit 1";
        $command = $connection->createCommand($query);
        $arr_data = $command->queryAll();
		
		//echo '<pre>'; print_r($arr_data); die;
        if (is_array($arr_data)) {
            foreach ($arr_data as $key => $value) {
                //if ($key == 0) {
				
				
			   // $data = $objflipkartProduct->getAllProductDetailsOnPage($value['site_url'], $categoryName);
			   
				 $data = $objflipkartProduct->getSaveOnlyProductUrls($value['site_url'], $categoryName,$value['id_category_url']);
				
				if(!empty($data)){
					    $q = "update category_url SET status = '1' where id_category_url = '" . $value['id_category_url'] . "'";
						echo $q.'<br>';
						$comm = $connection->createCommand($q);
						$comm->bindParam(':id_category_url', $key);
						$comm->execute();
				}
                
               
            }
        }
    }
	
   private function getSaveOnlyProductUrls($url, $categoryName, $category_url_id) {
	 
       $response = $this->fetchContentUsingProxy($url);
        if (!$response)
            return false;
		
        $html = $this->LoadHTMLContent($response);
		$arr_product = array();
        if ($html->find('.old-grid', 0)) {
            $siteName = 'http://www.flipkart.com';
            foreach ($html->find(".gd-col") as $key => $pval) {
                if ($pval->find('.pu-visual-section a', 0)) {
                    $productUrl = $pval->find('.pu-visual-section a', 0)->getAttribute('href');
					 
					$arr_product[] = $productUrl;
					//echo '<pre>'; print_r($productUrl); die;
					$product_url = $siteName . $productUrl;
					  $exitProductUrl = ProductUrls::find()->where(['url' => $product_url, 'store_id' => 1])->one();
						if (empty($exitProductUrl)) {
						   
									$productUrlsModel = new ProductUrls();
									$productUrlsModel->store_id = 1;
									$productUrlsModel->cateogry_url_id = $category_url_id;
									$productUrlsModel->status = 0;
									$productUrlsModel->url = $product_url;
									$productUrlsModel->save();
									
									$exitProductUrl = ProductUrls::find()->where(['url' => $product_url, 'store_id' => 1])->one();
									
						}
                   
                }
            } 
        }
        $html->clear();
        unset($html);
		
        return $arr_product;
    } **/

    public function saveProductMongoData($productUrlData, $categoryName, $queueMessageData = null)
    {
        $exitProduct = MongoProducts::find()->where(['url' => $productUrlData->url])->one();
        if (!empty($exitProduct)) {
            $ProductUrlsModel = ProductUrls::findOne($productUrlData->id);
            $ProductUrlsModel->status = 1;
            $ProductUrlsModel->save(false);

            if (!empty($queueMessageData)) {
                $aws = new \common\components\Aws();
                $aws->deleteQueueMessage($queueMessageData, 'product_scraper');

            }
        }

        if (!empty($productUrlData) && empty($exitProduct)) {

                try {
                $pval = $this->getProductDetail($productUrlData->url, $categoryName);

                if (!empty($pval)) {
                    $name = $pval['name'];
                    $price = $pval['product_price'];
                    $model = isset($pval['product_detail']['model_number']) ? $pval['product_detail']['model_number'] : '';
                    $brand = isset($pval['product_detail']['brand']) ? $pval['product_detail']['brand'] : '';
                    $description = isset($pval['product_detail']['descritpion']) ? $pval['product_detail']['descritpion'] : '';
                    $emi = isset($pval['product_detail']['emi']) ? $pval['product_detail']['emi'] : '';
                    $colors = isset($pval['product_colors']) ? $pval['product_colors'] : '';
                    $sizes = isset($pval['product_sizes']) ? $pval['product_sizes'] : '';
                    $offers = isset($pval['product_offers']) ? $pval['product_offers'] : '';
                    $instock = (isset($pval['instock']) && $pval['instock']) ? 1 : 0;
                    $cash_on_delivery = isset($pval['product_detail']['cash_on_delivery']) ? $pval['product_detail']['cash_on_delivery'] : '';
                    $return_policy = isset($pval['product_detail']['return_policy']) ? $pval['product_detail']['return_policy'] : '';
                    $imgpath = isset($pval['imgpath']) ? $pval['imgpath'] : '';
                    $murl = isset($pval['merchant_arr']['murl']) ? $pval['merchant_arr']['murl'] : '';
                    $m_unique_id = isset($pval['merchant_arr']['m_unique_id']) ? $pval['merchant_arr']['m_unique_id'] : '';

                    $discount = isset($pval['product_detail']['discount']) ? $pval['product_detail']['discount'] : '';// vinay new
                    $rating = isset($pval['product_detail']['rating']) ? $pval['product_detail']['rating'] : '';// vinay new
                    $product_delivery = isset($pval['product_detail']['product_delivery']) ? $pval['product_detail']['product_delivery'] : '';// vinay new
                    $reviews = isset($pval['product_detail']['reviews']) ? $pval['product_detail']['reviews'] : '';// vinay new

                    $shipping = isset($pval['product_detail']['shipping']) ? $pval['product_detail']['shipping'] : '';// vinay new
                    $original_price = isset($pval['product_detail']['original_price']) ? $pval['product_detail']['original_price'] : '';// vinay new
                    $rating_user_count = isset($pval['product_detail']['rating_user_count']) ? $pval['product_detail']['rating_user_count'] : '';// vinay new

                    $importer = new BaseFlipkartProductImporter($categoryName, $name);
                    $importer->setLowestPrice($price);
                    $importer->setLinkRewrite($name);
                    $importer->setModel($model);
                    $importer->setBrand($brand);
                    $importer->setdescription($description);
                    $importer->setProductEMI(strip_tags($emi));
                    $importer->setProductColors($colors);
                    $importer->setProductSizes($sizes);
                    $importer->setProductOffers($offers);
                    $importer->setProductInstock($instock);
                    $importer->setCategory($categoryName);

                    $importer->setCOD($cash_on_delivery);
                    $importer->setReturnPolicy($return_policy);
                    $importer->setMainImages($imgpath);
//                    $importer->setProducturl($murl);
                    $importer->setProducturl($productUrlData->url);
                    $importer->setProductUniqueId($m_unique_id);

                    $importer->setProductDiscount($discount);
                    $importer->setProductRating($rating);
                    $importer->setProductDelivery(strip_tags($product_delivery));
                    $importer->setProductReviews($reviews);
                    $importer->setProductShipping($shipping);
                    $importer->setProductOriginalPrice($original_price);
                    $importer->setProductRatingUserCount($rating_user_count);
                    $importer->setMultipleImages($pval['product_img_arr']);
                    $importer->setProductFeature($pval['product_features']);
                    $importer->setProductUrlId($productUrlData->id);
                    $importer->setQueueMessageData($queueMessageData);// vinay 13 oct
                    $importer->save();
                }

            } catch (\Exception $e) {
                error_log(var_export($e->getMessage(), true));
                error_log(var_export($e->getTraceAsString(), true));
                throw  $e;
            }
        }
    }

    public function saveProductSupplierData(ProductsSuppliers $productSupplier, $categoryName)
    {
        try {
            $pval = $this->getProductDetail($productSupplier->url, $categoryName);
            error_log(var_export($pval, true));
            if (!empty($pval)) {
                $name = $pval['name'];
                $price = $pval['product_price'];
                $model = isset($pval['product_detail']['model_number']) ? $pval['product_detail']['model_number'] : '';
                $brand = isset($pval['product_detail']['brand']) ? $pval['product_detail']['brand'] : '';
                $description = isset($pval['product_detail']['descritpion']) ? $pval['product_detail']['descritpion'] : '';
                $emi = isset($pval['product_detail']['emi']) ? $pval['product_detail']['emi'] : '';
                $colors = isset($pval['product_colors']) ? $pval['product_colors'] : '';
                $sizes = isset($pval['product_sizes']) ? $pval['product_sizes'] : '';
                $offers = isset($pval['product_offers']) ? $pval['product_offers'] : '';
                $instock = (isset($pval['instock']) && $pval['instock']) ? 1 : 0;
                $cash_on_delivery = isset($pval['product_detail']['cash_on_delivery']) ? $pval['product_detail']['cash_on_delivery'] : '';
                $return_policy = isset($pval['product_detail']['return_policy']) ? $pval['product_detail']['return_policy'] : '';
                $imgpath = isset($pval['imgpath']) ? $pval['imgpath'] : '';
                $murl = isset($pval['merchant_arr']['murl']) ? $pval['merchant_arr']['murl'] : '';
                $m_unique_id = isset($pval['merchant_arr']['m_unique_id']) ? $pval['merchant_arr']['m_unique_id'] : '';

                $discount = isset($pval['product_detail']['discount']) ? $pval['product_detail']['discount'] : '';// vinay new
                $rating = isset($pval['product_detail']['rating']) ? $pval['product_detail']['rating'] : '';// vinay new
                $product_delivery = isset($pval['product_detail']['product_delivery']) ? $pval['product_detail']['product_delivery'] : '';// vinay new
                $reviews = isset($pval['product_detail']['reviews']) ? $pval['product_detail']['reviews'] : '';// vinay new

                $shipping = isset($pval['product_detail']['shipping']) ? $pval['product_detail']['shipping'] : '';// vinay new
                $original_price = isset($pval['product_detail']['original_price']) ? $pval['product_detail']['original_price'] : '';// vinay new
                $rating_user_count = isset($pval['product_detail']['rating_user_count']) ? $pval['product_detail']['rating_user_count'] : '';// vinay new


                $importer = new BaseProductImporter($categoryName, $name);

                $importer->setLowestPrice($price);
                $importer->setLinkRewrite($name);
                $importer->setModel($model);
                $importer->setBrand($brand);
                $importer->setdescription($description);
                $importer->setProductEMI(strip_tags($emi));
                $importer->setProductColors($colors);
                $importer->setProductSizes($sizes);
                $importer->setProductOffers($offers);
                $importer->setProductInstock($instock);
                $importer->setCategory($categoryName);
                $importer->setCOD($cash_on_delivery);
                $importer->setReturnPolicy($return_policy);
                $importer->setMainImages($imgpath);
                $importer->setProducturl($productSupplier->url);
                $importer->setProductUniqueId($m_unique_id);
                $importer->setProductDiscount($discount);
                $importer->setProductRating($rating);
                $importer->setProductDelivery(strip_tags($product_delivery));
                $importer->setProductReviews($reviews);
                $importer->setProductShipping($shipping);
                $importer->setProductOriginalPrice($original_price);
                $importer->setProductRatingUserCount($rating_user_count);
                $importer->setMultipleImages($pval['product_img_arr']);
                $importer->setProductFeature($pval['product_features']);
                $importer->setProductUrlId($productSupplier->id);
                $importer->updateProductSupplier($productSupplier);
            }

        } catch (\Exception $e) {
            error_log(var_export($e->getMessage(), true));
            error_log(var_export($e->getTraceAsString(), true));
            throw  $e;
        }

    }
}
