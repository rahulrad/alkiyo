<?php

namespace backend\models;

use Yii;
use yii\behaviors\SluggableBehavior;


/**
 * This is the model class for table "blogs".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $status
 * @property string $created
 */
class Logs extends \yii\db\ActiveRecord
{
   
  
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_logs';
    }
	
	
	/**
     * @return yii\db\ActiveQuery
     */
    public function saveUserLogs($data)
    {
  		$user_logs = new Logs();
        $user_logs->model = $data['model'];
		$user_logs->action = $data['action'];
	    $user_logs->action_id = $data['action_id'];
		$user_logs->activity = $data['activity'];
        $user_logs->user_id = Yii::$app->user->identity->id;
		$user_logs->created = date('Y-m-d h:i:s');
        
        return $user_logs->save() ? $user_logs : null;
 	}

   
}
