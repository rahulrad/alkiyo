<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MongoFeatures;

/**
 * MongoFeaturesSearch represents the model behind the search form about `backend\models\MongoFeatures`.
 */
class MongoFeaturesSearch extends MongoFeatures
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'id', 'name', 'category_id', 'feature_group_id', 'is_filter', 'is_required', 'slug', 'type', 'unit', 'display_text', 'status', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MongoFeatures::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'category_id', $this->category_id])
            ->andFilterWhere(['like', 'feature_group_id', $this->feature_group_id])
            ->andFilterWhere(['like', 'is_filter', $this->is_filter])
            ->andFilterWhere(['like', 'is_required', $this->is_required])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'unit', $this->unit])
            ->andFilterWhere(['like', 'display_text', $this->display_text])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'created', $this->created]);

        return $dataProvider;
    }
}
