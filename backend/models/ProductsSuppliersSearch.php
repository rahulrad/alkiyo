<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProductsSuppliers;

/**
 * ProductsSuppliersSearch represents the model behind the search form about `app\models\ProductsSuppliers`.
 */
class ProductsSuppliersSearch extends ProductsSuppliers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'categories_category_id', 'brands_brand_id', 'supplier_count'], 'integer'],
            [['product_name', 'slug', 'image', 'product_description', 'model_number', 'cod', 'emi', 'return_policy', 'delivery', 'price', 'unique_id', 'url', 'product_status', 'date_add', 'date_update', 'date_launch', 'link_rewrite', 'store_name', 'meta_title', 'meta_keyword', 'meta_desc', 'active', 'created','store_id'], 'safe'],
            [['lowest_price', 'highest_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductsSuppliers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'product_id' => $this->product_id,
            'categories_category_id' => $this->categories_category_id,
            'brands_brand_id' => $this->brands_brand_id,
            'date_add' => $this->date_add,
            'date_update' => $this->date_update,
            'date_launch' => $this->date_launch,
            'lowest_price' => $this->lowest_price,
            'highest_price' => $this->highest_price,
            'supplier_count' => $this->supplier_count,
            'created' => $this->created,
        ]);

        $query->andFilterWhere(['like', 'product_name', $this->product_name])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'product_description', $this->product_description])
            ->andFilterWhere(['like', 'model_number', $this->model_number])
            ->andFilterWhere(['like', 'cod', $this->cod])
            ->andFilterWhere(['like', 'emi', $this->emi])
            ->andFilterWhere(['like', 'return_policy', $this->return_policy])
            ->andFilterWhere(['like', 'delivery', $this->delivery])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'unique_id', $this->unique_id])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'product_status', $this->product_status])
            ->andFilterWhere(['like', 'link_rewrite', $this->link_rewrite])
            ->andFilterWhere(['like', 'store_name', $this->store_name])
			->andFilterWhere(['like', 'store_id', $this->store_id])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_keyword', $this->meta_keyword])
            ->andFilterWhere(['like', 'meta_desc', $this->meta_desc])
            ->andFilterWhere(['like', 'active', $this->active]);

        return $dataProvider;
    }
}
