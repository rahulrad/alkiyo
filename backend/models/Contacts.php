<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $message
 * @property string $phone
 * @property string $status
 * @property string $created
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message', 'status'], 'string'],
            [['status','email'], 'required'],
            [['created','is_delete'], 'safe'],
            [['name', 'email'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 15],
            [['email'], 'email'],
            //[['email'], 'unique',  'message' => 'This email address has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'message' => 'Message',
            'phone' => 'Phone',
            'status' => 'Status',
            'created' => 'Created',
        ];
    }
}
