<?php
namespace backend\models;
use Yii;


/**
 * This is the model class for table "categories".
 *
 * @property integer $category_id
 * @property string $category_name
 * @property integer $parent_category_id
 * @property string $category_description
 * @property string $category_status
 * @property string $created
 * @property int $is_index
 */


class Showroom extends \yii\db\ActiveRecord   
{
   /*  public $user_id;
	public $product_id;
	public $cat_id; 
	public $brands_brand_id;
	*/
	public $file;
	public $images;
	
	public static function tableName()   
    {   
        return 'showroom';   
    }  

	public function rules()   
    {   
        return [   
            [['name','city','pincode','address','brand_id'], 'required'],  
            [['created_at','is_delete'], 'safe'],
			['email', 'filter', 'filter' => 'trim'],
			['email', 'email'],
			[['file'], 'file', 'extensions' => 'csv', 'maxSize' => 1024 * 1024 * 5],
        ];   
    }
	
	public function attributeLabels() {
        return [
			'brand_id' => 'Brand',
            'name' => 'Name',
            'address' => 'Address',
			'city' => 'City',
            'pincode' => 'Pincode',
			'phone_no' => 'Phone No',
			'email' => 'Email',
			'status' => 'Status',
			'file' => 'File',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'images' => 'Delar Image',
        ];
    }
	
}
