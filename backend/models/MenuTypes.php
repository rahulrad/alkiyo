<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "menu_types".
 *
 * @property integer $id
 * @property integer $title
 * @property string $created
 * @property integer $status
 */
class MenuTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title','status','created','is_delete'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'created' => 'Created',
            'status' => 'Status',
        ];
    }
}
