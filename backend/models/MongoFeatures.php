<?php

namespace backend\models;

use Yii;
use common\models\Features;
use common\models\FeatureValues;
use common\models\FeatureGroups;
use common\models\ProductFeature;

/**
 * This is the model class for collection "MongoFeatures".
 *
 * @property \MongoId|string $_id
 * @property mixed $id
 * @property mixed $name
 * @property mixed $category_id
 * @property mixed $feature_group_id
 * @property mixed $is_filter
 * @property mixed $is_required
 * @property mixed $slug
 * @property mixed $type
 * @property mixed $unit
 * @property mixed $display_text
 * @property mixed $status
 * @property mixed $created
 */
class MongoFeatures extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['nayashoppy', 'MongoFeatures'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'id',
            'name',
			'display_name',
            'category_id',
            'feature_group_id',
            'is_filter',
            'is_required',
            'slug',
            'type',
            'unit',
			'sort_order',
            'display_text',
            'status',
            'created',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'display_name','category_id', 'feature_group_id', 'is_filter', 'is_required', 'slug', 'type', 'unit','sort_order', 'display_text', 'status', 'created'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'id' => 'Id',
            'name' => 'Name',
			'display_name' =>'Display Name',
            'category_id' => 'Category ID',
            'feature_group_id' => 'Feature Group ID',
            'is_filter' => 'Is Filter',
            'is_required' => 'Is Required',
            'slug' => 'Slug',
            'type' => 'Type',
            'unit' => 'Unit',
            'display_text' => 'Display Text',
            'status' => 'Status',
            'created' => 'Created',
        ];
    }
	
	public function findFeatureValuesToFeatureId($feature_id) {

        $FeatureValuesModel = new MongoFeatureValues();

        $FeatureValues = $FeatureValuesModel::find()->where(['feature_id' => (int) $feature_id])->all();

        $featuresValueArr = array();

        foreach ($FeatureValues as $FeatureVal) {

            $featuresValueArr[] = array('id' => $FeatureVal->id, 'value' => $FeatureVal->value);
        }
		//echo '<pre>'; print_r($featuresValueArr); die;
        return $featuresValueArr;
    }
	
	
	
	public function saveProductFeatureData($product_id, $category_id, $feature_group_id,$product_feature){
			
			
			$feature_data = '';
			
			foreach($product_feature as $feature){
							
							if(isset($feature['feature_id']) && !empty($feature['feature_id']) && isset($feature['feature_value_id']) && !empty($feature['feature_value_id'])){
								
								$getFeatureDetail = MongoFeatures::findOne(['id'=>  (int)$feature['feature_id']]);
								
								$feature_data = Features::findOne(['name'=>  $getFeatureDetail->name, 'category_id' => $category_id, 'feature_group_id' => $feature_group_id]);
								
								$max_sort_order = Features::find()->orderBy('sort_order DESC')->one();
								
								if(!empty($max_sort_order)){
								
									$sort_order = $max_sort_order->sort_order + 1;
								
								}else{
								
									$sort_order = 1;
								}
							
									if ($feature_data == null) {
									  
									//   Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');   
												$feature_model = new Features();
												$feature_model->category_id = $category_id;
												$feature_model->feature_group_id = $feature_group_id;
												$feature_model->name = $getFeatureDetail->name;
												$feature_model->display_name = $getFeatureDetail->name;
												$feature_model->sort_order = $sort_order;
												$feature_model->status = 1;
												$feature_model->created = date('Y-m-d h:i:s');
												$feature_model->save();
												
												$feature_data = Features::findOne(['name'=>  $getFeatureDetail->name, 'category_id' => $category_id, 'feature_group_id' => $feature_group_id]);
													
									}  
							//echo '<pre>'; print_r($feature_data); die;
							if(isset($feature_data->id) && !empty($feature_data->id)){
								$mongoFeatureValuesModel = new MongoFeatureValues();
								$mongoFeatureValuesModel->saveProductFeatureValue($product_id,$feature_data->id,$feature['feature_value_id']);
							
							}
						} 
				}
			return $feature_data;
	
	}

	
}
