<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Showroom;

/**
 * BrandsSearch represents the model behind the search form about `backend\models\Brands`.
 */
class ShowroomSearch extends Showroom
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','pincode'], 'integer'],
            [['name', 'address','city','phone_no','email','created_at','updated_at','status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Showroom::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 100,
			],
			'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

       /*  $query->andFilterWhere([
            'id' => $this->id,
            'created' => $this->created,
        ]);
        $query->andFilterWhere(['like', 'brand_name', $this->brand_name])
        ->andFilterWhere(['like', 'brand_description', $this->brand_description]); */
			
		$query->andFilterWhere(['!=', 'is_delete', 1]); 

        return $dataProvider;
    }
}
