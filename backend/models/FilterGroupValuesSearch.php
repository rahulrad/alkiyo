<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\FilterGroupValues;

/**
 * FilterGroupValuesSearch represents the model behind the search form about `backend\models\FilterGroupValues`.
 */
class FilterGroupValuesSearch extends FilterGroupValues
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'group_item_id', 'value_id', 'status', 'is_delete'], 'integer'],
            [['created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FilterGroupValues::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'group_item_id' => $this->group_item_id,
            'value_id' => $this->value_id,
            'status' => $this->status,
            'is_delete' => $this->is_delete,
            'created' => $this->created,
        ]);

        return $dataProvider;
    }
}
