<?php
/**
 * Description of BaseSnapDealProductImporter
 *
 * @author Vikash
 */
 

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;
use common\models\Brands;
use common\models\Categories;
use common\models\ProductImage;
use common\models\FeatureGroups;
use common\models\Features;
use common\models\FeatureValues;
use common\models\ProductFeature;
use backend\models\Mobile;
use backend\models\Computer;
use backend\models\MongoProducts;
use backend\models\Logs;

class BaseShopcluesProductImporter {
    
    private $_product;
    private $id_category;
    private $name;
    private $description;
    private $model;
    private $lowest_price;
    private $length;
    private $width;
    private $height;
    private $package_content;
    private $warranty;
    private $featured;
    private $link_rewrite;
    private $brand;
    private $category;
    private $features = array();
    private $images = array();
    private $tags = array();
    private $suppliers = array();
    private $active;
    private $video = array();
    private $launch_date;
    private $product_model;
    private $reference_number;
    private $suppliercount;
    private $keyField;
    private $keyValue;

    public function __construct($category_name, $name = '', $key_field = NULL, $key_value = NULL) {
		
        $this->setName($name);
        $this->setCategory($category_name);
        $this->setKeyField($key_field);
        $this->setKeyValue($key_value);        
        $this->init();
		
		
    }
    
    private function init() {
        $category = $this->saveCategory();
		
        if (is_object($category)) {
            $this->category_id = $category->category_id;
			$this->category_name = $category->category_name;
			//echo $this->category_name; die;
//            if ($this->keyField != NULL)            
//                $this->_product = Product::model()->findByAttributes(array($this->keyField => $this->keyValue, 'id_category' => $category->id_category));            
//            else
//                $this->_product = Product::model()->findByAttributes(array('name' => $this->name));
//                //$this->_product = Product::model()->findByAttributes(array('name' => $this->name, 'id_category' => $category->id_category));
//            if (!is_object($this->_product)) {
//                $this->_product = new Product();
//                $this->_product->active = 1;
//            }
        }  
		
		
		/*
		 
		 $cat_name = ucfirst($category->category_name);  
		 if($cat_name == 'Computer'){
       		 $this->_product = new Computer();
		 } elseif($cat_name == 'Mobile'){
			$this->_product = new Mobile();
		 }*/
		
		$this->_product = new MongoProducts();
        
    }
    
    public function setName($name) {
        $this->name = $name;
    }

    public function setdescription($description) {
        $this->description = $description;
    }

    public function setModel($model) {
        $this->model = $model;
    }

    public function setLowestPrice($lowest_price) {
        $this->price = $lowest_price;
    }

    public function setWidth($width) {
        $this->width = $width;
    }

    public function setHeight($height) {
        $this->height = $height;
    }

    public function setPackageContent($package_content) {
        $this->package_content = $package_content;
    }

    public function setWarranty($warranty) {
        $this->package_content = $warranty;
    }

    public function setFeatured($featured) {
            $this->featured = $featured;
    }

    public function setLinkRewrite($name) {
        $this->link_rewrite = $name;
    }

    public function setBrand($brand) {
        $this->brand = $brand;
    }

    public function setTags($tag) {
        $this->tags = $tag;
    }

    public function setCategory($category) {
        $this->category = $category;
    }

    public function setFeatures($features) {
        $this->features = $features;
    }

    public function setImages($images) {
		$this->images = $images;
    }

    public function setSuppliers($suppliers) {
        $this->suppliers = $suppliers;
    }

    public function setActive($active=1) {
        $this->active = $active;
    }

    public function setVideos($video) {
        $this->video = $video;
    }

    public function setLaunchDate($launch_date) {
        $this->launch_date = $launch_date;
    }

    public function setProductModel($name) {
        $this->product_model = $name;
    }

    public function setReferenceNumber($reference_number) {
        $this->reference_number = $reference_number;
    }

    public function setSupplierCount($suppliercount=1) {
         $this->suppliercount = $suppliercount;
    }

    public function setKeyField($keyfield) {
        $this->keyField = $keyfield;
    }

    public function setKeyValue($keyvalue) {
        $this->keyValue = $keyvalue;
    }
	
	
	
	
	
	
    
    public function getName() {
        return (isset($this->name) AND $this->name) ? $this->name : $this->_product->product_name;
    }
    
    public function getModel() {
        return (isset($this->model) AND $this->model) ? $this->model : $this->_product->model_number;
    }
    
    public function getLowestPrice() {
        return (isset($this->price) AND $this->price) ? $this->price : $this->_product->price;
    }
    
    public function getDescription() {
        return (isset($this->description) AND $this->description) ? $this->description : $this->_product->product_description;
    }
    
    public function getHeight() {
        return (isset($this->height) AND $this->height) ? $this->height : $this->_product->height;
    }
    
    public function getLength() {
        return (isset($this->length) AND $this->length) ? $this->length : $this->_product->length;
    }
    
    public function getLinkRewrite() {
        return (isset($this->link_rewrite) AND $this->link_rewrite) ? $this->link_rewrite : $this->_product->link_rewrite;
    }
    
    public function getWarranty() {
        return (isset($this->warranty) AND $this->warranty) ? $this->warranty : $this->_product->warranty;
    }
    
    public function getWidth() {
        return (isset($this->width) AND $this->width) ? $this->width : $this->_product->width;
    }
    
    public function getActive() {
        return isset($this->active) ? $this->active : $this->_product->active;
    }
    
    public function getDateAdd() {
        return isset($this->_product->date_add) ? $this->_product->date_add : time();
    }
    
    public function getDateUpdate() {
        return date('Y-m-d');
    }
    
    public function getDateLaunch() {
        return (isset($this->launch_date) AND $this->launch_date) ? $this->launch_date : $this->_product->date_launch;
    }
    
    public function getReferenceNumber() {
        return (isset($this->reference_number) AND $this->reference_number) ? $this->reference_number : $this->_product->reference_number;
    }
    
    public function getSupplierCount() {
        return (isset($this->suppliercount) AND $this->suppliercount) ? $this->suppliercount : $this->_product->suppliercount;
    }
    
    public function getIdBrand() {        
        $brand = $this->saveBrand();
		//echo '<pre>'; print_r($brand->brand_name); die;
        return (isset($brand->brand_name) AND $brand->brand_name) ? $brand->brand_name : $this->_product->brand_name;        
    }
    
    public function getIdCategory() {
        return (isset($this->category_name) AND $this->category_name) ? $this->category_name : $this->_product->category_name;
    }
	
	
	// vinay New Fields
	
	 public function setMainImages($image) {
        $this->main_image = $image;
    }
	
	public function getMainImages() {
        return (isset($this->main_image) AND $this->main_image) ? $this->main_image : $this->_product->image;
    }
	
	 public function setCOD($cod) {
        $this->cash_on_delivery = $cod;
    }
	
	public function getCOD() {
        return (isset($this->cash_on_delivery) AND $this->cash_on_delivery) ? $this->cash_on_delivery : $this->_product->cod;
    }
	
	 public function setReturnPolicy($return_policy) {
        $this->return_policy = $return_policy;
    }
	
	public function getReturnPolicy() {
        return (isset($this->return_policy) AND $this->return_policy) ? $this->return_policy : $this->_product->return_policy;
    }
	
	 public function setProducturl($url) {
        $this->url = $url;
    }
	
	public function getProducturl() {
        return (isset($this->url) AND $this->url) ? $this->url : $this->_product->url;
    }
	
	 public function setProductUniqueId($product_unique_id) {
        $this->unique_id = $product_unique_id;
    }
	
	public function getProductUniqueId() {
        return (isset($this->unique_id) AND $this->unique_id) ? $this->unique_id : $this->_product->unique_id;
    }
	
	
	public function setMultipleImages($all_images) {
        $this->all_images = $all_images;
    }
	
	public function getMultipleImages() {
        return (isset($this->all_images) AND $this->all_images) ? $this->all_images : '';
    }
	
    
	public function setProductFeature($product_features) {
        $this->productFeature = $product_features;
    }
	
	public function getProductFeature() {
        return (isset($this->productFeature) AND $this->productFeature) ? $this->productFeature : '';
    }
	
	
	 
	public function setProductEMI($emi) {
        $this->emi = $emi;
    }
	
	public function getProductEMI() {
        return (isset($this->emi) AND $this->emi) ? $this->emi : '';
    }
	
    
		
	public function setProductDiscount($discount) {
        $this->discount = $discount;
    }
	
	public function getProductDiscount() {
        return (isset($this->discount) AND $this->discount) ? $this->discount : '';
    }
	
	public function setProductRating($rating) {
        $this->rating = $rating;
    }
	
	public function getProductRating() {
        return (isset($this->rating) AND $this->rating) ? $this->rating : '';
    }
	
	public function setProductDelivery($product_delivery) {
        $this->delivery = $product_delivery;
    }
	
	public function getProductDelivery() {
        return (isset($this->delivery) AND $this->delivery) ? $this->delivery : '';
    }

	
	public function setProductReviews($reviews) {
        $this->reviews = $reviews;
    }
	
	public function getProductReviews() {
        return (isset($this->reviews) AND $this->reviews) ? $this->reviews : '';
    }
    
	
	public function setProductShipping($shipping) {
        $this->shipping = $shipping;
    }
	
	public function getProductShipping() {
        return (isset($this->shipping) AND $this->shipping) ? $this->shipping : '';
    }
    
    
    
	public function setProductOriginalPrice($original_price) {
        $this->original_price = $original_price;
    }
	
	public function getProductOriginalPrice() {
        return (isset($this->original_price) AND $this->original_price) ? $this->original_price : '';
    }
    
	
	public function setProductRatingUserCount($rating_user_count) {
        $this->rating_user_count = $rating_user_count;
    }
	
	public function getProductRatingUserCount() {
        return (isset($this->rating_user_count) AND $this->rating_user_count) ? $this->rating_user_count : '';
    }
    
    
    public function save($refresh=false, $is_product_log = false) {  
        if (isset($this->_product)) {
            if ($this->_product->_id != NULL && $refresh === false) {
                if ($is_product_log)
                    return $this->_product->_id;
                return FALSE;
            }

            try {  
				
				$searchData = array();
				
				$searchData['product_name'] =  $this->getName();
				$searchData['store_id'] = 4;
				$cat_name = $this->getIdCategory();
				$brand_name = $this->getIdBrand();
				if(!empty($cat_name)){
					$searchData['category_name'] =  $cat_name;
				}
				if(!empty($brand_name)){
					$searchData['brand_name'] = $brand_name;
				}
				// getting exiting product before save 
				$exitProduct = MongoProducts::find()->where($searchData)->one();
				
				$productPrice = $this->getLowestPrice();
				$productName = $this->getName();
				if(!empty($productPrice) && !empty($productName) && empty($exitProduct)){
				
					$last_insert_id = MongoProducts::find()->orderBy(['product_id' => SORT_DESC])->one();
							
					$current_id = 1;
						if(!empty($last_insert_id)){
							$current_id = (int)$last_insert_id->product_id + 1;
						}
					$this->_product->product_id = $current_id;
					
					$this->_product->store_name = 'shopclues';
					$this->_product->store_id = 4;
					$this->_product->brand_name = $this->getIdBrand();
					$this->_product->category_name = $this->getIdCategory();
					$this->_product->product_name = $this->getName();
					$this->_product->product_description = $this->getDescription();
					$this->_product->model_number =  $this->getModel();
					$this->_product->price =  $this->getLowestPrice();
					//$this->_product->cod = $this->getCOD();
			
					$this->_product->emi = $this->getProductEMI();
					$this->_product->cod = $this->getCOD();
					$this->_product->return_policy = $this->getReturnPolicy();
					$this->_product->url = $this->getProducturl();
					$this->_product->unique_id = $this->getProductUniqueId();
					$this->_product->product_description = $this->getDescription();
					$this->_product->image = $this->getMainImages();
					$this->_product->status = 1;
					
					$this->_product->discount = $this->getProductDiscount();
					
					$rating = $this->getProductRating();
					if(!empty($rating)){
							$this->_product->rating =  number_format($rating, 2, '.', '');
					}else{
						$this->_product->rating = '';
					}
					
					$this->_product->delivery = $this->getProductDelivery();
					$this->_product->reviews = $this->getProductReviews();
					$this->_product->shipping = $this->getProductShipping();
					$this->_product->original_price = $this->getProductOriginalPrice();
					$this->_product->rating_user_count = $this->getProductRatingUserCount();
					$this->_product->created = date('Y-m-d h:i:s');
					 $this->_product->updated = date('Y-m-d h:i:s');
					$all_images = $this->getMultipleImages();
					if(!empty($all_images)){	
						$this->_product->other_images = serialize($all_images);
					}
					
					
					if ($this->_product->validate()) {            
						$this->_product->save(); 
						
					//	echo '<pre>'; print_r($this->_product); die;
						echo $this->_product->_id.' This Product is saved<br>'; 
						
						$productFeatures = $this->getProductFeature();
						
						if(!empty($productFeatures)){
							
							$this->saveProductFeatureGroup($this->_product,$productFeatures);
							
							$categoryDetail = Categories::find()->select(['category_id','category_name'])->where(['category_name' => $this->_product->category_name])->one();
							$this->saveProductFeature($this->_product->product_id,$categoryDetail->category_id,$productFeatures);
						}
						
						
						
						
						
							
							
						return $this->_product->_id;
					}else {
						var_dump($this->_product->errors);
						return false;
					}
				}
				
            } catch (Exception $e) {
                $message = $e->getMessage();
                echo '<pre>';
                                print_r($message);exit;
                $subject = 'Error while saving the product';
                //Tools::alertMail($message, $subject);
            }
        }
        return false;       
    }
	
	
	public function saveProductFeatureGroup($product,$productFeatures){
				//echo '<pre>'; print_r($product); die;
				$categoryDetail = Categories::find()->select(['category_id','category_name'])->where(['category_name' => $product->category_name])->one();
				if(!empty($productFeatures)){
						foreach($productFeatures as $productFeature){
					
					$feature_group = MongoFeatureGroups::findOne(['name'=>  $productFeature['group']]);
					
					$max_sort_order = MongoFeatureGroups::find()->orderBy('sort_order DESC')->one();
					
					if(!empty($max_sort_order)){
					
						$sort_order = $max_sort_order->sort_order + 1;
					
					}else{
					
						$sort_order = 1;
					}
					
					//echo '<pre>'; print_r($max_sort_order); 
					
					if ($feature_group == null) {
					  
					// Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');
							$last_insert_id = MongoFeatureGroups::find()->orderBy(['id' => SORT_DESC])->one();
							
							$current_id = 1;
							if(!empty($last_insert_id)){
								$current_id = (int)$last_insert_id->id + 1;
							}
								$feature_group_model = new MongoFeatureGroups();
								$feature_group_model->id = $current_id;
								$feature_group_model->category_id = $categoryDetail->category_id;
								$feature_group_model->name = $productFeature['group'];
								$feature_group_model->sort_order = $sort_order;
								$feature_group_model->created = date('Y-m-d h:i:s');
								$feature_group_model->save();
								
								$feature_group = MongoFeatureGroups::findOne(['name'=>  $productFeature['group']]);
									
					}  
					//echo '<pre>'; print_r($feature_group); die;
					if(isset($feature_group->id) && !empty($feature_group->id)){
						
						$this->saveProductFeatureData($product->product_id,$categoryDetail->category_id,$feature_group->id,$productFeature);
					
					}
				} 
				
				return $feature_group;
				}
				
	}
	
	
	public function saveProductFeatureData($product_id, $category_id, $feature_group_id,$product_feature){
			
			if(!empty($product_feature)){
				$feature_data = '';
			foreach($product_feature['feature'] as $features){
					
					foreach($features as $feature){
							//echo '<pre>vinay'; print_r($features); die;
							if(isset($feature['feature']) && !empty($feature['feature'])){
							
								$feature_data = MongoFeatures::findOne(['name'=>  $feature['feature'], 'category_id' => $category_id, 'feature_group_id' => $feature_group_id]);
								//echo '<pre>vinay===>'; echo $feature['feature']; print_r($feature_data); die;
								$max_sort_order = MongoFeatures::find()->orderBy('sort_order DESC')->one();
								
								if(!empty($max_sort_order)){
								
									$sort_order = $max_sort_order->sort_order + 1;
								
								}else{
								
									$sort_order = 1;
								}
							
									if ($feature_data == null) {
									  
									  $last_insert_id = MongoFeatures::find()->orderBy(['id' => SORT_DESC])->one();
							
										$current_id = 1;
										if(!empty($last_insert_id)){
											$current_id = (int)$last_insert_id->id + 1;
										}
									//   Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');   
												$feature_model = new MongoFeatures();
												$feature_model->id = $current_id;
												$feature_model->category_id = $category_id;
												$feature_model->feature_group_id = $feature_group_id;
												$feature_model->name = $feature['feature'];
												$feature_model->display_name = $feature['feature'];
												$feature_model->sort_order = $sort_order;
												$feature_model->status = 1;
												$feature_model->created = date('Y-m-d h:i:s');
												$feature_model->save();
												
												$feature_data = MongoFeatures::findOne(['name'=>  $feature['feature'], 'category_id' => $category_id, 'feature_group_id' => $feature_group_id]);
													
									}  
							//echo '<pre>'; print_r($product_feature); die;
							if(isset($feature_data->id) && !empty($feature_data->id)){
								if( isset($feature['feature_value']) && !empty($feature['feature_value'])){
									$this->saveProductFeatureValue($product_id,$feature_data->id,$feature['feature_value']);
								}
								
							
							}
						}	
					} 
				}
		return $feature_data;
			}
			
			
	}
	
	
	public function saveProductFeatureValue($product_id, $feature_id, $product_feature_values){
			$feature_value_data = '';
			if(!empty($product_feature_values)){
						
							$feature_value_multi = explode(',',$product_feature_values);
							
								foreach($feature_value_multi as $feature_value_value){
									//echo $feature_value_value.'<br>===>';
									if(!empty($feature_value_value)){
											
											$feature_value_data = MongoFeatureValues::findOne(['value'=>  $feature_value_value, 'feature_id' => $feature_id]);
											
											if ($feature_value_data == null) {
											  
											  $last_insert_id = MongoFeatureValues::find()->orderBy(['id' => SORT_DESC])->one();
									
												$current_id = 1;
												if(!empty($last_insert_id)){
													$current_id = (int)$last_insert_id->id + 1;
												}
												
											//   Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');   
														$feature_value_model = new MongoFeatureValues();
														$feature_value_model->id = $current_id;
														$feature_value_model->feature_id = $feature_id;
														$feature_value_model->value = $feature_value_value;
														$feature_value_model->display_name = $feature_value_value;
														$feature_value_model->created = date('Y-m-d h:i:s');
														$feature_value_model->save();
														
														$feature_value_data = MongoFeatureValues::findOne(['value'=>  $feature_value_value, 'feature_id' => $feature_id]);
															
											
										/**	if(isset($feature_value_data->id) && !empty($feature_value_data->id)){
									
													$this->saveProductFeature($product_id, $feature_id,$feature_value_data->id);
									
										} **/
									
									}  
								//echo '<pre>'; print_r($feature_value_data); echo '<br>===>'.$feature_value_data->id;  die;
							
								
							}
						}
					}
			
		//echo '<pre>'; print_r($feature_value_data); 
		return $feature_value_data;
	}
	
	
	
	
	public function saveProductFeature($product_id, $category_id,$productFeaturesData){
		//echo $product_id.'===>'.$category_id.'===>';
		//echo '<pre>'; print_r($productFeaturesData); 
			if(!empty($productFeaturesData)){
					foreach($productFeaturesData as $productFeatures){
						
							$feature_group = MongoFeatureGroups::findOne(['name'=>  $productFeatures['group']]);
							//echo '<pre>first record'; print_r($productFeatures);
							//echo '<pre>second record'; print_r($feature_group);
							if(isset($productFeatures['feature'][0])){
								foreach($productFeatures['feature'][0] as $features){
									//echo '<pre>third record'; print_r($features);
									//foreach($productFeature as $features){
										
											if(isset($features['feature']) && isset($features['feature_value'])){
												
												$feature_data = MongoFeatures::findOne(['name'=>  $features['feature'], 'category_id' => $category_id, 'feature_group_id' => $feature_group->id]);
												//echo '<pre>fourth record'; print_r($feature_data);
												$feature_value_multi = explode(',',$features['feature_value']);
												
												foreach($feature_value_multi as $feature_value_value){
													//echo $feature_value_value.'<br>===>';
													if(!empty($feature_value_value)){
														$feature_value_data = MongoFeatureValues::findOne(['value'=>  $feature_value_value, 'feature_id' => $feature_data->id]);
														
														if(!empty($feature_data) && !empty($feature_value_data)){
															//echo $feature_data->id.'===>'.$feature_value_data->id.'<br>';
															$last_insert_id = MongoProductFeatures::find()->orderBy(['id_product_feature' => SORT_DESC])->one();
														
															$current_id = 1;
															if(!empty($last_insert_id)){
																$current_id = (int)$last_insert_id->id_product_feature + 1;
															}
															$product_feature_model = new MongoProductFeatures();
															$product_feature_model->id_product_feature = $current_id;
															$product_feature_model->id_product = $product_id;
															$product_feature_model->id_feature = $feature_data->id;
															$product_feature_model->id_feature_value = $feature_value_data->id;
															$product_feature_model->save();
														}
														//echo '<pre>five record'; print_r($feature_value_data);
													}
												}
											}
								//	}
									
								}
						}
					}
				//die('exit');
			}
			return true;
		
	}
	
	
	/**
	public function saveProductFeature($product_id, $feature_id,$feature_value_id){
				//echo 'product_id=>'.$product_id.'<br>feature_id=>'.$feature_id.'<br>feature_value_id=>'.$feature_value_id; die;
				if(!empty($product_id) && !empty($feature_id) && !empty($feature_value_id)){
						
						$product_feature = MongoProductFeatures::findOne(['id_product'=>  $product_id, 'id_feature' => $feature_id,'id_feature_value'=> $feature_value_id]);
						
						if ($product_feature == null) {
							
							 $last_insert_id = MongoProductFeatures::find()->orderBy(['id_product_feature' => SORT_DESC])->one();
							
								$current_id = 1;
								if(!empty($last_insert_id)){
									$current_id = (int)$last_insert_id->id_product_feature + 1;
								}
										
						//   Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');   
									$product_feature_model = new MongoProductFeatures();
									$product_feature_model->id_product_feature = $current_id;
									$product_feature_model->id_product = $product_id;
									$product_feature_model->id_feature = $feature_id;
									$product_feature_model->id_feature_value = $feature_value_id;
									$product_feature_model->save();
									
									$feature_value_data = MongoProductFeatures::findOne(['id_product'=>  $product_id, 'id_feature' => $feature_id,'id_feature_value'=> $feature_value_id]);
						}  
						//$this->saveProductFeature($product_id, $feature_id,$feature_value_data->id);
				
					return true;
				}	
		}
	
	***/
	
	
	public function saveMultipleImages($product_id, $images){
			
			if(!empty($images)){
				$i=1;
				foreach($images as $image){
					 if(!empty($image)){
						 $product_image_model = new ProductImage;
						 
						 $image = str_replace('400x400','1100x1100',$image);
						 $image_name = time().$i.'.jpg';
						 $product_image_model->id_product = $product_id;
						 $product_image_model->image_path = $image;
						 $product_image_model->image = $image_name;
						 
						 $product_image_model->created = date('Y-m-d h:i:s');
						 $product_image_model->save();
						 
						 copy($image, 'uploads/products/'.$image_name);
					 }
				$i++;
				}
			}
	}
	

    public function saveBrand() {
        try {
//            $brand = Brand::model()->findByAttributes(array('name' => $this->brand));
            $brand = Brands::findOne(['brand_name'=>  $this->brand]);
            if ($brand == null) {
               //	echo $this->brand; die;
//                if (!empty($this->brand))
					
						$brand_model = new Brands();
						$brand_model->brand_name = $this->brand;
						$brand_model->brand_description = $this->brand;
						$brand_model->status = 'active';
						$brand_model->created = date('Y-m-d h:i:s');
						$logsModel = new Logs();
						$brand_model->folder_slug = $logsModel->getSlugFromName($this->brand);
						$brand_model->save();
						
						//Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vinayverma158@gmail.com','Vinay');  
						
						$brand = Brands::findOne(array('brand_name' => $this->brand));
				        
            }  
			//echo '<pre>'; print_r($brand); die;          
            return $brand;
        } catch (Exception $e) {
            $message = $e->getMessage();
            echo $message;
            //$subject = 'Error while saving brand in BaseProductImporter Class';        
            //Tools::alertMail($message, $subject, 'vikashkumar@girnarsoft.com');
        }
    }

    public function saveCategory() {
        try {
//            $category = Category::model()->findByAttributes(array('name' => $this->category));
            $category = Categories::findOne(['category_name'=>  $this->category]);
			
            if ($category == null) {
					    $category_model = new Categories();
						$category_model->category_name = $this->category;
						$category_model->category_description = $this->category;
						$category_model->category_status = 'active';
						$category_model->created = date('Y-m-d h:i:s');
						$logsModel = new Logs();
						$category_model->folder_slug = $logsModel->getSlugFromName($this->category);
						$category_model->save();
						
						 //Tools::alertMail("Create a new category named $this->category", "Create Category", 'vinayverma158@gmail.com','Vinay');
						 
						$category = Categories::findOne(['category_name'=>  $this->category]);
//                \common\components\Tools::alertMail("Create a new category named $this->category", "Create Category", 'vikashkumar@girnarsoft.com');
            }
            return $category;
        } catch (Exception $e) {
            $message = $e->getMessage();
            echo $message;
            //$subject = 'Error while saving category in BaseProductImporter Class';
            //Tools::alertMail($message, $subject, 'vikashkumar@girnarsoft.com');
        }
    }

    public function saveFeatures($product) {
        $product->features = $this->features;
        $product->saveFeatures();
    }

    public function saveTags($product) {
        $product->tags = $this->tags;
        $product->saveTags(true);
    }

    public function saveImages($product) {
        $product->images = $this->images;
        $product->saveImage();
    }


}

?>
