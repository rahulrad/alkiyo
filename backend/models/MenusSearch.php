<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Menus;

/**
 * MenusSearch represents the model behind the search form about `backend\models\Menus`.
 */
class MenusSearch extends Menus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id','brand_id', 'parent_id', 'type', 'status'], 'integer'],
            [['title', 'slug', 'others', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$menu_type)
    {
		
        $query = Menus::find();
		
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);
		
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'parent_id' => $this->parent_id,
            'type' => $this->type,
			'menu_type' => $menu_type,
            'status' => $this->status,
            //'created' => $this->created,
        ]);
		
		if(!empty($this->created)){
			$query->andFilterWhere(['like', 'created', date('Y-m-d', strtotime($this->created))]);
		}
		
		

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'others', $this->others]);
		
		$query->andFilterWhere(['!=', 'is_delete', 1]);
		
        return $dataProvider;
    }
}
