<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use common\models\Brands;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\Brands */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
		<?php
		echo $form->field($model, 'brand_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Brands::find()->andWhere(['status'=>'active'])->all(),'brand_id','brand_name'),
			'language' => 'eg',
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
    <?php echo  $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<?php /*  echo $form->field($model, 'address')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) */ ?>
	<?php echo  $form->field($model, 'address')->textarea(['rows' => 6]) ?>
	<?php echo  $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
	<?php echo  $form->field($model, 'pincode')->textInput(['maxlength' => true]) ?>
	<?php echo  $form->field($model, 'phone_no')->textInput(['maxlength' => true]) ?>
	<?php echo  $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
	<?php echo  $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>
	<?php echo  $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>
	
	<?php if($model->image): ?>
        <div class="form-group">
		<?php 
				//$path = str_replace('admin','frontend/web/',Yii::$app->getUrlManager()->getBaseUrl());
				$path = Yii::getAlias('@frontends').'/';
			?>
			<?php echo Html::img($path.$model->image,['class'=>'img-responsive  img-thumbnail small-thumbnail','alt'=>'delar Image']); ?>
        </div>
    <?php endif; ?>

	 <?php echo  $form->field($model,'images')->fileInput(); ?>
	<?php echo  $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>
	
    <div class="form-group">
        <?php echo  Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
