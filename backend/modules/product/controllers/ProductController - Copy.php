<?php
  namespace backend\modules\product\controllers;
use Yii;
use common\models\Products;
use backend\models\ProductsSearch;
use common\models\Brands;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Logs;
use backend\models\MongoProducts;
use backend\models\MongoProductFeatures;
use common\models\Categories;
use common\models\ProductsSuppliers;
use common\models\Suppliers;
use common\models\Features;
use common\models\FeatureValues;
use common\models\FeatureGroups;
use common\models\ProductFeature;
use common\models\ProductImage;
use common\models\ProductReviews;
use backend\models\ProductsTag;
use backend\models\ProductReviewsSearch;
use yii\web\UploadedFile;
use common\components\CImage;


/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductController extends Controller
{

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'mapping', 'products_mapping', 'searching_similler_products', 'product_map', 'products_mapping_custom', 'save_filter_mongo_products', 'product_features', 'importdata', 'export', 'unlink_product', 'ajax_mongo_product_popup_form', 'save_ajax_mongo_product', 'product_reviews', 'change_status', 'mongo_product_features', 'view_suppilers', 'change_is_approved', 'change_multiproduct_is_approved', 'update_name', 'products_mapping_user_friendly', 'search_nayashoppy_products_for_mapping', 'upload-file', 'products-mapping', 'search-mongo-products-for-mapping', 'delete-suppilers', 'edit-suppiler', 'product-ajax-map', 'delete-product-image', 'delete-product-other-image','menu-list'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        /**$products = Products::find()->select(['product_id','product_name','brands_brand_id'])->all();
         *
         * $name = array();
         * foreach($products as $product){
         * $n = explode(' ',$product->product_name);
         * $name[] = $n[0];
         *
         * $brands = Brands::find()->select(['brand_id','brand_name'])->where(['brand_name' => $n[0]])->one();
         * //echo $product->product_id.'===>'.$brands->brand_id.'===>'.$product->brands_brand_id.'<br>';
         * if(!empty($brands->brand_id)){
         * if($brands->brand_id != $product->brands_brand_id){
         * $productModel = Products::find()->where(['product_id' => $product->product_id])->one();
         * $productModel->brands_brand_id = $brands->brand_id;
         * $productModel->save(false);
         * }
         * }
         *
         *
         * }
         * echo '<pre>'; print_r($name); die; **/


        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new ProductsSearch();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Displays Product model for product mapping.
     * //@param integer $id
     * @return mixed
     */

    // step first
    public function actionMapping()
    {
        $model = new Products();
        return $this->render('mapping', [
            'model' => $model,
        ]);

    }

    //step second
    public function actionProducts_mapping()
    {

        $model = new Products();

        $getFormData = Yii::$app->request->get();


        $supplierResult = Suppliers::find()->where(['id' => $getFormData['Products']['store_id']])->one();
        if (!empty($supplierResult)) {
            $getFormData['Products']['store_name'] = $supplierResult->name;
        }

        // echo '<pre>'; print_r($getFormData); die;
        if ($model->load(Yii::$app->request->get())) {

            $postData = Yii::$app->request->get(); //
            $supplierResult = Suppliers::find()->where(['id' => $postData['Products']['store_id']])->one();
            if (!empty($supplierResult)) {
                $postData['Products']['store_name'] = $supplierResult->name;
            }
            //echo '<pre>'; print_r($postData); die;
            if (!empty($postData)) {

                $category_id = (int)$postData['Products']['categories_category_id'];
                $category_model = new Categories();
                $categoryDetail = $category_model::find()->where(['category_id' => $category_id])->one();

                $brand_id = (int)$postData['Products']['brands_brand_id'];
                $brandModel = new Brands();
                $brandDetail = $brandModel::find()->where(['brand_id' => $brand_id])->one();

                //echo 'store_id => '.$postData['Products']['store_id']; die;
                //echo $postData['Products']['store_id']; die;
                $MongoProductsModel = new MongoProducts();

                $searchData = array();
                if (!empty($categoryDetail->category_name)) {
                    $searchData['category_name'] = $categoryDetail->category_name;
                }
                if (!empty($brandDetail->brand_name)) {
                    $searchData['brand_name'] = $brandDetail->brand_name;
                }
                if (!empty($postData['Products']['store_id'])) {
                    $searchData['store_id'] = (int)$postData['Products']['store_id'];
                }
                $searchData['status'] = 1;

                if (isset($postData['Products']['created']) && !empty($postData['Products']['created'])) {
                    $products = $MongoProductsModel::find()->select(['_id', 'product_name', 'price', 'url', 'brand_name', 'store_name', 'store_id'])->where($searchData)->andWhere(['like', 'created', date('Y-m-d', strtotime($postData['Products']['created']))])->all();
                } else {
                    $products = $MongoProductsModel::find()->select(['_id', 'product_name', 'price', 'url', 'brand_name', 'store_name', 'store_id'])->where($searchData)->all();
                }


                return $this->render('products_mapping', ['products' => $products, 'model' => $model, 'getFormData' => $getFormData]);


            } else {
                return $this->redirect(['products/mapping']);
            }

        } else {
            return $this->redirect(['products/mapping']);
        }
    }


    // step third
    public function actionSearching_similler_products()
    {
        $model = new Products();

        if (Yii::$app->request->isAjax) {

            $postData = Yii::$app->request->post();

            $similer_products = $model->Check_similler_products($postData);

            \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;

            return $this->renderPartial('_searching_similler_products', ['similer_products' => $similer_products, 'code' => 100, 'model' => $model, 'postData' => $postData]);
            /*return [
                'similer_products' => $similer_products,
                'code' => 100,
            ];*/
        }
    }


   


   

    // step fourth
    public function actionProduct_map()
    {
        $model = new Products();
        if ($model->load(Yii::$app->request->post())) {

            $postData = Yii::$app->request->post();


            $productDetail = $this->findModel($postData['Products']['product_id']);

            $MongoProductsModel = new MongoProducts();
            $mongoProductDetail = $MongoProductsModel::findOne($postData['Products']['map_product_id']);
            //echo '<pre>'; print_r($mongoProductDetail); die;
            $ProductsSuppliersModel = new ProductsSuppliers();
            $saveProductSuppilerData = $ProductsSuppliersModel->saveSuppilerDataFromMongoDb($mongoProductDetail, $productDetail);
            //echo '<pre>test'; print_r($saveProductSuppilerData); die;
//            $model->changeProductFlagAccordingPrice($productDetail, $saveProductSuppilerData);

            if (!is_null($saveProductSuppilerData)) {
                Yii::$app->session->setFlash('success', 'Product is successfully mapp from mongoDb to our database');
            } else {
                Yii::$app->session->setFlash('error', "Unable to save product supplier");
            }
            return $this->redirect(Yii::$app->request->referrer);

        }
    }

    public function actionProductAjaxMap()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new Products();
        if ($model->load(Yii::$app->request->post())) {

            $postData = Yii::$app->request->post();
            if (isset($postData['Products']['product_id']) && isset($postData['Products']['map_product_id'])) {
                $productDetail = $this->findModel($postData['Products']['product_id']);
                $MongoProductsModel = new MongoProducts();
                $mongoProductDetail = $MongoProductsModel::findOne($postData['Products']['map_product_id']);
                $ProductsSuppliersModel = new ProductsSuppliers();
                $saveProductSuppilerData = $ProductsSuppliersModel->saveSuppilerDataFromMongoDb($mongoProductDetail, $productDetail);

                if (!is_null($saveProductSuppilerData)) {
                    return [
                        'status' => true,
                        'data' => "Product is successfully mapp from mongoDb to our database",
                    ];
                } else {
                    return [
                        'status' => false,
                        'data' => "Unable to save product supplier,Try again Later!",
                    ];
                }
            } else {
                return [
                    'status' => false,
                    'data' => "Select Both Mongo And NS product before mapping request",
                ];
            }
        } else {
            return [
                'status' => false,
                'data' => "Invalid Data Send,Try again Later!",
            ];
        }
    }

    public function actionProducts_mapping_custom()
    {

        $model = new Products();

        $productModel = new Products();

        $getFormData = Yii::$app->request->get();
        $supplierResult = Suppliers::find()->where(['id' => $getFormData['Products']['store_id']])->one();
        if (!empty($supplierResult)) {
            $getFormData['Products']['store_name'] = $supplierResult->name;
        }
        if ($model->load(Yii::$app->request->get())) {

            $postData = Yii::$app->request->get(); //
            $supplierResult = Suppliers::find()->where(['id' => $postData['Products']['store_id']])->one();
            if (!empty($supplierResult)) {
                $postData['Products']['store_name'] = $supplierResult->name;
            }
            //echo '<pre>'; print_r($postData); die;
            if (!empty($postData)) {

                $category_id = (int)$postData['Products']['categories_category_id'];
                $category_model = new Categories();
                $categoryDetail = $category_model::find()->where(['category_id' => $category_id])->one();


                $brand_id = (int)$postData['Products']['brands_brand_id'];
                $brandModel = new Brands();
                $brandDetail = $brandModel::find()->where(['brand_id' => $brand_id])->one();

                $searchData = array();
                if (!empty($categoryDetail->category_name)) {
                    $searchData['category_name'] = $categoryDetail->category_name;
                }
                if (!empty($brandDetail->brand_name)) {
                    $searchData['brand_name'] = $brandDetail->brand_name;
                }
                if (!empty($postData['Products']['store_id'])) {
                    $searchData['store_id'] = (int)$postData['Products']['store_id'];
                }
                $searchData['status'] = 1;
                //echo '<pre>'; print_r($searchData); die;
                $MongoProductsModel = new MongoProducts();
                if (isset($postData['Products']['created']) && !empty($postData['Products']['created'])) {
                    $mongoProducts = $MongoProductsModel::find()->select(['_id', 'product_name', 'price', 'url', 'brand_name', 'store_name', 'store_id'])->where($searchData)->andWhere(['like', 'created', date('Y-m-d', strtotime($postData['Products']['created']))])->all();
                } else {
                    $mongoProducts = $MongoProductsModel::find()->select(['_id', 'product_name', 'price', 'url', 'brand_name', 'store_name', 'store_id'])->where($searchData)->all();
                }


                //echo '<pre>'; print_r($mongoProducts); die;


                $productModel = new Products();
                /**$searchKeyProdcuts = array();
                 * if(!empty($category_id)){
                 * $searchKeyProdcuts['categories_category_id'] = $category_id;
                 * }
                 * if(!empty($brand_id)){
                 * $searchKeyProdcuts['brands_brand_id'] = $brand_id;
                 * }
                 * $searchKeyProdcuts['active'] = 'active'; **/

                $productLists = $productModel::find()->joinWith('brand')->where(['categories_category_id' => $category_id, 'brands_brand_id' => $brand_id, 'active' => 'active'])->all();

                //echo '<pre>'; print_r($productLists); die;
                //$products = $MongoProductsModel::find()->joinWith('brand')->select(['product_id', 'product_name','lowest_price','url','brands_brand_id'])->where(['categories_category_id' => $postData['Products']['categories_category_id'],'store_name' => $postData['Products']['store_name']])->all();

                return $this->render('products_mapping_custom', ['products' => $mongoProducts, 'model' => $model, 'getFormData' => $getFormData, 'productLists' => $productLists, 'productModel' => $productModel]);


            } else {
                return $this->redirect(['products/mapping']);
            }

        } else {
            return $this->redirect(['products/mapping']);
        }
    }


    public function actionProducts_mapping_user_friendly()
    {

        $model = new Products();

        $productModel = new Products();

        $getFormData = Yii::$app->request->get();
        $supplierResult = Suppliers::find()->where(['id' => $getFormData['Products']['store_id']])->one();
        if (!empty($supplierResult)) {
            $getFormData['Products']['store_name'] = $supplierResult->name;
        }
        if ($model->load(Yii::$app->request->get())) {

            $postData = Yii::$app->request->get(); //
            $supplierResult = Suppliers::find()->where(['id' => $postData['Products']['store_id']])->one();
            if (!empty($supplierResult)) {
                $postData['Products']['store_name'] = $supplierResult->name;
            }
            //echo '<pre>'; print_r($postData); die;
            if (!empty($postData)) {

                $category_id = (int)$postData['Products']['categories_category_id'];
                $category_model = new Categories();
                $categoryDetail = $category_model::find()->where(['category_id' => $category_id])->one();


                $brand_id = (int)$postData['Products']['brands_brand_id'];
                $brandModel = new Brands();
                $brandDetail = $brandModel::find()->where(['brand_id' => $brand_id])->one();

                $searchData = array();
                if (!empty($categoryDetail->category_name)) {
                    $searchData['category_name'] = $categoryDetail->category_name;
                }
                if (!empty($brandDetail->brand_name)) {
                    $searchData['brand_name'] = $brandDetail->brand_name;
                }
                if (!empty($postData['Products']['store_id'])) {
                    $searchData['store_id'] = (int)$postData['Products']['store_id'];
                }
                $searchData['status'] = 1;
                //echo '<pre>'; print_r($searchData); die;
                $MongoProductsModel = new MongoProducts();
                if (isset($postData['Products']['created']) && !empty($postData['Products']['created'])) {
                    $mongoProducts = $MongoProductsModel::find()->select(['_id', 'product_name', 'price', 'url', 'brand_name', 'store_name', 'store_id'])->where($searchData)->andWhere(['like', 'created', date('Y-m-d', strtotime($postData['Products']['created']))])->all();
                } else {
                    $mongoProducts = $MongoProductsModel::find()->select(['_id', 'product_name', 'price', 'url', 'brand_name', 'store_name', 'store_id'])->where($searchData)->all();
                }


                //echo '<pre>'; print_r($mongoProducts); die;


                $productModel = new Products();

                $productLists = $productModel::find()->joinWith('brand')->where(['categories_category_id' => $category_id, 'brands_brand_id' => $brand_id, 'active' => 'active'])->all();


                return $this->render('products_mapping_user_friendly', ['products' => $mongoProducts, 'model' => $model, 'getFormData' => $getFormData, 'productLists' => $productLists, 'productModel' => $productModel]);


            } else {
                return $this->redirect(['products/mapping']);
            }

        } else {
            return $this->redirect(['products/mapping']);
        }
    }


    public function actionSave_filter_mongo_products()
    {

        $selection_data = (array)Yii::$app->request->post('selection');//typecasting

        if (!empty($selection_data)) {
            //echo '<pre>'; print_r($selection_data); die;
            foreach ($selection_data as $id) {

                $MongoProductsModel = new MongoProducts();
                $mongoProduct = $MongoProductsModel::find()->where(['_id' => $id])->one();

                if (!empty($mongoProduct)) {

                    $product_model = new Products();
                    $product_model->saveFilterMongoProducts($mongoProduct);

                }

            }
            Yii::$app->session->setFlash('success', 'Product is successfully saved');

            return $this->redirect(Yii::$app->request->referrer);

        }

    }

    /**
     * Product Feature Function
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */

    public function actionProduct_features()
    {

        $model = new FeatureValues();
        $getData = Yii::$app->request->get();

        if (!empty($getData) && isset($getData['product_id'])) {

            $product_id = $getData['product_id'];

            $productDetail = Products::find()->select(['product_id', 'product_name'])->where(['product_id' => $product_id])->one();

            $productFeatureModel = new ProductFeature();

            $productFeatures = $productFeatureModel->getProductFeatures($product_id);

            $selectedFeatureValues = $productFeatureModel->getSelectedProductFeatures($product_id);


            $postData = Yii::$app->request->post();

            if (!empty($postData)) {

                $updateProductFeatures = $productFeatureModel->updateProductFeatures($postData);
				$productDetail->updated=date('Y-m-d H:i:s');
                $productDetail->save();
                Yii::$app->session->setFlash('success', 'Product Feature is successfully update');
                return $this->redirect(['products/']);

            }
            return $this->render('product_features', ['productFeatures' => $productFeatures, 'model' => $model, 'productDetail' => $productDetail, 'selectedFeatureValues' => $selectedFeatureValues]);


        }

    }


    public function actionUnlink_product()
    {
        $model = new Products();
        $getData = Yii::$app->request->get();

        if (!empty($getData) && isset($getData['product_id'])) {

            $result = $model->unlink_product_from_suppilers($getData['product_id']);

            // save user logs data in user_logs table
            $logs_model = new Logs();

            $user_logs = array('model' => 'Products', 'action' => 'Unlink Product', 'activity' => $result->product_name . ' is added', 'action_id' => $result->product_id);
            $logs_model->saveUserLogs($user_logs);

            Yii::$app->session->setFlash('success', 'Product Unlink to stores successfully saved');

            return $this->redirect(['index']);

        }
    }


    /**
     * Import Product Data
     */

    public function actionImportdata()
    {
        $model = new Products();
        if ($model->load(Yii::$app->request->post())) {
			$file = UploadedFile::getInstance($model, 'file');
            $filename = 'Data-' . Date('YmdGis') . '.' . $file->extension;
            $upload = $file->saveAs('uploads/product_import/' . $filename);
            //echo '<pre>'; print_r($fields); die;
            if ($upload) {
                define('CSV_PATH', 'uploads/product_import/');
                $csv_file = CSV_PATH . $filename;
                $row = 1;
            if (($handle = fopen($csv_file, "r")) !== FALSE) {
					
                    while (($data = fgetcsv($handle, 2000, ",")) !== FALSE) {
								if($row==1)
								{
									$headers = $data;
								}
								if($row == 2)
								{
									$categories = \common\models\Categories::find()->select(['category_id'])->where(['slug' => $data[1]])->one();
									if(!empty($categories)){
										$catId = $categories->category_id;
									}else{
										$catId = 0;
									}
										
									$tableFeidls = Products::find()->one();
									 $fields = array();
										foreach ($tableFeidls as $key => $field_name) {
											$fields[] = $key;
										}
										if($catId>0){
											$featureData = Features::find()->where(['category_id'=>$catId])->All();
												foreach ($featureData as  $field_name) {
												$fields[] = strtolower(str_replace(" ","_",$field_name['name']));
											}
										}
										$arraysAreEqual = ($headers == $fields);
										if($arraysAreEqual != 1) {
											break;
										}
								}
								
								if($row != 1){
								
									$productModel = new \common\models\Products();									
									$countRecords = $productModel::find()->orderBy('product_id desc')->one();
									if(!empty($countRecords)){
										$b_id = $countRecords->product_id + 1;
									}else{
										$b_id = 1;
									}
									
									if(empty($checkResult)){
										$productModel->product_id = $b_id;
										$productModel->categories_category_id = $catId;
										if(isset($data[2]))
										{
											$brandId = \common\models\Brands::find()->select(['brand_id'])->where(['slug'=>strtolower(str_replace(" ","-",$data[2]))])->one();
											$productModel->brands_brand_id = $brandId['brand_id'];
										}
										else
										{
											$productModel->brands_brand_id = 0;
										}
										//$categoryModel->promo_id = isset($data[0])?$data[0]:'';
										$productModel->product_name = isset($data[3])?$data[3]:'';
										$productModel->slug = strtolower(str_replace(" ","-",$data[3]));
										$productModel->store_product_name = isset($data[5])?$data[5]:'';
										$productModel->image = isset($data[6])?$data[6]:'';
										$productModel->image_path = isset($data[7])?$data[7]:'';
										$productModel->scraper_image_status = isset($data[8])?$data[8]:'';
										$productModel->product_description = isset($data[9])?$data[9]:'';
										$productModel->care_instruction = isset($data[10])?$data[10]:'';
										$productModel->model_number = isset($data[11])?$data[11]:'';
										$productModel->cod = isset($data[12])?$data[12]:'';
										$productModel->emi = isset($data[13])?$data[13]:'';
										$productModel->colors = isset($data[14])?$data[14]:'';
										$productModel->sizes = isset($data[15])?$data[15]:'';
										$productModel->offers = isset($data[16])?$data[16]:'';
										$productModel->return_policy = isset($data[17])?$data[17]:'';
										$productModel->delivery = isset($data[18])?$data[18]:'';
										$productModel->shipping = isset($data[19])?$data[19]:'';
										$productModel->price = isset($data[20])?$data[20]:'';
										$productModel->unique_id = isset($data[21])?$data[21]:'';
										$productModel->discount = isset($data[22])?$data[22]:'';
										$productModel->rating = isset($data[23])?$data[23]:'';
										$productModel->rating_user_count = isset($data[24])?$data[24]:'';
										$productModel->reviews = isset($data[25])?$data[25]:'';
										$productModel->url = isset($data[26])?$data[26]:'';
										$productModel->product_status = isset($data[27])?$data[27]:'';
										$productModel->sizes = isset($data[28])?$data[28]:'';
										$productModel->date_add = isset($data[29])?$data[29]:'';
										$productModel->date_update = isset($data[30])?$data[30]:'';
										$productModel->date_launch = isset($data[31])?$data[31]:'';
										$productModel->lowest_price = isset($data[32])?$data[32]:'';
										$productModel->original_price = isset($data[33])?$data[33]:'';
										$productModel->highest_price = isset($data[34])?$data[34]:'';
										$productModel->link_rewrite = isset($data[35])?$data[35]:'';
										$productModel->supplier_count = isset($data[36])?$data[36]:'';
										$productModel->store_name = isset($data[37])?$data[37]:'';
										$productModel->store_id = isset($data[38])?$data[38]:'';										
										$productModel->meta_title = isset($data[39])?$data[39]:'';
										$productModel->meta_keyword = isset($data[40])?$data[40]:'';
										$productModel->meta_desc = isset($data[41])?$data[41]:'';
										$productModel->show_home_page = isset($data[42])?$data[42]:'';
										$productModel->active = isset($data[43])?$data[43]:'';
										$productModel->is_delete = 0;
										$productModel->is_approved = 1;
										$productModel->created = date('Y-m-d h:i:s');
										$productModel->date_update = date('Y-m-d h:i:s');
										$productModel->instock = isset($data[47])?$data[47]:'';
										$productModel->image_scrapped = 1;
										$productModel->feature_scrapped = isset($data[49])?$data[49]:'';
										$productModel->upcoming = 'active';
										$productModel->save(false); 
										$productId = Yii::$app->db->getLastInsertID();
										
										$i = 51;
										
										foreach($featureData as $features)
										{
											
											if(isset($data[$i]) && $data[$i] != '')
											{
												$fetureValue = \common\models\FeatureValues::find()->where(['feature_id'=>$features['id']])->andWhere(['LIKE','value',$data[$i]])->All();
												if(empty($fetureValue))
												{
													$featureValue = new \common\models\FeatureValues();
													$featureValue->feature_id = $features['id'];
													$featureValue->value = ucwords($data[$i]);
													$featureValue->display_name = ucwords($data[$i]);
													$featureValue->is_delete = 0;
													$featureValue->created = date('Y-m-d h:i:s');
													$featureValue->save(false);
													$featureValueId = Yii::$app->db->getLastInsertID();
												}
												else{												
													$featureValueId = $fetureValue[0]['id'];												
												}										
												$productFeatures = new \common\models\ProductFeature();
												$productFeatures->id_product = $productId;
												$productFeatures->id_feature = $features['id'];
												$productFeatures->id_feature_value = $featureValueId;
												$productFeatures->save(false);
											}										
											$i++;
										}
										
									}
								}
							$row++;
							
						}
						 fclose($handle);
                    
                }
                //unlink('uploads/delar/' . $filename);
				if($arraysAreEqual != 1) {
											 Yii::$app->session->setFlash('success', 'Product upload in Correct format');
											 return $this->redirect(['index']);
										}else {
											 Yii::$app->session->setFlash('success', 'import success');
											 return $this->redirect(['index']);
										}
            }
        } else {
            return $this->render('importdata', ['model' => $model]);
        }

    }

    /**
     * Export Products Data
     */

    public function actionExport()
    {
		$data = Yii::$app->request->get();
        $products = Products::find()->All();
        $tableFeidls = Products::find()->one();
        $fields = array();
        foreach ($tableFeidls as $key => $field_name) {
            $fields[$key] = $key;
        }
		
		if($data['cat_id']>0){
			$featureData = Features::find()->where(['category_id'=>$data['cat_id']])->All();
				foreach ($featureData as  $field_name) {
				$fields[strtolower(str_replace(" ","_",$field_name['name']))] = strtolower(str_replace(" ","_",$field_name['name']));
			}
		}
		
		
		
        //echo '<pre>'; print_r($fields); die;

        $filename = 'Data-' . Date('YmdGis') . '-Products.csv';
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=" . $filename);
        header("Pragma: no-cache");
        header("Expires: 0");


        $file = fopen('php://output', 'w');
        fputcsv($file, $fields);
		
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Products();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $post_data = Yii::$app->request->post();

            if (!empty($post_data['Products']['slug'])) {

                $model->slug = $post_data['Products']['slug'];

            }


            // save brand image
           /*  $model->main_image = UploadedFile::getInstance($model, 'main_image');
            if (!empty($model->main_image)) {
                $imageName = $model->main_image->baseName;

                $model->main_image->saveAs('uploads/products/' . $imageName . '.' . $model->main_image->extension);


                $model->image = $imageName . '.' . $model->main_image->extension;

            } */
			
			
			$model->main_image = UploadedFile::getInstance($model,'main_image');
			
			if(!empty($model->main_image)){
				//$imageName = $model->file->baseName;
                $path = Yii::getAlias('@frontend') .'/web/';
                $imageName = time().uniqid();
				
				//$model->file->saveAs('uploads/categories/'.$imageName.'.'.$model->file->extension);
                $model->main_image->saveAs($path.'uploads/products/'.$imageName.'.'.$model->main_image->extension);
				
				$model->image = 'uploads/products/'.$imageName.'.'.$model->main_image->extension;
			}
			
			
			
			
			
            if (!isset($post_data['Products']['is_approved'])) {
                $model->is_approved = 0;
            }
            if (!isset($post_data['Products']['show_home_page'])) {
                $model->show_home_page = 0;
            }

            $model->created = date('Y-m-d h:i:s');

            $model->save();


            // update products tags
            if (isset($post_data['Products']['product_tag'])) {
                $product_tags = $post_data['Products']['product_tag'];
                if (!empty($product_tags)) {
                    $ProductsTagModel = new ProductsTag();
                    $ProductsTagModel::deleteAll(['product_id' => $model->product_id]);
                    $ProductsTagModel->saveProductsTag($product_tags, $model->product_id);
                }
            }


            // get all other images
            $other_images = UploadedFile::getInstances($model, 'other_images');
            // check if !empty then it will be enter this code
            if (!empty($other_images)) {
                $ProductImageModel = new ProductImage();
                $ProductImageModel::deleteAll(['id_product' => $model->product_id]);
                $ProductImageModel->saveProductMultiImages($other_images, $model->product_id);
            }


            // save user logs data in user_logs table
            $logs_model = new Logs();

            $user_logs = array('model' => 'Products', 'action' => 'Create', 'activity' => $model->product_name . ' is added', 'action_id' => $model->product_id);
            $logs_model->saveUserLogs($user_logs);

            Yii::$app->session->setFlash('success', 'Product is successfully saved');

            //  return $this->redirect(['view', 'id' => $model->product_id]);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post()) && $model->validate()) {


            $post_data = Yii::$app->request->post();

            if (!empty($post_data['Products']['slug'])) {

                $model->slug = $post_data['Products']['slug'];

            }

            // save main image

            $model->main_image = UploadedFile::getInstance($model, 'main_image');
            if (!empty($model->main_image)) {
                //$model->main_image->name;
//                $path = Yii::getAlias('@frontend') . '/web/';
//
                $categoryDetail = Categories::findOne(['category_id' => $post_data['Products']['categories_category_id']]);
                $brandDetail = Brands::findOne(['brand_id' => $post_data['Products']['brands_brand_id']]);
//
//                $folder_path = $path . 'uploads/products/' . $categoryDetail->folder_slug . '/' . $brandDetail->folder_slug;
//                if (!file_exists($folder_path)) {
//                    mkdir($folder_path, 0755, true);
//                }
//
//                $imageName = time() . $model->main_image->name;
//                $model->main_image->saveAs($folder_path . '/' . $imageName);
//
//                $model->image = $imageName;
//
//
//                $thumImageSizes = array('150x200', '300x400');
//                foreach ($thumImageSizes as $size) {
//                    $sizeArr = explode('x', $size);
//
//                    if (!file_exists($folder_path . '/' . $size)) {
//                        mkdir($folder_path . '/' . $size, 0755, true);
//                    }
//                    $destFile = $folder_path . '/' . $size . '/' . $imageName;
//                    $mainImage = $folder_path . '/' . $imageName;
//                    $objImageResize = new \common\components\CImage();
//                    $response = $objImageResize->imageResize($mainImage, $destFile, $sizeArr[1], $sizeArr[0], 'jpg');
//                }
                $model->image = Products::saveProductImage($categoryDetail, $brandDetail, $model->main_image, "main", true);

            }

            /**$model->main_image = UploadedFile::getInstance($model,'main_image');
             * if(!empty($model->main_image)){
             * $imageName = $model->main_image->baseName;
             * $path = Yii::getAlias('@frontend') .'/web/';
             * $model->main_image->saveAs($path.'uploads/products/'.$imageName.'.'.$model->main_image->extension);
             *
             *
             * $model->image = $imageName.'.'.$model->main_image->extension;
             *
             * } **/


            if (!isset($post_data['Products']['show_home_page'])) {
                $model->show_home_page = 0;
            }

            if (!isset($post_data['Products']['is_approved'])) {
                $model->is_approved = 0;
            }
             $model->updated = date('Y-m-d H:i:s');
            $model->save();


            // update products tags
            if (isset($post_data['Products']['product_tag'])) {
                $product_tags = $post_data['Products']['product_tag'];
                if (!empty($product_tags)) {
                    $ProductsTagModel = new ProductsTag();
                    $ProductsTagModel::deleteAll(['product_id' => $model->product_id]);
                    $ProductsTagModel->saveProductsTag($product_tags, $model->product_id);
                }
            }

            // get all other images
            $other_images = UploadedFile::getInstances($model, 'other_images');


            // check if !empty then it will be enter this code
            if (!empty($other_images)) {
                $ProductImageModel = new ProductImage();
                //$ProductImageModel::deleteAll(['id_product' => $model->product_id]);
                $ProductImageModel->saveProductMultiImages($other_images, $model->product_id);
            }


            // save user logs data in user_logs table
            $logs_model = new Logs();

            $user_logs = array('model' => 'Products', 'action' => 'Update', 'activity' => $model->product_name . ' is updated', 'action_id' => $model->product_id);
            $logs_model->saveUserLogs($user_logs);

            Yii::$app->session->setFlash('success', 'Product is successfully update');

            // return $this->redirect(['view', 'id' => $model->product_id]);
//		   return $this->redirect(['index']);
            return $this->goBack();
        } else {
            Yii::$app->user->setReturnUrl((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : Url::to(['index'])));
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        /**$model = $this->findModel($id);
         * $this->findModel($id)->delete();
         *
         * // delete product suppilers by product_id
         * ProductsSuppliers::deleteAll(['product_id' =>$id]);
         *
         * // delete product features by product_id
         * ProductFeature::deleteAll(['id_product' =>$id]);
         *
         * // delete product images by product_id
         * ProductImage::deleteAll(['id_product' =>$id]);
         *
         * // delete product tags by product_id
         * ProductsTag::deleteAll(['product_id' =>$id]);
         *
         * // delete product reviews by product_id
         * ProductReviews::deleteAll(['product_id' =>$id]); **/


        $model = $this->findModel($id);
        $modelDelete = $model;
        $modelDelete->is_delete = 1;
        $modelDelete->updated = date('Y-m-d H:i:s');
        
        $modelDelete->save(false);

        // save user logs data in user_logs table
        $logs_model = new Logs();

        $user_logs = array('model' => 'Products', 'action' => 'Delete', 'activity' => $model->product_name . ' is deleted', 'action_id' => $model->product_id);
        $logs_model->saveUserLogs($user_logs);

        $model->moveProductDataToMongo();

        Yii::$app->session->setFlash('success', 'Product is successfully delete');

        //  return $this->redirect(['index']);

        return $this->redirect(Yii::$app->request->referrer);
    }


    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {

            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionChange_status()
    {
        $model = new Products();
        $getData = Yii::$app->request->get();

        if (!empty($getData) && isset($getData['product_id'])) {

            $productModel = Products::find()->where(['product_id' => $getData['product_id']])->one();

            if ($getData['type'] == 'inactive') {
                $status = 'inactive';
            } else {
                $status = 'active';
            }

            $productModel->active = $status;
            $productModel->save(false);

            $logs_model = new Logs();

            $user_logs = array('model' => 'Products', 'action' => 'Change Product Status', 'activity' => $productModel->product_name . ' is change status', 'action_id' => $productModel->product_id);
            $logs_model->saveUserLogs($user_logs);

            Yii::$app->session->setFlash('success', 'Product Status changed successfully saved');

            return $this->redirect(['index']);

        }
    }


    public function actionView_suppilers()
    {

        $model = new ProductsSuppliers();
        $getData = Yii::$app->request->get();

        if (!empty($getData) && isset($getData['product_id'])) {

            $productsDetail = Products::find()->select(['product_name'])->where(['product_id' => $getData['product_id']])->one();

            $productsSuppliers = ProductsSuppliers::find()->where(['product_id' => $getData['product_id']])->all();

            return $this->render('view_suppilers', ['model' => $model, 'productsSuppliers' => $productsSuppliers, 'productsDetail' => $productsDetail]);

        }

    }

    public function actionEditSuppiler($id = 0)
    {
        $model = new ProductsSuppliers();
        if (($model = $model::findOne($id)) !== null) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->session->setFlash('success', 'Supplier updated successfully');
                return $this->redirect(Yii::$app->request->referrer);
            }
            return $this->render('update_product_supplier', [
                'model' => $model,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }

    public function actionDeleteSuppilers($id = 0)
    {
        $productsSupplier = ProductsSuppliers::findOne($id);
        if (!is_null($productsSupplier)) {
            $product = Products::findOne($productsSupplier->product_id);
            if (!is_null($product)) {
                $productsSupplierCount = $product->getSuppliersProductsCount($productsSupplier->product_id);
                if ($productsSupplierCount > 1) {
                    $mongoProductDetail = MongoProducts::find()->where(["url" => $productsSupplier->url])->one();
                    if (!is_null($mongoProductDetail)) {
                        if (!empty($mongoProductDetail->product_mapping)) {
                            $currentMapping = explode(",", $mongoProductDetail->product_mapping);
                            $currentMapping = array_filter($currentMapping, function ($v, $k) use ($productsSupplier) {
                                return $v != $productsSupplier->product_id;
                            }, ARRAY_FILTER_USE_BOTH);
                            $mongoProductDetail->product_mapping = implode(",", $currentMapping);
                            $mongoProductDetail->save(false);
                        }
                    }
                    $productsSupplier->delete();
                    $product->updateLowestProductSupplier();
                } else {
                    Yii::$app->session->setFlash('error', "Cann'nt delete last supplier!");
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }
        }

        Yii::$app->session->setFlash('success', 'Supplier unbinded successfully');
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionChange_is_approved()
    {
        $model = new Products();
        $getData = Yii::$app->request->get();

        if (!empty($getData) && isset($getData['product_id'])) {

            $productModel = Products::find()->where(['product_id' => $getData['product_id']])->one();

            if ($getData['type'] == 0) {
                $is_approved = 0;
                $status = 'inactive';
            } else {
                $is_approved = 1;
                $status = 'active';
            }

            $productModel->active = $status;
            $productModel->is_approved = $is_approved;
            $productModel->save(false);

            $logs_model = new Logs();

            $user_logs = array('model' => 'Products', 'action' => 'Change Product Is Approved', 'activity' => $productModel->product_name . ' is change is_approved', 'action_id' => $productModel->product_id);
            $logs_model->saveUserLogs($user_logs);

            Yii::$app->session->setFlash('success', 'Product Approved changed successfully saved');

            return $this->redirect(['index']);

        }
    }

    public function actionChange_multiproduct_is_approved()
    {
        $selection_data = (array)Yii::$app->request->post('selection');//typecasting


        if (!empty($selection_data)) {
            //echo '<pre>'; print_r($selection_data); die;
            foreach ($selection_data as $id) {

                $productModel = Products::find()->where(['product_id' => $id])->one();


                $productModel->active = 'active';
                $productModel->is_approved = 1;
                $productModel->save(false);

                $logs_model = new Logs();

                $user_logs = array('model' => 'Products', 'action' => 'Change Product Is Approved', 'activity' => $productModel->product_name . ' is change is_approved', 'action_id' => $productModel->product_id);
                $logs_model->saveUserLogs($user_logs);


            }

            Yii::$app->session->setFlash('success', 'Product Approved changed successfully saved');

            return $this->redirect(['index']);


        }

    }


    public function actionUpdate_name()
    {

        $products = Products::find()->select(['product_id', 'product_name', 'store_product_name'])->all();

        if (!empty($products)) {

            foreach ($products as $product) {

                $product_model = $product;

                $product_model->store_product_name = $product->product_name;

                $product_model->save(false);
            }
        }

    }

    public function actionSearch_nayashoppy_products_for_mapping()
    {

        $model = new Products();

        if (Yii::$app->request->isAjax) {

            $postData = Yii::$app->request->post();

            parse_str($postData['formData'], $searcharray);


            $similer_products = $model->Check_similler_products_for_nayashoppy($searcharray);

            \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;

            return $this->renderPartial('_search_nayashoppy_products_for_mapping', ['similer_products' => $similer_products, 'code' => 100, 'model' => $model, 'postData' => $searcharray]);
            /*return [
                'similer_products' => $similer_products,
                'code' => 100,
            ];*/
        }
    }

    public function actionUploadFile()
    {
        $module = Yii::$app->getModule('articles');
        $imagePath = Yii::getAlias($module->itemImagePath);
        $imagePathUrl = Yii::getAlias($module->itemUplaodImageURL);
        $imgName = time();
        $fileField = "upload";

        $file = UploadedFile::getInstanceByName($fileField);
        $message = '';
        $url = '';
        if (is_null($file)) {
            $message = 'Failed Upload File';
        } else {
            $fileExt = $file->extension;
            if ($fileExt != 'jpg' && $fileExt != 'jpeg' && $fileExt != 'png' && $fileExt != 'gif') {
                $message = "The image must be in either JPG, JPEG, GIF or PNG format.";
            } else {
                $file->name = $imgName . ".{$fileExt}";
                $image = $file->saveAs($imagePath . $imgName . ".{$fileExt}");
                if ($image !== false) {
                    $url = $module->frontPageUrl . $imagePathUrl . '/' . $file->name;
                } else {
                    $message = 'Success Upload Failure,Try again!';
                }
            }
        }
        $funcNum = $_GET ['CKEditorFuncNum'];
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
    }

    public function actionProductsMapping()
    {
        $model = new Products();

        $productModel = new Products();

        $getFormData = Yii::$app->request->get();
//        $supplierResult = Suppliers::find()->where(['id' => $getFormData['Products']['store_id']])->one();
//        if (!empty($supplierResult)) {
//            $getFormData['Products']['store_name'] = $supplierResult->name;
//        }
        if ($model->load(Yii::$app->request->get())) {

            $postData = Yii::$app->request->get(); //
            $supplierResult = Suppliers::find()->where(['id' => $postData['Products']['store_id']])->one();
            if (!empty($supplierResult)) {
                $postData['Products']['store_name'] = $supplierResult->name;
            }
            //echo '<pre>'; print_r($postData); die;
            if (!empty($postData)) {

                $category_id = (int)$postData['Products']['categories_category_id'];
                $category_model = new Categories();
                $categoryDetail = $category_model::find()->where(['category_id' => $category_id])->one();


                $brand_id = (int)$postData['Products']['brands_brand_id'];
                $brandModel = new Brands();
                $brandDetail = $brandModel::find()->where(['brand_id' => $brand_id])->one();

                $searchData = array();
                if (!empty($categoryDetail->category_name)) {
                    $searchData['category_name'] = $categoryDetail->category_name;
                }
                if (!empty($brandDetail->brand_name)) {
                    $searchData['brand_name'] = $brandDetail->brand_name;
                }
                if (!empty($postData['Products']['store_id'])) {
                    $searchData['store_id'] = (int)$postData['Products']['store_id'];
                }
                $searchData['status'] = 1;
                //echo '<pre>'; print_r($searchData); die;
                $MongoProductsModel = new MongoProducts();
                if (isset($postData['Products']['created']) && !empty($postData['Products']['created'])) {
                    $mongoProducts = $MongoProductsModel::find()->select(['_id', 'product_name', 'price', 'url', 'brand_name', 'store_name', 'store_id'])->where($searchData)->andWhere(['like', 'created', date('Y-m-d', strtotime($postData['Products']['created']))])->all();
                } else {
                    $mongoProducts = $MongoProductsModel::find()->select(['_id', 'product_name', 'price', 'url', 'brand_name', 'store_name', 'store_id'])->where($searchData)->all();
                }


                $productModel = new Products();

                $productLists = $productModel::find()->joinWith('brand')->where(['categories_category_id' => $category_id, 'brands_brand_id' => $brand_id, 'active' => 'active'])->all();


                return $this->render('products-mapping', ['products' => $mongoProducts, 'model' => $model, 'getFormData' => $getFormData, 'productLists' => $productLists, 'productModel' => $productModel]);


            } else {
                return $this->redirect(['products/mapping']);
            }

        } else {
            return $this->render('products-mapping', ['products' => [], 'model' => $model, 'getFormData' => $getFormData, 'productLists' => [], 'productModel' => $productModel]);
        }
    }

    function actionSearchMongoProductsForMapping()
    {
        $postData = Yii::$app->request->post();
        parse_str($postData['formData'], $postData);

        $category_id = (int)$postData['Products']['categories_category_id'];
        $category_model = new Categories();
        $categoryDetail = $category_model::find()->where(['category_id' => $category_id])->one();


        $brand_id = (int)$postData['Products']['brands_brand_id'];
        $brandModel = new Brands();
        $brandDetail = $brandModel::find()->where(['brand_id' => $brand_id])->one();

        $searchData = array();
        if (!empty($categoryDetail->category_name)) {
            $searchData['category_name'] = $categoryDetail->category_name;
        }
        if (!empty($brandDetail->brand_name)) {
            $searchData['brand_name'] = $brandDetail->brand_name;
        }
        if (!empty($postData['Products']['store_id'])) {
            $searchData['store_id'] = (int)$postData['Products']['store_id'];
        }
        $searchData['status'] = 1;
        //echo '<pre>'; print_r($searchData); die;
        $MongoProductsModel = new MongoProducts();
        if (isset($postData['Products']['created']) && !empty($postData['Products']['created'])) {
            $mongoProducts = $MongoProductsModel::find()->select(['_id', 'product_name', 'price', 'url', 'brand_name', 'store_name', 'store_id', 'product_mapping'])->where($searchData)->andWhere(['like', 'created', date('Y-m-d', strtotime($postData['Products']['created']))])->all();
        } else {
            $mongoProducts = $MongoProductsModel::find()->select(['_id', 'product_name', 'price', 'url', 'brand_name', 'store_name', 'store_id', 'product_mapping'])->where($searchData)->all();
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        return $this->renderPartial('_search_mongo_products_for_mapping', ['similer_products' => $mongoProducts, 'getFormData' => $postData]);
    }

    public function actionDeleteProductImage()
    {
        $resonse = array();
        try {
            if (isset($_REQUEST['key'])) {
                $model = $this->findModel($_REQUEST['key']);
                $path = Yii::getAlias('@frontend') . '/web/';
                $imgFolder = explode(".", $model->image);
                $uploadPath = 'uploads/products/' . $_REQUEST['category_folder_slug'] . '/' . $_REQUEST['brand_folder_slug'] . "/" . $imgFolder[0];
                if (is_dir($path . $uploadPath)) {
                    FileHelper::removeDirectory($path . $uploadPath);
                }
                $model->image = null;
                $model->save();
            }
        } catch (\Exception $e) {
            $resonse['error'] = "Somthing went wrong ,Try again";
        }
        echo Json::encode($resonse);
        die;
    }

    public function actionDeleteProductOtherImage()
    {
        $resonse = array();
        try {
            if (isset($_REQUEST['key'])) {
                if (($model = ProductImage::findOne($_REQUEST['key'])) !== null) {
                    $path = Yii::getAlias('@frontend') . '/web/';
                    $imgFolder = explode(".", $model->image);
                    $uploadPath = 'uploads/products/' . $_REQUEST['category_folder_slug'] . '/' . $_REQUEST['brand_folder_slug'] . "/" . $imgFolder[0];
                    if (is_dir($path . $uploadPath)) {
                        FileHelper::removeDirectory($path . $uploadPath);
                    }
                    $model->delete();
                } else {
                    throw new NotFoundHttpException('The requested product image does not exist.');
                }
            } else {
                throw new NotFoundHttpException('Product image id not found');
            }
        } catch (\Exception $e) {
            $resonse['error'] = $e->getMessage();
        }
        echo Json::encode($resonse);
        die;
    }

    public function actionMenuList($q = null, $brandId = null, $categoryId = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        $query = new Query();
        $query->select('product_id as id, product_name as text, product_name as title')
            ->from('products')
            ->where(['like', 'product_name', $q])
            ->limit(20);

        if (!is_null($brandId) && !empty($brandId)) {
            $query->andWhere(['brands_brand_id' => $brandId]);
        }
        if (!is_null($categoryId) && !empty($categoryId)) {
            $query->andWhere(['categories_category_id' => $categoryId]);
        }
        $command = $query->createCommand();
        $data = $command->queryAll();
        $out['results'] = array_values($data);
        return $out;
    }
}
