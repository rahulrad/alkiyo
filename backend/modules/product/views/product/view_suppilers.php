<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use common\models\Suppliers;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BlogsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Stores';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = ['label' => $productsDetail->product_name, 'url' => ['view', 'id' => $productsDetail->product_id]];
?>
<section class="content-header">
    <h1><?php echo Html::encode($this->title) . ' : ' . $productsDetail->product_name; ?></h1>
    <?php echo Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
</section>
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                </div><!-- /.box-header -->
                <div class="box-body">

                    <?php
                    if (!empty($productsSuppliers)) {

                        foreach ($productsSuppliers as $suppiler) {

                            $storeData = Suppliers::find()->where(['id' => $suppiler->store_id])->one();
                            ?>

                            <div class="panel-body">
                                <div class="detail-row">
                                    <div class="row colume5">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 dealer">
                                            <div class="colume-outer">
                                                <div class="colume-inner">
                                                    <?php
                                                    //$path = str_replace('admin','frontend/web/',Yii::$app->getUrlManager()->getBaseUrl());
                                                    $path = '/';
                                                    ?>
                                                    <?php echo Html::img($path . $storeData->image, ['class' => 'img-responsive  img-thumbnail small-thumbnail', 'alt' => $storeData->name]); ?>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <div class="colume-outer">

                                                <div class="colume-inner">
                                                    <ul class="buyconditioin">
                                                        <li>
                                                            <i class="fa  fa-check-circle"></i><?php echo $suppiler->cod; ?>
                                                        </li>
                                                        <li>
                                                            <i class="fa  fa-check-circle"></i><?php echo $suppiler->delivery; ?>
                                                        </li>
                                                        <li>
                                                            <i class="fa  fa-check-circle"></i><?php echo $suppiler->emi; ?>
                                                        </li>
                                                        <li>
                                                            <i class="fa  fa-check-circle"></i><?php echo trim($suppiler->return_policy); ?>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <div class="colume-outer">
                                                <div class="colume-inner">
                                                    <!--ul class="buyconditioin">
                                                        <li><strong>Promo Code </strong> <br>  UPGRADE4G Applied to get 10% cashback*  </li>
                                                    </ul-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <div class="colume-outer">
                                                <div class="colume-inner">
                                                    <h4><?php if (!empty($suppiler->price)) {
                                                            echo 'Rs. ' . $suppiler->price;
                                                        } ?> <!--span> Free Shiping</span--> </h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <div>

                                                <a class="btn btn-block btn-blue btn-store"
                                                   href="<?php echo $suppiler->url; ?>"> GO TO STORE </a>
                                            </div>
                                            <div>
                                                <a href="<?php echo \yii\helpers\Url::to(["/products/delete-suppilers", "id" => $suppiler->id]) ?>"
                                                   class="btn btn-block btn-blue btn-store"
                                                   onclick="return confirm('Are you sure?')">UnBind Supplier</a>
                                            </div>
                                            <div>
                                                <a href="<?php echo \yii\helpers\Url::to(["/products/edit-suppiler", "id" => $suppiler->id]) ?>"
                                                   class="btn btn-block btn-blue btn-store">Edit Supplier</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php }
                    } ?>


                </div>
            </div>
        </div>

    </div>
</section>
