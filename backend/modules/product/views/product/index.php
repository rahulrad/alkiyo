<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Brands;
use common\models\Products;
use common\models\Categories;
use common\models\Suppliers;
use common\models\ProductsSuppliers;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'NS Products';

$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>
<section class="content"  >
    
		<?php 
			$category_id = '';
			$brand_id = '';
			$product_id = '';
			$created = '';
			$store_id = 1;
			$rating = '';
			$discount = '';
			$auto_product_name = '';
			$auto_brand_name = '';
			if(isset($_GET['ProductsSearch'])){
				$category_id = $_GET['ProductsSearch']['categories_category_id'];
				$brand_id = !empty($_GET['auto_brand_name']) ? $_GET['ProductsSearch']['brands_brand_id'] : '';
				
				$product_id = !empty($_GET['auto_product_name']) ? $_GET['ProductsSearch']['product_id'] : '';
				
			}
	?>
	
	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
					<?php echo  Html::a('Export', ['export','cat_id'=>$category_id], ['class' => 'btn btn-success','style' => 'float:right']) ?>
					<?php echo  Html::a('Import', ['importdata','cat_id'=>$category_id], ['class' => 'btn btn-success','style' => 'float:right']) ?>
				  <p><?php echo  Html::a('Create Products', ['create'], ['class' => 'btn btn-success','style' => 'float:right']) ?></p>		
                </div>
				<?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
	

	<div class="searchblock">

	 <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12">
<?php

		
		$model->categories_category_id = $category_id;
		// Normal select with ActiveForm & model
		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryLists();
		echo $form->field($model, 'categories_category_id')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			//'options' => ['placeholder' => 'Select Category','id' => 'category_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
			
		
	?>
	</div>


	 <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12">
<div class="form-group" style="margin-top:24px;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-block searchSubmitButton']) ?>
    </div></div>
	</div>
	<?php ActiveForm::end() ?>
    <div class="clearfix"></div>
	
	
				
				
				<!-- /.box-header -->
                <div class="box-body">
				<?php // echo Html::beginForm(['products/change_multiproduct_is_approved'],'post');?>
<?php // echo Html::submitButton('Approved Products', ['class' => 'btn btn-primary pull-right',]);?>
     <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			 

           // 'product_id',
          	   [
				'attribute'=>'categories_category_id',
				'value'=>'categoriesCategory.category_name',
			   ],
			   [
				'attribute'=>'brands_brand_id',
				'value'=>'brandsBrand.brand_name',
			   ],
           // 'product_name',
			 
			 
			 [
			 	'attribute'=>'product_name',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($data) {
						$product_name = substr($data->product_name, 0, 50);
						return Html::tag('div', $product_name, ['title'=>$data->product_name,'style'=>'cursor:pointer;']);
                 },
             ],
			

			'lowest_price',
			//'model_number',
			 'discount',
			 'rating',
			 
			//'unique_id',
           // 'product_description:ntext',
		   //	'date_update',
			//'product_status',
            // 'created',
			
			[
				'attribute'=>'created',
			    'value'=> function($data){
					$date = '';
					if(!empty($data->created)){ $date = date('Y-m-d', strtotime($data->created));	}
					return $date;
				}
		     ],
			
			/**[
			 	'label'=>'Status',
			 	'format' => 'raw',
				'attribute'=>'active',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     if($dataProvider->active == 'active'){
						return Html::a('<i class="fa fa-check" aria-hidden="true" title="Product Status"></i>', Yii::$app->homeUrl.'products/change_status?type=inactive&product_id=' . $dataProvider->product_id);
					 }else{
						 return Html::a('<i class="fa fa-close" aria-hidden="true" title="Product Status"></i>', Yii::$app->homeUrl.'products/change_status?type=active&product_id=' . $dataProvider->product_id); 
					 }
                 },
             ], **/
		/**	 [
			 	'label'=>'Apporved',
				'attribute'=>'is_approved',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     if($dataProvider->is_approved == 1){
						return Html::a('<i class="fa fa-check" aria-hidden="true" title="Product Apporved"></i>', Yii::$app->homeUrl.'products/change_is_approved?type=0&product_id=' . $dataProvider->product_id);
					 }else{
						 return Html::a('<i class="fa fa-close" aria-hidden="true" title="Product Apporved"></i>', Yii::$app->homeUrl.'products/change_is_approved?type=1&product_id=' . $dataProvider->product_id); 
					 }
                 },
             ],**/
			
			 
			 
			[
			 	'label'=>'Stores',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
					$products_suppilers = ProductsSuppliers::find()->select(['id'])->where(['product_id'=>$dataProvider->product_id])->all();
					$products_suppilers = count($products_suppilers);
                     return Html::a($products_suppilers.'</i>',  Yii::$app->homeUrl.'products/view_suppilers?product_id='. $dataProvider->product_id, ['target'=>'_blank']);
                 },
             ],
			/**	[
			 	//'label'=>'unlink',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     if($dataProvider->store_id != 0){
					 return Html::a('<i class="fa fa-unlink" aria-hidden="true" title="Unlink Product to Stores"></i>', Yii::$app->homeUrl.'products/unlink_product?product_id=' . $dataProvider->product_id);
					 }
                 },
             ],**/
			 [
			 	//'label'=>'Reviews',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     return Html::a('<i class="fa fa-comments" title="Reviews"></i>',  Yii::$app->homeUrl.'product-reviews/index?product_id='. $dataProvider->product_id, ['target'=>'_blank']);
                 },
             ],
			 [
			 	//'label'=>'sort up',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     return Html::a('Features', Yii::$app->homeUrl.'products/product_features?product_id=' . $dataProvider->product_id, ['target'=>'_blank']);
                 },
             ],
			 [
			 	//'label'=>'sort down',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     return Html::a('<i class="fa fa-eye" aria-hidden="true" title="View Store"></i>', $dataProvider->url, ['target'=>'_blank']);
                 },
             ],
            [
			'class' => 'yii\grid\ActionColumn',
			'template' => '{update}&nbsp; |&nbsp;  {delete}',
			],
			/**[
			'class' => 'yii\grid\CheckboxColumn',
				'checkboxOptions' => function ($dataProvider) {
					if($dataProvider->is_approved == 1){
							return ['checked' => 'checked','value'=>$dataProvider->product_id];
					}else{
						return ['value'=>$dataProvider->product_id];
					}
					
				}
			] **/
        ],
    ]); ?>
	</div>
	</div></div></div>

</section>
