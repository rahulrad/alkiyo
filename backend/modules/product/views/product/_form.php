<?php
use backend\models\ProductsTag;
use backend\models\Tags;
use common\models\Brands;
use common\models\Categories;
use common\models\ProductImage;
use common\models\Suppliers;
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	
<?php echo $form->field($model, 'upcoming')->checkBox(['label' => 'Is Upcoming', 'uncheck' => null, 'checked' => 'checked']);  ?>
<?php echo  $form->field($model, 'date_launch')->textInput(); ?>	
    <?php echo $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?>
	
	<?php echo  $form->field($model, 'slug')->textInput(); ?>
	
	<?php //echo $form->field($model, 'categories_category_id')->dropDownList(ArrayHelper::map(Categories::find()->all(),'category_id','category_name'),	['prompt' => '-Select Category-'] ); ?>
	
	<?php
		// Normal select with ActiveForm & model
		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryLists();
		echo $form->field($model, 'categories_category_id')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);	
		
	?>

    <?php //echo $form->field($model, 'brands_brand_id')->dropDownList(ArrayHelper::map(Brands::find()->all(),'brand_id','brand_name'),	['prompt' => '-Select Brand-'] ); ?>
	<?php
		// Normal select with ActiveForm & model
		echo $form->field($model, 'brands_brand_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Brands::find()->where(['status'=>'active'])->andWhere(['!=', 'is_delete' ,1])->all(),'brand_id','brand_name'),
			'language' => 'de',
			'options' => ['placeholder' => 'Select Brand'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	
	<?php
		echo $form->field($model, 'store_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Suppliers::find()->andWhere(['!=', 'is_delete' ,1])->all(),'id','name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Store'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>

	
	<?php echo  $form->field($model, 'model_number')->textInput(); ?>
	<?php echo  $form->field($model, 'cod')->textInput(); ?>
	<?php echo  $form->field($model, 'emi')->textInput(); ?>
	<?php echo  $form->field($model, 'colors')->textInput(); ?>
	<?php echo  $form->field($model, 'sizes')->textInput(); ?>
	<?php echo  $form->field($model, 'offers')->textInput(); ?>
	<?php echo  $form->field($model, 'return_policy')->textInput(); ?>
	<?php echo  $form->field($model, 'delivery')->textInput(); ?>
	<?php echo  $form->field($model, 'discount')->textInput(); ?>
	<?php echo  $form->field($model, 'rating')->textInput(); ?>
	<?php echo  $form->field($model, 'lowest_price')->textInput(); ?>
	
	<?php 
	
		$selected_product_tags = ProductsTag::find()->select(['tag_id'])->where(['product_id' => $model->product_id])->all();
		
		$selected_tags = array();
		if(!empty($selected_product_tags)){
			foreach($selected_product_tags as $product_tag){
				$selected_tags[$product_tag->tag_id] = $product_tag->tag_id;
			}
		}
	 
	?>
	
   
	<?php
	// Normal select with ActiveForm & model
		$model->product_tag = $selected_tags;
		echo $form->field($model, 'product_tag')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Tags::find()->where(['status'=>'active','type'=>'products'])->all(),'id','tag'),
			'language' => 'eg',
			//'value'=> $selected_tags,
			'options' => ['multiple' => true, 'placeholder' => 'Select Tags',array('$data->active'=>'selected')],
			'pluginOptions' => [
				'tags' => true,
				'allowClear' => true
			],
		]);
		 
		   
	?>

	<?php //echo  $form->field($model, 'main_image')->fileInput(['accept' => 'image/*']); ?>
	<?php
    $categoryDetail = Categories::findOne(['category_id'=>  $model->categories_category_id]);
    $brandDetail = Brands::findOne(['brand_id'=>  $model->brands_brand_id]);
    echo $form->field($model, 'main_image')->widget(\kartik\file\FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
        ],
        'pluginOptions' => [
            'initialPreview' => [
                $model->getPreviewImage($categoryDetail, $brandDetail)
            ],
            'initialPreviewAsData' => true,
            'initialCaption' => $model->product_name,
            'initialPreviewConfig' => [
                [
                    'caption' => $model->product_name,
                    'key' => $model->product_id
                ]
            ],
            'overwriteInitial' => false,
            'maxFileSize' => 4000,
            'deleteUrl' => \yii\helpers\Url::to(['/products/delete-product-image']),
            'deleteExtraData' => array(
                'category_folder_slug' => !is_null($categoryDetail) ? $categoryDetail->folder_slug : null,
                'brand_folder_slug' => !is_null($brandDetail) ? $brandDetail->folder_slug : null,
            )
        ]
    ]); ?>

	<?php if ($model->image): ?>
	
       <div class="form-group">
		    <?php 
				//$path = str_replace('admin','frontend/web/',Yii::$app->getUrlManager()->getBaseUrl());
				/**$path = 'https://s3.ap-south-1.amazonaws.com/nayashoppy/';
				$categoryDetail = Categories::findOne(['category_id'=>  $model->categories_category_id]);
				$brandDetail = Brands::findOne(['brand_id'=>  $model->brands_brand_id]); **/
			?>
			<?php //echo Html::img($path.'uploads/products/'.$categoryDetail->folder_slug.'/'.$brandDetail->folder_slug.'/300x400/'.$model->image,['class'=>'img-responsive  img-thumbnail small-thumbnail','alt'=>'User Image']); ?>
           <?php //echo Html::img($model->image_path,['class'=>'img-responsive  img-thumbnail small-thumbnail','alt'=>'User Image']); ?>
           
        </div> 
		
    <?php endif; ?>
	
	<?php //echo  $form->field($model, 'other_images[]')->fileInput(['multiple' => true,'accept' => 'image/*']); ?>
    <?php

    $productImagesPrevies = array();
    $productImagesPreviewConfig = array();
    $product_images = ProductImage::find()->where(['id_product' => $model->product_id])->all();
    foreach ($product_images as $product_image) {
        $productImagesPrevies[] = $product_image->getPreviewImage($categoryDetail, $brandDetail);
        $productImagesPreviewConfig[] = array(
            'caption' => $model->product_name,
            'key' => $product_image->id
        );
    }

    echo $form->field($model, 'other_images[]')->widget(\kartik\file\FileInput::classname(), [
        'options'=>[
            'accept' => 'image/*',
            'multiple'=>true
        ],
        'pluginOptions' => [
            'initialPreview' => $productImagesPrevies,
            'initialPreviewAsData' => true,
            'initialCaption' => $model->product_name . " Other Images",
            'initialPreviewConfig' => $productImagesPreviewConfig,
            'overwriteInitial' => false,
            'maxFileSize' => 4000,
            'deleteUrl' => \yii\helpers\Url::to(['/products/delete-product-other-image']),
            'deleteExtraData' => array(
                'category_folder_slug' => !is_null($categoryDetail) ? $categoryDetail->folder_slug : null,
                'brand_folder_slug' => !is_null($brandDetail) ? $brandDetail->folder_slug : null,
            )
        ]
    ]);
    ?>
	<div class="form-group">
	<?php 
		//$product_images = ProductImage::find()->where(['id_product' => $model->product_id])->all();
	
			 if (!empty($product_images)): 
				//$categoryDetail = Categories::findOne(['category_id'=>  $model->categories_category_id]);
				//$brandDetail = Brands::findOne(['brand_id'=>  $model->brands_brand_id]);
				foreach($product_images as $product_image):
					//echo $product_image->image; die;
	?>
	<?php //echo Html::img($path.'uploads/products/'.$categoryDetail->folder_slug.'/'.$brandDetail->folder_slug.'/300x400/'.$product_image->image,['class'=>'img-responsive img-thumbnail small-thumbnail','alt'=>'Product Image']); ?>
	<?php //echo Html::img($product_image->image_path,['class'=>'img-responsive img-thumbnail small-thumbnail','alt'=>'Product Image']); ?>

     <?php endforeach; endif; ?>
	</div>
    <?php //echo $form->field($model, 'product_description')->textarea(['rows' => 6]) ?>
	<?php echo $form->field($model, 'product_description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced',
	 'clientOptions' => [
            'filebrowserUploadUrl' => \yii\helpers\Url::to(['/products/upload-file']),
        ]
    ]) ?>
	
	<?php echo $form->field($model, 'care_instruction')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced',
	 'clientOptions' => [
            'filebrowserUploadUrl' => \yii\helpers\Url::to(['/products/upload-file']),
        ]
    ]) ?>
	
	 <?php echo  $form->field($model, 'meta_title')->textarea(['rows' => 1]) ?>
	 
	 <?php echo  $form->field($model, 'meta_keyword')->textarea(['rows' => 2]) ?>
	 
	 <?php echo  $form->field($model, 'meta_desc')->textarea(['rows' => 2]) ?>
	 
	 

    <?php echo $form->field($model, 'active')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>
	
<?php echo $form->field($model, 'is_approved')->checkBox(['label' => 'Approved', 'uncheck' => null, 'checked' => 'checked']);  ?>
   <?php echo $form->field($model, 'show_home_page')->checkBox(['label' => 'show home page', 'uncheck' => null, 'checked' => 'checked']);  ?>
   <?php echo $form->field($model, 'image_scrapped')->checkBox();  ?>
    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
