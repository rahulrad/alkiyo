<?php
use yii\helpers\Html;


$output = "";
if (!empty($similer_products)) {
    $noOfProducts = count($similer_products);
    $s_no = 1;

    foreach ($similer_products as $product) {
        $productSuppliers = array();
        foreach ($product->suppliersProducts as $supplier) {
            $productSuppliers[] = $supplier->store_name . '-' . $supplier->price;
        }
        if ($postData['Products']['showUnMapped']) {
            if (count($productSuppliers) <= 1) {
                $supplier = $product->supplier;
                $productCategory = $product->categoriesCategory;
                $output .= '<p class="OurProductLists our_products">' . $s_no . '. ';
                $output .= '<input type="radio" name=Products[product_id] value="' . $product->product_id . '">';
                $output .= '<span class="ourProductName">' . Html::encode($product->product_name) . "<code>(" . implode(",", $productSuppliers) ." url : ".$product->url. ")</code>" . '</span>';
                $output .= ' <a href="' . \yii\helpers\Url::to(["/products/view_suppilers", 'product_id' => $product->product_id]) . '" target="_blank">Suppliers mappings</a><br>';
                if (!is_null($productCategory)) {
                    $output .= ' <a href="' . \Yii::$app->getModule("articles")->frontPageUrl . $productCategory->slug . '/' . $product->slug . '" target="_blank">View Product</a><br>';
                }
                $output .= '</p>';
                $s_no++;
            } else {
                $noOfProducts--;
            }
        } else {
            $supplier = $product->supplier;
            $productCategory = $product->categoriesCategory;
            $output .= '<p class="OurProductLists our_products">' . $s_no . '. ';
            $output .= '<input type="radio" name=Products[product_id] value="' . $product->product_id . '">';
            $output .= '<span class="ourProductName">' . Html::encode($product->product_name) . "<code>(" . implode(",", $productSuppliers) . ")</code>" . '</span>';
            $output .= ' <a href="' . \yii\helpers\Url::to(["/products/view_suppilers", 'product_id' => $product->product_id]) . '" target="_blank">Suppliers mappings</a><br>';
            if (!is_null($productCategory)) {
                $output .= ' <a href="' . \Yii::$app->getModule("articles")->frontPageUrl . $productCategory->slug . '/' . $product->slug . '" target="_blank">View Product</a><br>';
            }
            $output .= '</p>';
            $s_no++;
        }
    }

    $output = '<span>' . $noOfProducts . ' products.</span>' . $output;
} else {
    $output .= '<h4>No Any Similer Product</h4>';
}

echo $output;
?>

<?php /**if(!empty($similer_products)){ ?>
 * <div class="form-group">
 * <?php echo Html::submitButton($model->isNewRecord ? 'Mapp' : 'Mapp', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
 * </div>
 * <?php } **/ ?>
