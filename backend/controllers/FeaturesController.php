<?php

namespace backend\controllers;

use Yii;
use common\models\Features;
use backend\models\FilterGroups;
use backend\models\FeaturesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Logs;
use common\models\Categories;
use common\models\FeatureGroups;
use yii\web\UploadedFile;

/**
 * FeaturesController implements the CRUD actions for Features model.
 */
class FeaturesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete','change_is_filter_status','importdata','export','get_features','change_keyfeature_status'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Features models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FeaturesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$model = new FeaturesSearch();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model'=>$model,
        ]);
    }

    /**
     * Displays a single Features model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Features model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Features();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
		
			
			if(!empty($post_data['Features']['slug'])){
				
				$model->slug = $post_data['Features']['slug'];
			
			}
			$model->save();
		
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Features','action'=>'Create','activity' => $model->name.' is added','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
			
                        Yii::$app->session->setFlash('success', 'Features is successfully saved');
                        
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Features model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
		
			
			if(!empty($post_data['Features']['slug'])){
				
				$model->slug = $post_data['Features']['slug'];
			
			}
			$model->save();
		
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Features','action'=>'Update','activity' => $model->name.' is updated','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
                        
                        Yii::$app->session->setFlash('success', 'Features is successfully update');
			
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Features model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		
		die('not access');
		$model = $this->findModel($id);
        $this->findModel($id)->delete();
		
		// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Features','action'=>'Delete','activity' => $model->name.' is deleted','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
                           
                        Yii::$app->session->setFlash('success', 'Features is successfully delete');
                        
        return $this->redirect(['index']);
    }

    /**
     * Finds the Features model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Features the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Features::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
	
/*	
	public function actionChange_is_filter_status(){
		$model = new Features();
		 $getData = Yii::$app->request->get();
		 	if(!empty($getData) && isset($getData['feature_id'])){
				
				$FeaturesModel = Features::find()->where(['id' => $getData['feature_id']])->one();
				
				if($getData['type'] == 1){
					$is_filter = 1;
				}else{
					$is_filter = 0;
				}
				
				$FeaturesModel->is_filter = $is_filter;
				$FeaturesModel->save(false);
				
				$logs_model = new Logs();
				
				$user_logs = array('model'=>'Features','action'=>'Change Feature Is Filter Status','activity' => $FeaturesModel->name.' is change status','action_id'=>$FeaturesModel->id);
				$logs_model->saveUserLogs($user_logs);
				
				Yii::$app->session->setFlash('success', 'Feature Status changed successfully saved');
			
				return $this->redirect(['index']);
				
			}
	}
	
	
*/



public function actionChange_is_filter_status(){
		$model = new Features();
		 $getData = Yii::$app->request->get();
		 	if(!empty($getData) && isset($getData['feature_id'])){
				$FeaturesModel = Features::find()->where(['id' => $getData['feature_id']])->one();
              $filterGroup= FilterGroups::find()->where(['category_id' =>$FeaturesModel->category_id,'title'=>$FeaturesModel->name,'is_delete'=>0])->one();
                if(!isset($filterGroup) || empty($filterGroup)){

		
                $filterGroupCount= FilterGroups::find()->where(['category_id' =>$FeaturesModel->category_id,'is_delete'=>0])->all();

                     $filterGroup=        new FilterGroups();
                     $filterGroup->title=$FeaturesModel->name;
                      $filterGroup->category_id=$FeaturesModel->category_id;
                      $filterGroup->status=1;
   $filterGroup->sort_order=count($filterGroupCount)+1;
$filterGroup->created=date('Y-m-d  H:i:s');
                       $filterGroup->save();

 if($getData['type'] == 1){
                                        $is_filter = 1;
                                }else{
                                        $is_filter = 0;
                                }

                                $FeaturesModel->is_filter = $is_filter;
                                $FeaturesModel->save(false);
                }
               Yii::$app->session->setFlash('success', 'Filter group creted successfully ');
		

return $this->redirect(Yii::$app->request->referrer);	
//				return $this->redirect(['index']);
				
			}
	}


 public function actionChange_keyfeature_status(){
                $model = new Features();
                 $getData = Yii::$app->request->get();
                        if(!empty($getData) && isset($getData['feature_id'])){

                                $FeaturesModel = Features::find()->where(['id' => $getData['feature_id']])->one();

                                if($getData['type'] == 1){
                                        $is_filter = 1;
                                }else{
                                        $is_filter = 0;
                                }

                                $FeaturesModel->show_feature = $is_filter;
                                $FeaturesModel->save(false);

                                $logs_model = new Logs();

                                $user_logs = array('model'=>'Features','action'=>'Change show feautre statue','activity' => $FeaturesModel->name.' is change status','action_id'=>$FeaturesModel->id);
                                $logs_model->saveUserLogs($user_logs);

                                Yii::$app->session->setFlash('success', 'Show Feature status changed successfully saved');

                                return $this->redirect(['index']);

                        }
        }










	
	
    /**
    * Import Category Data
    */



    public function actionImportdata()
    {
        $model = new Features();

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'features_csv_file');
            $filename = 'Data-' . Date('YmdGis') . '.' . $file->extension;
            $upload = $file->saveAs('uploads/features_import/' . $filename);
            define('CSV_PATH', 'uploads/features_import/');
            $csv_file = CSV_PATH . $filename;
            $row = 1;

            if (($handle = fopen($csv_file, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                    if ($row != 1) {
                        if ((isset($data[0]) && !empty($data[0])) && (isset($data[2]) && !empty($data[2])) && (isset($data[3]) && !empty($data[3]))) {

                            $categoryModel = Categories::find()->where([
                                'category_name' => trim(strtolower($data[2]))
                            ])->one();
                            if (!is_null($categoryModel)) {
                                $groupModel = FeatureGroups::find()->where([
                                    'name' => trim(strtolower($data[3])),
                                    'category_id' => $categoryModel->category_id
                                ])->one();
                                if (!is_null($groupModel)) {
                                    $featureDetail = Features::find()->where([
                                        'name' => trim($data[0]),
                                        'category_id' => $categoryModel->category_id,
                                        'feature_group_id' => $groupModel->id,

                                    ])->andWhere('id > :id', [
                                        ':id' => Features::FEATURE_LAST_ID
                                    ])->one();
                                    if (is_null($featureDetail)) {
                                        $featureDetail = new Features();
                                        $featureDetail->name = trim($data[0]);
                                        $featureDetail->display_name = !empty($data[1]) ? trim($data[1]) : trim($data[0]);
                                        $featureDetail->category_id = $categoryModel->category_id;
                                        $featureDetail->feature_group_id = $groupModel->id;
                                        $featureDetail->is_filter = !empty($data[4]) ? $data[4] : $featureDetail->is_filter;
                                        $featureDetail->created = date('Y-m-d h:i:s');
                                        $featureDetail->modified = date('Y-m-d h:i:s');
                                    } else {
//                                        $featureDetail->display_name = !empty($data[1]) ? trim($data[1]) : trim($data[0]);
//                                        $featureDetail->is_filter = !empty($data[4]) ? $data[4] : $featureDetail->is_filter;
                                        $featureDetail->modified = date('Y-m-d h:i:s');
                                        $featureDetail->feature_group_id = $groupModel->id;
                                    }
                                    $featureDetail->new = 1;
                                    $featureDetail->save();
                                }
                            }
                        }
                    }
                    $row++;

                }
                fclose($handle);
            }
            //  unlink('uploads/category_import/'.$filename);
            Yii::$app->session->setFlash('success', 'Import Success');
            return $this->redirect(['index']);
        } else {
            return $this->render('importdata', ['model' => $model]);
        }

    }


    /**
    * Export Category Data
    */

    public function actionExport()
    {

        $FeaturesResults = $this->get_features($_SERVER['HTTP_REFERER']);


        $filename = 'Data-' . Date('YmdGis') . '-Features.csv';
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=" . $filename);
        header("Pragma: no-cache");
        header("Expires: 0");


        $file = fopen('php://output', 'w');
        fputcsv($file, array('name', 'display_name', 'category_id', 'feature_group_id', 'is_filter'));

        $featuresModel = new Features();
        foreach ($FeaturesResults as $features) {
if($features['id']<Features::FEATURE_LAST_ID){
continue;
}
            $categoryModel = new Categories();
            $category_name = $categoryModel::find()->select(['category_name'])->where(['category_id' => $features->category_id])->one();
            $category = !empty($category_name) ? $category_name->category_name : '';


            $FeatureGroupsModel = new FeatureGroups();
            $feature_group = $FeatureGroupsModel::find()->select(['name'])->where(['id' => $features->feature_group_id])->one();
            $feature_group_name = !empty($feature_group) ? $feature_group->name : '';


            $record = array($features->name, $features->display_name, $category, $feature_group_name, $features->is_filter);


            fputcsv($file, $record);

        }

    }


    public function get_features($getData)
    {
        $FeaturesResults = '';

        $dataSearch = array();
        if (!empty($getData)) {

            $url = parse_url($getData);

            if (isset($url['query'])) {

                parse_str($url['query'], $getQueryString);
                if (!empty($getQueryString) && isset($getQueryString['FeaturesSearch'])) {
                    $category_id = $getQueryString['FeaturesSearch']['category_id'];
                    $feature_group_id = $getQueryString['FeaturesSearch']['feature_group_id'];
                    $featureId = $getQueryString['FeaturesSearch']['id'];

                    if (!empty($category_id)) {
                        $dataSearch['category_id'] = $category_id;
                    }
                    if (!empty($feature_group_id)) {
                        $dataSearch['feature_group_id'] = $feature_group_id;
                    }
                    if (!empty($featureId)) {
                        $dataSearch['id'] = $featureId;
                    }
			 $dataSearch['id'] > 11938;

                }

            }


        }
        $FeaturesResults = Features::find()->where($dataSearch)->All();

        return $FeaturesResults;
    }


		
	
}
