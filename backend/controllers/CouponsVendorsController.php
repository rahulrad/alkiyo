<?php

namespace backend\controllers;

use Yii;
use backend\models\CouponsVendors;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Logs;
use yii\web\UploadedFile;

/**
 * CouponsVendorsController implements the CRUD actions for CouponsVendors model.
 */
class CouponsVendorsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CouponsVendors models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new CouponsVendors();
        $dataProvider = $model->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single CouponsVendors model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CouponsVendors model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CouponsVendors();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
             $post_data = Yii::$app->request->post();
			
// save category image
			$model->file = UploadedFile::getInstance($model,'file');
			
			if(!empty($model->file)){
			//	$imageName = $model->file->baseName;
                $path = Yii::getAlias('@frontend') .'/web/';
                $imageName = $model->slug;
				
				$model->file->saveAs($path.'uploads/couponsvendors/'.$imageName.'.'.$model->file->extension);
				
				$model->image = 'uploads/couponsvendors/'.$imageName.'.'.$model->file->extension;
			}
			
            if(!isset($post_data['CouponsVendors']['show_home'])){
                $model->show_home = 0;
            }

            $model->created = date('Y-m-d h:i:s');
            $model->save();

            // save user logs data in user_logs table
            $logs_model = new Logs();
            
            $user_logs = array('model'=>'CouponsVendor','action'=>'Create','activity' => $model->name.' is added','action_id'=>$model->id);
            $logs_model->saveUserLogs($user_logs);
            Yii::$app->session->setFlash('success', 'Coupons vendor is successfully saved');
            
            return $this->redirect(['index']);

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CouponsVendors model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           
            $post_data = Yii::$app->request->post();
			
// save category image
			$model->file = UploadedFile::getInstance($model,'file');
			
			if(!empty($model->file)){
			//	$imageName = $model->file->baseName;
                $path = Yii::getAlias('@frontend') .'/web/';
                $imageName = $model->slug;
				
				$model->file->saveAs($path.'uploads/couponsvendors/'.$imageName.'.'.$model->file->extension);
				
				$model->image = 'uploads/couponsvendors/'.$imageName.'.'.$model->file->extension;
			}
			
            if(!isset($post_data['CouponsVendors']['show_home'])){
                $model->show_home = 0;
            }

            $model->save();
            
            // save user logs data in user_logs table
            $logs_model = new Logs();
            
            $user_logs = array('model'=>'CouponsVendors','action'=>'Update','activity' => $model->name.' is updated','action_id'=>$model->id);
            $logs_model->saveUserLogs($user_logs);
                        
            Yii::$app->session->setFlash('success', 'Coupons Vendor is successfully updated');
            
            return $this->redirect(['index']);

        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CouponsVendors model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CouponsVendors model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CouponsVendors the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CouponsVendors::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
