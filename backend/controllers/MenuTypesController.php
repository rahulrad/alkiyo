<?php

namespace backend\controllers;

use Yii;
use backend\models\MenuTypes;
use backend\models\MenuTypesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use backend\models\Logs;
use common\models\Menus;

/**
 * MenuTypesController implements the CRUD actions for MenuTypes model.
 */
class MenuTypesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MenuTypes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MenuTypesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MenuTypes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MenuTypes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MenuTypes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
             
			 // save user logs data in user_logs table
			$logs_model = new Logs();
			$user_logs = array('model'=>'MenuTypes','action'=>'Create','activity' => $model->title.' is added','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);	
			
            Yii::$app->session->setFlash('success', 'Menu Type is successfully saved');
            
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MenuTypes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
			// save user logs data in user_logs table
			$logs_model = new Logs();
			$user_logs = array('model'=>'MenuTypes','action'=>'Updated','activity' => $model->title.' is updated','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);	
			
            Yii::$app->session->setFlash('success', 'Menu Type is successfully update');
            
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MenuTypes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        
		
			$model = $this->findModel($id);
			
			$modelDelete = $model;
			$modelDelete->is_delete = 1;
			$modelDelete->save(false);
			
			 $this->deleteAllMenusFromMenuType($id);
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			$userdetail = $logs_model->getUserDetailForEnterLog();
			$user_logs = array('model'=>'MenuTypes','action'=>'Delete','activity' => $model->title.' is deleted','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);	
                        
            Yii::$app->session->setFlash('success', 'Blogs is successfully delete');

        return $this->redirect(['index']);
    }
	
	
	public function deleteAllMenusFromMenuType($type){
			$menus = Menus::find()->select(['id'])->where(['type' => 2])->andWhere(['!=', 'is_delete', 1])->all();
			
			$menu_ids= array();
			
			if(!empty($menus)){
				
				foreach($menus as $menu){
					$model = Menus::findOne($menu->id);
					$modelDelete = $model;
					$modelDelete->is_delete = 1;
					$modelDelete->save(false);
					
					$logs_model = new Logs();
					$user_logs = array('model'=>'Menus','action'=>'Delete','activity' => $model->title.' is deleted','action_id'=>$model->id);
					$logs_model->saveUserLogs($user_logs);	
					
				}
				
			} 
			
			
	}

    /**
     * Finds the MenuTypes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MenuTypes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MenuTypes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
