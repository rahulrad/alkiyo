<?php

namespace backend\controllers;

use Yii;
use common\models\Sliders;
use backend\models\SlidersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use backend\models\Logs;
use common\models\SliderImages;

/**
 * SlidersController implements the CRUD actions for Sliders model.
 */
class SlidersController extends Controller
{
	
	
	public function beforeAction($action) {
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

	
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete','ajax_slider_validation'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sliders models.
     * @return mixed
     */
    public function actionIndex()
    {
		
        $searchModel = new SlidersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sliders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sliders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sliders();

        if ($model->load(Yii::$app->request->post())   && $model->validate()) {
			$postData = Yii::$app->request->post();
			$checkData = array();
			$checkData['start_date'] = $postData['Sliders']['start_date'];
			$checkData['end_date'] = $postData['Sliders']['end_date'];
			$checkData['slider_type'] = $postData['Sliders']['slider_type'];
			$checkData['category_id'] = $postData['Sliders']['category_id'];
			$checkData['type'] = 'create';
			
			$sliderModel = new Sliders();
			$exitSlider = $sliderModel->getExitSliderAcordingDateRange($checkData);
			
			if(!empty($exitSlider)){
				 Yii::$app->session->setFlash('error', 'Slider is already exit for same date');
				  return $this->render('create', [
                'model' => $model,
            ]);
			}else{
				
				
				// save category image
				/** $model->file = UploadedFile::getInstance($model,'file');
				if(!empty($model->file)){
					//$imageName = $model->file->baseName;
					$path = Yii::getAlias('@frontend') .'/web/';
					$imageName = time().uniqid();
					
					//$model->file->saveAs('uploads/sliders/'.$imageName.'.'.$model->file->extension);
					$model->file->saveAs($path.'uploads/sliders/'.$imageName.'.'.$model->file->extension);
					
					$model->image = 'uploads/sliders/'.$imageName.'.'.$model->file->extension;
					
				} **/
				
				// get all other images
				$model->created = date('Y-m-d h:i:s');
				
				$model->save();
				$sliderImagesData['content'] = $postData['SliderImage'];
				
				$sliderImagesData['images'] = $_FILES;
				
				// check if !empty then it will be enter this code
				if(!empty($sliderImagesData)){
				
					$SliderImagesModel = new SliderImages();
					//$SliderImagesModel::deleteAll(['slider_id' =>$model->id]);
					$SliderImagesModel->saveSliderAddMoreData($sliderImagesData,$model->id);
				} 
							
				
				// save user logs data in user_logs table
				$logs_model = new Logs();
				$user_logs = array('model'=>'Sliders','action'=>'Create','activity' => $model->title.' is added','action_id'=>$model->id);
				$logs_model->saveUserLogs($user_logs);			
				Yii::$app->session->setFlash('success', 'Slider is successfully saved');
				return $this->redirect(['index']);
				
			}
			
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Sliders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		
        if ($model->load(Yii::$app->request->post())   && $model->validate()) {
			
			$postData = Yii::$app->request->post();
			
			$checkData = array();
			$checkData['start_date'] = $postData['Sliders']['start_date'];
			$checkData['end_date'] = $postData['Sliders']['end_date'];
			$checkData['slider_type'] = $postData['Sliders']['slider_type'];
			$checkData['category_id'] = $postData['Sliders']['category_id'];
			$checkData['type'] = 'update';
			$checkData['id'] = $model->id;
			
			$sliderModel = new Sliders();
			$exitSlider = $sliderModel->getExitSliderAcordingDateRange($checkData);
			
			if(!empty($exitSlider)){
				 
				 Yii::$app->session->setFlash('error', 'Slider is already exit for same date');
				 return $this->render('update', [
						'model' => $model,
					]);
			
			}else{
				
			// save category image
			/**$model->file = UploadedFile::getInstance($model,'file');
			
			if(!empty($model->file)){
				//$imageName = $model->file->baseName;
                $imageName = time().uniqid();
				
				$path = Yii::getAlias('@frontend') .'/web/';
				
				$model->file->saveAs($path.'uploads/sliders/'.$imageName.'.'.$model->file->extension);
				
				$model->image = 'uploads/sliders/'.$imageName.'.'.$model->file->extension;
			} **/
			
			
			// get all other images
			
			$sliderImagesData['content'] = $postData['SliderImage'];
			
			$sliderImagesData['images'] = $_FILES;
			
			// check if !empty then it will be enter this code
			if(!empty($sliderImagesData)){
				
				$SliderImagesModel = new SliderImages();
				
				$SliderImagesModel::deleteAll(['slider_id' =>$model->id]);
				$SliderImagesModel->saveSliderAddMoreData($sliderImagesData,$model->id);
			} 
					
			$model->created = date('Y-m-d h:i:s');
			
			$model->save();
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Sliders','action'=>'Update','activity' => $model->title.' is updated','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
                        
                        Yii::$app->session->setFlash('success', 'Slider is successfully update');
			
            return $this->redirect(['index']);
			}
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Sliders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		$model = $this->findModel($id);
			$modelDelete = $model;
			$modelDelete->is_delete = 1;
			$modelDelete->save(false);
		
		// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Sliders','action'=>'Delete','activity' => $model->title.' is deleted','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
                        
                        Yii::$app->session->setFlash('success', 'Slider is successfully delete');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sliders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sliders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sliders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionAjax_slider_validation(){
		return Yii::$app->request->post();
		if (Yii::$app->request->isAjax) {
			
		//parse_str($this->request->data['postform'], $searcharray);
		}
	}
}
