<?php

namespace backend\controllers;

use Yii;
use backend\models\MongoProducts;
use backend\models\MongoProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Logs;

/**
 * MongoProductsController implements the CRUD actions for MongoProducts model.
 */
class MongoProductsController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete','change_status','search_form'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
	
	
	
    /**
     * Lists all MongoProducts models.
     * @return mixed
     */
    public function actionIndex()
    {
		
		
        $searchModel = new MongoProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		//echo '<pre>dataProvider'; print_R($dataProvider); die;
		$model = new MongoProductsSearch();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model' =>$model,
        ]);
    }
	
	
	public function actionSearch_form(){
		  $model = new MongoProductsSearch();
		   return $this->render('_search_form', [
					'model' => $model,
				]);
		 
	}

    /**
     * Displays a single MongoProducts model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MongoProducts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MongoProducts();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MongoProducts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MongoProducts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id)
    {
		
		//die('access dined');
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the MongoProducts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return MongoProducts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MongoProducts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
	public function actionChange_status(){
		$model = new MongoProducts();
		 $getData = Yii::$app->request->get();
		 
			if(!empty($getData) && isset($getData['product_id'])){
				
				$productModel = MongoProducts::find()->where(['product_id' => (int)$getData['product_id']])->one();
				
				if($getData['type'] == 'inactive'){
					$status = 0;
				}else{
					$status = 1;
				}
				
				$productModel->status = $status;
				$productModel->save(false);
				
				$logs_model = new Logs();
				
				$user_logs = array('model'=>'MongoProducts','action'=>'Change Mongo Product Status','activity' => $productModel->product_name.' is change status','action_id'=>$productModel->product_id);
				$logs_model->saveUserLogs($user_logs);
				
				Yii::$app->session->setFlash('success', 'Product Status changed successfully saved');
			
				return $this->redirect(Yii::$app->request->referrer);
				
			}
	}
	
	
}
