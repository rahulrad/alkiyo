<?php

namespace backend\controllers;

use Yii;
use backend\models\Ads;
use backend\models\AdsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use backend\models\Logs;

/**
 * AdsController implements the CRUD actions for Ads model.
 */
class AdsController extends Controller
{
    public function behaviors()
    {
        return [

            'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'actions' => ['index', 'view', 'create', 'update','delete'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ads models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ads model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ads model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		
        $model = new Ads();


            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $post_data = Yii::$app->request->post();
            
            // save Ads image
            $model->file = UploadedFile::getInstance($model,'file');
            
            if(!empty($model->file)){
                //$imageName = $model->file->baseName;
                $path = Yii::getAlias('@frontend') .'/web/';
                $imageName = time().uniqid();
                
                 $model->file->saveAs($path.'uploads/ads/'.$imageName.'.'.$model->file->extension);
                
                $model->image = 'uploads/ads/'.$imageName.'.'.$model->file->extension;
            }
                        
            $model->created = date('Y-m-d h:i:s');
            
            $model->save();
			
			
            
            
            
            // save user logs data in user_logs table
            $logs_model = new Logs();
            
            $user_logs = array('model'=>'Ads','action'=>'Create','activity' => $model->title.' is added','action_id'=>$model->id);
            $logs_model->saveUserLogs($user_logs);
            
            Yii::$app->session->setFlash('success', 'Ad is successfully saved');
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Updates an existing Ads model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())   && $model->validate()) {
            $post_data = Yii::$app->request->post();
            if(!empty($post_data['Ads']['slug'])){
                
                $model->slug = $post_data['Ads']['slug'];
            
            }
            
            // save category image
            $model->file = UploadedFile::getInstance($model,'file');
            
            if(!empty($model->file)){
                //$imageName = $model->file->baseName;
                $imageName = time().uniqid();

                $path = Yii::getAlias('@frontend') .'/web/';
				
                $model->file->saveAs($path.'uploads/ads/'.$imageName.'.'.$model->file->extension);
                
                $model->image = 'uploads/ads/'.$imageName.'.'.$model->file->extension;
            }
            $model->save();
            
            // save user logs data in user_logs table
            $logs_model = new Logs();
            
            //echo $id; die;
            $user_logs = array('model'=>'Ads','action'=>'Update','activity' => $model->title.' is updated','action_id'=>$id);
            $logs_model->saveUserLogs($user_logs);
            Yii::$app->session->setFlash('success', 'Ad is successfully saved');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Ads model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        	$model = $this->findModel($id);
			$modelDelete = $model;
			$modelDelete->is_delete = 1;
			$modelDelete->save(false);
		
		// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Advertistment','action'=>'Delete','activity' => $model->title.' is deleted','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);

         Yii::$app->session->setFlash('success', 'Advertistment is successfully delete');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Ads model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ads the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ads::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
