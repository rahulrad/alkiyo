<?php

namespace backend\controllers;

use Yii;
use backend\models\Logs;
use backend\models\LogsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\FeatureValues;
use common\models\Features;
use common\models\FeatureGroups;
use common\models\Categories;
use common\models\Products;
use common\models\Brands;
use backend\models\MongoProducts;
/**
 * LogsController implements the CRUD actions for Logs model.
 */
class LogsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
          return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete','ajax-auto-complete-search'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Logs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

   

    /**
     * Finds the Logs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Logs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Logs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionScraper_logs(){
		$dir = "/images/";

		// Open a directory, and read its contents
		if (is_dir($dir)){
		  if ($dh = opendir($dir)){
			while (($file = readdir($dh)) !== false){
			  echo "filename:" . $file . "<br>";
			}
			closedir($dh);
		  }
		}
	}
	
	public function actionAjaxAutoCompleteSearch(){
		if (Yii::$app->request->isAjax) {
				
				$searchData = Yii::$app->request->get();
				
				$modelName = $searchData['model_name'];
				$fieldName = $searchData['field_name'];
				$term = $searchData['term'];
				$id_type = $searchData['id_value'];
				
				if($modelName == 'FeatureValues'){
					$model = new FeatureValues();
					$resultData = $model::find()->select(['id','value','feature_id'])->andWhere(['like',$fieldName,$term])->all();
					$result = $this->getFeatureValues($resultData);
					
				}elseif($modelName == 'Features'){
					$resultData = Features::find()->select(['id','name','feature_group_id','category_id'])->andWhere(['like',$fieldName,$term])->all();
					$result = $this->getFeatures($resultData);
				
				}elseif($modelName == 'Products'){
					$is_approved = ($id_type == 'productssearch-product_id') ? 1 : 0 ;
					$resultData = Products::find()->select(['product_id','product_name','is_approved'])->where(['is_approved' => $is_approved,'is_delete' => 0])->andWhere(['like',$fieldName,$term])->all();
					$result = $this->getProducts($resultData);
				
				}elseif($modelName == 'Brands'){
					$brand_key = ($id_type == 'mongoproductssearch-brand_name') ? 'brand_name' : 'brand_id' ;
					$resultData = Brands::find()->select([$brand_key,'brand_name'])->andWhere(['like',$fieldName,$term])->andWhere(['!=', 'is_delete' ,1])->all();
					$result = $this->getBrands($resultData,$brand_key);
				
				}elseif($modelName == 'MongoProducts'){
					$resultData = MongoProducts::find()->select(['product_name','product_name','status'])->where(['status'=>1])->andWhere(['like',$fieldName,$term])->all();
					$result = $this->getMongoProducts($resultData);
				
				}
				
				
				$jsonResult = array();
				 
				  if(!empty($result)){
				  foreach ($result as $key => $val) {
				  
				   $resultArr['key'] = $key;
				   $resultArr['value'] = $val;
				   array_push($jsonResult, $resultArr);
				   //array_push($id, $auth['Author']['id']);
				  }
				  }else{
				   $resultArr['key'] = '';
				   $resultArr['value'] = 'No Record Found';
				   array_push($jsonResult, $resultArr);
				  }
				  
				  
				  echo json_encode($jsonResult);
		}
	}
	
	public function getFeatureValues($FeaturesValueData){
			$Feature_values = array();
				if(!empty($FeaturesValueData)){
					foreach($FeaturesValueData as $feature_val){
						
						$featureDetail = Features::find()->select(['name'])->where(['id' => $feature_val->feature_id])->one();
						
						if(!empty($featureDetail->name)){
							$Feature_values[$feature_val->id] = $feature_val->value.' ('.$featureDetail->name .' )';
						}else{
							$Feature_values[$feature_val->id] = $feature_val->value;
						}
						
					}
				}
			return $Feature_values;
	}
	
	
	public function getFeatures($FeaturesData){
			$Features = array();
			if(!empty($FeaturesData)){
				foreach($FeaturesData as $feature){
					
					$featureGroup = FeatureGroups::find()->select(['name'])->where(['id' => $feature->feature_group_id])->one();
					
					$category = Categories::find()->select(['category_name'])->where(['category_id' => $feature->category_id])->one();
					
					if(!empty($category->category_name)){
						$Features[$feature->id] = $feature->name.' ('.$featureGroup->name.' ~~ '. $category->category_name .' )';
					}else{
						$Features[$feature->id] = $feature->name.' ('.$featureGroup->name.' )';
					}
					
				}
			}
			
			return $Features;
	}
	
	
	public function getProducts($ProductsData){
			$productsArr = array();
				if(!empty($ProductsData)){
					foreach($ProductsData as $product){
						
						$productsArr[$product->product_id] = $product->product_name;
						
					}
				}
			return $productsArr;
	}
	
	public function getMongoProducts($MongoProductsData){
			$productsArr = array();
				if(!empty($MongoProductsData)){
					foreach($MongoProductsData as $product){
						
						$productsArr[$product->product_name] = $product->product_name;
						
					}
				}
			return $productsArr;
	}
	
	public function getBrands($brandsData,$brand_key){
			$brandsArr = array();
				if(!empty($brandsData)){
					foreach($brandsData as $brand){
						$key = ($brand_key == 'brand_name') ? $brand->brand_name : $brand->brand_id;
						$brandsArr[$key] = $brand->brand_name;
						
					}
				}
			return $brandsArr;
	}
	
	
}
