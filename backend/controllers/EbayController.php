<?php

namespace backend\controllers;

use yii\console\Controller;
use yii\helpers\Console;
use common\models\Products;
use backend\models\CategoriesData;//
use backend\models\CategoryUrl;
use backend\models\Mobile;
use backend\models\Logs;
use backend\models\ProductUrls;
use common\models\Categories;
use common\components\HttpService;
use common\components\Aws;

class EbayController extends Controller {

    public function actionIndex() {
		$mobile = new Mobile();
		$mobile->test();
        echo "Hello world.";
    }
    
    public function actionHello() {
        echo "eeeee";exit;
    }
    
	
	public function actionSaveCatUrl(){
		
		$categories_data_model = new CategoriesData;
		$categoriesDatas = CategoriesData::find()->where(['type' => 0,'store_id'=>7])->all();
		//echo '<pre>'; print_r($categoriesDatas); die;
		
		if(!empty($categoriesDatas)){
			foreach($categoriesDatas as $categoriesData){
				
				// $model = CategoriesData::find($categoriesData->id)->one();
				 $model = CategoriesData::find()->where(['id' => $categoriesData->id])->one();
				 $model->type = 1;
			  	 $model->save();
				 
				/** $url = parse_url($categoriesData->url); 
				parse_str($url['query'], $query); **/
				
				$count_products = $model->count_products;
				$perpage = 100;
				$list_page = $count_products / $perpage;
				$pages = round($list_page);
				$x  = 1;
				$y = 1;
				
				$categoryUrlModel = new CategoryUrl;
				$countLastInsertUrls = $categoryUrlModel->getLastInsertCategoryUrl($categoriesData);
				$pages = $pages + $countLastInsertUrls; // count pages aur last insert url for same category url
				$i_value = ($countLastInsertUrls != 0) ? $countLastInsertUrls + 1 : 1;
				
				for($i = $i_value; $i<=$pages; $i++){
					
					
					$category_urls = 'http://svcs.ebay.in/services/search/FindingService/v1?OPERATION-NAME=findItemsByCategory&GLOBAL-ID=EBAY-IN&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=vinayver-nayashop-PRD-899e85f71-1c6f9163&RESPONSE-DATA-FORMAT=JSON&categoryId='.$categoriesData->path.'&RESTPAYLOAD&paginationInput.entriesPerPage='.$perpage.'&paginationInput.pageNumber='.$i.'&sortOrder=StartTimeNewes';
					 //echo $i.'==><br>'.$category_urls.'</br>';
					 $category_url_model = new CategoryUrl;
					 $category_url_model->status = 0;
					 $category_url_model->store_name = 'ebay';
					 $category_url_model->store_id = 7;
					 $category_url_model->category_name = $categoriesData->category_name;
					 $category_url_model->category_type = $categoriesData->category_type;
					 $category_url_model->site_url = $category_urls;
					 
					 $category_url_model->scraper_group =$y;
					 if($x==5)
						{
							$y++;
							$x=0;
						}
						
						 $category_url_model->save();
						// echo 'pass'. $category_url_model->category_name.'<br>';
						$x++;	
					
				}  
			  
			}	die('save category url for ebay');
		}else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('ebay scraper','EbayController','No Any Category Data Url for ebay','Output',__LINE__);
				echo 'No Any Category Data for Save category Url for ebay'; die;
		}
		 
	}

	
	
	
	
public function actionScraperProductUrls($latest_product=0) {
		
		$max_scraper_group = CategoryUrl::find()->where(['store_id' => 7])->orderBy(['scraper_group' => SORT_DESC])->one();
		if(!empty($max_scraper_group)){
				
				for($i = 1; $i <=  $max_scraper_group->scraper_group; $i++){
					
					$categories_url_model = new CategoryUrl;
					$categories_urls= CategoryUrl::find()->where(['status' => 0,'store_id' => 7])->all();
					
					if(!empty($categories_urls)){
						 foreach($categories_urls as $categories_url){
							
							$categoryData = Categories::find()->where(['category_name' => $categories_url->category_name])->one();
							$category_id = !empty($categoryData) ? $categoryData->category_id : 0;
							
							$this->getProductFromCurl($categories_url->site_url,$categories_url->id_category_url,$category_id);
								
							
						} 
						//echo 'save products for flipkart'; die;
					}
				} //die('sadfasd');
		}else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('flipkart scraper','FlipkartController','No any category url for scrapping','Output',__LINE__);
				echo 'No Any URLs for scurpping'; die;
		}
		
  }
  
   
  
 public function getProductFromCurl($url,$id_category_url,$category_id){
						
						
						$objHttpService = new \common\components\HttpService();
						$response = $objHttpService->getResponse($url);
						 
							if(!empty($response)) {
								
								$foo = utf8_encode($response);
		
								// json encode for get data form url
								$productData = json_decode($foo, true);
								
								$productData = $productData['findItemsByCategoryResponse'][0]['searchResult'][0]['item'];
								if(isset($productData) && !empty($productData)){
									
									foreach($productData as $val){
											
											if(!empty($val)){
												
												$productDetail = array();
												$cod = '';
												if(in_array('COD',$val['title'])){ $cod = 'Cash On Delivery'; }
												
												$product_shipping = '';
												if(isset($val['shippingInfo'][0]['shippingType'][0])){ 
													$product_shipping = $val['shippingInfo'][0]['shippingType'][0]; 
												}
												
												$productDetail['title'] = isset($val['title'][0]) ? $val['title'][0] : '';
												$productDetail['subtitle'] = isset($val['subtitle'][0]) ? $val['subtitle'][0] : '';
												$productDetail['price'] = isset($val['sellingStatus'][0]['currentPrice'][0]['__value__']) ? $val['sellingStatus'][0]['currentPrice'][0]['__value__'] : '';
												$productDetail['url'] = isset($val['viewItemURL'][0]) ? $val['viewItemURL'][0] : '';
												$productDetail['cod'] = $cod;
												$productDetail['product_shipping'] = $product_shipping;
												
												$product_url = isset($val['viewItemURL'][0]) ? $val['viewItemURL'][0] : '';
												
												$exitProductUrl = ProductUrls::find()->where(['url' => $product_url, 'store_id' => 7])->one();
													if (empty($exitProductUrl) && !empty($product_url)) {
																
																$productUrlsModel = new ProductUrls();
																$productUrlsModel->store_id = 7;
																$productUrlsModel->cateogry_url_id = $id_category_url;
																$productUrlsModel->category_id = $category_id;
																$productUrlsModel->status = 0;
																$productUrlsModel->url = $product_url;
																$productUrlsModel->other = serialize($productDetail);
																$productUrlsModel->save();
																$exitProductUrl = ProductUrls::find()->where(['url' => $product_url, 'store_id' => 7])->one();
															
																$aws = new \common\components\Aws();
																$aws = $aws->sendMessage($exitProductUrl,'product_scraper');															
													}
											}
											
										}
										
										
									
								}else{
										echo '<pre>'; print_r($productData);
									}
								
										$CategoryUrlModel = CategoryUrl::findOne($id_category_url);
										$CategoryUrlModel->status = 1;
										$CategoryUrlModel->save(false);
										echo 'save category url id=>'.$CategoryUrlModel->id_category_url.'<br>';
							}
	 
 }
 
	
  
  public function actionScraper($latest_product=0) {
			
			
		$product_urls = ProductUrls::find()->where(['store_id' => 7,'status' => 0])->all();
		
		if(!empty($product_urls)){
				
				foreach($product_urls as $product_url){
					$categories_urls= Categories::find()->where(['category_id' => $product_url->category_id])->one();
					$category_name = !empty($categories_urls) ? $categories_urls->category_name : '';
					$objebayProduct = new \backend\models\EbayProductScrapper();
					$objebayProduct->saveProductData($product_url,$category_name);
					
				}
				
		}else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('ebay scraper','EbayController','No any product url for scrapping','Output',__LINE__);
				echo 'No Any URLs for scurpping'; die;
		}
			
		
	
	}
	
	
	/**public function actionScraper($latest_product=0) {
		
		$objEbayProduct = new \backend\models\EbayProductScrapper();
        //Tools::killOtherProcess("php console.php jungleeproductscrapper new");
        $siteName = 'http://www.ebay.com';
        // for men shoes category
        $url = 'http://svcs.ebay.in/services/search/FindingService/v1?OPERATION-NAME=findItemsByCategory&GLOBAL-ID=EBAY-IN&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=vinayver-nayashop-PRD-899e85f71-1c6f9163&RESPONSE-DATA-FORMAT=JSON&categoryId=15032&RESTPAYLOAD&paginationInput.entriesPerPage=100&paginationInput.pageNumber=1&sortOrder=StartTimeNewes';
        $categoryName = 'Mobile';
        $categoryType = 'Mobile';
		
//        $arr_url = $objflipkartProduct->getAllPagingUrl($url);
//        $objflipkartProduct->saveAllPagingUrl($arr_url, $categoryName, $categoryType);

//        $arr_department_type = array('For' => 'Men');
        $objEbayProduct->saveProduct($categoryName, '', $categoryType, $latest_product,'ebay');
		echo '<pre>'; print_r($objEbayProduct); die;
    } **/

}
