<?php

namespace backend\controllers;

use Yii;
use backend\models\FilterGroupItems;
use backend\models\FilterGroupItemsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Logs;
use yii\filters\AccessControl;
use common\models\Features;
use common\models\FeatureValues;
use backend\models\FilterGroupValues;

/**
 * FilterGroupItemsController implements the CRUD actions for FilterGroupItems model.
 */
class FilterGroupItemsController extends Controller
{
	
	public function beforeAction($action) {
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}
    public function behaviors()
    {
         return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete','feature_values_by_feature_id','save_sort_order'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all FilterGroupItems models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FilterGroupItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$model = new FilterGroupItemsSearch();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model' => $model,
        ]);
    }

    /**
     * Displays a single FilterGroupItems model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FilterGroupItems model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FilterGroupItems();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
			$postData = Yii::$app->request->post();
			
			$model->sort_order = $this->getSortorder($postData['FilterGroupItems']['group_id']);
			
			$model->created = date('Y-m-d h:i:s');
			
			$model->save();
				
				//$postData['FilterGroupValues']
				
				if(isset($postData['FilterGroupValues']['value_id']) && !empty($postData['FilterGroupValues']['value_id'])){
					
					foreach($postData['FilterGroupValues']['value_id'] as $value){
						$filterGroupValuesModel = new FilterGroupValues();
						$filterGroupValuesModel->value_id = $value;
						$filterGroupValuesModel->group_item_id = $model->id;
						$filterGroupValuesModel->created = date('Y-m-d h:i:s');
						$filterGroupValuesModel->save();
					}
					
				}
				
				
				$logs_model = new Logs();
				$user_logs = array('model'=>'FilterGroup','action'=>'Create ','activity' => $model->title.' is added','action_id'=>$model->id);
				$logs_model->saveUserLogs($user_logs);
			
            return $this->redirect(['index?FilterGroupItemsSearch[group_id]='.$model['group_id']]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FilterGroupItems model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
			$postData = Yii::$app->request->post();
			
			 if(isset($postData['FilterGroupValues']['value_id']) && !empty($postData['FilterGroupValues']['value_id'])){
					FilterGroupValues::deleteAll(['group_item_id' => $model->id]);
					foreach($postData['FilterGroupValues']['value_id'] as $value){
						$filterGroupValuesModel = new FilterGroupValues();
						$filterGroupValuesModel->value_id = $value;
						$filterGroupValuesModel->group_item_id = $model->id;
						$filterGroupValuesModel->created = date('Y-m-d h:i:s');
						$filterGroupValuesModel->save();
					}
					
				}
				
			$logs_model = new Logs();
				$user_logs = array('model'=>'FilterGroup','action'=>'Update ','activity' => $model->title.' is updated','action_id'=>$model->id);
				$logs_model->saveUserLogs($user_logs);
				
           return $this->redirect(['index?FilterGroupItemsSearch[group_id]='.$model['group_id']]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FilterGroupItems model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
          //$model = $this->findModel($id);
			
		  // $this->findModel($id)->delete();
			
			$model = $this->findModel($id);
			$modelDelete = $model;
			$modelDelete->is_delete = 1;
			$modelDelete->save(false);
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'FilterGroupItems','action'=>'Delete','activity' => $model->title.' is deleted','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);	
                        
            Yii::$app->session->setFlash('success', 'FilterGroupItems is successfully delete');

       return $this->redirect(['index?FilterGroupItemsSearch[group_id]='.$model['group_id']]);
    }

    /**
     * Finds the FilterGroupItems model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FilterGroupItems the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FilterGroupItems::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
	public function actionChange_status(){
		$model = new FilterGroupItems();
		 $getData = Yii::$app->request->get();
		 
			if(!empty($getData) && isset($getData['filter_group_item_id'])){
				
				$filterGroupItemsModel = FilterGroupItems::find()->where(['id' => $getData['filter_group_item_id']])->one();
				
				if($getData['type'] == 1){
					$status = 1;
				}else{
					$status = 0;
				}
				
				$filterGroupItemsModel->status = $status;
				$filterGroupItemsModel->save(false);
				
				$logs_model = new Logs();
				
				$user_logs = array('model'=>'FilterGroupItem','action'=>'Change FilterGroupItem Status','activity' => $filterGroupItemsModel->title.' is change status','action_id'=>$filterGroupItemsModel->id);
				$logs_model->saveUserLogs($user_logs);
				
				Yii::$app->session->setFlash('success', 'Filter Group Item Status changed successfully saved');
			
				return $this->redirect(['index?FilterGroupItemsSearch[group_id]='.$getData['filter_group_item_id']]);
				
			}
	}
	
	
	public function actionFeature_values_by_feature_id(){
		
			$model = new FeatureValues();
			 
			if (Yii::$app->request->isAjax) {
			
					$postData = Yii::$app->request->post();
					
					$selectedValuesArr = array();
					if(isset($postData['group_item_id']) && $postData['group_item_id'] != 0){
							$selectedFilterGroupValues = FilterGroupValues::find()->select(['group_item_id','id','value_id'])->where(['group_item_id' => $postData['group_item_id']])->all();
							
							if(!empty($selectedFilterGroupValues)){
								foreach($selectedFilterGroupValues as $value){
									$selectedValuesArr[$value->value_id] = $value->value_id;
								}
							}
					}
					
					
					
					
					$feature_values = FeatureValues::find()->select(['id','value'])->where(['feature_id' => $postData['feature_id']])->andWhere(['!=','is_delete',1])->all();
					
						\Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
						
						return $this->renderPartial('_feature_values_by_feature_id',['feature_values' => $feature_values,'feature_id'=>$postData['feature_id'],'selectedValuesArr'=>$selectedValuesArr,'code'=>100]);
					
				  }
		
	}
	
	
	
	public function actionSave_sort_order(){
			
			if(isset($_POST['sort_order']) && !empty($_POST['sort_order'])){
					
					
					$this->save_order($_POST['sort_order']);
					
					Yii::$app->session->setFlash('success', 'Filter Group Sort Order successfully saved');
				
					return $this->redirect(Yii::$app->request->referrer);
				
			}
			
	}
	
	public function save_order($sortData){
			
			if(!empty($sortData)){
				
				foreach($sortData as $key => $data){
					
						$FilterGroupItemsModel = FilterGroupItems::findOne($key);
						$FilterGroupItemsModel->sort_order = $data;
						$FilterGroupItemsModel->save(false);
						
						$logs_model = new Logs();
						$user_logs = array('model'=>'FilterGroup','action'=>'Change Save_sort_order','activity' => $FilterGroupItemsModel->title.' is change status','action_id'=>$FilterGroupItemsModel->id);
						$logs_model->saveUserLogs($user_logs);
					
				}
				
			}
		
	}
	
	
		
	public function getSortorder($group_id){
		
			if(!empty($group_id)){
				
				$result = FilterGroupItems::find()->where(['group_id' => $group_id])->orderBy('sort_order desc')->one();
				
				if (!empty($result)) {
					$sort_order = $result->sort_order + 1;
				} else {
					$sort_order = 1;
				}
			}else{
				$sort_order = 1;
			}
			
			return $sort_order;
	}
	
	
}
