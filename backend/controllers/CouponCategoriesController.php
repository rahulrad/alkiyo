<?php

namespace backend\controllers;

use Yii;
use common\models\CouponCategories;
use backend\models\CouponCategoriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Logs;
use yii\web\UploadedFile;

/**
 * CuponCategoriesController implements the CRUD actions for CuponCategories model.
 */
class CouponCategoriesController extends Controller
{
    public function behaviors()
    {
        return [

                'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CuponCategories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CouponCategoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CuponCategories model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CuponCategories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CouponCategories();

        if ($model->load(Yii::$app->request->post())   && $model->validate()) {
            $post_data = Yii::$app->request->post();
			
			// save category image
			$model->file = UploadedFile::getInstance($model,'file');
			
			if(!empty($model->file)){
				//$imageName = $model->file->baseName;
                $path = Yii::getAlias('@frontend') .'/web/';
                $imageName = time().uniqid();
				
				$model->file->saveAs($path.'uploads/coupons/'.$imageName.'.'.$model->file->extension);
				
				$model->image = 'uploads/coupons/'.$imageName.'.'.$model->file->extension;
			}
			
            if(!isset($post_data['CouponCategories']['show_home'])){
                $model->show_home = 0;
            }

            $model->created = date('Y-m-d h:i:s');
            $model->save();

            // save user logs data in user_logs table
            $logs_model = new Logs();
            
            $user_logs = array('model'=>'CouponCategories','action'=>'Create','activity' => $model->name.' is added','action_id'=>$model->id);
            $logs_model->saveUserLogs($user_logs);
            Yii::$app->session->setFlash('success', 'Coupon Category is successfully saved');
            
            return $this->redirect(['index']);

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CuponCategories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())  && $model->validate()) {
            $post_data = Yii::$app->request->post();
			
			// save category image
			$model->file = UploadedFile::getInstance($model,'file');
			
			if(!empty($model->file)){
				//$imageName = $model->file->baseName;
                $path = Yii::getAlias('@frontend') .'/web/';
                $imageName = time().uniqid();
				
				$model->file->saveAs($path.'uploads/coupons/'.$imageName.'.'.$model->file->extension);
				
				$model->image = 'uploads/coupons/'.$imageName.'.'.$model->file->extension;
			}
			
            if(!isset($post_data['CouponCategories']['show_home'])){
                $model->show_home = 0;
            }

            $model->save();
            
            // save user logs data in user_logs table
            $logs_model = new Logs();
            
            $user_logs = array('model'=>'CouponCategories','action'=>'Update','activity' => $model->name.' is updated','action_id'=>$model->id);
            $logs_model->saveUserLogs($user_logs);
                        
            Yii::$app->session->setFlash('success', 'Coupon Category is successfully updated');
            
            return $this->redirect(['index']);


        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CuponCategories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        	
			$model = $this->findModel($id);
			$modelDelete = $model;
			$modelDelete->is_delete = 1;
			$modelDelete->save(false);
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Coupons Category','action'=>'Delete','activity' => $model->name.' is deleted','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);	
                        
            Yii::$app->session->setFlash('success', 'Coupons Category is successfully delete');

        return $this->redirect(['index']);
    }

    /**
     * Finds the CuponCategories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CuponCategories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CouponCategories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
