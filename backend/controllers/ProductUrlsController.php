<?php

namespace backend\controllers;

use Yii;
use backend\models\ProductUrls;
use backend\models\ProductUrlsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Logs;
use yii\filters\AccessControl;
use backend\models\CategoryUrl;
use common\models\Categories;
use common\models\Suppliers;
use common\models\Products;
use yii\web\UploadedFile;

/**
 * ProductUrlsController implements the CRUD actions for ProductUrls model.
 */
class ProductUrlsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'importdata', 'export', 'download_blank_csv_format', 'product-scraper'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductUrls models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new ProductUrlsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new ProductUrlsSearch();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single ProductUrls model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductUrls model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductUrls();
        $model->setScenario('create');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $logs_model = new Logs();
            $user_logs = array('model' => 'ProductUrls', 'action' => 'Create', 'activity' => $model->url . ' is added', 'action_id' => $model->id);
            $logs_model->saveUserLogs($user_logs);

            if ($model->status) {
                $objProductScraper = $this->getStoreScraper($model->store_id);
                $category = Categories::findOne($model->category_id);
                if (!is_null($objProductScraper) && !is_null($category)) {
                    try {
                        $objProductScraper->saveProductData($model, $category->category_name);
                        Yii::$app->session->setFlash('success', 'Product Url is successfully saved');
                    } catch (\Exception $e) {
                        error_log(var_export($e->getTraceAsString(), true));
                        //echo "SQS Error in scraper: ".$e->getMessage();
                        Yii::$app->session->setFlash('error', "SQS Error in scraper: " . $e->getMessage());
                        $model->status = 0;
                        $model->save();
                    }
                }
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ProductUrls model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $logs_model = new Logs();
            $user_logs = array('model' => 'ProductUrls', 'action' => 'Update', 'activity' => $model->url . ' is updated', 'action_id' => $model->id);
            $logs_model->saveUserLogs($user_logs);
            Yii::$app->session->setFlash('success', 'Page is successfully saved');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProductUrls model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        die('Access dined');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductUrls model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductUrls the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductUrls::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * Import Category Data
     */

    public function actionImportdata()
    {

        $model = new ProductUrls();

        if ($model->load(Yii::$app->request->post())) {

            $file = UploadedFile::getInstance($model, 'category_csv_file');

            $filename = 'Data-' . Date('YmdGis') . '.' . $file->extension;

            $upload = $file->saveAs('uploads/category_import/' . $filename);

            define('CSV_PATH', 'uploads/category_import/');
            $csv_file = CSV_PATH . $filename;
            $row = 1;
	$errorString="";
            if (($handle = fopen($csv_file, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {


                    if ($row != 1) {

                        $ProductUrlsModel = new ProductUrls();

                        $url = isset($data[3]) ? $data[3] : '';

                        $checkResult = $ProductUrlsModel::find()->where(['url' => $url])->one();

                        $countRecords = $model::find()->orderBy('id desc')->one();

                        if (!empty($countRecords)) {
                            $c_id = $countRecords->id + 1;
                        } else {
                            $c_id = 1;
                        }


                        if (empty($checkResult) && isset($data[3]) && !empty($data[3])) {
                            $store_name = Suppliers::find()->where(['name' => trim($data[1])])->one();
                            $category_name = Categories::find()->where(['category_name' => trim($data[2])])->one();

                            $ProductUrlsModel->id = $c_id;
                            $ProductUrlsModel->store_id = !empty($store_name->id) ? $store_name->id : '';
                            $ProductUrlsModel->category_id = isset($category_name->category_id) ? $category_name->category_id : '';
                            $ProductUrlsModel->url = isset($data[3]) ? $data[3] : '';
                            $ProductUrlsModel->cateogry_url_id = 0;
                            $ProductUrlsModel->save(false);

                        }else{

				  $errorString.=$data[3]." url already exist<br/>";
			}
                    }
                    $row++;
                }
                fclose($handle);
            }
            //  unlink('uploads/category_import/'.$filename);
            Yii::$app->session->setFlash('success', 'Import Success<br/>'. $errorString);
            return $this->redirect(['index']);
        } else {
            return $this->render('importdata', ['model' => $model]);
        }

    }


    /**
     * Export Category Data
     */

    public function actionExport()
    {

        $productUrlsResults = ProductUrls::find()->All();


        $filename = 'Data-' . Date('YmdGis') . '-ProductUrls.csv';
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=" . $filename);
        header("Pragma: no-cache");
        header("Expires: 0");


        $file = fopen('php://output', 'w');
        fputcsv($file, array('ID', 'Store Name', 'Category', 'Url'));

        $categoryModel = new Categories();
        foreach ($productUrlsResults as $product_url) {

            $productModel = new Products();

            $categoryName = $productModel->categoryNameToCategoryId($product_url->category_id);

            $storeName = $productModel->storeNameToStoreId($product_url->store_id);


            $record = array($product_url->id, $storeName, $categoryName, $product_url->url);

            fputcsv($file, $record);

        }

    }


    /**
     * Export Blank Category Data csv file
     */

    public function actionDownload_blank_csv_format()
    {


        $filename = 'Data-' . Date('YmdGis') . '-ProductUrlsBlank.csv';
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=" . $filename);
        header("Pragma: no-cache");
        header("Expires: 0");

        $file = fopen('php://output', 'w');
        fputcsv($file, array('S.No', 'Store Name', 'Category', 'Url', 'Auto Scrap'));

    }

    public function actionProductScraper($id)
    {
        if (isset($id) && !empty($id)) {

            $product_url = ProductUrls::find()->where(['id' => $id])->one();
            //echo '<pre>'; print_r($product_url); die;
            if (!empty($product_url)) {

                $categories_urls = Categories::find()->where(['category_id' => $product_url->category_id])->one();
                $category_name = !empty($categories_urls) ? $categories_urls->category_name : '';

                $objProductScraper = $this->getStoreScraper($product_url->store_id);

                if (!is_null($objProductScraper)) {

                    try {
                        $objProductScraper->saveProductData($product_url, $category_name);
                        Yii::$app->session->setFlash('success', 'Product Scrape Success');
                        return $this->redirect(Yii::$app->request->referrer);
                    } catch (\Exception $e) {
                        error_log(var_export($e->getTraceAsString(),true));
                        //echo "SQS Error in scraper: ".$e->getMessage();
                        Yii::$app->session->setFlash('error', "SQS Error in scraper: " . $e->getMessage());
                        return $this->redirect(Yii::$app->request->referrer);
                    }

                }

            }
        }
    }

    /**
     * @param $storeId
     * @return null
     */
    private function getStoreScraper($storeId)
    {
        if ($storeId == 1) {
            return new \backend\models\FlipKartProductScrapper();

        } elseif ($storeId == 3) {
            return new \backend\models\SnapDealProductScrapper();

        } elseif ($storeId == 5) {
            return new \backend\models\AmazonProductScrapper();

        } elseif ($storeId == 6) {
            return new \backend\models\JabongProductScrapper();

        } elseif ($storeId == 7) {
            return new \backend\models\EbayProductScrapper();
        } else {
            return null;
        }
    }
}
