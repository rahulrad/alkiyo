<?php
namespace backend\controllers;

use Yii;
use common\models\Products;
use backend\models\ProductsSearch;
use backend\models\NayashoppyProductsSearch;
use common\models\Brands;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Logs;
use backend\models\MongoProducts;
use backend\models\MongoProductFeatures;
use common\models\Categories;
use common\models\ProductsSuppliers;
use common\models\Suppliers;
use common\models\Features;
use common\models\FeatureValues;
use common\models\FeatureGroups;
use common\models\ProductFeature;
use common\models\ProductImage;
use common\models\ProductReviews;
use backend\models\ProductsTag;
use backend\models\ProductReviewsSearch;
use yii\web\UploadedFile;



/**
 * ProductsController implements the CRUD actions for Products model.
 */
class NayashoppyProductsController extends Controller
{
	
	public function beforeAction($action) {
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete','mapping','products_mapping','searching_similler_products','product_map','products_mapping_custom','save_filter_mongo_products','product_features','importdata','export','unlink_product','ajax_mongo_product_popup_form','save_ajax_mongo_product','product_reviews','change_status','mongo_product_features','view_suppilers','change_is_approved','change_multiproduct_is_approved'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

	
    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
		
		
		
        $searchModel = new NayashoppyProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$model = new NayashoppyProductsSearch();
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model' =>$model,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
	
	
	
	
	public function actionMongo_product_features(){
		
		
		 $model = new Products();
			 
		$postData = Yii::$app->request->get();
					if(!empty($postData)){
						$mongoProductsModel = new MongoProducts();
						$mongoProductDetail  = MongoProducts::findOne($postData['mongo_product_id']);
						
						$productFeatures = $mongoProductsModel->getMongoProductFeatures($postData['mongo_product_id']);
						
						$mongoProductFeatureModel = new MongoProductFeatures();
						$selectedFeatureValues = $mongoProductFeatureModel->getSelectedProductFeatures($mongoProductDetail->product_id);
						
						
						
						return $this->render('_mongo_product_features',['postData' => $postData,'code'=>100,'model'=>$model,'productFeatures'=>$productFeatures,'selectedFeatureValues'=>$selectedFeatureValues,'mongoProductDetail'=>$mongoProductDetail]);
					}
					
		
	}
	
	
	
	public function actionSave_ajax_mongo_product(){
		
		 $MongoProductsModel = new MongoProducts();
		 
		 if ($MongoProductsModel->load(Yii::$app->request->post())) {
			
			$postData = Yii::$app->request->post();
			
			$mongoProductDetail = $MongoProductsModel::findOne($postData['MongoProducts']['_id']);
			
			if(!empty($mongoProductDetail)){
					$mongoProductDetail->product_name = $postData['MongoProducts']['product_name'];
					
					$product_model = new Products();
					$productDeatil = $product_model->saveFilterMongoProducts($mongoProductDetail);
					//echo '<pre>'; print_r($productDeatil); die;
					if(!empty($postData['ProductFeature']) && !empty($productDeatil->product_id)){
							$mongoProductFeatureModel = new MongoProductFeatures();
							$mongoProductFeatureModel->saveMongoProductFeatureData($productDeatil->product_id,$productDeatil->categories_category_id, $postData['ProductFeature']);
					}
					
					
				}
			 Yii::$app->session->setFlash('success', 'Product is successfully mapp from mongoDb to our database');
			return $this->redirect(Yii::$app->request->referrer);
		 }
		
		
	}
	
	
	/**
     * Product Feature Function
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
	 
	 public function actionProduct_features(){
		 
		 $model = new FeatureValues();
		 $getData = Yii::$app->request->get();
		 
			if(!empty($getData) && isset($getData['product_id'])){
				
				$product_id = $getData['product_id'];
				
				$productDetail = Products::find()->select(['product_id','product_name'])->where(['product_id' => $product_id])->one();
				
				$productFeatureModel = new ProductFeature();
				
				$productFeatures = $productFeatureModel->getProductFeatures($product_id);
				
				$selectedFeatureValues = $productFeatureModel->getSelectedProductFeatures($product_id);
				
				
				$postData = Yii::$app->request->post();
					
					if(!empty($postData)){
						
						$updateProductFeatures = $productFeatureModel->updateProductFeatures($postData);
						
						 Yii::$app->session->setFlash('success', 'Product Feature is successfully update');
			
					   return $this->redirect(['nayashoppy-products/']);
						
					}
					
				return $this->render('product_features',['productFeatures' => $productFeatures,'model'=>$model,'productDetail'=>$productDetail,'selectedFeatureValues'=>$selectedFeatureValues]);
				
			
			
			
			
			}
			
	 }
	 
	
	 
	 
	 public  function actionUnlink_product(){
		 $model = new Products();
		 $getData = Yii::$app->request->get();
		 
			if(!empty($getData) && isset($getData['product_id'])){
				
				$result = $model->unlink_product_from_suppilers($getData['product_id']);
				
				// save user logs data in user_logs table
				$logs_model = new Logs();
				
				$user_logs = array('model'=>'Products','action'=>'Unlink Product','activity' => $result->product_name.' is added','action_id'=>$result->product_id);
				$logs_model->saveUserLogs($user_logs);
				
				Yii::$app->session->setFlash('success', 'Product Unlink to stores successfully saved');
			
				return $this->redirect(['index']);
				
			}
	 }
	 
	 
    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		
		
        if ($model->load(Yii::$app->request->post())  && $model->validate()) {
			
			
				$post_data = Yii::$app->request->post();
				
				if(!empty($post_data['Products']['slug'])){
				
					$model->slug = $post_data['Products']['slug'];
				
				}
				
			// save main image
			$model->main_image = UploadedFile::getInstance($model,'main_image');
			if(!empty($model->main_image)){
				$imageName = $model->main_image->baseName;
				$path = Yii::getAlias('@frontend') .'/web/';
				$model->main_image->saveAs($path.'uploads/products/'.$imageName.'.'.$model->main_image->extension);
				
				
				$model->image = $imageName.'.'.$model->main_image->extension;
				
			}
			
                                
            if(!isset($post_data['Products']['show_home_page'])){
                $model->show_home_page = 0;
            }
			
			if(!isset($post_data['Products']['is_approved'])){
                $model->is_approved = 0;
            }
			$model->save();
			
			
			// update products tags
			if(isset($post_data['Products']['product_tag'])){
				$product_tags = $post_data['Products']['product_tag'];
				if(!empty($product_tags)){
					$ProductsTagModel = new ProductsTag();
					$ProductsTagModel::deleteAll(['product_id' =>$model->product_id]);
					$ProductsTagModel->saveProductsTag($product_tags,$model->product_id);
				}
			}
			
			// get all other images 
			$other_images = UploadedFile::getInstances($model, 'other_images');
			
			
			// check if !empty then it will be enter this code
			if(!empty($other_images)){
				$ProductImageModel = new ProductImage();
				$ProductImageModel::deleteAll(['id_product' =>$model->product_id]);
				$ProductImageModel->saveProductMultiImages($other_images,$model->product_id);
			}
			
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Products','action'=>'Update','activity' => $model->product_name.' is updated','action_id'=>$model->product_id);
			$logs_model->saveUserLogs($user_logs);
                        
            Yii::$app->session->setFlash('success', 'Product is successfully update');
			
           // return $this->redirect(['view', 'id' => $model->product_id]);
//		   return $this->redirect(['index']);
            return $this->goBack();
        } else {
            Yii::$app->user->setReturnUrl((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : Url::to(['index'])));
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		
		/**$model = $this->findModel($id);
        $this->findModel($id)->delete();
		
		// delete product suppilers by product_id
		ProductsSuppliers::deleteAll(['product_id' =>$id]);
		
		// delete product features by product_id
		ProductFeature::deleteAll(['id_product' =>$id]);
		
		// delete product images by product_id
		ProductImage::deleteAll(['id_product' =>$id]);
		
		// delete product tags by product_id
		ProductsTag::deleteAll(['product_id' =>$id]);
		
		// delete product reviews by product_id
		ProductReviews::deleteAll(['product_id' =>$id]); **/
		
		
		$model = $this->findModel($id);
			$modelDelete = $model;
			$modelDelete->is_delete = 1;
			$modelDelete->save(false);
		
		// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Products','action'=>'Delete','activity' => $model->product_name.' is deleted','action_id'=>$model->product_id);
			$logs_model->saveUserLogs($user_logs);
                        
                         Yii::$app->session->setFlash('success', 'Product is successfully delete');

        return $this->redirect(['index']);
    }
	
	

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
			
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
	
	public function actionChange_status(){
		$model = new Products();
		 $getData = Yii::$app->request->get();
		 
			if(!empty($getData) && isset($getData['product_id'])){
				
				$productModel = Products::find()->where(['product_id' => $getData['product_id']])->one();
				
				if($getData['type'] == 'inactive'){
					$status = 'inactive';
				}else{
					$status = 'active';
				}
				
				$productModel->active = $status;
				$productModel->save(false);
				
				$logs_model = new Logs();
				
				$user_logs = array('model'=>'Products','action'=>'Change Product Status','activity' => $productModel->product_name.' is change status','action_id'=>$productModel->product_id);
				$logs_model->saveUserLogs($user_logs);
				
				Yii::$app->session->setFlash('success', 'Product Status changed successfully saved');
			
				return $this->redirect(['index']);
				
			}
	}
	
	
	
	public function actionView_suppilers(){
		
			$model = new ProductsSuppliers();
			$getData = Yii::$app->request->get();
			
			if(!empty($getData) && isset($getData['product_id'])){
				
				$productsDetail = Products::find()->select(['product_name'])->where(['product_id' => $getData['product_id']])->one();
				
				$productsSuppliers = ProductsSuppliers::find()->where(['product_id' => $getData['product_id']])->all();
				
				return $this->render('view_suppilers', ['model' => $model,'productsSuppliers'=>$productsSuppliers,'productsDetail'=>$productsDetail]);
				
			}
		
	}
	
	
	public function actionChange_is_approved(){
		$model = new Products();
		 $getData = Yii::$app->request->get();
		 
		 
			if(!empty($getData) && isset($getData['product_id'])){
				
				$productModel = Products::find()->where(['product_id' => $getData['product_id']])->one();
				
				if($getData['type'] == 0){
					$is_approved = 0;
					$status = 'inactive';
				}else{
					$is_approved = 1;
					$status = 'active';
				}
				
				$productModel->active = $status;
				$productModel->is_approved = $is_approved;
				$productModel->save(false);
				
				$logs_model = new Logs();
				
				$user_logs = array('model'=>'Products','action'=>'Change Product Is Approved','activity' => $productModel->product_name.' is change is_approved','action_id'=>$productModel->product_id);
				$logs_model->saveUserLogs($user_logs);
				
				Yii::$app->session->setFlash('success', 'Product Approved changed successfully saved');
			
				return $this->redirect(['index']);
				
			}
	}
	
	public function actionChange_multiproduct_is_approved(){
		$selection_data=(array)Yii::$app->request->post('selection');//typecasting
		
		
		if(!empty($selection_data)){
			//echo '<pre>'; print_r($selection_data); die;
			foreach($selection_data as $id){
				
				$productModel = Products::find()->where(['product_id' => $id])->one();
				
				
				$productModel->active = 'active';
				$productModel->is_approved = 1;
				$productModel->save(false);
				
				$logs_model = new Logs();
				
				$user_logs = array('model'=>'Products','action'=>'Change Product Is Approved','activity' => $productModel->product_name.' is change is_approved','action_id'=>$productModel->product_id);
				$logs_model->saveUserLogs($user_logs);
				
				
				
			}
			
			Yii::$app->session->setFlash('success', 'Product Approved changed successfully saved');
			
				return $this->redirect(['index']);
					
					
		} 
		
	}
	

}
