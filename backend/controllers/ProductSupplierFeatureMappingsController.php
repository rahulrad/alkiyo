<?php

namespace backend\controllers;

use common\models\Categories;
use common\models\ProductsSuppliers;
use common\models\Suppliers;
use Yii;
use common\models\ProductSupplierFeatureMappings;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductSupplierFeatureMappingsController implements the CRUD actions for ProductSupplierFeatureMappings model.
 */
class ProductSupplierFeatureMappingsController extends Controller
{


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'import-data', 'export', 'download-sample-csv'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductSupplierFeatureMappings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ProductSupplierFeatureMappings::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductSupplierFeatureMappings model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductSupplierFeatureMappings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductSupplierFeatureMappings();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ProductSupplierFeatureMappings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProductSupplierFeatureMappings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductSupplierFeatureMappings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductSupplierFeatureMappings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductSupplierFeatureMappings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDownloadSampleCsv()
    {
        $filename = 'Data-' . Date('YmdGis') . '-ProductUrlsBlank.csv';
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=" . $filename);
        header("Pragma: no-cache");
        header("Expires: 0");
        $file = fopen('php://output', 'w');
        fputcsv($file, array('Store Name', 'Category', 'emi', 'cod', 'delivery', 'return_policy'));
    }

    public function actionImportData()
    {

        $model = new ProductSupplierFeatureMappings();

        if ($model->load(Yii::$app->request->post())) {

            $file = UploadedFile::getInstance($model, 'uploadFile');

            if (!is_null($file)) {

                $filename = 'Data-' . Date('YmdGis') . '.' . $file->extension;
                define('CSV_PATH', 'uploads/category_import/');
                $upload = $file->saveAs(CSV_PATH . $filename);
                $csv_file = CSV_PATH . $filename;
                $row = 1;
                if (($handle = fopen($csv_file, "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        if ($row != 1 && (isset($data[0]) && !empty($data[0])) && (isset($data[1]) && !empty($data[1]))) {

                            $supplierModel = Suppliers::find()->where([
                                'name' => trim(strtolower($data[0]))
                            ])->one();
                            $categoryModel = Categories::find()->where([
                                'category_name' => trim(strtolower($data[1]))
                            ])->one();
                            if (!empty($supplierModel) && !empty($categoryModel)) {

                                $model = ProductSupplierFeatureMappings::find()->where([
                                    'store_id' => $supplierModel->id,
                                    'category_id' => $categoryModel->category_id

                                ])->one();
                                if (is_null($model)) {
                                    $model = new ProductSupplierFeatureMappings();
                                }
                                $model->store_id = $supplierModel->id;
                                $model->store_name = $supplierModel->name;
                                $model->category_id = $categoryModel->category_id;
                                $model->emi = trim($data[2]);
                                $model->cod = trim($data[3]);
                                $model->delivery = trim($data[4]);
                                $model->return_policy = trim($data[5]);
                                $model->save();
                            }
                        }
                        $row++;
                    }
                    fclose($handle);
                }
                unlink(CSV_PATH . $filename);
                Yii::$app->session->setFlash('success', 'Mappings successfully imprted!');
                return $this->redirect(['index']);
            } else {
                return $this->render('importdata', ['model' => $model]);
            }
        } else {
            return $this->render('importdata', ['model' => $model]);
        }
    }

    public function actionExport()
    {
        $filename = 'Data-' . Date('YmdGis') . '-ProductUrls.csv';
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=" . $filename);
        header("Pragma: no-cache");
        header("Expires: 0");

        $file = fopen('php://output', 'w');
        fputcsv($file, array('Store Name', 'Category', 'emi', 'cod', 'delivery', 'return_policy'));

        $productUrlsResults = ProductSupplierFeatureMappings::find()->All();
        foreach ($productUrlsResults as $model) {
            $categoryModel = $model->category;
            $supplierModel = $model->supplier;
            $record = array($supplierModel->name, $categoryModel->category_name, $model->emi, $model->cod, $model->delivery, $model->return_policy);
            fputcsv($file, $record);
        }
    }
}
