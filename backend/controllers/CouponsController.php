<?php

namespace backend\controllers;

use Yii;
use common\models\Coupons;
use backend\models\CouponsSearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Logs;
use yii\web\UploadedFile;
use common\models\CouponCategories;
use backend\models\CouponsVendors;

/**
 * CouponsController implements the CRUD actions for Coupons model.
 */
class CouponsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post','scraper'],
                ],
            ],
        ];
    }

    



	 public function actionIndex()
    {
        $searchModel = new CouponsSearch();
	
	    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


public function actionUploadFile()
    {
           $module = Yii::$app->getModule('articles');
        $imagePath = '/uploads/coupons/cms/';
        $imagePathUrl = '/uploads/coupons/cms/';
        $imgName = time();
        $fileField = "upload";
		$path = Yii::getAlias('@frontend') .'/web/';

        $file = UploadedFile::getInstanceByName($fileField);
        $message = '';
        $url = '';
        if (is_null($file)) {
            $message = 'Failed Upload File';
        } else {
            $fileExt = $file->extension;
            if ($fileExt != 'jpg' && $fileExt != 'jpeg' && $fileExt != 'png' && $fileExt != 'gif') {
                $message = "The image must be in either JPG, JPEG, GIF or PNG format.";
            } else {
                $file->name = $imgName . ".{$fileExt}";
                $image = $file->saveAs($path.$imagePath . $imgName . ".{$fileExt}");
                if ($image !== false) {
                    $url =  $module->frontPageUrl. $imagePathUrl . '/' . $file->name;
                } else {
                    $message = 'Success Upload Failure,Try again!';
                }
            }
        }
        $funcNum = $_GET ['CKEditorFuncNum'];
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
    }


	 public function actionStage()
    {
        $searchModel = new CouponsSearch();
		$searchModel->moved = 0;
		$searchModel->type = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Coupons model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Coupons model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Coupons();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
				
						
			$model->created = date('Y-m-d h:i:s');

            if ($model->save()) {
                $model->track_link = $model->storeLink;
                $model->save();
            }
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Coupons','action'=>'Create','activity' => $model->title.' is added','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
			
			Yii::$app->session->setFlash('success', 'Coupons is successfully saved');
            return $this->redirect(['index']);
			
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Coupons model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->created = date('Y-m-d h:i:s');

            if ($model->save()) {
                $model->track_link = $model->storeLink;
                $model->save();
            }
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Coupons','action'=>'Update','activity' => $model->title.' is added','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
			
			Yii::$app->session->setFlash('success', 'Coupons is successfully saved');
			if($model->moved){
            return $this->redirect(['online']);
			}else{
				     return $this->redirect(['stage']);
			}
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Coupons model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
		
		$this->findModel($id)->delete();
		
		// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Coupons','action'=>'Delete','activity' => $model->title.' is deleted','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);	
                        
            Yii::$app->session->setFlash('success', 'Coupons is successfully delete');
		if($model->moved){
            return $this->redirect(['online']);
			}else{
				     return $this->redirect(['stage']);
			}
        
    }

	  public function actionMovecoupons()
    {
        $selection_data = (array)Yii::$app->request->post('selection');//typecasting

        if (!empty($selection_data)) {
            //echo '<pre>'; print_r($selection_data); die;
            foreach ($selection_data as $id) {

                $Coupons = new Coupons();
                $couponsData = $Coupons::find()->where(['id' => $id])->one();

                if (!empty($couponsData)) {
                   $couponsData->delete();

                }

            }
            Yii::$app->session->setFlash('success', 'Coupons Deleted Successfully');

            return $this->redirect(Yii::$app->request->referrer);

        }
        
    }


	

    /**
     * Finds the Coupons model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Coupons the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Coupons::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
	public function actionSave_coupons_api()
	{
		
		$apiurl = 'https://api.hasoffers.com/Apiv3/json?NetworkId=vcm&Target=Affiliate_Offer&Method=findAll&api_key=7588c21a58502cb238a908dd68cb03bc2f83d8884ab03b98e0e3ba861224d7ff';

         $data = json_decode(file_get_contents($apiurl), true);
       //  echo '<pre>'; print_r($data); die;
		 if(!empty($data['response']['data']) && isset($data['response']['data'])){
				
				foreach($data['response']['data'] as $dataline){
			 
				 $couponModel = new Coupons();
				 
				 $checkResult = $couponModel::find()->where(['offer_id' => $dataline['Offer']['id']])->one();
				
				 if(empty($checkResult))
				 {
					  $couponModel->title = $dataline['Offer']['name'];
					  $couponModel->offer_id = $dataline['Offer']['id'];
					  $couponModel->description = $dataline['Offer']['description'];
					  $couponModel->expiry_date = $dataline['Offer']['expiration_date'];
					  $couponModel->offer_url = $dataline['Offer']['preview_url'];
					  $couponModel->save(false);
				 }
			}  
		 }		
	}
	

	 
	/**
    * Import Product Data
    */

    public function actionImportdata()
    {	
    	$model = new Coupons();

    	if($model->load(Yii::$app->request->post())){
       
        $file = UploadedFile::getInstance($model,'file');
		
        $filename = 'Data-'.Date('YmdGis').'.'.$file->extension;
		
        $upload = $file->saveAs('uploads/coupons_import/'.$filename);
		
		define('CSV_PATH','uploads/coupons_import/'); 
		$csv_file = CSV_PATH . $filename;
			$row = 1;
			$charset = 'UTF-8';

			$errorstring=[];
			if (($handle = fopen($csv_file, "r")) !== FALSE) {
					while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
					
					if($row != 1){
						
						$couponsModel = new Coupons();
						
						$merchant = isset($data[0])?$data[0]:'';
						$category = isset($data[1])?$data[1]:'';

					if (!empty($merchant) && ($model = CouponsVendors::findOne($merchant)) !== null) {
						$couponsModel->merchant = isset($data[0])?$data[0]:'';
							$couponsModel->category = isset($data[1])?$data[1]:'';
							$couponsModel->title = isset($data[2])?$data[2]:'';

							
							$couponsModel->meta_title = isset($data[3])?$data[3]:'';
							if(mb_strlen($couponsModel->meta_title, $charset) > 70) {
 									$couponsModel->meta_title = mb_substr($couponsModel->meta_title, 0, 70, $charset);
							}
							$couponsModel->meta_description = isset($data[4])?$data[4]:'';

							if(mb_strlen($couponsModel->meta_description , $charset) > 160) {
 									$couponsModel->meta_description  = mb_substr($couponsModel->meta_description , 0, 160, $charset);
							}
							$couponsModel->meta_keywords = isset($data[5])?$data[5]:'';
							$couponsModel->link = isset($data[6])?$data[6]:'';
							$couponsModel->start_date = isset($data[7])?$data[7]:'';
							$couponsModel->expiry_date = isset($data[8])?$data[8]:'';
							$couponsModel->discount = isset($data[9])?$data[9]:'';
							$couponsModel->featured = isset($data[10])?$data[10]:'';
							$couponsModel->exclusive = isset($data[11])?$data[11]:'';
							$couponsModel->description = isset($data[12])?$data[12]:'';
							$couponsModel->status = isset($data[13])?$data[13]:'';
							$couponsModel->code = isset($data[14])?$data[14]:'';
							$couponsModel->type = isset($data[15])?$data[15]:'';
							$couponsModel->verified_on = isset($data[16])?$data[16]:'';
							$couponsModel->call_to_action_button = isset($data[17])?$data[17]:'';
							try{
							if($couponsModel->save(false)){
                                $couponsModel->track_link = $couponsModel->storeLink;
                                $couponsModel->save();

							}else{
                                array_push($errorstring, "excpetion for record" . $row);
							}
							}catch(\yii\db\Exception $p){
                                array_push($errorstring, "excpetion for record" . $row . "<br/>");
							}
						}
						else{
                            array_push($errorstring, "Merchant is not exist " . $merchant . "<br/>");
						}
						
					}
					$row++;
					
					}
				fclose($handle);
			}
			//  unlink('uploads/coupons_import/'.$filename);
            Yii::$app->session->setFlash('success', 'Import Success.<br/>' . implode("<br>", $errorstring), false);
			return $this->redirect(['index']);
	    }else{
	        return $this->render('importdata',['model'=>$model]);
	    }

    }
	
	
	  /**
	* Export Products Data
    */

    public function actionExport(){
			
			$couponsResults = Coupons::find()->All();
			
			
			$filename = 'Data-'.Date('YmdGis').'-Coupons.csv';
			header("Content-type: text/csv");
			header("Content-Disposition: attachment; filename=".$filename);
			header("Pragma: no-cache");
			header("Expires: 0");


			 $file = fopen('php://output', 'w');                              
			 fputcsv($file, array('merchant', 'category', 
			 'title', 'meta_title','meta_description','meta_keywords',
			 'link','start_date','expiry_date','discount','featured','exclusive',
			 
			 'description','status','code','type','verified_on','call_to_action_button'
			 )); 
				foreach($couponsResults as $coupons){

					$record = array($coupons->merchant,$coupons->category,$coupons->title,
					$coupons->meta_title,$coupons->meta_description,$coupons->meta_keywords,
					$coupons->link,$coupons->start_date,$coupons->expiry_date,$coupons->discount,$coupons->featured,
					$coupons->exclusive,$coupons->description,$coupons->status,
					$coupons->code,$coupons->type,$coupons->verified_on,
					$coupons->call_to_action_button
					);
					fputcsv($file, $record);
						
				}

		}
		
		
	/**
	* Export Products Data
    */

    public function actionDownload_coupon_csv_format(){
			
			
			$filename = 'Data-'.Date('YmdGis').'-Coupons.csv';
			header("Content-type: text/csv");
			header("Content-Disposition: attachment; filename=".$filename);
			header("Pragma: no-cache");
			header("Expires: 0");

			 $file = fopen('php://output', 'w');                              
			 fputcsv($file, array('merchant', 'category', 
			 'title', 'meta_title','meta_description','meta_keywords',
			 'link','start_date','expiry_date','discount','featured','exclusive',
			 
			 'description','status','code','type','verified_on','call_to_action_button'
			 )); 

		}
		
	public function actionScraper(){
			
			//http://www.optimise.co.in
			//Usr: kishore@nayashoppy.com
			//Pwd : Raditya2014
			//api : 0791e488-ad84-4999-826f-5f5824846b7e
			//private key : 92e7698f8d784acfb4221dd35b2c3834
			//http://api.omgpm.com/network/OMGNetworkApi.svc/v1.2/Affiliate/GetVoucherCodes?Key={Key}&Sig={Signature}&SigData={SignatureData}&AID={AID}&AgencyID={AgencyID}&Status={Status}&StartDate={StartDate}&EndDate={EndDate}
			//$optimise_apiurl = 'http://api.omgpm.com/network/OMGNetworkApi.svc/v1.2/Affiliate/GetVoucherCodes?Key=0791e488-ad84-4999-826f-5f5824846b7e&Sig=92e7698f8d784acfb4221dd35b2c3834&SigData='.$sig_data.'&AID=965781&AgencyID=95';//
			
			
			/***********************************************************/
	
			
			// get data form optomise api for coupons
			
			// Delete All coupons before save
			
			
			Coupons::deleteOldDeals();  // delete coupouns
			//CouponCategories::deleteAll();
			
			
			$optomiseCouponsData = $this->getOptimiseCouponsData();

			if(!empty($optomiseCouponsData)){
		
				foreach($optomiseCouponsData['GetVoucherCodesForAffiliateResult'] as $data){
					
				$couponsModel="";
				$type=2;
				if (strpos($data['Code'], 'No Voucher Code') !== false)
    					{
						$type=1;
						}
					$data['Code'] = str_replace('No Voucher Code','',$data['Code']);
					
					
					$couponsModel = Coupons::find()->where(['offer_id' => $data['VoucherCodeId'], 'api_name'=>'optimisemedia'])->one();
						

						if(empty($couponsModel)  ){

								$couponsModel = new Coupons();
						}
							
							$couponsModel->type=$type;
							$couponsModel->title = $data['Title'];
							$couponsModel->offer = $data['Product'];
							$couponsModel->code = $data['Code'];
							$couponsModel->link = $data['TrackingURL'];
							$couponsModel->promo_id = $data['PID'];
							$couponsModel->offer_id = $data['VoucherCodeId'];
							$couponsModel->offer_url = $data['TrackingURL'];
							$couponsModel->description = $data['Description'];
							$couponsModel->api_name = 'optimisemedia';
							$couponsModel->merchant = $data['Merchant'];
							$couponsModel->merchant_logo_url = $data['MerchantLogoURL'];
							
							
							$couponsModel->start_date = self::convertDate($data['ActivationDate']);
							$couponsModel->expiry_date =self::convertDate( $data['ExpiryDate']);
							$couponCategoriesModel = new CouponCategories();
							$categoryResult = $couponCategoriesModel->saveCategoryFromApi($data['CategoryName']);
							
							if(!empty($categoryResult)){
									$couponsModel->category = $categoryResult->id;
							}
							
							
							$couponsModel->created = date('Y-m-d h:i:s');
							
							$couponsModel->save(false);
							
						
					
				}
			} 


			
			
			// $payoom_apiurl = 'http://payoom.in/deeplinking/coupons-json.php?affid=20917';//

			// $payoomCouponsData = json_decode(file_get_contents($payoom_apiurl), true);
			
			// if(!empty($payoomCouponsData)){
				
			// 	foreach($payoomCouponsData as $data){
					
			// 		$couponsModel = new Coupons();
			// 		$checkResult = $couponsModel::find()->where(['offer' => $data['campaign'],'offer_id' => $data['offerID']])->one();
					
						
			// 			if(empty($checkResult)){
							
			// 				$couponsModel->title = $data['title'];
			// 				$couponsModel->offer = $data['campaign'];
			// 				$couponsModel->code = $data['coupon'];
			// 				$couponsModel->start_date = $data['start_date'];
			// 				$couponsModel->expiry_date = $data['end_date'];
			// 				$couponsModel->link = $data['link'];
			// 				$couponsModel->created = date('Y-m-d h:i:s');
			// 				$couponsModel->api_name = 'payoom';
							
			// 				$couponsModel->save(false);
							
			// 			}
					
			// 	}
			// } 
				
			
			$vcommission_apiurl = 'https://tools.vcommission.com/api/coupons.php?apikey=7588c21a58502cb238a908dd68cb03bc2f83d8884ab03b98e0e3ba861224d7ff';//

			// $couponsData = json_decode(file_get_contents($vcommission_apiurl), true);
			// print_r($couponsData);


			$headers = array( 
			 "Content-Type: application/json",
			 "Accept: application/json",
			 "Access-Control-Request-Method: GET" 
			 );

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $vcommission_apiurl);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
			 
			$result = curl_exec($ch);
			$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			 print_r($status);
			$couponsData = json_decode($result, true);

			


			if(!empty($couponsData)){
				
				foreach($couponsData as $data){
					


					$couponsModel="";
					$couponsModel = Coupons::find()->where(['promo_id' => $data['promo_id'],'api_name'=>'vcommission'])->one();
					if(empty($couponsModel)){
								$couponsModel = new Coupons();
					}


if( isset($data) && array_key_exists('coupon_type',$data) &&  $data['coupon_type']==='Coupon'){
$couponsModel->type =1;
}

if( isset($data) && array_key_exists('coupon_type',$data) &&  $data['coupon_type']==='Promotion'){
$couponsModel->type =2;
}
							$couponsModel->promo_id = $data['promo_id'];
							$couponsModel->offer_id = $data['offer_id'];
							$couponsModel->offer = $data['offer_name'];
							$couponsModel->offer_type = $data['coupon_type'];
							$couponsModel->code = $data['coupon_code'];
							$couponsModel->title = $data['coupon_title'];
							$couponsModel->description = $data['coupon_description'];
							
							$couponCategoriesModel = new CouponCategories();
							$categoryResult = $couponCategoriesModel->saveCategoryFromApi($data['category']);
							
							if(!empty($categoryResult)){
									$couponsModel->category = $categoryResult->id;
							}
							
							$couponsModel->offer_url = $data['preview_url'];
							$couponsModel->start_date = $data['added'];
							$couponsModel->expiry_date = $data['coupon_expiry'];
							
							$couponsModel->featured = $data['featured'];
							$couponsModel->exclusive = $data['exclusive'];
							$couponsModel->ref_id = $data['ref_id'];
							$couponsModel->link = $data['link'];
							$couponsModel->store_link = $data['store_link'];
							$couponsModel->created = date('Y-m-d h:i:s');


							if(isset($data['offer_name'])){
								$resultsetarray=	explode('.',$data['offer_name']);
								$couponsModel->merchant = $resultsetarray[0];
							}
								

									$couponsModel->merchant_logo_url =$data['store_image'];

							$couponsModel->api_name = 'vcommission';
							$couponsModel->save(false);
					
				}
			
			echo 'Saved Coupons'; die; 			
			//Yii::$app->session->setFlash('success','Copouns Saved Success');
			//return $this->redirect(['index']);
				
			}
		 
			
	}
	
	
	public function getOptimiseCouponsData(){
			
			error_reporting(E_ALL ^ E_WARNING); 

			date_default_timezone_set("UTC");
			$t = microtime(true);
			$micro = sprintf("%03d",($t - floor($t)) * 1000);
			$utc = gmdate('Y-m-d H:i:s.', $t).$micro;

			$sig_data= $utc;
			//########################Please Add your API Key & Private Key here to test##################################################
			$api_key='0791e488-ad84-4999-826f-5f5824846b7e';
			$private_key='92e7698f8d784acfb4221dd35b2c3834';
			//############################################################################################################################

			$concateData = $private_key.$sig_data;
			$sig = md5($concateData);

			//############# This is a test url. You have to change the parameters according to your need #######################
			$url="http://api.omgpm.com/network/OMGNetworkApi.svc/v1.2/Affiliate/GetVoucherCodes?". http_build_query(array(
			 'AgencyID' => 95, 
			 'AID'=>965781,
			 'Key' => $api_key,
			 'Sig' => $sig,
			 'SigData' => $sig_data,
			 'Status'=> 'Active'
			));

			//echo "URL: ".$url."<br><br>";

			$headers = array( 
			 "Content-Type: application/json",
			 "Accept: application/json",
			 "Access-Control-Request-Method: GET" 
			 );

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			 
			$result = curl_exec($ch);
			$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			 
			$optimiseCouponsData = json_decode($result, true);
			
			return $optimiseCouponsData;
		
	}
	

	public static function convertDate($dateString){
		
$match = preg_match('/\/Date\((\d+)([-+])(\d+)\)\//', $dateString, $date);

$timestamp = $date[1]/1000;
$operator = $date[2];
$hours = $date[3]*36; // Get the seconds

$datetime = new \DateTime();

$datetime->setTimestamp($timestamp);
$datetime->modify($operator . $hours . ' seconds');
return $datetime->format('Y-m-d H:i:s');
	}
	
	
	
}
