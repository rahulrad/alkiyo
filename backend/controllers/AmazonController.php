<?php


namespace backend\controllers;
use yii;
use yii\console\Controller;
use yii\helpers\Console;
use common\models\Products;
use backend\models\CategoriesData;//
use backend\models\CategoryUrl;
use backend\models\Mobile;
use backend\models\Logs;
use common\components\AmazonECS;
use backend\models\ProductUrls;
use common\models\Categories;
use common\components\HttpService;
use common\components\Aws;


class AmazonController extends Controller {


	 public function actionTest() {
		 
        $aws = \Yii::$app->awssdk->getAwsSdk();
        $sqs = $aws->createSqs();
		$queueUrl = \Yii::$app->awssdk->extra['queueUrl'];

        try {
	
			$productUrls =  ProductUrls::find()->select(['id','url','store_id'])->where(['status' => 0])->one();
			//echo '<pre>'; print_r($productUrls); die;
            $response = $sqs->sendMessage([
                    'QueueUrl' => $queueUrl, 
                    'MessageBody' => yii\helpers\Json::encode($productUrls)
                ]);
				
			$result = $sqs->receiveMessage(array(
				'QueueUrl' => $queueUrl,
			));
			
			
			echo '<pre>'; print_r($result); die;
			
			/**foreach ($result->getPath('Messages//Body') as $messageBody) {
				// Do something with the message
				echo $messageBody;
			} **/
			
			$deleteResult = $sqs->deleteMessageBatch(array(
					// QueueUrl is required
					'QueueUrl' => $queueUrl,
					// Entries is required
					'Entries' => array(
						array(
							// Id is required
							'Id' => 'test',
							// ReceiptHandle is required
							'ReceiptHandle' => 'AQEBgF8TyfLRds6g69XJl+JE3v5sv+I7M4Y99XOBnBVN29NbpXJIGeHduMcN0h8E0hEfTwoHrh0WZvCBavvlPioD8Hg5olF2zFP1qWlsBpFAxcu8emLANToIpF5h/TjytiyExuYx1iILOB1fvZNWeg3g7U1Cd3lYu1dvrvr+DCte2WMb57H9OxHq/eOE8Iu4SQVBnKwSvdAwe1MnMlmkRYsh6gzAl2EmFVabc/WUionSDPUhop663Y96gyw/HkkMjaA2Fyj7beJ63h9e5MpCKTwDK9QTVTYdOhZilBziY9ozfLqU0jFL9qnayKs+YQNnPWfx+Jm4e+H+9Tsu1uBwHPnVWWGnm8vYdt69jA6O5p3rLNgoQuB7TjrpmU7AuCOqOQb+ioGuKPIivLXs6T/nEcr2Yg==',
						),
						// ... repeated
					),
				)); 
            echo '<pre>'; print_r($deleteResult); die;
        } catch (\Exception $e) {
            echo "SQS Not added: ".$e->getMessage();
        }
    }
    public function actionIndex() {
		echo phpinfo();
        echo "Hello world.";
    }
    
    public function actionHello() {
        echo "eeeee";exit;
    }
    
	public function actionSaveCatUrl(){
		$categories_data_model = new CategoriesData;
		$categoriesDatas = CategoriesData::find()->where(['type' => 0,'store_id'=>5])->all();
		 	
			
		 if(!empty($categoriesDatas)){
				foreach($categoriesDatas as $categoriesData){
				
				// $model = CategoriesData::find($categoriesData->id)->one();
				 $model = CategoriesData::find()->where(['id' => $categoriesData->id])->one();
				 $model->type = 1;
			     $model->save();
				 
				$count_products = $model->count_products;
				$perpage = 10;
				$list_page = $count_products / $perpage;
				$pages = round($list_page);
				$x  = 1;
				$y = 1;
				
				$categoryUrlModel = new CategoryUrl;
				$countLastInsertUrls = $categoryUrlModel->getLastInsertCategoryUrl($categoriesData);
				$pages = $pages + $countLastInsertUrls; // count pages aur last insert url for same category url
				$i_value = ($countLastInsertUrls != 0) ? $countLastInsertUrls + 1 : 1;
				for($i = $i_value; $i<=$pages; $i++){
					
					
					$category_urls = 'amazon_api/index.php?category='.$categoriesData->url.'&sub_category='.$categoriesData->category_name.'&page_number='.$i;
					
					  
					 $category_url_model = new CategoryUrl;
					 $category_url_model->status = 0;
					 $category_url_model->store_name = 'amazon';
					  $category_url_model->store_id = 5;
					 $category_url_model->category_name = $categoriesData->category_name;
					 $category_url_model->category_type = $categoriesData->category_type;
					 $category_url_model->site_url = $category_urls;
					
					
					$category_url_model->scraper_group =$y;
							 if($x==10)
							 {
								$y++;
								$x=0;
							 }
					$category_url_model->save();
					// echo 'pass'. $category_url_model->category_name.'<br>';
					$x++;
				}  
			  
			}
			echo 'save category url for snapdeal'; die;
		 }else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('amazon scraper','AmazonController','No Any Category Data Url for amazon','Output',__LINE__);
				echo 'No Any Category Data for Save category Url for amazon'; die;
		}
		 
	}




	
	

  public function actionScraperProductUrls($latest_product=0) {
		
		$max_scraper_group = CategoryUrl::find()->where(['store_id' => 5])->orderBy(['scraper_group' => SORT_DESC])->one();
		
		if(!empty($max_scraper_group)){
				
				for($i = 1; $i <=  $max_scraper_group->scraper_group; $i++){
					
					$categories_url_model = new CategoryUrl;
					$categories_urls= CategoryUrl::find()->where(['status' => 0,'store_id' => 5,'scraper_group' => $i])->all();
					
					if(!empty($categories_urls)){
						 foreach($categories_urls as $categories_url){
								
								$objsnapdealProduct = new \backend\models\AmazonProductScrapper();
								
								//Tools::killOtherProcess("php console.php jungleeproductscrapper new");
								$siteName = 'http://www.amazon.com';
								// for men shoes category
								//$url = $siteName . '/lc/pr/pv1/spotList1/spot1/productList?sid=tyy%2C4io&filterNone=true&start=1&ajax=true&_=1460746573007';
								$url = $categories_url->site_url;
								$categoryName = $categories_url->category_name;
								$categoryType = $categories_url->category_type;
								$scraper_group = $i;
									
								//$objsnapdealProduct->saveProductUrls($categoryName, '', $categoryType, $latest_product,'snapdeal',$scraper_group);
								
								$this->getProductFromCurl($categories_url);
							
						}
						//echo 'save products for flipkart'; die;
					}
				} //die('sadfasd');
		}else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('amazon scraper','AmazonController','No any category url for scrapping','Output',__LINE__);
				echo 'No Any URLs for scurpping'; die;
		}
		
  }
  

  
  
 public function getProductFromCurl($categories_url){
					//echo $categories_url->site_url; die;
					
					$objHttpService = new \common\components\HttpService();
					$proxy = $objHttpService->getProxyIps();
						
					$path = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'http://139.59.19.196';
					$crlUrl = $path.'/'.$categories_url->site_url; //live path
					//echo $categories_url->site_url; die;
					//$crlUrl = 'http://localhost/nayashoppy/frontend/web/'.$categories_url->site_url; // local path
					
					//$crlUrl = 'http://localhost/nayashoppy/frontend/web/amazon_api/index.php?category=Electronics&sub_category=Mobiles&page_number=1'; // local path
					//echo $crlUrl; 
					$arr_product = array();
					
					if(!empty($crlUrl)){
						
						
						$headers = array( 
							 "Content-Type: application/json",
							 "Accept: application/json",
							 "Access-Control-Request-Method: GET" 
						 );
						   $ch = curl_init();
						   curl_setopt($ch, CURLOPT_URL, $crlUrl);
						   curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
						   curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
						   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
						   $result = curl_exec($ch);
						   $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						   
							$productUrls = json_decode($result, true);
							
							if(!empty($productUrls)){
								foreach($productUrls as $product_url){
											 //echo '<pre>'; print_r($product_url); die;
											   $exitProductUrl = ProductUrls::find()->where(['url' => $product_url, 'store_id' => 5])->one();
											   
													if (empty($exitProductUrl)) {
																
																$categoryData = Categories::find()->where(['category_name' => $categories_url->category_name])->one();
																$category_id = !empty($categoryData) ? $categoryData->category_id : 0;
																
																$productUrlsModel = new ProductUrls();
																$productUrlsModel->store_id = 5;
																$productUrlsModel->cateogry_url_id = $categories_url->id_category_url;
																$productUrlsModel->status = 0;
																$productUrlsModel->url = $product_url;
																$productUrlsModel->category_id = $category_id;
																$productUrlsModel->save();
																
																$exitProductUrl = ProductUrls::find()->where(['url' => $product_url, 'store_id' => 5])->one();
																
																$aws = new \common\components\Aws();
																$aws = $aws->sendMessage($exitProductUrl,'product_scraper');
																
													}
								}
								
								
								$CategoryUrlModel = CategoryUrl::findOne($categories_url->id_category_url);
								$CategoryUrlModel->status = 1;
								$CategoryUrlModel->save(false);
								echo 'save category url id=>'.$CategoryUrlModel->id_category_url.'<br>';
										
							}
						   
					}
						
	 
 }
 
 
  
		
public function actionScraper($latest_product=0) {
		
		$product_urls = ProductUrls::find()->where(['store_id' => 5,'status' => 0])->all();
		
			if(!empty($product_urls)){
					
					foreach($product_urls as $product_url){
						
						/**$categories_url_model = new CategoryUrl;
						$categories_urls= CategoryUrl::find()->where(['id_category_url' => $product_url->cateogry_url_id])->one(); **/
						
						$categories_urls = Categories::find()->where(['category_id' => $product_url->category_id])->one();
						$category_name = !empty($categories_urls) ? $categories_urls->category_name : '';
					
						$objflipkartProduct = new \backend\models\AmazonProductScrapper();
						$objflipkartProduct->saveProductData($product_url,$category_name);
						
					}
					
			}else{
					$scraperLog = new Logs();
					$scraperLog->saveScraperLog('amazon scraper','AmazonController','No any category url for scrapping','Output',__LINE__);
					echo 'No Any URLs for scurpping'; die;
			}
		
	
	}

 
 /** public function actionScraper($latest_product=0) {
			
		$categories_url_model = new CategoryUrl;
		$categories_urls= CategoryUrl::find()->where(['status' => 0,'store_id' => 5,'scraper_group' => 1])->all();
		//echo '<pre>'; print_r($categories_urls); die;
		 if(!empty($categories_urls)){
			 foreach($categories_urls as $categories_url){
					
					$objamazonProduct = new \backend\models\AmazonProductScrapper();
					
					// $tools_componant = new \common\components\Tools();
					// $tools_componant->alertMail("Create a new brand named", "Create Brand", 'vinayverma158@gmail.com'); die;
					//Tools::killOtherProcess("php console.php jungleeproductscrapper new");
					$siteName = 'http://www.amazon.com';
					// for men shoes category
					//$url = $siteName . '/lc/pr/pv1/spotList1/spot1/productList?sid=tyy%2C4io&filterNone=true&start=1&ajax=true&_=1460746573007';
					$url = $categories_url->site_url;
					$categoryName = $categories_url->category_name;
					$categoryType = $categories_url->category_type;
					//echo $categoryName.'<br>';
						$objamazonProduct->saveProduct($categoryName, '', $categoryType, $latest_product,'amazon');
			
				
			}
			echo 'save products for snapdeal'; die;
		}else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('amazon scraper','AmazonController','No any category url for scrapping','Output',__LINE__);
				echo 'No Any URLs for scurpping'; die;
		}
	
	} **/
	
	/*public function actionScraper($latest_product=0) {
		
		$objSnapDealProduct = new \backend\models\SnapDealProductScrapper();
        //Tools::killOtherProcess("php console.php jungleeproductscrapper new");
        $siteName = 'http://www.snapdeal.com';
        // for men shoes category
        $url = $siteName . '/acors/json/product/get/search/57/0/48?q=&sort=plrty&brandPageUrl=&keyword=&vc=&webpageName=categoryPage&campaignId=&brandName=&isMC=false&clickSrc=';
        $categoryName = 'computer';
        $categoryType = 'computer';
		
//        $arr_url = $objflipkartProduct->getAllPagingUrl($url);
//        $objflipkartProduct->saveAllPagingUrl($arr_url, $categoryName, $categoryType);

//        $arr_department_type = array('For' => 'Men');
        $objSnapDealProduct->saveProduct($categoryName, '', $categoryType, $latest_product,'snapdeal');
		echo '<pre>'; print_r($objSnapDealProduct); die;
    }*/

}
