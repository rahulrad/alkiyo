<?php

namespace backend\controllers;

use Yii;
use common\models\ProductReviews;
use backend\models\ProductReviewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Logs;
use common\models\Products;

/**
 * ProductReviewsController implements the CRUD actions for ProductReviews model.
 */
class ProductReviewsController extends Controller
{
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductReviews models.
     * @return mixed
     */
    public function actionIndex()
    {
		
		/**
        $searchModel = new ProductReviewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]); **/
		
		$model = new ProductReviews();
		 $getData = Yii::$app->request->get();
		 
			if(!empty($getData) && isset($getData['product_id'])){
				
				$product_id = $getData['product_id'];
				
				$productDetail = Products::find()->select(['product_id','product_name'])->where(['product_id' => $product_id])->one();
				$searchModel = new ProductReviewsSearch();
				$dataProvider = $searchModel->search(Yii::$app->request->queryParams,$product_id);
				
				return $this->render('index', [
					'searchModel' => $searchModel,
					'dataProvider' => $dataProvider,
					'productDetail' => $productDetail,
				]); //die;
			
			}else{
				
			}
			
    }

    /**
     * Displays a single ProductReviews model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductReviews model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductReviews();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->created = date('Y-m-d h:i:s');
            $model->save();

            // save user logs data in user_logs table
            $logs_model = new Logs();
            $user_logs = array('model'=>'ProductReviews','action'=>'Create','activity' => $model->email.' is added','action_id'=>$model->id);
            $logs_model->saveUserLogs($user_logs);

            Yii::$app->session->setFlash('success','Product Review has been saved');
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ProductReviews model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->created = date('Y-m-d h:i:s');
            $model->save();
           
            // save user logs data in user_logs table
            $logs_model = new Logs();
            
            //echo $id; die;
            $user_logs = array('model'=>'ProductReviews','action'=>'Update','activity' => $model->title.' is updated','action_id'=>$id);
            $logs_model->saveUserLogs($user_logs);

            Yii::$app->session->setFlash('success','Product Review has been saved');
            return $this->redirect(['index?product_id='.$model->product_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProductReviews model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		
		$model = ProductReviews::findOne($id);
		
        $this->findModel($id)->delete();
		
		
		
        return $this->redirect(['index?product_id='.$model->product_id]);
    }

    /**
     * Finds the ProductReviews model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductReviews the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductReviews::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
}
