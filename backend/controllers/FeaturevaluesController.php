<?php
namespace backend\controllers;

use Yii;
use common\models\FeatureValues;
use backend\models\FeatureValuesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Logs;
use common\models\Features;
use yii\web\UploadedFile;

/**
 * FeatureValuesController implements the CRUD actions for FeatureValues model.
 */
class FeaturevaluesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete','importdata','export','get_feature_values'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FeatureValues models.
     * @return mixed
     */
    public function actionIndex()
    {
		/**ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
		echo 'hello'; die; **/
        $searchModel = new FeatureValuesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		//echo '<pre>'; print_R($dataProvider); die;
		$model = new FeatureValuesSearch();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model'=>$model,
        ]);
    }

    /**
     * Displays a single FeatureValues model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FeatureValues model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FeatureValues();

        if ($model->load(Yii::$app->request->post())   && $model->validate()) {
			
			$model->created = date('Y-m-d h:i:s');
			$model->modified = date('Y-m-d h:i:s');
			$model->save();
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'FeatureValues','action'=>'Create','activity' => $model->feature_id.' is added','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
                        
                        Yii::$app->session->setFlash('success', 'FeatureValues is successfully saved');
			
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FeatureValues model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())   && $model->validate()) {
		
			$model->modified = date('Y-m-d h:i:s');
			$model->save();
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'FeatureValues','action'=>'Update','activity' => $model->feature_id.' is updated','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
                        
                        Yii::$app->session->setFlash('success', 'FeatureValues is successfully update');
			
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FeatureValues model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		die('not access');
		$model = $this->findModel($id);
        $this->findModel($id)->delete();
		
		// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'FeatureValues','action'=>'Delete','activity' => $model->feature_id.' is deleted','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
                        
                        Yii::$app->session->setFlash('success', 'FeatureValues is successfully delete');

        return $this->redirect(['index']);
    }

    /**
     * Finds the FeatureValues model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FeatureValues the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FeatureValues::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
	
		
	
	
    /**
    * Import Category Data
    */

    public function actionImportdata()
    {   
        $model = new FeatureValues();

        if($model->load(Yii::$app->request->post())){
       
	   
        $file = UploadedFile::getInstance($model,'features_csv_file');
        
        $filename = 'Data-'.Date('YmdGis').'.'.$file->extension;
        
        $upload = $file->saveAs('uploads/feature_values_import/'.$filename);
        
        define('CSV_PATH','uploads/feature_values_import/'); 
        $csv_file = CSV_PATH . $filename;
            $row = 1;
            if (($handle = fopen($csv_file, "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    
                    
                    if($row != 1){
                        if(isset($data[0]) && !empty($data[0])){
							
							$featureValuesModel = FeatureValues::find()->where(['id' => $data[0]])->one();
							//$featureValuesModel->value = !empty($data[2]) ? $data[2] : $featureValuesModel->value;
							$featureValuesModel->display_name = !empty($data[3]) ? $data[3] : $featureValuesModel->value;
                            $featureValuesModel->save(false);
						}
							
                     
                    }
                    $row++;
                    
                    }
                fclose($handle);
            }
            //  unlink('uploads/category_import/'.$filename);
            Yii::$app->session->setFlash('success','Import Success');
            return $this->redirect(['index']);
        }else{
            return $this->render('importdata',['model'=>$model]);
        }

    }


    /**
    * Export Category Data
    */

    public function actionExport(){
           
           $FeatureValuesResults = $this->get_feature_values($_SERVER['HTTP_REFERER']);
           
            $filename = 'Data-'.Date('YmdGis').'-FeatureValues.csv';
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=".$filename);
            header("Pragma: no-cache");
            header("Expires: 0");


             $file = fopen('php://output', 'w');                              
             fputcsv($file, array('id', 'feature_id', 'value','display_name'));
			
				$FeatureValuesModel = new FeatureValues();
                foreach($FeatureValuesResults as $featurevalues){
					
					$FeaturesModel = new Features();
					$feature = $FeaturesModel::find()->select(['name'])->where(['id' => $featurevalues->feature_id])->one();
					$feature_name = !empty($feature) ? $feature->name : '';
					
					
                    $record = array($featurevalues->id,$feature_name,$featurevalues->value,$featurevalues->display_name);
                    
					
					fputcsv($file, $record);
                        
                }

        }
		
		
	public function get_feature_values($getData){
				$FeatureValuesResults = '';
				
				$dataSearch = array();
				if(!empty($getData)){
					 
					 $url = parse_url($getData);
					 
					 if(isset($url['query'])){
						
						 parse_str($url['query'], $getQueryString);
						 if(!empty($getQueryString) && isset($getQueryString['FeatureValuesSearch'])){
							$feature_id = $getQueryString['FeatureValuesSearch']['feature_id'];
							$feature_value_id = $getQueryString['FeatureValuesSearch']['id'];
							
								if(!empty($feature_id)){
									$dataSearch['feature_id'] = $feature_id;
								}
								if(!empty($feature_value_id)){
									$dataSearch['id'] = $feature_value_id;
								}	
												
						}
						
					 }
						
					
					
				}
				
				$FeaturesResults = FeatureValues::find()->where($dataSearch)->all();
				
				return $FeaturesResults;
	}


public function actionAjaxsearch(){
			
			$model = new Products();
			 
			if (Yii::$app->request->isAjax) {
			
					$postData = Yii::$app->request->post();
			}
}
	
	
	
}
