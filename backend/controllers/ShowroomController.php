<?php

namespace backend\controllers;

use Yii;
use backend\models\Showroom;
use backend\models\ShowroomSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use backend\models\Logs;
/**
 * BrandsController implements the CRUD actions for Brands model.
 */
class ShowroomController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete','folder_slug'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Brands models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShowroomSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Brands model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Brands model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Showroom();
        if ($model->load(Yii::$app->request->post())   && $model->validate()) {
	
			$post_data = Yii::$app->request->post();
			$model->brand_id = $post_data['Showroom']['brand_id'];
			$model->name = $post_data['Showroom']['name'];
			$model->address = $post_data['Showroom']['address'];
			$model->city = $post_data['Showroom']['city'];
			$model->pincode = $post_data['Showroom']['pincode'];
			$model->phone_no = $post_data['Showroom']['phone_no'];
			$model->email = $post_data['Showroom']['email'];
			$model->latitude = $post_data['Showroom']['latitude'];
			$model->longitude = $post_data['Showroom']['longitude'];
			$model->status = $post_data['Showroom']['status'];
			$logsModel = new Logs();
			$model->created_at = date('Y-m-d h:i:s');			
			$model->save();
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Showroom','action'=>'Create','activity' => $model->brand_id.' is added','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
            Yii::$app->session->setFlash('success', 'Showroom is successfully saved');
                        
			return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Brands model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())  && $model->validate()) {
				$post_data = Yii::$app->request->post();
				$model->brand_id = $post_data['Showroom']['brand_id'];
				$model->name = $post_data['Showroom']['name'];
				$model->address = $post_data['Showroom']['address'];
				$model->city = $post_data['Showroom']['city'];
				$model->pincode = $post_data['Showroom']['pincode'];
				$model->phone_no = $post_data['Showroom']['phone_no'];
				$model->email = $post_data['Showroom']['email'];
				$model->status = $post_data['Showroom']['status'];
				$model->latitude = $post_data['Showroom']['latitude'];
				$model->longitude = $post_data['Showroom']['longitude'];
				
				$model->updated_at = date('Y-m-d h:i:s');
				$model->save();
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'BrandsHistory','action'=>'Update','activity' => $model->brand_id.' is updated','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
			Yii::$app->session->setFlash('success', 'Brand History is successfully saved');
                         
		   return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Brands model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		$model = $this->findModel($id);
			$modelDelete = $model;
			$modelDelete->is_delete = 1;
			$modelDelete->save(false);
		
		// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Showroom','action'=>'Delete','activity' => $model->id.' is deleted','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
                    Yii::$app->session->setFlash('success', 'Showroom is successfully delete');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Brands model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Brands the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Showroom::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionFolder_slug(){
			$brandsList = Brands::find()->all();
			foreach($brandsList as $brand){
				$brandModel = $brand;
				$logsModel = new Logs();
				$brandModel->folder_slug = $logsModel->getSlugFromName($brand->brand_name);
				$brandModel->save(false);
			}
	}
	
}
