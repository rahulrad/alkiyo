<?php

namespace backend\controllers;

use Yii;
use backend\models\Users;

use backend\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\Admin;
use backend\models\Logs;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete','change-password'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Admin();
			
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
			$user_data = Yii::$app->request->post();
			$model->first_name = $user_data['Admin']['first_name'];
			$model->last_name = $user_data['Admin']['last_name'];
			$model->username = $user_data['Admin']['username'];
			$model->email = $user_data['Admin']['email'];
			$model->created_at = date('Y-m-d h:i:s');
			$model->updated_at = date('Y-m-d h:i:s');
			
			$model->setPassword($user_data['Admin']['password_hash']);
			$model->generateAuthKey();
			
			
			$model->save();
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'AdminUsers','action'=>'Create','activity' => $model->username.' is added','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
                        
                        Yii::$app->session->setFlash('success', 'Admin User is successfully saved');
			
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
   /** public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
			//$model->created_at = date('Y-m-d h:i:s');
			
			$model->save();
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'AdminUsers','action'=>'Update','activity' => $model->username.' is updated','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
                        
                        Yii::$app->session->setFlash('success', 'Admin User is successfully update');
			
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    } **/
	
	public function actionUpdate($id)
    {
        $model = Admin::findOne($id);
		
		$postData = Yii::$app->request->post();
			
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
			//echo '<pre>'; print_r($postData); die;
			$model->first_name = $postData['Admin']['first_name'];
			$model->last_name = $postData['Admin']['last_name'];
			$model->username = $postData['Admin']['username'];
			$model->status = $postData['Admin']['status'];
			$model->setPassword($postData['Admin']['password_hash']);
			$model->save();
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'AdminUsers','action'=>'Update','activity' => $model->username.' is updated','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
                        
                        Yii::$app->session->setFlash('success', 'Admin User is successfully update');
			
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    } 

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		
			$model = $this->findModel($id);
			$modelDelete = $model;
			$modelDelete->is_delete = 1;
			$modelDelete->save(false);		
		
		// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'AdminUsers','action'=>'Delete','activity' => $model->username.' is deleted','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
                        
            Yii::$app->session->setFlash('success', 'Admin User is successfully delete');

        return $this->redirect(['index']);
    }

	public function actionChangePassword(){
		$userDetail = Yii::$app->user->identity;
		
		$model = new Admin();
		
		 if ($model->load(Yii::$app->request->post()) && $model->validate()) {
		
			$AdminModel = $this->findModel($userDetail->id);
			$postData = Yii::$app->request->post();
			$AdminModel->password_hash = Yii::$app->security->generatePasswordHash($postData['Admin']['password_hash']);
			//$model->setPassword($postData['Admin']['password_hash']);
			$AdminModel->save(false);
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'AdminUsers','action'=>'ChangePassword','activity' => $userDetail->username.' is changed password','action_id'=>$userDetail->id);
			$logs_model->saveUserLogs($user_logs);
                        
            Yii::$app->session->setFlash('success', 'Admin User  password is successfully changed');
			
            return $this->redirect(['/']);
			 
		 }
		
		return $this->render('change_password', [
                'model' => $model,
            ]);
	}

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
