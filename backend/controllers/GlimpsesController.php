<?php

namespace backend\controllers;

use Yii;
use common\models\Glimpses;
use backend\models\GlimpsesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use backend\models\Logs;
/**
 * BrandsController implements the CRUD actions for Brands model.
 */
class GlimpsesController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete','folder_slug'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Brands models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GlimpsesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Brands model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Brands model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Glimpses();
        if ($model->load(Yii::$app->request->post())   && $model->validate()) {
			
			$post_data = Yii::$app->request->post();
			$model->brand_id = $post_data['Glimpses']['brand_id'];
			$model->name = $post_data['Glimpses']['name'];
			$model->description = $post_data['Glimpses']['description'];
			$model->active = $post_data['Glimpses']['active'];
			
			
			
			$model->file = UploadedFile::getInstance($model,'file');
			if(!empty($model->file)){
                $path = Yii::getAlias('@frontend') .'/web/';
                $imageName = time().uniqid();
                $model->file->saveAs($path.'uploads/glimpses/'.$imageName.'.'.$model->file->extension);
				$model->image = 'uploads/glimpses/'.$imageName.'.'.$model->file->extension;
			}
			
			
			$logsModel = new Logs();
			$model->created_at = date('Y-m-d h:i:s');			
			$model->save();
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Glimpses','action'=>'Create','activity' => $model->brand_id.' is added','action_id'=>$model->glimpese_id);
			$logs_model->saveUserLogs($user_logs);
            Yii::$app->session->setFlash('success', 'Glimpses is successfully saved');
                        
			return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Brands model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
			
        if ($model->load(Yii::$app->request->post())  && $model->validate()) {
         	
				$post_data = Yii::$app->request->post();
				$model->brand_id = $post_data['Glimpses']['brand_id'];
				$model->name = $post_data['Glimpses']['name'];
				$model->description = $post_data['Glimpses']['description'];
				$model->file = UploadedFile::getInstance($model,'file');
			
			if(!empty($model->file)){
				//$imageName = $model->file->baseName;
                $path = Yii::getAlias('@frontend') .'/web/';
                $imageName = time().uniqid();
				
				//$model->file->saveAs('uploads/brands/'.$imageName.'.'.$model->file->extension);
                $model->file->saveAs($path.'uploads/glimpses/'.$imageName.'.'.$model->file->extension);
				
				
				$model->image = 'uploads/glimpses/'.$imageName.'.'.$model->file->extension;
			}
				$model->active = $post_data['Glimpses']['active'];
				$model->updated_at = date('Y-m-d h:i:s');
				$model->save();
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Glimpses','action'=>'Update','activity' => $model->brand_id.' is updated','action_id'=>$model->glimpese_id);
			$logs_model->saveUserLogs($user_logs);
			
                         Yii::$app->session->setFlash('success', 'Glimpses is successfully saved');
                         
		   return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Brands model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		$model = $this->findModel($id);
			$modelDelete = $model;
			$modelDelete->is_delete = 1;
			$modelDelete->save(false);
		
		// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Glimpses','action'=>'Delete','activity' => $model->glimpese_id.' is deleted','action_id'=>$model->glimpese_id);
			$logs_model->saveUserLogs($user_logs);
                    Yii::$app->session->setFlash('success', 'Glimpses is successfully delete');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Brands model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Brands the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Glimpses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionFolder_slug(){
			$brandsList = Glimpses::find()->all();
			foreach($brandsList as $brand){
				$brandModel = $brand;
				$logsModel = new Logs();
				$brandModel->folder_slug = $logsModel->getSlugFromName($brand->brand_name);
				$brandModel->save(false);
			}
	}
	
}
