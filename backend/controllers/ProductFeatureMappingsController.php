<?php

namespace backend\controllers;

use common\models\Categories;
use common\models\Features;
use common\models\ProductFeatureMappingsSearch;
use Yii;
use common\models\ProductFeatureMappings;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductFeatureMappingsController implements the CRUD actions for ProductFeatureMappings model.
 */
class ProductFeatureMappingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'import-data', 'export', 'download-sample-csv'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductFeatureMappings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductFeatureMappingsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Creates a new ProductFeatureMappings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductFeatureMappings();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ProductFeatureMappings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $existingModel = ProductFeatureMappings::find()->where([
                'id_feature' => $model->id_feature,
                'id_category' => $model->id_category,
                'store_feature_name' => $model->store_feature_name
            ])->one();
            if (is_null($existingModel)) {
                Yii::$app->session->setFlash('success', 'Mappings saved Successfully!');
                $model->save();
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProductFeatureMappings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the ProductFeatureMappings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductFeatureMappings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductFeatureMappings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDownloadSampleCsv()
    {
        $filename = 'Data-' . Date('YmdGis') . '-ProductFeatureMapping.csv';
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=" . $filename);
        header("Pragma: no-cache");
        header("Expires: 0");
        $file = fopen('php://output', 'w');
        fputcsv($file, array('category', 'ns_feature_name', 'store_feature_name'));
    }

    public function actionImportData()
    {

        $model = new ProductFeatureMappings();
        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'uploadFile');
            if (!is_null($file)) {
                $filename = 'Data-' . Date('YmdGis') . '.' . $file->extension;
                define('CSV_PATH', 'uploads/category_import/');
                $upload = $file->saveAs(CSV_PATH . $filename);
                $csv_file = CSV_PATH . $filename;
                $row = 1;
                if (($handle = fopen($csv_file, "r")) !== FALSE) {

                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                        if ($row != 1 && (isset($data[0]) && !empty($data[0])) && (isset($data[1]) && !empty($data[1])) && (isset($data[2]) && !empty($data[2]))) {

                            $categoryModel = Categories::find()->where([
                                'category_name' => trim(strtolower($data[0]))
                            ])->one();

                            if (!is_null($categoryModel)) {

                                $featureModel = Features::find()->where([
                                    'name' => trim(strtolower($data[1])),
                                    'category_id' => $categoryModel->category_id
                                ])->andWhere('id > :id', [
                                    ':id' => Features::FEATURE_LAST_ID
                                ])->one();

                                if (!is_null($featureModel)) {

                                    $featureGroupModel = $featureModel->featureGroup;

                                    error_log(var_export($featureGroupModel->attributes,true));
                                    error_log(var_export($categoryModel->attributes,true));

                                    if (!is_null($featureGroupModel) && ($featureGroupModel->category_id == $categoryModel->category_id)) {

                                        $model = ProductFeatureMappings::find()->where([
                                            'id_feature' => $featureModel->id,
                                            'id_category' => $categoryModel->category_id,
                                            'store_feature_name' => trim($data[2]),
                                        ])->one();
                                        if (is_null($model)) {
                                            $model = new ProductFeatureMappings();
                                            $model->id_feature = $featureModel->id;
                                            $model->id_category = $categoryModel->category_id;
                                            $model->store_feature_name = trim($data[2]);
                                            $model->save();
                                        }
                                    }
                                }
                            }
                        }
                        $row++;
                    }
                    fclose($handle);
                }
                unlink(CSV_PATH . $filename);
                Yii::$app->session->setFlash('success', 'Mappings successfully imprted!');
                return $this->redirect(['index']);
            } else {
                return $this->render('importdata', ['model' => $model]);
            }
        } else {
            return $this->render('importdata', ['model' => $model]);
        }
    }

    public function actionExport()
    {
        $filename = 'Data-' . Date('YmdGis') . '-ProductFeatureMapping.csv';
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=" . $filename);
        header("Pragma: no-cache");
        header("Expires: 0");

        $file = fopen('php://output', 'w');
        fputcsv($file, array('category', 'ns_feature_name', 'store_feature_name'));

        $productUrlsResults = $this->getFeaturesMapping($_SERVER['HTTP_REFERER']);

        foreach ($productUrlsResults as $model) {
            $categoryModel = $model->category;
            $supplierModel = $model->feature;
            $record = array($categoryModel->category_name, $supplierModel->display_name, $model->store_feature_name);
            fputcsv($file, $record);
        }
    }

    public function getFeaturesMapping($getData)
    {
        $dataSearch = array();
        $query = ProductFeatureMappings::find();
        if (!empty($getData)) {
            $url = parse_url($getData);
            if (isset($url['query'])) {
                parse_str($url['query'], $getQueryString);
                if (!empty($getQueryString) && isset($getQueryString['ProductFeatureMappingsSearch'])) {
                    $category_id = $getQueryString['ProductFeatureMappingsSearch']['id_category'];
                    $featureId = $getQueryString['ProductFeatureMappingsSearch']['featureName'];
                    if (!empty($category_id)) {
                        $query->andFilterWhere([
                            'id_category' => $category_id,
                        ]);
                    }
                    if (!empty($featureId)) {
                        $query->joinWith('feature');
                        $query->andFilterWhere(['like', 'features.name', $featureId]);
                    }
                }
            }
        }
        $FeaturesResults = $query->All();
        return $FeaturesResults;
    }
}
