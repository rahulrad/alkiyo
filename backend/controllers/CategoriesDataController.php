<?php

namespace backend\controllers;

use Yii;
use backend\models\CategoriesData;
use backend\models\CategoriesDataSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Suppliers;
use yii\web\UploadedFile;

/**
 * CategoriesDataController implements the CRUD actions for CategoriesData model.
 */
class CategoriesDataController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete','sort_order_update','importdata','export','download_blank_csv_format'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CategoriesData models.
     * @return mixed
     */
    public function actionIndex()
    {
		
		//CategoriesData::deleteAll();
        $searchModel = new CategoriesDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$model = new CategoriesDataSearch();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model' => $model,
        ]);
    }

    /**
     * Displays a single CategoriesData model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CategoriesData model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CategoriesData();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$post_data = Yii::$app->request->post();
			$model->category_type = $post_data['CategoriesData']['category_name'];
			$model->save();
             Yii::$app->session->setFlash('success', 'Category Data is successfully saved');
             
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CategoriesData model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
				
				$post_data = Yii::$app->request->post();
				$model->category_type = $post_data['CategoriesData']['category_name'];
				
				$model->save();
				
               Yii::$app->session->setFlash('success', 'Category Data is successfully update');
               
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CategoriesData model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
           
        Yii::$app->session->setFlash('success', 'Category Data is successfully delete');
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the CategoriesData model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CategoriesData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CategoriesData::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
	
	
    /**
    * Import Category Data
    */

    public function actionImportdata()
    {   
        $model = new CategoriesData();

        if($model->load(Yii::$app->request->post())){
       
        $file = UploadedFile::getInstance($model,'category_csv_file');
        
		$filename = 'Data-'.Date('YmdGis').'.'.$file->extension;
        
        $upload = $file->saveAs('uploads/category_data_import/'.$filename);
        
        define('CSV_PATH','uploads/category_data_import/'); 
        $csv_file = CSV_PATH . $filename;
            $row = 1;
            if (($handle = fopen($csv_file, "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    
                    
                    if($row != 1){
                        $categoriesDataModel = new CategoriesData();
                       // echo '<pre>'; print_r($data); die;
						$store_name = isset($data[3]) ? $data[3] : '';
                        $suppilerModel = new Suppliers();
                        $suppilerData = $suppilerModel::find()->select(['id'])->where(['name' => $store_name])->one();
						if(!empty($suppilerData))
						{
							//$checkResult = $categoriesDataModel::find()->where(['category_name' => $data[1],'url' => $data[2],'store_id' => $suppilerData->id])->one();
						
							if(!empty($suppilerData)){
								$store_id = $suppilerData->id;
							}else{
								$store_id = NULL;
							}

							$countRecords = $categoriesDataModel::find()->orderBy('id desc')->one();
							if(!empty($countRecords)){
								$c_id = $countRecords->id + 1;
							}else{
								$c_id = 1;
							}
						   // echo $c_id; die;
							//if(empty($checkResult)){
								$categoriesDataModel->id = $c_id;
								$categoriesDataModel->category_name = isset($data[1])?$data[1]:'';
								$categoriesDataModel->category_type = isset($data[1])?$data[1]:'';
								$categoriesDataModel->url = isset($data[2])?$data[2]:'';
								$categoriesDataModel->store_name = isset($data[3])?$data[3]:'';
								$categoriesDataModel->store_id = $store_id;
								$categoriesDataModel->path = isset($data[4])?$data[4]:'';
								$categoriesDataModel->count_products = isset($data[5])?$data[5]:'';
								$categoriesDataModel->type = 0;
								$categoriesDataModel->status = 'active';
								$categoriesDataModel->save(false);
							//}
						}
						
                        
                    }
                    $row++;
                    
                    }
                fclose($handle);
            }
            //  unlink('uploads/category_import/'.$filename);
            Yii::$app->session->setFlash('success','Import Success');
            return $this->redirect(['index']);
        }else{
            return $this->render('importdata',['model'=>$model]);
        }

    }


    /**
    * Export Category Data
    */

    public function actionExport(){
            
            $CategoriesDataResults = CategoriesData::find()->All();
            
            
           $filename = 'Data-'.Date('YmdGis').'-CategoriesData.csv';
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=".$filename);
            header("Pragma: no-cache");
            header("Expires: 0");


             $file = fopen('php://output', 'w');                              
             fputcsv($file, array('S.No', 'Category Name', 'Url','Store Name','Path','Count Products')); 
			
				 $suppliersModel = new Suppliers();
                foreach($CategoriesDataResults as $categoriesData){
					
					$store_name_data = $suppliersModel::find()->select(['name'])->where(['id' => $categoriesData->store_id])->one();
					
					if(!empty($store_name_data)){
						$store_name = $store_name_data->name;
					}else{
						$store_name = '';
					}
					
                    $record = array($categoriesData->id,$categoriesData->category_name,$categoriesData->url,$store_name,$categoriesData->path,$categoriesData->count_products);
                    
					fputcsv($file, $record);
                        
                }

        }


    /**
    * Export Blank Category Data csv file
    */

    public function actionDownload_blank_csv_format(){
            
            
            $filename = 'Data-'.Date('YmdGis').'-CategoriesDataBlank.csv';
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=".$filename);
            header("Pragma: no-cache");
            header("Expires: 0");

             $file = fopen('php://output', 'w');                              
             fputcsv($file, array('S.No', 'Category Name', 'Url','Store Name','Path','Count Products')); 

        }
	
	
	
}
