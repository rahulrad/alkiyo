<?php

namespace backend\controllers;
use yii;
use yii\console\Controller;
use yii\helpers\Console;
use common\models\Products;
use common\models\User;
use backend\models\CategoriesData;
use backend\models\CategoryUrl;
use common\models\ProductsSuppliers;
use common\models\PriceUpdate;
use backend\models\Logs;
use common\models\EmailTemplates;


class PriceScrapperController extends Controller {

    public function actionIndex() {
        echo "Hello world.";
    }
    
   
	
	public function actionScraper(){
			
			$today = date('Y-m-d');
			$ProSuppliersModel = new ProductsSuppliers();
			$products = $ProSuppliersModel::find()->where(['<', 'created', $today])->select(['id','product_id','url','store_id','price','original_price'])->all();
			//echo '<pre>'; print_r($products); die;
			 $objflipkartProduct = new \backend\models\PriceProductScrapper();
			 
			 if(!empty($products)){
				
				foreach($products as $product){
				
					$result = $objflipkartProduct->update_product_prices($product);
				
				} die('Updated product new price');
				
			 }else{
				 $scraperLog = new Logs();
				 $scraperLog->saveScraperLog('price scraper','PriceScrapperController','No Any Product for Price Scraper','Output',__LINE__);
				 die('No Any Product for Price Scraper');
			 }
			 
			
	}
	
	
	
	public function actionPriceAlert(){
			$priceAlertModel = new PriceUpdate();
			$productModel = new Products();
			$userModel = new User();
			$emailTemplatesModel = new EmailTemplates();
		
			// find price_alerts list where mail is not sent before
			$priceAlertLists = $priceAlertModel->find()->where(['send_mail' => 0])->all();
			
			if(!empty($priceAlertLists)){
				foreach($priceAlertLists as $price_alert){
						// find product detail by product id
						$productDetail = $productModel->find()
															->select(['product_id','lowest_price'])
															->where(['product_id' => $price_alert->product_id])
															->one();
						
						
						if($productDetail->lowest_price <= $price_alert->alert_price){
							
							// find user detail by user_id
							$userDetail = $userModel->find()
													->select(['id','username','email'])
													->where(['id' => $price_alert->user_id])
													->one();
													
							
							// getting email format for price_alert email
							$emailTemplatesDetail = $emailTemplatesModel->getEmailTemplateBySlug('price-alert');
																			
							
							// we are getting email actual content
							$emailData = array('USERNAME' => $userDetail->username,'EMAIL' =>$userDetail->email );
							$message = $emailTemplatesModel->getUserEmailMessage($emailTemplatesDetail->content, $emailData);
							
							$Email =Yii::$app->mailer->compose()
												->setFrom('vinayverma158@gmail.com')
												->setTo($userDetail->email)
												->setSubject($emailTemplatesDetail->subject)
												//->setTextBody('Plain text content')
												->setHtmlBody($message)
												->send();
							
							// update send_email field in price_update table after sending user mail for price alert
							$priceAlertModel = $price_alert;
							$priceAlertModel->send_mail = 1;
							$priceAlertModel->save(false);
							
						}
						
				}
				echo 'Price alert is successfully done'; die;
			}else{
				echo 'No any data for price alert'; die;
			}
			
	}
	

}
