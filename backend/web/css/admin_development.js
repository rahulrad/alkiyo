$(window).load(function(){
	var base_url = $('#base_url').val(); // this is base url value main.php view
	var feature_id =  $('#groupItemFeatureId').val();
	var group_item_id = $('#groupItemId').val();
	//alert(group_item_id); return false;
	 $.ajax({
			
			url: base_url+'/filter-group-items/feature_values_by_feature_id',
				   
				   type: 'post',
				   
				   dataType : 'html',
				   
				   data: {
							 feature_id: feature_id,
							 group_item_id : group_item_id,
						 },
				   success: function (data) {
					 
					 $('#featureValuesByFeatureId').html(data);
					   
					  console.log(data.search);
				   }
			  });
	
	
	
});

$(function() {
		setTimeout(function(){
			$("#w1-success-0").fadeOut('slow');
		},2000)
	});
	
   $(function() {
    $( "#start_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
	$( "#end_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
	$( "#verified_on" ).datepicker({ dateFormat: 'yy-mm-dd' });

$( "#products-date_launch" ).datepicker({ dateFormat: 'yy-mm-dd' });


if(!$('#products-upcoming').is(":checked")){
$( "#products-date_launch" ).parent().hide();
}


 $('#products-upcoming').change(function() {
        if($(this).is(":checked")) {
$(this).val(1);
$( "#products-date_launch" ).parent().show();
}else{
    $(this).val(0);
$( "#products-date_launch" ).parent().hide();}
});



  });
  
  
 (function($){
        $(window).load(function(){
            $(".left-scrollblock").mCustomScrollbar();
        });
    })(jQuery);
	
	
	
	
$(document).ready(function(){
						   
	var base_url = $('#base_url').val(); // this is base url value main.php view
	var _csrf = $('#_csrf').val(); // this is _csrf value difined in main.php view
	
	
	// function for get simmiler products from mongodb to flipkart
	
	$('.list_product_for_mapping').click(function(){  // button on click function for similler product search for product mapping
			$('.list_product_for_mapping').removeClass('clickMongoProduct');
			$(this).addClass('clickMongoProduct');
			$('#loader').show();							  
			var productData = $(this).data('product'); // product id get to onclick
			
			//alert(product_name); return false;
			$.ajax({
				   
				   
				   url: base_url+'/products/searching_similler_products',
				   
				   type: 'post',
				   
				   dataType : 'html',
				   
				   data: {
							 productData: productData,
							 _csrf : _csrf
						 },
				   success: function (data) {
					  
					  $('#loader').hide();
					 
					 $('#similerProductData').html(data);
					   
					  console.log(data.search);
				   }
			  });
	});
	
	
	// function for redirect action 
	
	$('.product_mapp_search_form_button').click(function(){
			
		 var productMappType = $('.productMappType').val();
		
			if(productMappType == 'custom'){
				$('#productMappSearchForm').attr('action', base_url+'/products/products_mapping_custom');
			}else if(productMappType == 'user_friendly'){
				$('#productMappSearchForm').attr('action', base_url+'/products/products_mapping_user_friendly');
			}else{
				$('#productMappSearchForm').attr('action', base_url+'/products/products_mapping');
			}
			
	});
	
	
	// toggle button for menu filters
	$('#showFilters').click(function(){
			$('#filtersListinMenu').toggle();
		});
	
	var category_id = $('#menus-category_id').val();
	var menu_id = $('#menu_id').val();
	var model_name = $('#model_name').val();
	//alert(menu_id); return false;
	
	if(model_name == 'menus'){
		
		$.ajax({
				   
				   
				   url: base_url+'/menus/features_by_category_id',
				   
				   type: 'post',
				   
				   dataType : 'html',
				   
				   data: {
							 category_id: category_id,
							 menu_id:menu_id,
							 _csrf : _csrf
						 },
				   success: function (data) {
					  
					  $('#loader').hide();
					 
					 $('#featuresByCategoryId').html(data);
					   
					  console.log(data.search);
				   }
			  });
	}
	
		// on change we will get features of specific category
		$('#menus-category_id').change(function(){
			var category_id = $('#menus-category_id').val(); // category id value
			$.ajax({
				   
				   
				   url: base_url+'/menus/features_by_category_id',
				   
				   type: 'post',
				   
				   dataType : 'html',
				   
				   data: {
							 category_id: category_id,
							 menu_id:menu_id,
							 _csrf : _csrf
						 },
				   success: function (data) {
					  
					  $('#loader').hide();
					 
					 $('#featuresByCategoryId').html(data);
					   
					  console.log(data.search);
				   }
			  });
			
		});
	
	
	// this function for search similler mongo products in listing
        $('#searchingMongoProduct').keyup(function(){
        var query = $(this).val();
           
            $('.MongoProductLists .mongoProductName').each(function(){

                 var $this = $(this);
                 if($this.text().toLowerCase().indexOf(query.toLowerCase()) === -1)
                     $this.closest('p.mongo_products').fadeOut();
                else $this.closest('p.mongo_products').fadeIn();
            });					  
        });	
	
        
        // this function for search similler our products in listing
        $('#searchingOurProduct').keyup(function(){
        var query = $(this).val();
           
            $('.OurProductLists .ourProductName').each(function(){

                 var $this = $(this);
                 if($this.text().toLowerCase().indexOf(query.toLowerCase()) === -1)
                     $this.closest('p.our_products').fadeOut();
                else $this.closest('p.our_products').fadeIn();
            });					  
        });
        
        
        // this function for when we will change slider type then it will be apply
        $('.sliderType').change(function(){
            
            // if value will be cateogry then category box will be open in bottom
           // alert($(this).val()); return false;
            if($(this).val() == 'category'){
                
                $('#sliderCategoryList').show();
                
            }else{ // else it will be hide
                
                $('#sliderCategoryList').hide();
            }
            
        });
		
		
	// function for save mongo product in our database
	
	$('.AddMongoproduct').click(function(){  // button on click function for similler product search for product mapping
			
			var mongo_product_id = $(this).attr('mongo_product_id'); // product id get to onclick
			
			var product_name = $(this).attr('product_name'); // product id get to onclick
			
			
			
			$.ajax({
				   
				   
				   url: base_url+'/products/ajax_mongo_product_popup_form',
				   
				   type: 'post',
				   
				   dataType : 'html',
				   
				   data: {
							product_name:product_name,
							 mongo_product_id: mongo_product_id,
							 _csrf : _csrf
						 },
				   success: function (data) {
					  
					  
					 $('#mongo_product_popup_form').html(data);
					   
					  console.log(data.search);
				   }
			  });
	});
	
        
   
  $('#end_date').change(function(){
	 var startDate = new Date($('#start_date').val());
	 var endDate = new Date($('#end_date').val());

		if (startDate > endDate){
			$('#end_date').val('');
			alert('end date is not valid');
			return false;
		} 
  });
  
  
  $('#start_date').change(function(){
	 var startDate = new Date($('#start_date').val());
	 var endDate = new Date($('#end_date').val());

		if (startDate > endDate){
			$('#start_date').val('');
			alert('start date is not valid');
			return false;
		} 
  });
  
  
  $('.searchSubmitButton').click(function(){
		
		if($('#store_id').val() == '' || $('#store_id').val() == null){
			alert('Please select any store'); return false;
		}
		
		/**if($('#category_id').val() == '' || $('#category_id').val() == null){
			alert('Please select any category'); return false;
		} **/
	  
  });
  
  
  $('#groupItemFeatureId').change(function(){
	 
	 var feature_id =  $(this).val();
	 
	 $.ajax({
			
			url: base_url+'/filter-group-items/feature_values_by_feature_id',
				   
				   type: 'post',
				   
				   dataType : 'html',
				   
				   data: {
							 feature_id: feature_id,
							 _csrf : _csrf
						 },
				   success: function (data) {
					 
					 $('#featureValuesByFeatureId').html(data);
					   
					  console.log(data.search);
				   }
			  });
			  
  });
  
  
  // code for user frindly mapping form 
  $('.searchButtonForNayashoppyProduct').click(function(){
	  
		var formData = $('#nayashoppyProductForm').serialize();
		
		$.ajax({
			
			url: base_url+'/products/search_nayashoppy_products_for_mapping',
				   
				   type: 'post',
				   
				   dataType : 'html',
				   
				   data: {
							 formData: formData,
							 _csrf : _csrf
						 },
				   success: function (data) {
					// alert(data); return false;
					 $('#search_nayashoppy_products_for_mapping').html(data);
					   
					  console.log(data.search);
				   }
			  });
	  
  });
  
  
   // code for user frindly mapping form 
  $('.list_product_for_mongo_mapping').click(function(){  // button on click function for similler product search for product mapping
			var id = $(this).attr('id');
			
			$('.list_product_for_mongo_mapping').removeClass('clickMongoProduct');
			$(this).addClass('clickMongoProduct');
			
			$('.radio_'+id).attr('checked',true);
			
			
	});
	 
	
	
	// function for save mongo product in our database
	
	$('.searchAutoComplete').focus(function(){ 
        var model_name = $(this).attr('model_name');
		var field_name = $(this).attr('field_name');
		var id_value = $(this).attr('id_value');
		var search_value  = $(this).val();
		
		if(search_value == ''){
			$("#"+id_value).val('');
		}
		
			$(".searchAutoComplete").autocomplete({
		
			 source: base_url+'/logs/ajax-auto-complete-search/?model_name='+model_name+'&field_name='+field_name+'&id_value='+id_value,
			 minLength: 1,
			 select: function(event, ui) {
				
				$("#"+id_value).val(ui.item ? ui.item.key : "");
				
			 },
			 
			 html: true // optional (jquery.ui.autocomplete.html.js required)
			});
		
        
 });
	
	// function for save mongo product in our database
	
	$('.searchAutoComplete').keyup(function(){ 
        var model_name = $(this).attr('model_name');
		var field_name = $(this).attr('field_name');
		var id_value = $(this).attr('id_value');
		var search_value  = $(this).val();
		
		if(search_value == ''){
			$("#"+id_value).val('');
		}
		
			$(".searchAutoComplete").autocomplete({
		
			 source: base_url+'/logs/ajax-auto-complete-search/?model_name='+model_name+'&field_name='+field_name+'&id_value='+id_value,
			 minLength: 1,
			 select: function(event, ui) {
				
				$("#"+id_value).val(ui.item ? ui.item.key : "");
				
			 },
			 
			 html: true // optional (jquery.ui.autocomplete.html.js required)
			});
		
        
 });
 
 
 
 
	/**$('.FeatureValuesValue').keyup(function(){  // button on click function for similler product search for product mapping
			
			var value = $(this).val();
			alert(value); return false;
			$.ajax({
				   
				   
				   url: base_url+'/products/ajax_mongo_product_popup_form',
				   
				   type: 'post',
				   
				   dataType : 'html',
				   
				   data: {
							value:value,
							 _csrf : _csrf
						 },
				   success: function (data) {
					  
					  
					 $('#mongo_product_popup_form').html(data);
					   
					  console.log(data.search);
				   }
			  });
	}); **/
	
	
	$("#admin-confirm_password").blur(function(){
		var newpassword = $("#admin-password_hash").val();
		var confirm_password  = $(this).val();
		
		if(newpassword != confirm_password){
			alert('password and confirm password is not matched!')
			$('.changePasswordButton').attr('disabled','disabled');
		}else{
			$('.changePasswordButton').removeAttr('disabled');
		}
	});
	
       
						   
});

