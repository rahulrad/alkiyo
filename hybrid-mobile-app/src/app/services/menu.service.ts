import { Injectable } from '@angular/core';
import { ApiBaseService } from "../services/service.api.base";
import { ApiConstant } from "../Utility/api.constant";
import { Observable } from "rxjs/Observable";
import { ApiResponse } from "../models/api.response";


@Injectable()
export class MenuService {

    observable: Observable<ApiResponse>;

    constructor(
        private baseService: ApiBaseService
    ) {

    }

    /**
     * menu list api
     */
    public getMenuList(): Observable<ApiResponse> {
        if (!this.observable) {
            let url: string = ApiConstant.baseDomain + ApiConstant.menuListUrl;
            this.observable = this.baseService.get(url, false);
        }
        return this.observable;
    }

    getHomeSlider(): any {
        let url: string = ApiConstant.baseDomain + ApiConstant.homeSliderUrl;
        return this.baseService.get(url, false);
    }

}