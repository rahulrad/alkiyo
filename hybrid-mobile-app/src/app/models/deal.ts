export interface Deal {
    title: string,
    discount: string
}