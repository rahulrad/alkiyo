export interface ApiResponse {
    data: any;
    status: boolean;
    error: string;
    payload: string;
}