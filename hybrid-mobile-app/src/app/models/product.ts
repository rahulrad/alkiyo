export interface Product {
    product_name: string,
    slug: string
}