import { CouponListComponent } from './couponList.component';
import { Common } from './../../Utility/common';

import { Component } from '@angular/core';
import { OnInit, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subscription } from 'rxjs/Subscription';
import { Loading } from 'ionic-angular/components/loading/loading';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { CouponService } from '../../services/coupon.service';
import { Coupon } from '../../models/coupon';
import { ViewChild } from '@angular/core';
import { ProductSearch } from '../../../pages/products/product.search';


@Component({
  templateUrl: '../../../pages/coupon/couponhome.html'
})
export class CouponHomeComponent implements OnInit, OnDestroy {


   menuTitle: string;
  private subscribe: Subscription;
  cashBackAndCoupons: Coupon[];

  @ViewChild("searchInput") searchInput;
  constructor(private _couponService: CouponService, private utility: Common, public navParams: NavParams
    , public navCtrl: NavController) {

  }




  ngOnInit() {
    this.getVendors();
    this.menuTitle = "Coupons";
  }


  ngOnDestroy() {


  }

  updateFilterList() {
    if (undefined != this.searchInput.value && this.searchInput.value != '') {
      console.log(this.searchInput.value);
      // this.filteredList.splice(0, this.filteredList.length);
      console.log(this.cashBackAndCoupons.length);
      this.cashBackAndCoupons.forEach(element => {
        if (element.name.toLowerCase().startsWith(this.searchInput.value.toLowerCase())) {
          element.selected = 1;
        } else {
          element.selected = 0;
        }

      });
    } else {
      this.resetList();
    }
  }

  resetList() {
    this.cashBackAndCoupons.forEach(element => {

      element.selected = 1;
    });


  }

  onSearch(event) {
    this.navCtrl.push(ProductSearch);
  }


  open(id: number, name: string) {
    this.navCtrl.push(CouponListComponent, { 'id_vendor': id, 'name': name });
  }

  isNotActive(couponObj) {
    console.log(this.cashBackAndCoupons[couponObj].selected === 0);
    return this.cashBackAndCoupons[couponObj].selected === 0;
  }
  private getVendors() {
    let loader: Loading;

    loader = this.utility.showProgressBar({
      content: 'loading Deals',
      spinner: 'dots'
    });
    loader.present();
    this.subscribe = this._couponService.getCouponsAndCashbacks(1).subscribe(
      data => {
        console.log(data.data);
        this.cashBackAndCoupons = data.data;

      },
      error => {

      },
      () => {
        this.utility.dismissProgressBar(loader);
      }
    );

  }

}

