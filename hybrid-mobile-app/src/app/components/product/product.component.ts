import { ProductSpecsComponent } from './product.specs.component';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { ApiConstant } from './../../Utility/api.constant';
import { ProductStoreComponent } from './product.store.component';
import { Common } from './../../Utility/common';

import { Component } from '@angular/core';
import { OnInit, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ProductService } from '../../services/product.service';
import { Subscription } from 'rxjs/Subscription';
import { Loading } from 'ionic-angular/components/loading/loading';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { NavController } from 'ionic-angular/navigation/nav-controller';


@Component({
  templateUrl: '../../../pages/product/product.html'
})
export class ProductComponent implements OnInit, OnDestroy {

  options: InAppBrowserOptions = {
    location: 'yes',//Or 'no' 
    hidden: 'no', //Or  'yes'
    clearcache: 'yes',
    clearsessioncache: 'yes',
    zoom: 'yes',//Android only ,shows browser zoom controls 
    hardwareback: 'yes',
    mediaPlaybackRequiresUserAction: 'no',
    shouldPauseOnSuspend: 'no', //Android only 
    closebuttoncaption: 'Close', //iOS only
    disallowoverscroll: 'no', //iOS only 
    toolbar: 'yes', //iOS only 
    enableViewportScale: 'no', //iOS only 
    allowInlineMediaPlayback: 'no',//iOS only 
    presentationstyle: 'pagesheet',//iOS only 
    fullscreen: 'yes',//Windows only    
  };
  productObj: any;
   menuTitle: string;
   subscribe: Subscription;
  constructor(private _productService: ProductService, private utility: Common, public navParams: NavParams
    , public navCtrl: NavController, private theInAppBrowser: InAppBrowser) {

  }


  public openWithSystemBrowser(url: string) {
    let target = "_system";
    this.theInAppBrowser.create(url, target, this.options);
  }
  public openWithInAppBrowser(url: string) {
    let target = "_blank";
    this.theInAppBrowser.create(url, target, this.options);
  }
  public openWithCordovaBrowser(url: string) {
    let target = "_self";
    this.theInAppBrowser.create(url, target, this.options);
  }

  ngOnInit() {
    this.productObj = this.navParams.get('obj');
    if (this.productObj) {
      this.menuTitle = this.productObj.product.product_name;
    }
    this.getProductData(this.navParams.get('slug'));


  }




  ngOnDestroy() {


  }

  getFetureImagePath(id: number) {
    return ApiConstant.staticImagesDomain + ApiConstant.featureMobileThumb + "1.svg";
  }

  redirect(component: string) {

    if (component === 'store') {

      this.navCtrl.push(ProductStoreComponent, { 'obj': this.productObj });
    }
    if (component === 'specs') {

      this.navCtrl.push(ProductSpecsComponent, { 'obj': this.productObj });
    }

  }

  openPage(slug) {
    this.navCtrl.push(ProductComponent, { 'slug': slug });
  }

  private getProductData(slug: string) {

    console.log(this.productObj);
    if (this.productObj === undefined) {
      let loader: Loading;

      loader = this.utility.showProgressBar({
        content: 'loading Products',
        spinner: 'dots'
      });
      loader.present();
      this.subscribe = this._productService.getProductObject(slug).subscribe(
        data => {
          this.productObj = data.data;
          this.menuTitle = this.productObj.product.product_name;
        },
        error => {

        },
        () => {
          this.utility.dismissProgressBar(loader);
        }
      );
    }
  }

}

