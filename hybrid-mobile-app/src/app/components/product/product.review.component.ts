import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Common } from './../../Utility/common';
import { Component, Input } from '@angular/core';
import { OnInit, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { PipeTransform } from '@angular/core/src/change_detection/pipe_transform';
import { Pipe } from '@angular/core/src/metadata/directives';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { ProductService } from '../../services/product.service';


@Component({
  templateUrl: '../../../pages/product/product.review.html',

})
export class ProductReviewComponent implements OnInit, OnDestroy {


  productObj: any;
   menuTitle: string;
  shownGroup: null;
  selectedSegment: string = "review";
   reviewForm: FormGroup;
  rating: any;
   formInputClass: string;
  formSubmitted = false;

  title = new FormControl('', [
    Validators.required,
    Validators.minLength(5),
    Validators.maxLength(100),

  ]);
  description = new FormControl('', [Validators.required]);
  rate = new FormControl('', [Validators.required]);





  constructor(private alertCtrl: AlertController, public navParams: NavParams, private _formbuilder: FormBuilder, private _productService: ProductService, private utility: Common) {

    this.productObj = this.navParams.get('obj');
    this.reviewForm = this._formbuilder.group({
      'product_id': this.productObj.product.product_id,
      'description': this.description,
      'title': this.title,
      'rating': this.rate,

    });

  }

  show() {


  }
  postForm() {
    let reviewObj = this.reviewForm.value;

    this._productService.addUserReview(reviewObj).subscribe(data => {
      if (data.status) {
        this.formSubmitted = true;
        this.utility.generateToast({
          message: data.data,
          duration: 3000,
          position: 'bottom'
        }).present();

        this.formSubmitted = true;
        this.reviewForm.reset();
        (<FormControl>this.reviewForm.controls['product_id']).setValue(this.productObj.product.product_id);
        this.rating = 0;
      } else {
        console.log("manu");
        this.showPopup(data.error, data.data);
      }
    });


  }

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {

          }
        }
      ]
    });
    alert.present();
    console.log("manuaxde");
  }


  selectRating(event: any) {
    (<FormControl>this.reviewForm.controls['rating']).setValue(event);
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };

  isGroupShown(group) {
    return this.shownGroup === group;
  };

  toggleSegment(segmenttitle) {
    this.selectedSegment = segmenttitle;
  }


  ngOnInit() {

    this.menuTitle = this.productObj.product.product_name;
    this.init();

  }

  init() {

  }

  ngOnDestroy() {

  }




}
