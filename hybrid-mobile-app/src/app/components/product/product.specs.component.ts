import { Component, Input } from '@angular/core';
import { OnInit, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { PipeTransform } from '@angular/core/src/change_detection/pipe_transform';
import { Pipe } from '@angular/core/src/metadata/directives';


@Component({
  templateUrl: '../../../pages/product/product.specs.html',

})
export class ProductSpecsComponent implements OnInit, OnDestroy {


  productObj: any;
   menuTitle: string;
  shownGroup: null;
  selectedSegment: string = "specs";



  constructor(public navParams: NavParams) {

    this.productObj = this.navParams.get('obj');
    console.log("specs");
    console.log(this.productObj.featureGroups);

  }

  show() {

  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };

  isGroupShown(group) {
    return this.shownGroup === group;
  };

  toggleSegment(segmenttitle) {
    this.selectedSegment = segmenttitle;
  }


  ngOnInit() {
    this.menuTitle = this.productObj.product.product_name;
  }

  ngOnDestroy() {

  }




}
