import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'keys' })
export class KeysPipe implements PipeTransform {
  transform(value, args: string[]): any {
    let keys = [];
    let re = /\-/gi;
    for (let key in value) {
      keys.push({ key: key.replace(re, ' '), value: value[key] });
    }
    return keys;
  }
}

@Pipe({ name: 'ValuesPipe' })
export class ValuesPipe implements PipeTransform {
  transform(value: any, args?: any[]): any[] {
    // create instance vars to store keys and final output
    let keyArr: any[] = Object.keys(value),
      dataArr = [];

    // loop through the object,
    // pushing values to the return array
    keyArr.forEach((key: any) => {
      dataArr.push(value[key]);
    });

    // return the resulting array
    return dataArr;
  }
}

