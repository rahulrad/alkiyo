export class ApiConstant {

    public static get baseDomain(): string { return "http://api.nayashopi.in" };


    public static get staticImagesDomain(): string { return "http://www.nayashopi.in" };

    public static get categoryMobileThumb(): string { return "/uploads/categories/mobile/" };

    public static get featureMobileThumb(): string { return "/uploads/features/mobile/" };

    public static get couponCategoryMobileThumb(): string { return "/uploads/coupons/category_mobile/" };

    public static get couponVendorMobileSliderImages(): string { return "/uploads/coupons/coupon_vendor_mobile_slider/" };



    public static get menuListUrl(): string { return "/v1/menu/list" };

    public static get homeSliderUrl(): string { return "/v1/menu/slider-data" };

    public static get topCategoriesUrl(): string { return "/v1/category/top" };

    public static get popularStoreUrl(): string { return "/v1/coupon/popular-stores" };

    public static get trendingDealsUrl(): string { return "/v1/coupon/trending-deals" };

    public static get couponsAndCashbacksUrl(): string { return "/v1/coupon/coupons-and-cash-backs" };

    public static get categoryWiseProductUrl(): string { return "/v1/product/category-wise-products?categoryId={categoryId}&sort={sort}" };

    public static get menuWiseProductUrl(): string { return "/v1/product/menu-wise-products?{query}" };

    public static get getProductdata(): string { return "/v1/product/detail?slug=" };

    public static get menuWiseFiltersUrl(): string { return "/v1/product/filters?slug={slug}" };

    public static get reviewPostUrul(): string { return "/v1/product/addreview" };


    public static get getCouponsListwithMerchant(): string { return "/v1/coupon/available-coupons-vendors?id_vendor=" };

    public static get productSearchByTerm(): string { return "/v1/product/search?term={term}" };

    public static get productCompareSearchByTerm(): string { return "/v1/product/compare-search?term={term}&category={category}" };

    public static get productCompareData(): string { return "/v1/product/compare?{query}" };

    public static get userLogin(): string { return "/v1/user/login?email={email}&password={password}" };

    public static get userSocalLogin(): string { return "/v1/user/social-login?email={email}" };

    public static get userRegister(): string { return "/v1/user/register?email={email}&username={username}&password={password}" };

    public static get userForgetPassword(): string { return "/v1/user/recover?email={email}" };



}