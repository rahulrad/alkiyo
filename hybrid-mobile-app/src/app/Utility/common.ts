import {
    AlertController, LoadingController, Loading, ToastController, Toast, ToastOptions, AlertOptions, Alert
} from 'ionic-angular';
import { Injectable } from '@angular/core';
import { LoadingOptions } from 'ionic-angular/components/loading/loading-options';
import { Network } from '@ionic-native/network';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { HomePage } from '../../pages/home/home';

@Injectable()
export class Common {

    private offlineNetworkToast: Toast;

    constructor(
        private alertCtrl: AlertController,
        private _loadingCtrl: LoadingController,
        private toast: ToastController,
        private network: Network,
    ) {
        this.offlineNetworkToast = null;
    }

    /** this is progressBar Method */
    public showProgressBar(options: LoadingOptions): Loading {
        let loading: Loading = this._loadingCtrl.create(options);
        return loading;
    }

    public dismissProgressBar(loadingData: Loading) {
        loadingData.dismiss();
    }

    /**
     *  this is an alert msg for more option  
     */
    public doAlert(msg: string, title: string) {
        let alert = this.alertCtrl.create({
            title: title,
            message: msg,
            buttons: ['Ok']
        });
        alert.present()
    }

    presentConfirm(alertOption: AlertOptions): Alert {
        return this.alertCtrl.create(alertOption);
    }


    /**
     * this is toast msg for network connection error handelor
     */
    generateToast(toastOptions: ToastOptions): Toast {
        return this.toast.create(toastOptions);

    }

    /**  Common method for console log message */
    public consoleLog(msg: string) {
        console.log(msg);
    }

    connectionMoniter() {
        this.network.onConnect().subscribe(() => {
            if (this.offlineNetworkToast) {
                this.offlineNetworkToast.dismiss().then(
                    () => {
                        this.offlineNetworkToast = null;
                    })
            }
        });

        this.network.onDisconnect().subscribe(() => {
            if (!this.offlineNetworkToast) {
                this.offlineToast();
            }
        });
    }

    offlineToast() {
        this.offlineNetworkToast =
            this.generateToast({
                message: "Network failure.",
            });
        this.offlineNetworkToast.onDidDismiss(() => {

        });
        this.offlineNetworkToast.present();
    }
}