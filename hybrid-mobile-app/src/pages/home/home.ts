import { ApiConstant } from './../../app/Utility/api.constant';
import { CouponHomeComponent } from './../../app/components/coupon/couponhome.component';
import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Category } from '../../app/models/category';
import { Store } from '../../app/models/store';
import { CategoryService } from '../../app/services/category.service';
import { StoreService } from '../../app/services/store.service';
import { ProductService } from '../../app/services/product.service';
import { CouponService } from '../../app/services/coupon.service';
import { Product } from '../../app/models/product';
import { Deal } from '../../app/models/deal';
import { Coupon } from '../../app/models/coupon';
import { Slider } from '../../app/models/slider';
import { MenuService } from '../../app/services/menu.service';
import { ProductComponent } from '../../app/components/product/product.component';
import { CategoryProductsPage } from '../products/category.product';
import { CouponListComponent } from '../../app/components/coupon/couponList.component';
import { ProductSearch } from '../products/product.search';
import { Events } from 'ionic-angular/util/events';
import { Alert } from 'ionic-angular/components/alert/alert';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { BaseComponent } from '../../app/base.component';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage extends BaseComponent implements OnInit {

  sliderImages: Slider[];
  topCategories: Category[];
  categoryGrid: Array<Array<Category>>;
  categoryNewProducts: Product[];
  categoryPopularProducts: Product[];
  trendingDeals: Deal[];
  cashBackAndCoupons: Coupon[];
  categoryWiseProductBox: string;

  constructor(
    public navCtrl: NavController,
    public categoryService: CategoryService,
    public storeService: StoreService,
    public productService: ProductService,
    public couponService: CouponService,
    public menuService: MenuService,
    public events: Events,
    public alertCtrl: AlertController,
  ) {
    super(alertCtrl);
    this.sliderImages = [];
    this.topCategories = [];
    this.categoryNewProducts = [];
    this.categoryPopularProducts = [];
    this.trendingDeals = [];
    this.cashBackAndCoupons = [];
    this.categoryWiseProductBox = "new";
    events.subscribe('tab:popToRoot', (tab) => {
      this.navCtrl.popToRoot();
    });
  }

  ngOnInit(): void {
    this.loadSliderImages();
    this.loadTopCategory();
    this.loadNewProducts();
    this.loadPopularProducts();
    this.loadTrendingDeals();
    this.loadCouponAndCashBack();
  }

  loadSliderImages(): any {
    this.menuService.getHomeSlider().subscribe(
      response => {
        this.sliderImages = response.data;
      },
      error => {
        this.handleErrors(error);
      }
    );
  }

  openStore(id: number, name: string) {
    this.navCtrl.push(CouponListComponent, { 'id_vendor': id, 'name': name });
  }

  openPage(slug) {
    this.navCtrl.push(ProductComponent, { 'slug': slug });
  }

  redirecttocategroy(category: any) {
    this.navCtrl.push(CategoryProductsPage, { 'slug': category.slug, 'title': category.category_name });
  }

  getSliderImagePath(slider: Slider) {
    console.log('https://www.nayashopi.in/' + slider.image);
    return 'https://www.nayashopi.in/' + slider.image;
  }


  getImagePath(id) {
    return ApiConstant.staticImagesDomain + ApiConstant.categoryMobileThumb + id + ".svg";
  }
  /**
   * load top categories
   */
  loadTopCategory() {
    this.categoryService.getTopCategories().subscribe(
      response => {
        this.topCategories = response.data;
        this.categoryGrid = Array(Math.ceil(this.topCategories.length / 2)); //MATHS!
        this.loadTopCategoryGrid();
      },
      error => {
      }
    );
  }

  loadTopCategoryGrid() {
    let rowNum = 0;
    for (let i = 0; i < this.topCategories.length; i += 2) {
      this.categoryGrid[rowNum] = Array(4);
      if (this.topCategories[i]) {
        this.categoryGrid[rowNum][0] = this.topCategories[i]
      }
      if (this.topCategories[i + 1]) {
        this.categoryGrid[rowNum][1] = this.topCategories[i + 1]
      }
      rowNum++;
    }
  }

  /**
   * load popular stores
   */
  loadTrendingDeals() {
    this.couponService.getTrendingDeals().subscribe(
      response => {
        this.trendingDeals = response.data;
      },
      error => {

      }
    );
  }
  loadNewProducts() {
    let categoryId = "1";
    let sort = "new";
    this.productService.getNewProducts(categoryId, sort).subscribe(
      response => {
        this.categoryNewProducts = response.data;
      },
      error => {

      }
    );

  }

  openCoupons() {

    this.navCtrl.push(CouponHomeComponent, {});
  }

  loadPopularProducts() {
    let categoryId = "1";
    let sort = "popular";
    this.productService.getPopularProducts(categoryId, sort).subscribe(
      response => {
        this.categoryPopularProducts = response.data;
      },
      error => {

      }
    );
  }

  loadCouponAndCashBack() {
    this.couponService.getCouponsAndCashbacks(0).subscribe(
      response => {
        this.cashBackAndCoupons = response.data;
      },
      error => {

      }
    );
  }

  onSearch(event) {
    this.navCtrl.push(ProductSearch);
  }

}
