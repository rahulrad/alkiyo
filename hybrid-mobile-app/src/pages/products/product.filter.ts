import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../app/services/menu.service';
import { ProductService } from '../../app/services/product.service';
import { Product } from '../../app/models/product';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { Common } from '../../app/Utility/common';
import { Loading } from 'ionic-angular/components/loading/loading';
import { ProductComponent } from '../../app/components/product/product.component';
import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { concat } from 'rxjs/operators/concat';

@Component({
    templateUrl: 'product.filter.html'
})
export class ProductFiltertPage implements OnInit {

     filters: any;
    menuSlug: string;

    constructor(
        public productService: ProductService,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        public utility: Common,
        public viewCtrl: ViewController
    ) {
        this.filters = [];
        this.filters = JSON.parse(this.navParams.get('filtersData'));
    }

    ngOnInit(): void {

    }

    dismiss() {
        let data = { isApplied: false };
        this.viewCtrl.dismiss(data);
    }

    displayFilterOptions(filter: any) {
        this.filters.forEach(element => {
            element.display = false;
        });
        filter.display = true;
    }

    toggleFilter(filter, filterValue) {
        if (filter.selectType == 'multi') {
            filterValue.selected = !filterValue.selected;
        } else if (filter.selectType == 'single') {
            filter.values.forEach(element => {
                if (filterValue.name != element.name) {
                    element.selected = false;
                }
            });
            filterValue.selected = !filterValue.selected;
        }
    }


    removeFilter(filterValue) {
        filterValue.selected = !filterValue.selected;
    }

    clearAllFilters() {
        this.filters.forEach(filter => {
            filter.values.forEach(filterValue => {
                filterValue.selected = false;
            });
        });
    }

    filter() {
        let data = {
            isApplied: true,
            filtersData: this.filters
        };
        this.viewCtrl.dismiss(data);
    }

}
