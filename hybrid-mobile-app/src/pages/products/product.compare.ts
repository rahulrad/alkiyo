import { ValuesPipe } from './../../app/pipes/key.pipes';
import { Component } from "@angular/core";
import { ModalController } from "ionic-angular/components/modal/modal-controller";
import { NavController } from "ionic-angular/navigation/nav-controller";
import { CompareProduct } from "../../app/models/product.compare";
import { NavParams } from "ionic-angular/navigation/nav-params";
import { OnInit } from "@angular/core/src/metadata/lifecycle_hooks";
import { Loading } from "ionic-angular/components/loading/loading";
import { Common } from "../../app/Utility/common";
import { ProductService } from "../../app/services/product.service";
import { AlertController } from "ionic-angular/components/alert/alert-controller";
import { ProductCompareSearch } from "./product.compare.search";
import { BaseComponent } from '../../app/base.component';

@Component({
    templateUrl: "./product.compare.html"
})
export class ProductCompare extends BaseComponent implements OnInit {

    product1: CompareProduct = null;
    product2: CompareProduct = null;
    comparetab: string = "overview";
    supplierList: any;
    category: string = "";

    compareData: any = [];

    constructor(
        public modalCtrl: ModalController,
        public navParams: NavParams,
        public utility: Common,
        public alertCtrl: AlertController,
        public navCtrl: NavController,
        public productService: ProductService
    ) {
        super(alertCtrl);
        this.product1 = this.navParams.get("product1");
        this.product2 = this.navParams.get("product2");
        this.category = this.navParams.get("category");
    }

    ngOnInit(): void {
        this.loadProducts();
    }

    loadProducts() {
        let loader: Loading;

        loader = this.utility.showProgressBar({
            content: 'fetching product data',
            spinner: 'dots'
        });
        loader.present();
        this.productService.getCompareProductsData(this.getQuery()).subscribe(
            response => {
                if (response.status) {
                    this.compareData = response.data

                } else {
                    this.showErrorPopup("Error", response.error);
                }
            },
            error => {
                this.utility.dismissProgressBar(loader);
                this.handleErrors(error)
            },
            () => {
                this.utility.dismissProgressBar(loader);
            }
        );
    }

    showErrorPopup(title, text) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: text,
            buttons: [
                {
                    text: 'OK',
                    handler: data => {
                        this.navCtrl.popToRoot();
                    }
                }
            ]
        });
        alert.present();
    }

    getQuery(): string {
        return "products[]=" + this.product1.slug + "&products[]=" + this.product2.slug;
    }

    getProduct(slug: string, feature: any): string {
        let featureValues = this.compareData.productsFeatures[slug];
        let featureId = feature.id;
        if (featureValues && featureValues.hasOwnProperty(featureId)) {
            return featureValues[featureId];
        }
        return "NA";
    }

    getSupplier(index: number, supplier: any, key: number): string {

        let keyArr: any[] = Object.keys(this.compareData.supplier);
        let objectDatat = keyArr[key];
        let StoreObject = this.compareData.supplier[objectDatat];

        if (StoreObject[index]) {
            return StoreObject[index].price
        }
        return "N/A";
    }

    getSupplierName(index: number, supplier: any): any {
        let keyArr: any[] = Object.keys(this.compareData.supplier);
        let objectDatat = keyArr[index];
        return objectDatat;
    }


    formatSupplier() {
        let list = this.compareData.supplier;
        for (let supplier in list) {
            for (var _i = 0; _i < supplier.length; _i++) {
                var obj = supplier[_i];

            }
        }

    }


    selectFirstProduct() {
        let productFilterModal = this.modalCtrl.create(ProductCompareSearch, { 'category': this.category });
        productFilterModal.onDidDismiss(data => {
            this.product1 = data;

            this.loadProducts();
        });
        productFilterModal.present();
    }

    selectSecondProduct() {
        let productFilterModal = this.modalCtrl.create(ProductCompareSearch, { 'category': this.category });
        productFilterModal.onDidDismiss(data => {
            this.product2 = data;

            this.loadProducts();
        });
        productFilterModal.present();
    }

}