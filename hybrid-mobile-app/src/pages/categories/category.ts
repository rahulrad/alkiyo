import { ApiConstant } from './../../app/Utility/api.constant';
import { Common } from './../../app/Utility/common';
import { UtilityConstant } from './../../app/Utility/utility.constant';
import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../app/services/menu.service';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { CategoryProductsPage } from '../products/category.product';
import { Loading } from 'ionic-angular/components/loading/loading';
import { ProductSearch } from '../products/product.search';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { Events } from 'ionic-angular/util/events';
import { BaseComponent } from '../../app/base.component';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@Component({
    selector: 'category-page',
    templateUrl: 'category.html'
})
export class CategoryPage extends BaseComponent implements OnInit {

    categories: Array<{
        title: string,
        slug: string,
        open: boolean,
        children: any
    }>;

    constructor(
        public navCtrl: NavController,
        private menuService: MenuService,
        public utility: Common,
        public viewCtrl: ViewController,
        public events: Events,
        public alertCtrl: AlertController
    ) {
        super(alertCtrl);
        this.categories = [];
        events.subscribe('tab:popToRoot', (tab) => {
            this.navCtrl.popToRoot();
        });
    }

    goback() {
        this.navCtrl.parent.select(0);

    }

    onSearch(event) {
        this.navCtrl.push(ProductSearch);
    }


    ngOnInit(): void {
        this.loadMenuList();
    }
    getImagePath(id) {
        return ApiConstant.staticImagesDomain + ApiConstant.categoryMobileThumb + id + ".svg";
    }

    loadMenuList() {
        let loader: Loading = this.utility.showProgressBar({
            content: 'loading Categories',
            spinner: 'dots'
        });
        loader.present();
        this.menuService.getMenuList().subscribe(
            response => {
                this.categories = response.data;
            },
            error => {
                this.utility.dismissProgressBar(loader);
                this.handleErrors(error);
            },
            () => {
                this.utility.dismissProgressBar(loader);
            }
        );
    }

    openPage(category) {
        this.navCtrl.push(CategoryProductsPage, category);
    }

    toggleSection(i) {
        this.categories[i].open = !this.categories[i].open;
    }

    toggleItem(i, j) {
        this.categories[i].children[j].open = !this.categories[i].children[j].open;
    }
}
