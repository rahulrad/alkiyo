import { Component } from "@angular/core";
import { OnInit } from "@angular/core/src/metadata/lifecycle_hooks";
import { NavController } from "ionic-angular/navigation/nav-controller";
import { UserService } from "../../app/services/user.service";
import { AlertController } from "ionic-angular/components/alert/alert-controller";
import { UserSignIn } from "./user.signin";
import { Common } from "../../app/Utility/common";

@Component({
    templateUrl: "./user.forgot.pass.html"
})
export class UserForgotPassword implements OnInit {

    createSuccess = false;
    credentials = { email: '' };

    constructor(
        private navCtrl: NavController,
        private userService: UserService,
        private alertCtrl: AlertController,
        private utility: Common
    ) {

    }

    ngOnInit(): void {

    }


    public forgotPassword() {
        this.userService.forgotPassword(this.credentials).subscribe(data => {
            if (data.status) {
                this.createSuccess = true;
                this.utility.generateToast({
                    message: data.data,
                    duration: 3000,
                    position: 'bottom'
                }).present();
                this.navCtrl.pop();
            } else {
                this.showPopup(data.error, data.data);
            }
        });
    }

    showPopup(title, text) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: text,
            buttons: [
                {
                    text: 'OK',
                    handler: data => {
                        if (this.createSuccess) {
                            this.navCtrl.popToRoot();
                        }
                    }
                }
            ]
        });
        alert.present();
    }

    showSignin() {
        this.navCtrl.push(UserSignIn);
    }
}