import { Component, OnInit, ViewChild } from "@angular/core";
import { NavController } from "ionic-angular";
import { UserSignUp } from "./user.signup";
import { UserForgotPassword } from "./user.forgot.pass";
import { Loading } from "ionic-angular/components/loading/loading";
import { AlertController } from "ionic-angular/components/alert/alert-controller";
import { LoadingController } from "ionic-angular/components/loading/loading-controller";
import { UserService } from "../../app/services/user.service";
import { HomePage } from "../home/home";
import { GooglePlus } from '@ionic-native/google-plus';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { Common } from "../../app/Utility/common";
import { ViewController } from "ionic-angular/navigation/view-controller";
import { Events } from "ionic-angular/util/events";


@Component({
    templateUrl: "./user.signin.html"
})
export class UserSignIn implements OnInit {

    loading: Loading;
    loginCredentials = { email: '', password: '' };
    selectedSegment = "login";


    constructor(
        public userService: UserService,
        public navCtrl: NavController,
        private alertCtrl: AlertController,
        private loadingCtrl: LoadingController,
        private googlePlus: GooglePlus,
        private facebook: Facebook,
        private utility: Common,
        private viewCtrl: ViewController,
        private events: Events
    ) {
        events.subscribe('tab:popToRoot', (tab) => {
            this.navCtrl.popToRoot();
        });
    }

    ngOnInit(): void {

    }

    ionViewDidEnter() {
        if (!this.userService.isGuest()) {
            this.navCtrl.parent.select(0);
        }

    }

    showSignUp() {
        this.navCtrl.push(UserSignUp);
    }

    showForgotPassword() {
        this.navCtrl.push(UserForgotPassword);
    }

    connectGoogle() {
        this.googlePlus.login({}).then(res => {
            this.loginWithSocial(res.email);
        }).catch(err => {
            console.log('Error logging into Google', err)
            this.showError("Unable to connect Google!");
        });
    }

    connectFacebook() {
        this.facebook.login(['public_profile', 'user_friends', 'email'])
            .then((res: FacebookLoginResponse) => {
                this.facebook.api('me?fields=id,name,email,first_name', []).then(profile => {
                    this.loginWithSocial(profile['email']);
                });
            }).catch(e => {
                console.log('Error logging into Facebook', e)
                this.showError("Unable to connect Facebook!");
            });
    }

    loginWithSocial(email: string) {
        this.showLoading()
        this.userService.loginWithSocial(email).subscribe(data => {
            if (data.status) {
                this.userService.setCurrentUser(data.data);
                this.utility.generateToast({
                    message: "Login Successfull",
                    duration: 3000,
                    position: 'bottom'
                }).present();
                this.navCtrl.parent.select(0);
            } else {
                this.showError(data.error);
            }
        });
    }

    login() {
        this.showLoading()
        this.userService.login(this.loginCredentials).subscribe(data => {
            if (data.status) {
                this.userService.setCurrentUser(data.data);
                this.utility.generateToast({
                    message: "Login Successfull",
                    duration: 3000,
                    position: 'bottom'
                }).present();
                this.navCtrl.parent.select(0);
            } else {
                this.showError(data.error);
            }
        });
    }

    showLoading() {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: true
        });
        this.loading.present();
    }

    showError(text) {
        this.loading.dismiss();
        let alert = this.alertCtrl.create({
            title: 'Error!',
            subTitle: text,
            buttons: ['OK']
        });
        alert.present();
    }


    dismiss() {
        this.navCtrl.parent.select(0);
    }
}