import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular/navigation/nav-controller";
import { UserSignIn } from "./user.signin";
import { UserService } from "../../app/services/user.service";
import { AlertController } from "ionic-angular/components/alert/alert-controller";
import { Common } from "../../app/Utility/common";
import { ViewController } from "ionic-angular/navigation/view-controller";

@Component({
    templateUrl: "./user.signup.html"
})
export class UserSignUp implements OnInit {

    createSuccess = false;
    registerCredentials = { email: '', username: '', password: '' };
    selectedSegment = "new";

    constructor(
        private navCtrl: NavController,
        private userService: UserService,
        private alertCtrl: AlertController,
        private utility: Common,
        private viewCtrl: ViewController,

    ) {

    }


    ngOnInit(): void {

    }

    public register() {
        this.userService.register(this.registerCredentials).subscribe(data => {
            if (data.status) {
                this.createSuccess = true;
                this.utility.generateToast({
                    message: data.data,
                    duration: 3000,
                    position: 'bottom'
                }).present();
                this.navCtrl.pop();
            } else {
                this.showPopup(data.error, data.data);
            }
        });
    }

    showPopup(title, text) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: text,
            buttons: [
                {
                    text: 'OK',
                    handler: data => {
                        if (this.createSuccess) {
                            this.navCtrl.popToRoot();
                        }
                    }
                }
            ]
        });
        alert.present();
    }

    showSignin() {
        this.navCtrl.push(UserSignIn);
    }



}