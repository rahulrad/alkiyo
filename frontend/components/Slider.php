<?php

namespace frontend\components;

use yii\base\Widget;
use yii\helpers\Html;
use common\models\HomeImageSlider;

class Slider extends Widget{
	public $categoryId = null;
	
	public function init(){
		parent::init();
	}
	
	public function run(){
			$homeimageSlider = HomeImageSlider::find()->andWhere('is_mobile=0')->all();
			
		return $this->render('@frontend/views/home/slider', ['categoryId' => $this->categoryId,'dealsbanner'=>$homeimageSlider]);
	}
}