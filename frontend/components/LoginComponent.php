<?php
namespace frontend\components;
 
 
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
 
class LoginComponent extends Component
{
 public function welcome()
 {
   Yii::app()->controller->renderPartial('frontend\views\site\login.php');
 }
}