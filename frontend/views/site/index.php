<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="container">
  <div class="row">
    <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12 main-banner">
      <div id="myCarousel" class="carousel b-shadow slide" data-ride="carousel" data-jcarousel="true"> 
        
        <!-- Wrapper for slides -->
        <div class="carousel-inner" style="left: 0px; top: 0px;">
          <div class="item"> <img src="images/banner/1.jpg" alt="" style="height: 233px;"> </div>
          <div class="item"> <img src="images/banner/2.jpg" alt="" style="height: 233px;"> </div>
          <div class="item active"> <img src="images/banner/3.jpg" alt="" style="height: 233px;"> </div>
          <div class="item"> <img src="images/banner/1.jpg" alt="" style="height: 233px;"> </div>
        </div>
        <ul class="nav nav-pills nav-justified">
          <li data-target="#myCarousel" data-slide-to="0" class=""><a href="#"><strong>Flipcart</strong><small>Up to 40% OFF</small></a></li>
          <li data-target="#myCarousel" data-slide-to="1" class=""><a href="#"><strong>New Mobile Lounch</strong><small>Up to 40% OFF</small></a></li>
          <li data-target="#myCarousel" data-slide-to="2" class="active"><a href="#"><strong>addidas</strong><small>Up to 60% OFF</small></a></li>
          <li data-target="#myCarousel" data-slide-to="3" class=""><a href="#"><strong>Amazon</strong><small>Up to 40% OFF</small></a></li>
        </ul>
      </div>
    </div>
    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12 no-left-padd addbanner">
      <picture>
     
        <img class="img-responsive" src="images/advertise/banner_trend_default.jpg" alt="Flowers"> </picture>
      
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
      <div class="panel panel-default deal dealofthe-day">
        <div class="panel-footer">
          <h3> Deals of the Day </h3>
          <ul class="nav nav-tabs pull-right" role="tablist">
            <li role="presentation" class=" active"><a href="#flipcart" aria-controls="flipcart" role="tab" data-toggle="tab"> Flipkart </a></li>
            <li role="presentation" class=""><a href="#snapdeal" aria-controls="snapdeal" role="tab" data-toggle="tab">Snapdeal</a></li>
            <li role="presentation" class=""><a href="#amazon" aria-controls="amazon" role="tab" data-toggle="tab">Amazon</a></li>
            <li role="presentation" class=""><a href="#paytm" aria-controls="paytm" role="tab" data-toggle="tab"> Paytm </a></li>
            <li role="presentation" class=""><a href="#mantra" aria-controls="mantra" role="tab" data-toggle="tab"> Myntra</a></li>
            <li role="presentation" class=""><a href="#ebay" aria-controls="ebay" role="tab" data-toggle="tab">Ebay</a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="flipcart">
            <div class="panel-body dealrow">
              <div class="row colume6">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/laptop-bag.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> Laptop Branded Bag </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/memorycard.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/pumashoes.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/mobile.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/pumashoes.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/watch.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a class="btn btn-block btn-loadmore bgnone" href="#"> View More <i class="fa fa-angle-down"></i> </a> </div>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane " id="snapdeal">
            <div class="panel-body dealrow">
              <div class="row colume6">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/pumashoes.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/pumashoes.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/mobile.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/laptop-bag.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> Laptop Branded Bag </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/memorycard.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/watch.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a class="btn btn-block btn-loadmore bgnone" href="#"> View More <i class="fa fa-angle-down"></i> </a> </div>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane " id="amazon">
            <div class="panel-body dealrow">
              <div class="row colume6">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/pumashoes.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/mobile.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/laptop-bag.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> Laptop Branded Bag </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/memorycard.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/pumashoes.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/watch.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a class="btn btn-block btn-loadmore bgnone" href="#"> View More <i class="fa fa-angle-down"></i> </a> </div>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane " id="paytm">
            <div class="panel-body dealrow">
              <div class="row colume6">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/memorycard.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/laptop-bag.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> Laptop Branded Bag </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/pumashoes.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/mobile.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/pumashoes.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/watch.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a class="btn btn-block btn-loadmore bgnone" href="#"> View More <i class="fa fa-angle-down"></i> </a> </div>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane " id="mantra">
            <div class="panel-body dealrow">
              <div class="row colume6">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/laptop-bag.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> Laptop Branded Bag </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/memorycard.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/pumashoes.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/mobile.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/pumashoes.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/watch.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a class="btn btn-block btn-loadmore bgnone" href="#"> View More <i class="fa fa-angle-down"></i> </a> </div>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane " id="ebay">
            <div class="panel-body dealrow">
              <div class="row colume6">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/mobile.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/laptop-bag.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> Laptop Branded Bag </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/memorycard.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/pumashoes.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/pumashoes.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="full-row dealbox bigdealbox text-center" style="min-height: 18px;"> <a href="#">
                    <figure> <img src="images/product/watch.jpg" class="img-responsive centerimg" alt=""> </figure>
                    <p class="discription"> 32GB Evo Plus </p>
                    <p class="price"><span> $38.00 </span> <strong> $38.00 </strong></p>
                    </a> </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a class="btn btn-block btn-loadmore bgnone" href="#"> View More <i class="fa fa-angle-down"></i> </a> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="full-row coupon-cashbook row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="text-center  pull-left"> Coupons &amp; Cashback</h3>
          
          <!-- Nav tabs -->
          <ul class="nav nav-tabs " role="tablist">
            <li role="presentation" class="active"><a href="#recharge" aria-controls="home" role="tab" data-toggle="tab"> <i> <img src="images/icon_recharge.png" alt=""> </i> Recharge</a></li>
            <li role="presentation"><a href="#flights" aria-controls="profile" role="tab" data-toggle="tab"><i> <img src="images/icon_flight.png" alt=""> </i> Travels</a></li>
            <li role="presentation"><a href="#food" aria-controls="profile" role="tab" data-toggle="tab"><i> <img src="images/icon_food.png" alt=""> </i>Food &amp; Dining </a></li>
            <li role="presentation"><a href="#groceries" aria-controls="messages" role="tab" data-toggle="tab"> <i> <img src="images/icon_grocery.png" alt=""> </i> Groceries</a></li>
            <li role="presentation"><a href="#movie-tickets" aria-controls="settings" role="tab" data-toggle="tab"> <i> <img src="images/icon_ticket.png" alt=""> </i> Movie Tickets</a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body">
          <div class="tab-content couponrow">
            <div role="tabpanel" class="tab-pane active " id="recharge">
              <div class="row colume5">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                  <div class="full-row coupon-block text-center dealbox">
                    <figure> <img src="images/brandlogo/freecharge.png" alt="yatra"> </figure>
                    <p> Pay Rs.450 and get Rs.550 Coupon of Freecharge </p>
                    <p> <strong class="bdr-dashed">Extra 500 <span> CASH BACK </span> </strong> </p>
                    <a href="#" class="btn btn-info"> GET THIS DEAL</a>
                    <p> <span> Expiring in 6 Hours </span> </p>
                  </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                  <div class="full-row coupon-block text-center dealbox">
                    <figure> <img src="images/brandlogo/freecharge.png" alt="yatra"> </figure>
                    <p> Pay Rs.450 and get Rs.550 Coupon of Freecharge </p>
                    <p> <strong class="bdr-dashed">Extra 500 <span> CASH BACK </span> </strong> </p>
                    <a href="#" class="btn btn-info"> GET THIS DEAL</a>
                    <p> <span> Expiring in 6 Hours </span> </p>
                  </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                  <div class="full-row coupon-block text-center dealbox">
                    <figure> <img src="images/brandlogo/freecharge.png" alt="yatra"> </figure>
                    <p> Pay Rs.450 and get Rs.550 Coupon of Freecharge </p>
                    <p> <strong class="bdr-dashed">Extra 500 <span> CASH BACK </span> </strong> </p>
                    <a href="#" class="btn btn-info"> GET THIS DEAL</a>
                    <p> <span> Expiring in 6 Hours </span> </p>
                  </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                  <div class="full-row coupon-block text-center dealbox">
                    <figure> <img src="images/brandlogo/freecharge.png" alt="yatra"> </figure>
                    <p> Pay Rs.450 and get Rs.550 Coupon of Freecharge </p>
                    <p> <strong class="bdr-dashed">Extra 500 <span> CASH BACK </span> </strong> </p>
                    <a href="#" class="btn btn-info"> GET THIS DEAL</a>
                    <p> <span> Expiring in 6 Hours </span> </p>
                  </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                  <div class="full-row coupon-block text-center dealbox">
                    <figure> <img src="images/brandlogo/freecharge.png" alt="yatra"> </figure>
                    <p> Pay Rs.450 and get Rs.550 Coupon of Freecharge </p>
                    <p> <strong class="bdr-dashed">Extra 500 <span> CASH BACK </span> </strong> </p>
                    <a href="#" class="btn btn-info"> GET THIS DEAL</a>
                    <p> <span> Expiring in 6 Hours </span> </p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a href="#" class="btn btn-block btn-loadmore bgnone"> View More <i class="fa fa-angle-down"></i> </a> </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane " id="flights">
              <div class="row colume5">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                  <div class="full-row coupon-block text-center dealbox">
                    <figure> <img src="images/logo_yatra.jpg" alt="yatra"> </figure>
                    <p> <strong class="bdr-dashed">Rs. 500 <span> CASH BACK </span> </strong> </p>
                    <a href="#" class="btn btn-info"> GET THIS DEAL</a>
                    <p> <span> Expiring in 6 Hours </span> </p>
                  </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                  <div class="full-row coupon-block text-center dealbox">
                    <figure> <img alt="yatra" src="images/logo_flight.jpg"> </figure>
                    <p> <strong class="bdr-dashed">Rs. 500 <span> CASH BACK </span> </strong> </p>
                    <a class="btn btn-info" href="#"> GET THIS DEAL</a>
                    <p> <span> Expiring in 6 Hours </span> </p>
                  </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                  <div class="full-row coupon-block text-center dealbox">
                    <figure> <img src="images/logo_travelguru.jpg" alt="yatra"> </figure>
                    <p> <strong class="bdr-dashed">Rs. 500 <span> CASH BACK </span> </strong> </p>
                    <a href="#" class="btn btn-info"> GET THIS DEAL</a>
                    <p> <span> Expiring in 6 Hours </span> </p>
                  </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                  <div class="full-row coupon-block text-center dealbox">
                    <figure> <img src="images/logo_flight.jpg" alt="yatra"> </figure>
                    <p> <strong class="bdr-dashed">Rs. 500 <span> CASH BACK </span> </strong> </p>
                    <a href="#" class="btn btn-info"> GET THIS DEAL</a>
                    <p> <span> Expiring in 6 Hours </span> </p>
                  </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                  <div class="full-row coupon-block text-center dealbox">
                    <figure> <img src="images/logo_flight.jpg" alt="yatra"> </figure>
                    <p> <strong class="bdr-dashed">Rs. 500 <span> CASH BACK </span> </strong> </p>
                    <a href="#" class="btn btn-info"> GET THIS DEAL</a>
                    <p> <span> Expiring in 6 Hours </span> </p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a href="#" class="btn btn-block btn-loadmore bgnone"> View More <i class="fa fa-angle-down"></i> </a> </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="bus">...</div>
            <div role="tabpanel" class="tab-pane" id="cab">...</div>
            <div role="tabpanel" class="tab-pane" id="hotels">...</div>
            <div role="tabpanel" class="tab-pane" id="food">...</div>
            <div role="tabpanel" class="tab-pane" id="groceries">...</div>
            <div role="tabpanel" class="tab-pane" id="movie-tickets">...</div>
            <div role="tabpanel" class="tab-pane" id="health">...</div>
            <div role="tabpanel" class="tab-pane" id="shopping">...</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="mobile">
    <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
        <div class="panel panel-default deal  dealofthe-day">
          <div class="panel-footer">
            <h3> Mobiles </h3>
            <ul class="nav nav-tabs pull-right" role="tablist">
              <li role="presentation" class=" active"><a href="#samsung" aria-controls="flipcart" role="tab" data-toggle="tab"> Samsung </a></li>
              <li role="presentation" class=""><a href="#micromax" aria-controls="snapdeal" role="tab" data-toggle="tab">Micromax</a></li>
              <li role="presentation" class=""><a href="#sony" aria-controls="amazon" role="tab" data-toggle="tab">Sony</a></li>
              <li role="presentation" class=""><a href="#htc" aria-controls="paytm" role="tab" data-toggle="tab"> HTC </a></li>
              <li role="presentation" class=""><a href="#lenovo" aria-controls="mantra" role="tab" data-toggle="tab"> Lenovo</a></li>
              <li role="presentation" class=""><a href="#nokia" aria-controls="ebay" role="tab" data-toggle="tab">Nokia</a></li>
              <li role="presentation" class=""><a href="#intex" aria-controls="ebay" role="tab" data-toggle="tab">Intex</a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="samsung">
              <div class="panel-body dealrow">
                <div class="row colume6">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> 
                    <div class="discount-badge2"> <span> 30% <strong> OFF</strong></span> </div>
                    
                    <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  
                    <div class="full-row dealbox mobile  text-center"> 
                    <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                    <a href="#">
                      <figure> <img alt="" src="images/product/samsung-gallaxy.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center">
                    <div class="discount-badge2"> <span> 30% <strong> OFF</strong></span> </div>
                     <a href="#">
                      <figure> <img alt="" src="images/product/samsung-note.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> 
                    <div class="discount-badge2"> <span> 30% <strong> OFF</strong></span> </div>
                    <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> 
                    <div class="discount-badge2"> <span> 30% <strong> OFF</strong></span> </div>
                    <a href="#">
                      <figure> <img alt="" src="images/product/samsung-gallaxy.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> 
                    <div class="discount-badge2"> <span> 30% <strong> OFF</strong></span> </div>
                    <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a class="btn btn-block btn-loadmore bgnone" href="#"> View More <i class="fa fa-angle-down"></i> </a> </div>
                </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane " id="micromax">
              <div class="panel-body dealrow">
                <div class="row colume6">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center">
                        <div class="discount-badge2"> <span> 30% <strong> OFF</strong></span> </div>
                     <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> 
                        <div class="discount-badge2"> <span> 30% <strong> OFF</strong></span> </div>
                    <a href="#">
                      <figure> <img alt="" src="images/product/samsung-gallaxy.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> 
                        <div class="discount-badge2"> <span> 30% <strong> OFF</strong></span> </div>
                    <a href="#">
                      <figure> <img alt="" src="images/product/samsung-note.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> 
                        <div class="discount-badge2"> <span> 30% <strong> OFF</strong></span> </div>
                    <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> 
                        <div class="discount-badge2"> <span> 30% <strong> OFF</strong></span> </div>
                    <a href="#">
                      <figure> <img alt="" src="images/product/samsung-gallaxy.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center">
                        <div class="discount-badge2"> <span> 30% <strong> OFF</strong></span> </div>
                     <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a class="btn btn-block btn-loadmore bgnone" href="#"> View More <i class="fa fa-angle-down"></i> </a> </div>
                </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane " id="sony">
              <div class="panel-body dealrow">
                <div class="row colume6">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung-gallaxy.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung-note.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung-gallaxy.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a class="btn btn-block btn-loadmore bgnone" href="#"> View More <i class="fa fa-angle-down"></i> </a> </div>
                </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane " id="htc">
              <div class="panel-body dealrow">
                <div class="row colume6">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung-gallaxy.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>

                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung-note.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung-gallaxy.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a class="btn btn-block btn-loadmore bgnone" href="#"> View More <i class="fa fa-angle-down"></i> </a> </div>
                </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane " id="lenovo">
              <div class="panel-body dealrow">
                <div class="row colume6">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung-gallaxy.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung-note.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung-gallaxy.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a class="btn btn-block btn-loadmore bgnone" href="#"> View More <i class="fa fa-angle-down"></i> </a> </div>
                </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane " id="nokia">
              <div class="panel-body dealrow">
                <div class="row colume6">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung-gallaxy.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung-note.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center">
                     <a href="#">
                      <figure> <img alt="" src="images/product/samsung-gallaxy.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a class="btn btn-block btn-loadmore bgnone" href="#"> View More <i class="fa fa-angle-down"></i> </a> </div>
                </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane " id="intex">
              <div class="panel-body dealrow">
                <div class="row colume6">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung-gallaxy.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung-note.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung-gallaxy.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> <a href="#">
                      <figure> <img alt="" src="images/product/samsung.jpg"> </figure>
                      <p class="discription"> Samsung Canvas Juice 2 <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a class="btn btn-block btn-loadmore bgnone" href="#"> View More <i class="fa fa-angle-down"></i> </a> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="footwear">
    <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
        <div class="panel panel-default deal  dealofthe-day">
          <div class="panel-footer">
            <h3> Footwear </h3>
            
            <div class="clearfix"></div>
          </div>
                <div class="tab-content">
                <div class="panel-body dealrow">
          <div class="row colume6">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> 
                        <div class="discount-badge2"> <span> 30% <strong> OFF</strong></span> </div>
                    <a href="#">
                      <figure> <img class="img-responsive" src="images/product/pumashoes.jpg"> </figure>
                      <p class="discription"> Puma Sports Shoes<span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 <i> (47% off)</i> </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center">
                        <div class="discount-badge2"> <span> 30% <strong> OFF</strong></span> </div>
                     <a href="#">
                      <figure> <img class="img-responsive" src="images/product/shoes.png" alt=""> </figure>
                      <p class="discription"> Puma Sports Shoes<span> <img alt="" src="images/star.jpg"> </span> </p>
                                          <p class="price"> <span>$42,383 </span> <strong> $42,383 <i> (47% off)</i> </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>

                      </a> 
                      </div>
                  </div>
                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center">
                        <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                     <a href="#">
                      <figure> <img class="img-responsive" src="images/product/pumashoes.jpg"> </figure>
                      <p class="discription"> Puma Sports Shoes<span> <img alt="" src="images/star.jpg"> </span> </p>
                                           <p class="price"> <span>$42,383 </span> <strong> $42,383 <i> (47% off)</i> </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>

                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center">
                                            <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>

                     <a href="#">
                      <figure> <img class="img-responsive" src="images/product/shoes.png" alt=""> </figure>
                      <p class="discription"> Puma Sports Shoes<span> <img alt="" src="images/star.jpg"> </span> </p>
                                          <p class="price"> <span>$42,383 </span> <strong> $42,383 <i> (47% off)</i> </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>

                      </a> 
                      </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center">
                                            <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>

                     <a href="#">
                      <figure> <img class="img-responsive" src="images/product/pumashoes.jpg"> </figure>
                      <p class="discription"> Puma Sports Shoes<span> <img alt="" src="images/star.jpg"> </span> </p>
                                         <p class="price"> <span>$42,383 </span> <strong> $42,383 <i> (47% off)</i> </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>

                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center">
                                            <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>

                     <a href="#">
                      <figure> <img class="img-responsive" src="images/product/shoes.png" alt=""> </figure>
                      <p class="discription"> Puma Sports Shoes<span> <img alt="" src="images/star.jpg"> </span> </p>
                                          <p class="price"> <span>$42,383 </span> <strong> $42,383 <i> (47% off)</i> </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>

                      </a> 
                      </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a href="#" class="btn btn-block btn-loadmore bgnone"> View More <i class="fa fa-angle-down"></i> </a> </div>
                </div>
                </div>
                </div>
          
        </div>
      </div>
    </div>
  </section>
  <section class="fashion">
    <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
        <div class="panel panel-default deal  dealofthe-day">
          <div class="panel-footer">
            <h3> Fashion </h3>
            
            <div class="clearfix"></div>
          </div>
                <div class="tab-content">
                <div class="panel-body dealrow">
          <div class="row colume6">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> 
                        <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>

                    <a href="#">
                      <figure> <img class="img-responsive" src="images/product/wallet.jpg"> </figure>
                      <p class="discription"> Black  Leather Wallet <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center">
                      <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                     <a href="#">
                      <figure> <img class="img-responsive" src="images/product/glass.jpg"> </figure>
                      <p class="discription">  Contrast Rim Aviators<span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center">
                      <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                     <a href="#">
                      <figure> <img class="img-responsive" src="images/product/belt.jpg"> </figure>
                      <p class="discription">  Contrast Rim Aviators<span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> 
                      <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                    <a href="#">
                      <figure> <img class="img-responsive" src="images/product/watch.jpg"> </figure>
                      <p class="discription">  Sports Watches<span> <img alt="" src="images/watch.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> 
                      <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                    <a href="#">
                      <figure> <img class="img-responsive" src="images/product/perfume.jpg"> </figure>
                      <p class="discription">  Branded Perfume<span> <img alt="" src="images/watch.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> 
                      <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                    <a href="#">
                      <figure> <img class="img-responsive" src="images/product/casual-jeans.jpg"> </figure>
                      <p class="discription">  Peter England Jeans<span> <img alt="" src="images/watch.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a href="#" class="btn btn-block btn-loadmore bgnone"> View More <i class="fa fa-angle-down"></i> </a> </div>
                </div>
                </div>
                </div>
          
        </div>
      </div>
    </div>
  </section>
  <section class="personalcare">
    <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
        <div class="panel panel-default deal  dealofthe-day">
          <div class="panel-footer">
            <h3> Personal Care </h3>
            
            <div class="clearfix"></div>
          </div>
                <div class="tab-content">
                <div class="panel-body dealrow">
          <div class="row colume6">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center">
                      <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                     <a href="#">
                      <figure> <img class="img-responsive" src="images/product/ele_shaver.jpg"> </figure>
                      <p class="discription">  Philips Aquatouch Shaver  <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center">
                      <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                     <a href="#">
                      <figure> <img class="img-responsive" src="images/product/treamer.jpg" alt=""> </figure>
                      <p class="discription">  Philips QT4011 Trimmer  <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> 
                      </div>
                  </div>
                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center">
                      <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                     <a href="#">
                      <figure> <img class="img-responsive" src="images/product/hair-driyer.jpg"> </figure>
                      <p class="discription">  Philips W Hair Dryer <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center"> 
                      <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                    <a href="#">
                      <figure> <img class="img-responsive" src="images/product/hygenic.jpg" alt=""> </figure>
                      <p class="discription">  Listerine  White  Vibrant<span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> 
                      </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center">
                      <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                     <a href="#">
                      <figure> <img class="img-responsive" src="images/product/bm-monitor.jpg"> </figure>
                      <p class="discription">  Omron7120 BP Monitor  <span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="full-row dealbox mobile  text-center">
                      <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                     <a href="#">
                      <figure> <img class="img-responsive" src="images/product/fat_analyzor.jpg" alt=""> </figure>
                      <p class="discription"> Fat analyzor<span> <img alt="" src="images/star.jpg"> </span> </p>
                      <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                      </a> 
                      </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a href="#" class="btn btn-block btn-loadmore bgnone"> View More <i class="fa fa-angle-down"></i> </a> </div>
                </div>
                </div>
                </div>
          
        </div>
      </div>
    </div>
  </section>
  
  <section class="mobilepricelist">
    <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
        <h3> <span> Mobile Phone Price Lists </span> </h3>
        <div class="full-row priceblock b-shadow">
          <ul class="row">
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <a href="#"> <img src="images/brandlogo/samsung.jpg" alt="">
              <p> Samsung  Mabile Price </p>
              </a></li>
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <a href="#"> <img src="images/brandlogo/micromax.jpg" alt="">
              <p> Micromax Mobile Price</p>
              </a></li>
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <a href="#"> <img src="images/brandlogo/sony.jpg" alt="">
              <p> Sony Mobile Price </p>
              </a></li>
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <a href="#"> <img src="images/brandlogo/lenovo.jpg" alt="">
              <p> Lenovo Mobile Price </p>
              </a></li>
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <a href="#"> <img src="images/brandlogo/htc.jpg" alt="">
              <p> HTC Mobile Price </p>
              </a></li>
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <a href="#"> <img src="images/brandlogo/nokia.jpg" alt="">
              <p> Nokia Mobile Price </p>
              </a></li>
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <a href="#"> <img src="images/brandlogo/intex.jpg" alt="">
              <p> Intex  Mobile Price </p>
              </a></li>
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <a href="#"> <img src="images/brandlogo/motorola.jpg" alt="">
              <p> Motorola Mobile Price </p>
              </a></li>
              
                <li class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <a href="#"> <img src="images/brandlogo/ashus.jpg" alt="">
              <p> Ashus Mobile Price </p>
              </a></li>
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <a href="#"> <img src="images/brandlogo/blackbery.jpg" alt="">
              <p> Blackberry Mobile Price </p>
              </a></li>
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <a href="#"> <img src="images/brandlogo/lg.jpg" alt="">
              <p> LG  Mobile Price </p>
              </a></li>
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <a href="#"> <img src="images/brandlogo/spice.jpg" alt="">
              <p> Spice Mobile Price </p>
              </a></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  
</div>

<div class="site-index">

    <!--div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div-->

    <div class="body-content">

        <div class="row">
            <!--div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div-->
        </div>

    </div>
</div>
