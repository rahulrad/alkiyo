<style>

.main-menu-mobile {
    left: 0;
	background-color:#fff;
    overflow: hidden;
    height: 100%;
    display: none;
    position: fixed;
    width: 285px;
    background-size: 100%;
    z-index: 99999;
    top: 0;
    opacity: 1;
    transition: all ease .4s;
    -webkit-transition: all ease .4s;
    transform: translateX(-300px);
    -webkit-transform: translateX(-300px);
    -o-transform: translateX(-300px);
    -ms-transform: translateX(-300px);
    -moz-transform: translateX(-300px);
    box-shadow: 0 0 17px rgba(0,0,0,0.5);
}
body .page.menuOpen{
    /* transform: translateX(285px);
     -webkit-transform: translateX(285px);
     -moz-transform: translateX(285px);
     -ms-transform: translateX(285px);
     -o-transform: translateX(285px);*/
 
    overflow:hidden;
}


.main-menu-mobile.open{
    transform: translateX(100%);
    -webkit-transform: translateX(0);
    -moz-transform: translateX(0);
    -ms-transform: translateX(0);
    overflow: hidden;
    display: block;
-webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    transition: all 300ms ease 0ms;
    transition-duration: 1s;
    transition-delay: 54ms;
}

.menu-user-login{ height:116px;}
.main-menu-mobile .user-detail {
    padding: 10px 15px;
    position: relative;
    
}
.menu-user-login .logout{
    color: #fff;
    position: absolute;
    right: 5px;
    top: 4px;
    border: 1px solid rgba(255,255,255,0.5);
    border-radius: 2px;
    padding: 5px 6px;
    font-size: 12px;
    line-height: 1;
    font-weight: 400;
}

.main-menu-mobile .user-detail .user-name {
    font-size: 13px;
    padding: 1px 55px 0px 41px;
   
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    line-height: 1.3;
    height: 36px;
    vertical-align: middle;
    display: table-cell;
	font-size:16px;
	color:#fff;

}
.main-menu-mobile .user-detail .user-name:before {
   
    width: 37px;
    height: 36px;
    left: 11px;
    top: 11px;
    position: absolute;
    content: '';
}
.main-menu-mobile .user-detail .edit-name {
    font-size: 13px;
    position: absolute;
    right: 10px;
    top: 39px;
    padding-right: 19px;
    color: #fff;
}
.main-menu-mobile. .user-detail .edit-name:after {

    width: 12px;
    height: 12px;
    position: absolute;
    right: 0px;
    top: 3px;
    content: '';
}
.main-menu-mobile. .avail-balance-block {
    background: #1b3e4f;
    padding: 9px 0px;
    box-shadow: -15px 2px 22px -7px rgba(0, 0, 0, 0.9);
}
.avail-balance-block:after {
    clear: both;
    display: table;
    content: '';
}
.main-menu-mobile .avail-balance {
    font-size: 14px;
    color: #fff;
    padding: 6px 0px 6px 10px;
    float: left;
}
.main-menu-mobile .redeem-btn {
    border: 1px solid #fff;
    border-radius: 2px;
    position: relative;
    padding: 4px 12px;
    float: right;
    margin-right: 8px;
    box-shadow: none;
    background: none;
    transition: all 0.3s;
    color: #fff;
}
.main-menu-mobile .redeem-btn:hover {
    color: #fff;
    border: 1px solid #fff;

}
.menu-login-btn{
    float: none !important;
    margin-left:15px;
    padding: 4px 25px !important;
}
.main-menu-mobile ul {

    margin: 0;
    padding: 0;
    overflow-y: auto !important;
}
.main-menu-mobile ul {margin: 0;padding: 0;overflow-y: auto !important;}
.main-menu-mobile li {
    padding: 10px 0px;
    transition: all 0.3s;
    -webkit-transition: all 0.3s;
}
.main-menu-mobile li.active{ padding: 10px 0px 8px !important;}
.main-menu-mobile li.active a {font-weight: 200;}
.main-menu-mobile li.selected {
    background: #fff;
    padding: 10px 0px;
}
.main-menu-mobile li.selected a {
    background: #fff;
}
.main-menu-mobile a {
    color: #6b6b6b;
    font-size: 14px;
    text-decoration: none;
  
    padding: 0px 15px;
}
.main-menu-mobile li.selected > a,
.main-menu-mobile li.selected .sub-nav li.selected a {

    color: #000;

}
.main-menu-mobile li.selected .sub-nav li.selected a{
    padding-left: 38px;
    width: 99%;
    text-overflow: ellipsis;
    overflow: hidden;
	
}

/* Sub-menu */
.sub-nav {
    transform: translateX(-150%);
    -webkit-transform: translateX(-150%);
    -ms-transform: translateX(-150%);
    -o-transform: translateX(-150%);
    -moz-transform: translateX(-150%);

    transition: all ease 0.5s;
    -webkit-transition: all ease 0.5s;
    -moz-transition: all ease 0.5s;
    -o-transition: all ease 0.5s;
    position: absolute;
    background: #f8f8f8;
    width: 284px;
    top: 105px;
    z-index: 9;
}
.h51 {
    height: 56px;
    display:block;
}

.slide-down {
    transition: all ease 0.5s;
    transform: translatex(-285px);
}

/* .main-menu li:before{
    position: absolute;
    content: "\e90b";
    left:0px;
    font-size: 10px;
    font-family: 'icomoon';

}*/
.main-menu-mobile li.selected .sub-nav.slide-up {
    transform: translateX(0);
    -webkit-transform: translateX(0);
    height: 100vh;
}
.main-menu-mobile li.selected .sub-nav > ul {
    border-top: none;
    width: 285px;
    height: 100vh;
    overflow-x: hidden;
}
.main-menu-mobile li.selected .sub-nav li a {
    border: none;
    background: none;
    padding: 0px 15px;
    width: auto;
    display: block;
    color: #555;
}
.main-menu-mobile li.selected .sub-nav li.selected {
    border-top: none;
    background: #fff;
    font-size:14px;
    width: 100%;
    z-index: 999;
}

.main-menu-mobile li.selected .sub-nav .selected:after {

    content: '';
    display: table;
}

.main-menu-mobile li.selected .sub-nav li {
    position: relative;
}
.main-menu-mobile li.selected .sub-nav li:first-child > a:after {
    display: none;
}
.main-menu-mobile li.selected .sub-nav li > a:after, .main-menu-mobile li.selected .sub-nav li .sub-child > a:after {

    position: absolute;
    right: 18px;
    margin-top: 3px;
    font-size: 10px;
    color: #999;
    z-index: 99;
   color:#555;
display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    transition: all ease 0.35s;
    -webkit-transition: all ease 0.35s;
    -moz-transition: all ease 0.35s;
    -o-transition: all ease 0.35s;
    -webkit-transition: all ease 0.35s;

}

.main-menu-mobile li.selected .sub-nav li > a:after{content: "\f067" ;opacity:.6}
.main-menu-mobile li.no-expand > a:after{content: ''!important;}

.main-menu-mobile li.selected .sub-nav li .sub-child  li > a:after{content: "";}

.main-menu-mobile li.selected .sub-nav li.active > a:after {
    content: "\f068" ;
    
}
.sideblock {
    background: #fff;
    height: 100%;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}


/* Submenu-child */


.sub-list .sub-child, .sub-list .sub-child .sub-child1{
    margin:0;
    padding:0;
    background:#f1f1f1;
    border-top:none;
    margin: 15px 0px 0px;
    border-top: 1px solid #f1f1f1;
}
.sub-list .sub-child li { padding: 15px 22px; line-height: 1;}
.sub-list .sub-child li a{padding:0 !important; color: #777 !important; font-weight:normal; }
.main-menu-mobile li.selected .sub-child li > a:after, .main-menu-mobile li.selected .sub-child li:first-child > a:after{
    display: block;
    top: 16px;
    position: absolute;
    transition: all ease 0.35s;
    -webkit-transition: all ease 0.35s;
    -moz-transition: all ease 0.35s;
    -o-transition: all ease 0.35s;
    -webkit-transition: all ease 0.35s;
    -moz-transform: rotate(0deg);
    -webkit-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    transform: rotate(0deg);
}
.sub-list .sub-child .sub-child1{margin:14px 0px -14px;}
.sub-list .sub-child .sub-child1 li{ padding: 12px 15px;}
.sub-list .sub-child .sub-child1 li a{padding:0 !important; font-size: 14px; color:#999 !important;}

.main-menu-mobile li.selected .sub-child .sub-child1 li > a:after{ display: none; }
.main-menu-mobile .more-categ {
    background: #e3e3e3;
    padding: 15px 0px;
    text-align: center;
    position: absolute;
    bottom: 0px;
    left: 0px;
    width: 285px;
}
.main-menu-mobile .more-categ a {
    border: none;
    text-align: center;
    display: block;
}
.main-menu-mobile .more-categ a .icon-three-dots {
    font-size: 8px;
    color: #888;
}
.more-categ .more-info {
    transform: translateY(150%);
    -webkit-transform: translateY(150%);
    transition: all ease 0.5s;
    position: fixed;
    background: #f8f8f8;
    width: 285px;
    top:101px;
    margin-top: 14px;
    z-index: 9;
    padding-top: 20px;
}

.more-categ .more-info.slide-up1 {
    transform: translateY(0);
    -webkit-transform: translateY(0);
}

#login-box{
   z-index: 100000 !important;
}

.menu-user-not-login{

background-color:#969696;
padding-bottom:23px;
}

.main-list > li{
	padding:8px 0px;
}
</style>


<div class="main-menu-mobile" id="manu-menu">
<div class="menu-user-not-login">
                                <div class="user-detail">
                                    <div class="user-name"> Welcome!</div>
                                </div>
                                <div class="avail-balance-block text-center">
                                    <button  data-toggle="modal" data-target="#login-box"  class="redeem-btn menu-login-btn" type="button" name="login">Login</button>
                                    <button  data-toggle="modal" data-target="#login-box"  class="redeem-btn menu-login-btn" type="button" name="signup">Sign Up</button>
                                </div>
                            </div>
<ul class="main-list" >
<?php foreach($menus as $menu){ 
 $string = str_replace('.png','',str_replace('uploads/menus/','',$menu->image)); 
?>
<li><i class="main_cashback"></i>

<a href="javascript:void(0);"><strong><img class="sprite sprite-<?php echo $string;?>" src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=" border="0"/> 
<?=$menu->title; ?> 
</strong>
</a>
<?php $subMenus = $menu->getChildMenus('sub_menu',$menu->id); ?>
<div class="sub-nav" >
           	  <ul class="sub-list" >
<?php foreach($subMenus as $k => $sMenu){ ?>
			<?php if($k===0){ ?>

			<li class=" selected" >
			<span class="sub-nav-close fa fa-angle-left" style="width:30px;text-align:center;font-weight:bold;font-size:20px;vertical-align:middle;opacity: .6;">&nbsp; </span><span style="vertical-align:middle;">  <?=$menu->title; ?></span>
														
			<?php } ?>
<li><a href="javascript:void(0);">  <?=$sMenu->title; ?></a>
<?php $customMenus = $menu->getChildMenus('custom_menu',$sMenu->id); ?>
<ul class="sub-child"  style="display:none;">

<?php foreach($customMenus as $j => $cMenu){ ?>

<li><a href="<?=\yii\helpers\Url::to(['prettyurl/index','slug'=>$cMenu->slug])?>
">  <?=$cMenu->title; ?></a>

<?php } ?>
 
</ul>
</li>
 
<?php } ?>
</ul>
        </div>




<?php } ?>
</ul>

</div>

<script>

$(function () {


        $('.header .navbar .main-menu-mobile').appendTo("body");
        $(".navbar-toggle").click(function () {
            $('.main-menu-mobile').addClass("open");
            $('body').css("overflow", "hidden");
            $('body .page').addClass("menuOpen");
            $(".glow-bg").addClass("onglow");
        });
        $('.glow-bg,.overlay2').click(function () {
            $('.main-menu-mobile').removeClass("open");
            $('body').css("overflow", "visible");
            $('body .page').removeClass("menuOpen");
            $(this).removeClass("onglow");
        });
        $(".main-menu-mobile .main-list > li > a").click(function () {
            $(".main-menu-mobile .main-list > li").removeClass("selected");
            $(".sub-list li:first-child").removeClass("selected");
            $(".main-menu-mobile .main-list > li").children(".sub-nav").addClass("slide-up");
            $(this).parent(".main-menu-mobile .main-list > li").addClass("selected");
            $(".sub-list > li:first-child").addClass("selected");
            // $(this).children('.sub-nav').toggleClass('slide-up');
        });
        $(".sub-nav .sub-nav-close").click(function () {
            $(this).parent(".main-menu-mobile .main-list > li").removeClass("selected");
            $(".slide-up li:first-child").removeClass("selected");
            $(this).parent().parent().parent().removeClass("slide-up");
        });
        $(".more-categ").click(function () {
            $(this).children(".more-info").addClass("slide-up1");
        });
        $(".more-info-close").click(function (e) {
            e.stopPropagation();
            $(this).parent(".more-info").removeClass("slide-up1");
        });
        $(document).ready(function () {
            $(".sub-list > li > a").click(function () {
if(!$(this).parent('.sub-list > li').hasClass('selected')){
                $(this).parent('.sub-list > li').toggleClass('active');
                $(this).parent(".sub-list > li").children('.sub-child').slideToggle(500);
}
            });
        });

        $(".parent_offer").click(function (e) {
            var parent_key = $(this).attr('data-parent-key');
            var target_element = $(this).find('.child_' + parent_key);
            if (target_element.hasClass('displaynone')) {
                target_element.removeClass('displaynone');
            } else {
                target_element.addClass('displaynone');
            }
        });


        function setHeight() {
            windowHeight = $(window).innerHeight();
            $('.main-menu-mobile > ul').css('height', (windowHeight - 100));
            $('.sub-nav > ul').css('height', (windowHeight - 100));
            $('.more-info').css('height', (windowHeight - 100));
            $('.sub-nav').css('height', (windowHeight - 100));

        };
        setHeight();
    });
</script>
