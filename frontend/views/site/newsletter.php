<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="full-row newsletter">
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"> <img src="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/images/lady.png" class="img-responsive" alt=""> </div>
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <?php $form = ActiveForm::begin(['id' => $model->formName(), 'action'=> \Yii::$app->getUrlManager()->createUrl('newsletter/insert'), 'method'=>'post']); ?>
            <h4> Get a Newsletter <span> We will respond within 3 bussiness days </span></h4>
            <div class="input-group">
             <?=$form-> field($model, 'email')?> 
             <div class="form-group">
               <?= Html::submitButton('Subscribe', ['class' => 'fa fa-send', 'name' => 'subscribe-button']) ?>
             </div> 
            </div>
             <?php ActiveForm::end(); ?> 
	</div>
</div>
