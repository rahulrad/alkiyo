<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\ActiveField;

$this->title = 'Contact';
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="wrap">
  <div class="container">
  	<div class="row">
  		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    		<h1 itemprop="name">Contact Us</h1>
    	</div>
  		<div class="full-row contact">
  			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
  				<div class="contact-left-side">
  				<?php $form = ActiveForm::begin(['id' => 'contact-form', 'options' => ['class' => 'form-horizontal']]); ?>

                <?= $form->field($model, 'name',[
                    'template' => '{label}
                		<div class="col-sm-9">{input}</div>',
                ])->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email',[
                    'template' => '{label}
                		<div class="col-sm-9">{input}</div>',
                ])?>
				<?= $form->field($model, 'number',[
                    'template' => '{label}
                		<div class="col-sm-9"><div class="input-group"><span class="input-group-addon">+91-</span>{input}</div></div>',
                ])?>
                <?= $form->field($model, 'subject',[
                    'template' => '{label}
                		<div class="col-sm-9">{input}</div>',
                ]) ?>
                <?= $form->field($model, 'body',[
                    'template' => '{label}
                		<div class="col-sm-9">{input}</div>',
                ])->textArea(['rows' => 6])?>
                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div>',
                ]) ?>
<script type="text/javascript">
$(document).ready(function(){
	$('#contact-form label').each(function(){
		$(this).addClass('col-lg-3');
	});
});
</script>
<input class="btn btn-danger pull-right" value="SEND MESSAGE" type="submit">
<div class="clearfix">&nbsp;</div>
            	<?php ActiveForm::end(); ?>
  				</div>
  			</div>
  			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="contact-right-side">
<div class="f-block">
<h2><i class="fa fa-phone">&nbsp;</i> Call Us</h2>

<h3>0877 645 6399</h3>
<p>
10:00 AM - 7:00 PM</p>
</div>

<div class="f-block">
<h2><i class="fa fa-at pr-five">&nbsp;</i> Email Us</h2>

<h3>&nbsp;</h3>

<p>For any questions or feedback<br>
please send us an email to</p>
<a href="mailto:Social@nayashopi.in"> Social@nayashopi.in</a></div>

<div class="f-block">
<h2><i class="fa fa-map-marker">&nbsp;</i> Our Address</h2>

<p>D.NO.7-54/A, LAKSHMI TOWERS,1-FLOOR, KARAKAMBADI ROAD, SUBBA REDDY NAGAR
<br/>TIRUPATI, Andhra Pradesh 517501

</p>
</div>
</div>
</div>
  		</div>
  		
  	</div>
  </div>
</div>
<?php /*?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.
    </p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true])->widget(Captcha::className(), [
                    'template' => '
                		<div class="col-sm-9">{input}</div>',
                ]) ?>

                <?= $form->field($model, 'email')->widget(Captcha::className(), [
                    'template' => '
                		<div class="col-sm-9">{input}</div>',
                ]) ?>

                <?= $form->field($model, 'subject')->widget(Captcha::className(), [
                    'template' => '
                		<div class="col-sm-9">{input}</div>',
                ]) ?>

                <?= $form->field($model, 'body')->textArea(['rows' => 6])->widget(Captcha::className(), [
                    'template' => '
                		<div class="col-sm-9">{input}</div>',
                ]) ?>

                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
*/ 
