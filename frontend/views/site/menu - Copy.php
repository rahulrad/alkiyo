<?php 
/*     $dependency = [
    'class' => 'yii\caching\DbDependency',
    'sql' => 'SELECT MAX(created) FROM categories',
];

if ($this->beginCache('menu-desktop', ['dependency' => $dependency])) { */
?>


 <div class="bottom-header">
        <nav class="navbar navbar-expand-md navbar-dark">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav mr-auto">
			  <?php 
			  $i=1;foreach($menus as $menu){
						$i++; 
                        $subMenus = $menu->getChildMenus('sub_menu',$menu->id);
						$string = str_replace('.png','',str_replace('uploads/menus/','',$menu->image));    
                  ?> 
              <li class="nav-item dropdown">
					        <a href="#" class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown">
								<?php echo $menu->title; ?></i> </strong> 
							</a>

				 <div class="dropdown-menu dropdown-upper">
                    <ul>
                     <?php foreach($subMenus as $sMenu){
                            $customMenus = $menu->getChildMenus('custom_menu',$sMenu->id);
                            $total_custom_menus = count($customMenus );
                       ?>
					   <?php if($total_custom_menus<1){ ?>
					    <li>
							<a class="dropdown-item" href="#"><?php echo $sMenu->title; ?></a>
						</li> 
					   <?php }else{ ?>
							<li class="sub-menu">
							<a class="dropdown-item dropdown-toggle dropdown-sub" href="#" id="navbardrop" data-toggle="dropdown"><?php echo $sMenu->title; ?></a>
							<div class="dropdown-menu dropdown-mid">
							  <ul class="pl-0">
							   <?php for($i=1;$i<=$total_custom_menus; $i++){
										  $cMenu =  $customMenus[$i-1] ;?>
								<li>
								  <a class="dropdown-item" href="<?=\yii\helpers\Url::to(['prettyurl/index','slug'=>$cMenu->slug])?>"><?=$cMenu->title; ?></a>
								</li>
									<?php } ?>
								</ul>
							</div>
						  </li>
						<?php } } ?>
					</ul>
				</div>
			</li>
		<?php } ?>	
            </ul>
            <a class="my-2 my-lg-0 category-link" href="<?=\yii\helpers\Url::to(['product/collection']);?>">Alkiyo Category</a>
          </div>  
        </nav>
      </div>




<?php 
 // $this->endCache();
?>