<?php 
$searchTerm = \yii\helpers\Html::encode($term);
$this->title = $searchTerm;
$this->registerCssFile("/css/jquery.mCustomScrollbar.css");
?>
<style type="text/css">
  #loading {
   width: 50%;
   height: 80%;
   top: 150px;
   left: 300px;
   position: fixed;
   display: block;
   opacity: 0.7;
   background-color: #fff;
   z-index: 99;
   text-align: center;
}
#loading-content img{margin-top:40%}
.hide{
  display: none;
}
ul.featureList , ul.brandList{
   list-style-type: none;  
}
</style>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item"  href="/"> <span itemprop="name"><i class="fa fa-home"></i> </span></a> ><meta itemprop="position" content="1" /></li>
        <li  itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active"> <span itemprop="name"> <?=$searchTerm?>  </span> <meta itemprop="position" content="2" /> </li>
      </ol>
      
    </div>
  </div>
  <input type="hidden" name="pageNumber" id="pageNumber" value="<?=$pageNumber?>" />
  <section class="innerpage ">
    <div class="row">
      
      
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padd-l7 caregori_podectblog right-block">
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 advertise-right" style="float:right">
      	    <div class="advertise"> <img alt="" class="img-responsive" src="/images/advertise/ADD.jpg"> </div>
     	  </div>
     	<div class="div col-lg-9 col-md-9 col-sm-9 col-xs-12 catgories-lisitng">
        <section class="product-section">
		  <div class="row visible-xs">
              <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
              <ul class="mobile-view-menu">
              <li class="filter"><i class="fa fa-filter"></i> Filter </li>
                <li class="dropdown"> <a href="#" class="dropdown-toggle" id="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i class="fa fa-long-arrow-up"></i><i class="fa fa-long-arrow-down"></i> Short By </a>  
                
                <div class="dropdown-menu">
                
                <ul role="tablist" class="nav nav-tabs">
                     <li role="presentation" class="active"><a data-toggle="tab" role="tab" aria-controls="popular" href="javascript:void(0);" aria-expanded="false">Popularity</a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="discount" href="javascript:void(0);" aria-expanded="false">Discount</a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="price_low" href="javascript:void(0);" aria-expanded="false">Lowprice</a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="price_high" href="javascript:void(0);" aria-expanded="false">Highprice</a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="new" href="javascript:void(0);" aria-expanded="false">New Arrivals</a></li>
            </ul>
                    </div>
             
                
                </li>
               
              
              </ul>
              
              </div>
              </div>
     	  <!-- Tab Order Start Div -->
          <div class="row">
         
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tab-order">
            <h1 class="pull-left"> <span style="font-size:12px"><?=\Yii::$app->params['listingResults']?> out of <?=$total?></span></h1>
            
            <ul role="tablist" class="nav nav-tabs hidden-xs" id="sortParams">
              <li role="presentation" class="active"><a data-toggle="tab" role="tab" aria-controls="popular" href="javascript:void(0);" aria-expanded="false">Popularity</a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="discount" href="javascript:void(0);" aria-expanded="false">Discount</a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="price_low" href="javascript:void(0);" aria-expanded="false">Lowprice</a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="price_high" href="javascript:void(0);" aria-expanded="false">Highprice</a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="new" href="javascript:void(0);" aria-expanded="false">New</a></li>
            </ul>
            
             <span class="view-type">
             <span> View  </span>
              <a href="#" id="listView"> <i class="fa fa-list"></i></a>
              <a href="#" id="gridView" class="active">  <i class="fa  fa-th"></i> </a>
             </span>
            
            </div>
        </div>
          <!-- Tab Order Start Div End -->
        <!-- Product Row -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="tab-content "> <!-- tab-content -->
             <?php // products will get populated here... ?>
             <!--main View-->
             <div id="mainContentDiv" class="tab-pane active" role="tabpanel">
                 
                 <!--display:none listViewContents-->
                <div class="full-row no-columepadding" style="display:none" id="listViewContents">
                <?=$this->render('productListing', ['products' => $products])?>
                </div>
                <!-- display:none listViewContents --> 
                 
                <!--display:block gridViewContents --> 
                  <div class="full-row no-columepadding" style="display:block" id="gridViewContents">
                	<?=$this->render('@frontend/views/search/productGridListing', ['products' => $products])?>
                  </div>
                  <!--display:block gridViewContents--> 
                  
                 <div class="row">
                 	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="errMsg" style="display:none"></div>
                 </div>
                 <?php if($total > \Yii::$app->params['listingResults']):?>
                 <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a href="javascript:void(0);" class="btn btn-block btn-loadmore bgnone" id="viewMore"> View More <i class="fa fa-angle-down"></i> </a> </div>
                 </div>
                 <?php endif;?>
                 
             </div>
             <!--main View-->  
            </div> <!-- tab-content-->
            <input type="hidden" value="1" id="page" name="page">
          </div>
        </div>
        <!-- Product Row End -->
        </section>
        </div>
        </div>
      </div>
      
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd-r7 brand left-panel filter-in">
      <div class="filterbutton-row" style="display:none">
      
      <div class="col-xs-6 no-padd">
       <button type="button" class="btn btn-default btn-filter btn-block no-radius"> Cancel </button>
       </div>
      <div class="col-xs-6  no-padd">
  <button type="button" class="btn btn-danger btn-block  btn-filter no-radius"> Apply </button>
         </div>       
  
      </div>
      
          <h3 class="subtitle">Search Results: <?=$searchTerm?></h3>
          
          
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="price_renge_slider">
              <label for="amount">Min - Max</label>
              <p>
                <input type="text" style="border:0; color:#f6931f; font-weight:bold;" readonly id="amount">
              </p>
              <div id="slider-range"></div>
            </div>
          </div>
        </div>
        <section class="brand brand-block">
        <!-- Brand Search Box -->
        <div class="row">
          <div class="col-xs-12 logo text-center ">
            <h4 class="text-left">
            Categories <a href="#"> <span id="clear_brands"> Clear  </span></a>
            </h4>
            <div class="input-group searcbar hidden-xs" >
              <input type="text" class="form-control" name="txtCategories" id="txtCategories">
              <span class="input-group-addon "> <i class="fa fa-search"></i> </span>
            </div>
          </div>
        </div>
        <!-- Brands Listing -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="full-row left-scrollblock">
              <!-- Feature Item -->
              <?php foreach($categories as $category):?>
                  <div class="form-group">
                    <label  class="checkbox-inline" >
                        <input type="checkbox" name="category_ids[]" value="<?=$category['key']?>" class="categoryValues" <?=in_array($category['key'], $arrCategory)? 'checked="checked"' : '' ?> />
                         <?=$category['name']?> 
                    </label>
                    <span class="pull-right" id="categoryTotalItems_1"><?=$category['count'];?></span>
                  </div>
              <!-- Feature Item -->
              <?php endforeach;?>
            </div>
          </div>
        </div>
        </section>
       <!-- Brands -->
       <section class="brand brand-block">
        <!-- Brand Search Box -->
        <div class="row">
          <div class="col-xs-12 logo text-center ">
            <h4 class="text-left">
            Brands <a href="#"> <span id="clear_brands"> Clear  </span></a>
            </h4>
            <div class="input-group searcbar hidden-xs" >
              <input type="text" class="form-control" name="txtBrand" id="txtBrand">
              <span class="input-group-addon "> <i class="fa fa-search"></i> </span>
            </div>
          </div>
        </div>
        <!-- Brands Listing -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="full-row left-scrollblock " >
              <!-- Feature Item -->
           
              <?php foreach($brands as $brand):?>
                  <div class="form-group">
                    <label  class="checkbox-inline" >
                        <input type="checkbox" name="brand_ids[]" value="<?=$brand['key']?>" class="brandsValues">
                         <?=$brand['name']?> 
                    </label>
                    <span class="pull-right" id="brandTotalItems_1"><?=$brand['count'];?></span>
                  </div>
              <!-- Feature Item -->
              <?php endforeach;?>
                
            </div>
          </div>
        </div>
        </section>
        <!-- Brands -->
       <?php //frontend\components\SearchFilters::widget(['features' => $features])?>
       <!--gurup--> 
      </div>
        
    </div>
  </section><?=\frontend\components\RecentlyViewed::widget()?>
</div>
<div id="loading" class="hide">
  <div id="loading-content"><img src="/images/ajax-gif-load.gif" /></div>
</div>

<script type="text/javascript">
    var currentView = "list";
    var min_price = "<?=$min_price?>";
    var max_price = "<?=$max_price?>";
    var page=1;
    var isHash=false;
    var str_brand_ids=commaSepratedCategoryIds = '';
    var sort = 'popular';
    
    $(document).ready(function() {
    	
    	$( "#slider-range" ).slider({
    	      range: true,
    	      min: 0,
    	      max: max_price,
    	      values: [ <?=$min_price?>, <?=$max_price?> ],
    	      slide: function( event, ui ) {
    	        $( "#amount" ).val( "Rs." + ui.values[ 0 ] + " - Rs." + ui.values[ 1 ] );
    	      }
    	});
    	$( "#amount" ).val( "Rs." + $( "#slider-range" ).slider( "values", 0 ) +
    		      " - Rs." + $( "#slider-range" ).slider( "values", 1 ) );
    	$('#sortParams a').click(function(e) {
    		  $('#sortParams li.active').removeClass('active');
    		  var $this = $(this);
    		  if (!$this.parent('li').hasClass('active')) {
    		    $this.parent('li').addClass('active');
    		  }
    		  sort = $this.attr('aria-controls');
    		  ajaxSearch();
    		  e.preventDefault();
    	});
    <?php /*foreach($features as $feature):?>
        $('#txtFeature_<?=$feature['key'];?>').on('keyup', function () {
            var value = this.value;  
            $('#featureList_<?=$feature['key'];?> div').hide().each(function () {
                if ($(this).text().search( new RegExp(value, "i")) > -1) {
                     $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
        
        $("#clear_<?=$feature['key'];?>").click(function(e){
            e.preventDefault();
            $('#txtFeature_<?=$feature['key'];?>').val('');
            $('#txtFeature_<?=$feature['key'];?>').keyup();
            $('#featureList_<?=$feature['key'];?> .featureValues').attr('checked', false); 
        });
	<?php endforeach;*/?>
        $('#txtBrand').on('keyup', function () {
            var value = this.value;  
            $('#brandList li').hide().each(function () {
                if ($(this).text().search( new RegExp(value, "i")) > -1) {
                     $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
        
        $("#clear_brands").click(function(e){
            e.preventDefault();
            $('#txtBrand').val('');
             $('#txtBrand').keyup();
             $('#brandList .brandsValues').attr('checked', false); 
        });
        
        $("#clickBtn").click(function(e){
            e.preventDefault();
        });
        
        $("#listView").click(function(e){
            e.preventDefault();
            currentView = "list";
            $(this).toggleClass('active');
            $("#gridView").toggleClass('active');
            $("#listViewContents").show();
            $("#gridViewContents").hide();
        });

        $("#gridView").click(function(e){
            e.preventDefault();
            currentView = "grid";
            $(this).toggleClass('active');
            $("#listView").toggleClass('active');
            $("#listViewContents").hide();
            $("#gridViewContents").show();
           
        });
        
        $(".featureValues").click(function(e){
           	ajaxSearch();
           
        });
        
        $(".brandsValues").click(function(e){
           	ajaxSearch();
        });
        
        $(document).ajaxStart(function(){
        	$("#loading").removeClass('hide');
        }).ajaxStop(function(){
            $("#loading").addClass('hide');
        });
        $( "#slider-range" ).slider({
             change: function( event, ui ) {
             min_price = $( "#slider-range" ).slider("values")[0];
             max_price = $( "#slider-range" ).slider("values")[1];
             if(isHash===false)
				ajaxSearch();
             }
		});

        $(".categoryValues").on('click',function(){
        	//ajaxSearch();
        	var Values = getCommaSepratedCategoryIds();
        	url = '<?php echo \Yii::$app->getUrlManager()->createUrl('search/?term='.\Yii::$app->request->get('term')) ?>';
        	if(Values.length == 0)
            	window.location = url;
        	else
            	window.location = url + '&categoryIds=' + Values;
        });
        $('#viewMore').on('click', function(){
        	var pagenum = parseInt($("#pageNumber").val()) + 1;
        	addResults(pagenum);
        });
        
        /*$(window).scroll(function(){
//         	console.log($(window).scrollTop() );
//             console.log($('#mainContentDiv').height() );
//             console.log($(window).height());
            //console.log($('#mainContentDiv').height() - $(window).height());
        	if ($(window).scrollTop() == ( $('#mainContentDiv').height() - ($('.affix').height() - 50 ) ) ){
        	  //if($(".pagenum:last").val() <= $(".rowcount").val()) {
        	    var pagenum = parseInt($("#pageNumber").val()) + 1;
        	    addResults(pagenum);
        	  //}
        	}
        });*/
     }); // end of document.ready 

    function getCategoryIdsArray(){
        var values = [];
     	$(".categoryValues").each(function() {
             if($(this).prop('checked')==true) 
                values.push($(this).val());
         });
        
     	return values;
 	}
    function getCommaSepratedCategoryIds(){
        var values = getCategoryIdsArray();
        if(values.length > 0)
        	return values.join(',');
    	return '';
	}

    function getFeaturesIdsArray(){
        var values = [];
    	$(".featureValues").each(function() {
            if($(this).prop('checked')==true) 
               values.push($(this).val());
        });
    	return values;
	}
	function getCommaSepratedBrandValues(){
		var values = [];
		$(".brandsValues").each(function() {
	    	if($(this).prop('checked')==true) 
	        	values.push($(this).val() );
	    });

	    if(values.length > 0)
	    	return values.join(',');
    	return '';
	}

	function getFeatureValuesGetString(){
		features = getFeaturesIdsArray();
		getString = '';
		if(features.length > 0){
			for(var i=0; i < features.length; i++){
				getString += '&feature_ids[]=' +  features[i];
			}
		}

		return getString;
	}
	function addResults(pageNumber = 1){
		
		categoryIds = getCommaSepratedCategoryIds();
	    
	    /*if(str_brand_ids.length > 0){
	    	str_brand_ids = str_brand_ids.substr(0, (str_brand_ids.length-1)  )
		}*/
		str_brand_ids = getCommaSepratedBrandValues();
		
		data = {
	            'feature_ids[]' : getFeaturesIdsArray(),
	            min_price : min_price,
	            max_price : max_price,
	            page : pageNumber,
	            currentView : currentView,
	            brands: str_brand_ids,
	            categoryIds: categoryIds,
	            sort: sort
	        };
		$.ajax({
            url: '<?php echo \Yii::$app->getUrlManager()->createUrl('search/?term='.\Yii::$app->request->get('term')) ?>',
            type: 'GET',
            data: data,
            success: function(response) {
            	$("#listViewContents").append(response.listHtml);
                $("#gridViewContents").append(response.gridHtml);
                $('#pageNumber').val(response.pageNumber);
                $('h3.pull-left').html('<span style="font-size:12px">'+(response.listingResults*response.pageNumber)+' out of ' + response.total + '</span>');
                $("#pageNumber").val((response.pageNumber + 1));
		$("img.lazy").lazyload({
   				 effect : "fadeIn"
		});
            }
		});
	}

    function ajaxSearch(pageNumber = 1){
        //page=$("#page").val();
        //page++ ;
        page = pageNumber;
        str_brand_ids ="";
        categoryIds = getCommaSepratedCategoryIds();
        str_brand_ids = getCommaSepratedBrandValues();
        
        data = {
            'feature_ids[]' : getFeaturesIdsArray(),
            min_price : min_price,
            max_price : max_price,
            page : page,
            currentView : currentView,
            brands: str_brand_ids,
            categoryIds: categoryIds,
            sort:sort
            
        };
        window.history.pushState('object or string', 'Title', '<?php echo \Yii::$app->getUrlManager()->createUrl('search/?term='.\Yii::$app->request->get('term')) ?>&categoryIds='+categoryIds+'&brands=' +str_brand_ids+getFeatureValuesGetString());
	    $.ajax({
            url: '<?php echo \Yii::$app->getUrlManager()->createUrl('search/?term='.\Yii::$app->request->get('term')) ?>',
            type: 'GET',
             data: data,
             success: function(response) {
                 $("#listViewContents").html('');
                 $("#gridViewContents").html('');
                 $("#pageNumber").val(1);
                 $("#listViewContents").html(response.listHtml);
                 $("#gridViewContents").html(response.gridHtml);
                 $('h3.pull-left').html('<span style="font-size:12px">'+(response.listingResults*response.pageNumber)+' out of ' + response.total + '</span>');
             }
         });
    } // end of ajax search
</script>
