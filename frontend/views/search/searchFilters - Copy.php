<?php $script = '';?>
<?php foreach($features as $key => $feature):?>
<?php foreach($feature as $searchFilter):?>


			<div class="filter-section" id="accordion">
                  <a data-toggle="collapse" href="#finish">
                    <h4 class="filter-heading"><?=$searchFilter['name'] ?></h4>
                  </a>
				  <?php  if(count($searchFilter['values'])>5){?>
				    <div class="filter-search collapse" id="company" data-parent="#accordion">
				     <div class="input-group">
                      <input type="text" class="form-control searchfeatures"  placeholder="Search <?=$searchFilter['name'] ?>" name="txtFeature_<?=$searchFilter['key'] ?>" id="txtFeature_<?=$searchFilter['key'] ?>">
                      <div class="input-group-append">
                        <button class="btn filter-search-btn" type="button">
                          <i class="fa fa-search"></i>
                        </button>
                      </div>
                    </div>
					<?php } ?>
                    <div  id="featureList_<?=$searchFilter['key'] ?>" class="filter-checkbox mt-2 collapse" id="finish" data-parent="#accordion">
                     
                     <?php foreach($searchFilter['values'] as $feature_value):?>
							<?php if($feature_value['count']>0) {?>
							
							 <label class="alkiyo-checkbox"><?=$feature_value['name'];?> 
								<input type="checkbox" name="featureValues[]" value="<?=$searchFilter['key'].'_'.$feature_value['key'];?>" class="featureValues">
								<span class="checkmark"></span>
							</label>
							<?php } ?>
					<?php endforeach;?>
					  
                    </div>
				 <?php  if(count($searchFilter['values'])>5){ ?>
					</div>
					   <div class="checkbox-add">
							<button type="button"  id="brand" class="btn p-0 ml-4 more ">See More (5+)</button>
                      </div>
				<?php } ?>
				</div>
				
       <!-- Feature End -->
       <?php $script .= '$(\'#txtFeature_'.$searchFilter['key'].'\').on(\'keyup\', function () {
            var value = this.value;  
            $(\'#featureList_'.$searchFilter['key'].' label\').hide().each(function () {
                if ($(this).text().search( new RegExp(value, "i")) > -1) {
                     $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
        
        $("#clear_'.$searchFilter['key'].'").click(function(e){
            e.preventDefault();
            $(\'#txtFeature_'.$searchFilter['key'].'\').val(\'\');
            $(\'#txtFeature_'.$searchFilter['key'].'\').keyup();
            $(\'#featureList_'.$searchFilter['key'].' .featureValues\').attr(\'checked\', false); 
            ajaxSearch();
        });'; ?>
<?php endforeach;?>
<?php endforeach;
echo '<script type="text/javascript">$(document).ready(function(){
  '.$script.'
});</script>';
