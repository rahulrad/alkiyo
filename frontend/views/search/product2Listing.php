<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

                <div class="card-body p-0">
                    <div class="row">
					<?php foreach($products as $prodct){ ?>
						<?php $productImage = \frontend\helpers\NoImage::getProductImageFullPathFromElasticResult($prodct);?>
                      <div class="col-lg-6 col-md-6 col-sm-6 col-12 mb-3">
                        <div class="collection-item">
                            <div class="collection-item-img">
                              <a href="<?=\yii\helpers\Url::to(['product/index','slug'=>$prodct['slug'], 'category' => $category['slug']])?>">
                                <img src="<?=$productImage?>" >
                              </a>
                            </div>
                            <div class="collection-item-name">
                              <a href="<?=\yii\helpers\Url::to(['product/index','slug'=>$prodct['slug'], 'category' => $category['slug']])?>"><h3><?php echo substr($prodct['product_name'],0,35); if(strlen($prodct['product_name'])>35){ echo '...'; } ?></h3></a>
                              <p class="modal-name"><?php echo $prodct['model_number']; ?></p>
                              <span class="price">₹ <?php echo $prodct['original_price']; ?></span>
                            </div>
                            <div class="compare-item">
                              <label class="alkiyo-checkbox">Add to compare  
                                  <input name="<?php echo $prodct['product_name'] ?>" slug="<?php echo $prodct['slug'] ?>" type="checkbox" class="js-add-to-cmpr">
                                  <span class="checkmark"></span>
                              </label>
                            </div>
                          </div>
                        </div>
					<?php } ?>
					
				<?php if ($total > $recordPerPage) : ?>

						  <div class="col-lg-9 col-md-8 mt-3"> <button class="btn btn-block btn-loadmore bgnone viewMore" > Show More <i class="fa fa-angle-right"></i> <i class="fa fa-angle-right"></i></button> </div>
						<?php endif; ?>
                    </div>
                </div>
      