<?php $categories = \common\models\Categories::getHomePageCategories();?>
<?php

  $slugarray=array('mobiles'=>'all-mobiles','laptops'=>'all-laptops','televisions'=>'all-televisions','air-conditioners'=>'air-conditioners');

 foreach ($categories as $category) {

$products =  $category->getProductsForHomePage();
	if(count($products)):
  if(array_key_exists($category->slug,$slugarray))
  $sluglink=$slugarray[$category->slug];
  else
  $sluglink=$category->slug;
?>
  <div class="container-fluid no-padding shoptrading" >
          <div class="section-header  deal  dealofthe-day">
               <h3 itemprop="name"> <?php echo $category->category_name;?> </h3> 
              <a href="<?=\yii\helpers\Url::to(['prettyurl/index','slug'=>$sluglink])?>"> View All </a>
                <div class="clearfix"></div>
          </div>
          <div class="tab-content">
                         <?php
            //foreach($rsTopRatedBrands as $brand){
               //$brand =  \common\models\Brands::find()->where(['brand_id' => $topBrand['brands_brand_id'] ])->one();
               ?> 
              <div role="tabpanel" id="<?php echo $category->category_id;?>">
              <div class="panel-body dealrow">
                <div class="row">
                <div class="owl-carousel">
                <?php  //$products =  $category->getProducts()->orderBy('rating DESC')->limit(6)->all();
                //$products =  $category->getProducts()->orderBy('rating DESC')->limit(6)->all();
                $i = 0;
                foreach($products as $product){
                	$productName = strlen($product->product_name) > 25 ? substr($product->product_name,0, 25). '...' : $product->product_name;?>
                     <div class="item"    itemscope="" itemtype="http://schema.org/Product">
				<meta itemprop="name"  content="<?=$product->product_name?>"/>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tradingbox" itemprop="offers"   itemscope="" itemtype="http://schema.org/AggregateOffer">

<meta value="<?php echo $product->lowest_price; ?>"  itemprop="lowPrice" content="<?php echo $product->lowest_price; ?>"/>
<meta itemprop="priceCurrency" content="INR" value="INR"/>
                    <div class="full-row  mobile text-center" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"> 

<meta itemprop="priceCurrency" content="INR" value="INR"/>
                   
                    <!-- <a href="<?php  echo Yii::$app->getUrlManager()->getBaseUrl();?>product/<?php echo $product->slug; ?>"><figure> <img itemprop="image" alt="" src="/uploads/products/mobiles/apple/150x200/14714311812.jpg"> </figure> -->
                    <a title="<?=$product->product_name?>" href="<?=\yii\helpers\Url::to(['product/index','slug'=>$product->slug, 'category' => $product->category->slug])?>"><figure> <img itemprop="image" alt="" src="/uploads/products/mobiles/apple/150x200/14714311812.jpg"> </figure>
                      <p  class="discription"> <?php echo $productName; ?></p>
                      <div class="starbase">
                        <div class="star-rating" style="width:<?php echo $product->rating*20?>%"></div>
                        </div>
                        <!-- <p class="price"> <span><?=empty($product->lowest_price) ? '' : '<span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span> ' . number_format($product->lowest_price,2)?> </span> <strong><?=empty($product->lowest_price) ? '' : '<span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span> ' . number_format($product->lowest_price,2)?> </strong> -->
                      
                         <h3  content="Fill Product Price"> <span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span><?=number_format(floor($product['lowest_price']),0)?></h3>
	        			<?php if((int)$product->lowest_price > 0):?>  
                        <p class="stroes">  <?php echo count($product->suppliersProducts);?> Stores at <?php echo number_format($product->lowest_price,0);?> </p> 
                        <?php endif;?>
                      </a> 
                    </div>
                  </div>
                  </div><?php //$i++;
                  } ?>

                  
                  
                </div>
                </div>
              </div>
            </div><!-- end samsung -->
            <?php //} // end of brand loop ?>  
       
    </div>
                </div>
    <?php 
    endif;
}?>
