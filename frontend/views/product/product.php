
<?php
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\helpers\NoImage;
use yii\widgets\ActiveForm;
$brandSlug =   $product->brand->folder_slug;
$categorySlug = $product->category->folder_slug;
$imagePreset = $product->category->image_scrap_preset;
$this->title = $product->product_name;

/*  print_r($similarProducts[0]['category'][0]['folder_slug']);
die();  */

?>
<main>
<!-- Top Categories start -->
<section class="bg-white pb-4">
    <div class="container-fluid">
      <div class="wapper">
        <div class="wapper-head border-0">
          <div class="row">

            <div class="col-md-12">
              <nav class="breadcrumb-nav">
                  <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li> / </li>
                    <li class="breadcrumb-item"><a href="<?=\yii\helpers\Url::to(['product/collection']);?>">Category</a></li>
                    <li> / </li>
                    <li class="breadcrumb-item"><a href="<?=\yii\helpers\Url::to(['product/childcollection','slug'=>$product->category->slug])?>"><?php echo $product->category->category_name; ?></a></li>
                    <li> / </li>
                    <li class="breadcrumb-item active"><?php echo $product['product_name']; ?></li>
                  </ul>
                </nav>
            </div>
          </div>
        </div>
        <div class="wapper-body mt-3">
          <div class="alkiyo-product-detail">
            <div class="row">
              <div class="col-lg-6">
                <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:646px;height:453px;overflow:hidden;visibility:hidden;background-color:#ffffff;">
        <!-- Loading Screen -->
				<div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
					<img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="images/spin.svg" />
				</div>
				<div data-u="slides" style="cursor:default;position:relative;top:0px;left:94px;width:552px;height:453px;overflow:hidden;">
								<?php 
									foreach($product->productImages as $product_image):
									$arrayImag = explode(".",$product_image->image);
								?>
									<div>
										<img data-u="image" src="<?php echo "/uploads/products/".$categorySlug."/".$brandSlug."/".$arrayImag[0]."/".$product_image->image ;?>" />
										<img data-u="thumb" src="<?php echo "/uploads/products/".$categorySlug."/".$brandSlug."/".$arrayImag[0]."/".$product_image->image ;?>" />
									</div>		
							<?php endforeach;?>
				   

			
				</div>
		
				<a data-scale="0" href="https://www.jssor.com" style="display:none;position:absolute;">image gallery</a>
				<!-- Thumbnail Navigator -->
				<div data-u="thumbnavigator" class="jssort101" style="position:absolute;left:0px;top:0px;width:84px;height:453px;background-color:#ffffff;" data-autocenter="2" data-scale-left="0.75">
					<div data-u="slides">
						<div data-u="prototype" class="p" style="width:84px;height:84px;">
							<div data-u="thumbnailtemplate" class="t"></div>
							<svg viewbox="0 0 16000 16000" class="cv">
								<circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
								<line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
								<line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
							</svg>
						</div>
					</div>
				</div>
        <!-- Arrow Navigator -->
       <!-- <div data-u="arrowleft" class="jssora093" style="width:30px;height:30px;top:0px;left:100px;" data-autocenter="2">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="c" cx="8000" cy="8000" r="5920"></circle>
                <polyline class="a" points="7777.8,6080 5857.8,8000 7777.8,9920 "></polyline>
                <line class="a" x1="10142.2" y1="8000" x2="5857.8" y2="8000"></line>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora093" style="width:30px;height:30px;top:0px;right:15px;" data-autocenter="2">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="c" cx="8000" cy="8000" r="5920"></circle>
                <polyline class="a" points="8222.2,6080 10142.2,8000 8222.2,9920 "></polyline>
                <line class="a" x1="5857.8" y1="8000" x2="10142.2" y2="8000"></line>
            </svg>
        </div>-->
		</div>
      </div>
			 <div class="col-lg-5">
                <div class="product-overviewdetail">
                  <div class="overviewdetail-heading">
				        	<input type="hidden" id="userid" value="<?php echo $userId; ?>">
				        	<input type="hidden" id="prodid" value="<?php echo $product['product_id']; ?>">
				        	<input type="hidden" id="catid" value="<?php echo $product['categories_category_id']; ?>">
				        	<input type="hidden" id="brandid" value="<?php echo $product['brands_brand_id']; ?>">
                    <span class="company-name"><?php echo $product['brand']['brand_name']; ?></span>
                    <h2><?php echo $product['product_name']; ?></h2>
                    <span class="modal-name">(<?php echo $product['model_number']; ?>)</span>
                  </div>
                  <div class="overviewdetail-price">
                    <p><i class="fa fa-inr"></i> <?php echo $product['original_price']; ?> MRP</p>
                    <span class="info tooltip">
                      <a href="javascript:;">
                        <i class="fa fa-info" aria-hidden="true"></i>
                      </a>
                    <span class="tooltiptext"><?php echo $product['price']; ?></span>
                    </span>
                  </div>
                  <div class="select-color">
                    <h5>Color</h5>
                    <nav>
                      <ul>
					  <?php   
							$arrColor = array();
							if(!empty($product['colors']))
							{
								$arrColor = explode('##',$product['colors']);
							}
							foreach($arrColor as $colorval){
						?>
						 <li class="active">
                          <a href="#" class="active">
                            <img src="<?php echo Url::base()."/uploads/products/colors/".$product['product_id']."-".$colorval; ?>" alt="">
                            <span>Chrome</span>
                          </a>
                        </li>
						<?php } ?>
                      </ul>
                    </nav>
                  </div>
                  <div class="overviewdetail-content">
                    <p><?php echo substr($product['product_description'],0, 200);  ?> 
                      <a href="#" id="desc"> See More</a></p>
                  </div>
                  <div class="search-dealer">
                    <h5>Find Dealer</h5>
                    <div class="row">
                      <div class="col-lg-5">
                        <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="City/Pincode" id="pinCode">
                          <div class="input-group-append">
                            <button class="btn" type="submit" id="findShowrooms"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="overviewdetail-btn">
                    <button  class="btn border-blue btn-lg mb-2" id="whislist">Add to wish list</button>
                    <a href="<?=yii\helpers\Url::to(['product/compare',]).'?products[]=' . $product->slug; ?>"  class="btn btn-border btn-lg mb-2">Add to compare</a>
                  </div>
                </div>
              </div>
            </div>

          <div class="container" id="desc">  
            <div class="row">
              <div class="col-lg-12" >
                <div class="product-description" name="product-description">
                  <h2>Product Description</h2>
                  <p><?php echo $product['product_description']; ?></p>
                </div>
              </div>
			<?php if(!empty($product['care_instruction'])){ ?>
              <div class="col-lg-12">
                <div class="product-description">
                  <h2>Care Instruction</h2>
                  <p><?php echo $product['care_instruction']; ?></p> 
                </div>
              </div>
		  <?php } ?>
            </div>
			
			<?php if(!empty($similarProducts)){ ?>
			
              <div class="row pb-3 pt-3">
                <div class="col-lg-6">
                  <div class="card-heading p-0">                   
                    <h3>Similar Product</h3>
                  </div>
                </div>    
                <div class="col-lg-6 text-right">
                    <a href="<?=\yii\helpers\Url::to(['product/childcollection','slug'=>$product->category->slug])?>" class="btn btn-border btn-lg view-more">View More</a>
                </div>   
              </div>

            <div class="row">
			<?php  foreach($similarProducts as $smi){ 
			
			?>
                <div class="col-lg-2 col-md-4 col-sm-6 col-12 mb-3">
                  <div class="collection-item">
                      <div class="collection-item-img">
                        <a href="<?=\yii\helpers\Url::to(['product/index', 'slug' =>$smi['slug'], 'category' => $smi['category'][0]['slug']])?>">
                          <!--<img src="<?=empty($smi->productImages[0]->image_path) ? '' : $smi->productImages[0]->image_path?>">-->
								<img src="<?php $imageNameArray=explode('.',$smi['image']);
								echo "/uploads/products/".$smi['category'][0]['folder_slug']."/".$smi['brand'][0]['folder_slug']."/".$imageNameArray[0]."/".$smi['image']; ?>" >					
                        </a>
                      </div>
                      <div class="collection-item-name">
                        <a href="<?=\yii\helpers\Url::to([ 'product/index', 'slug' => $smi['slug'], 'category' => $smi['category'][0]['slug']])?>"><h3><?php echo $smi['product_name']; ?></h3></a>
                        <p class="modal-name"><?php echo $smi['model_number']; ?></p>
                        <span class="price">₹ <?php echo $smi['original_price']; ?></span>
                      </div>
                      
                    </div>
                  </div>
					<?php } ?>
             </div>
			<?php } ?>

          </div>

          </div>  
        </div>

      </div>
    </div>
  </section>
<!-- Top Categories end -->

</main>


<!-- product-detail modal -->
<div class="modal" id="showroom-location">
    <div class="modal-dialog max-w-480">
      <div class="modal-content modal-forgot-password">
        <!-- Modal Header -->
        <div class="modal-header border-bottom-0 pb-0">
          <button type="button" class="close change-password-remove" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <h2>Fill information for get showroom<br> location</h2>
          <form>
            <div class="row">
              <div class="col-lg-12">
                  <div class="form-group">
                      <input type="text" class="form-control login-input" id="name" placeholder="Name">
                    </div>
              </div>
              <div class="col-lg-12">
                  <div class="form-group resend-btn-position">
                      <input type="text" class="form-control login-input" maxlength="10" id="mobileno" placeholder="Mobile No.">
                      <button type="button" id="sendOtp" class="resend-btn">Done</button>
                  </div>
              </div>
              <div class="col-lg-6 resend" style="display:none;">
                  <div class="form-group resend-btn-position resend-otp">
                      <input type="text"  id="otp" class="form-control login-input" placeholder="Please enter OTP" >
                      <button type="button" id="resendOtp" class="resend-btn">Resend</button>
                  </div>
              </div>
            </div>
            <button type="button" id="findShowroom" class="btn btn-grey">Find Showroom</button>  
          </form>
        </div>
      </div>
    </div>
</div>

<!-- showerm location password modal -->
<div class="modal" id="fill-location">
    <div class="modal-dialog modal-md">
      <div class="modal-content modal-forgot-password">
        <!-- Modal Header -->
        <div class="modal-header border-bottom-0 pb-0 ">
          <button type="button" class="close change-password-remove" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <h2>Fill information for get showroom<br> location</h2>
         <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action' => Yii::$app->urlManager->createUrl(['showroom/findShowrooms'])]); ?>
            <div class="row">
              <div class="col-lg-12">
                  <div class="form-group resend-btn-position">
                      <input type="text" id="pincodeshow" name="pincode" class="form-control login-input" placeholder="Pin Code" readonly >
                      <!--<button type="button" class="resend-btn">Edit</button>-->
                  </div>
              </div>
              <div class="col-lg-12">
                  <div class="form-group">
                      <input type="text" id="nameshow" name="name" class="form-control login-input" placeholder="Name" readonly >
                    </div>
              </div>
              <div class="col-lg-12">
                  <div class="form-group resend-btn-position">
                      <input type="text"  id="mobileshow" name="mobile" class="form-control login-input" placeholder="Mobile No." readonly >
                      <!--<button type="button" class="resend-btn">Edit</button>-->
                  </div>
              </div>

              <div class="col-lg-12">
                  <div class="form-group">
                      <h3 class="blue-title mb-2">Construction status?</h3>
                      <div class="d-flex">
                        <label class="alkiyo-radio mr-5">Going on
                          <input type="radio" value="1" class="checkboxClass" checked="checked" name="radio">
                          <span class="checkmark"></span>
                        </label>
                        <label class="alkiyo-radio">Planning
                          <input type="radio" class="checkboxClass" value="2" name="radio">
                          <span class="checkmark"></span>
                        </label>
                      </div>
                  </div>
              </div>

              <div class="col-lg-5">
                  <div class="form-group">
                      <h3 class="blue-title mb-2">How many bathroom ?</h3>
                      <div>
                          <div class="form-group">
                              <select class="form-control" name="constru" id="sel1">
                                <option value=""></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
								
                              </select>
                            </div>
                      </div>
                  </div>
              </div>
				<input type="hidden" id="idshow" name="id">
            </div>
            <button type="submit"  class="btn btn-orange">Find Showroom</button>  
          <?php ActiveForm::end(); ?>
        </div>
      </div>
    </div>
</div>
<script src="/js/product.js"></script>
<script type="text/javascript">
$(function(){
	productData.callProduct('<?=Yii::$app->request->getCsrfToken()?>','<?=\Yii::$app->getUrlManager()->createUrl('wishlist/save'); ?>','<?=\Yii::$app->getUrlManager()->createUrl('showroom/findshowroom'); ?>','<?=\Yii::$app->getUrlManager()->createUrl('showroom/resendotp'); ?>','<?=\Yii::$app->getUrlManager()->createUrl('showroom/otpverify'); ?>');
});




</script>