<?php 
use yii\helpers\Html;
use yii\helpers\Url;
?>
<main>

<!-- Top Categories start -->
	<input type="hidden" name="category_id" value="<?=$comparasionProducts[0]->categories_category_id;?>" />
	<input type="hidden" name="products[]" id="bensonSearch4" value="<?=$comparasionProducts[0]->slug;?>" />
<section class="bg-white pb-4 pt-4">
    <div class="container-fluid">
      <div class="wapper">
        <div class="wapper-body">
          <div class="container">
            <div class="compare-list">

              <div class="table-responsive">
                <table class="table">
                  <tr>
                    <th class="bb-1"><h2>Product <br>Compare</h2></th>
					<?php $image = \frontend\helpers\NoImage::getProductImageFullPathFromObject($comparasionProducts[0])?>
						<td class="bb-1">
						  <div class="collection-item">
							<div class="collection-item-img product-remove">
							  <a>
								<img src="<?=$image?>">
								<button  class="btn btn-product-remove" id="removeCompareProduct4">x</button>
							  </a>
							</div>
							<div class="collection-item-name">
							  <a href="<?php echo \yii\helpers\Url::to(['product/index','slug'=>$comparasionProducts[0]->slug, 'category' =>$catslug]); ?>"><h3><?=$comparasionProducts[0]->product_name ?></h3></a>
							  <p class="modal-name"><?=$comparasionProducts[0]->model_number ?></p>
							  <span class="price"><?=$comparasionProducts[0]->original_price ?></span>
							</div>
							
						  </div>
						</td>
					<?php if( !empty($comparasionProducts[1])):
					$image = \frontend\helpers\NoImage::getProductImageFullPathFromObject($comparasionProducts[1]);
					?>
						<td class="bb-1">
						  <div class="collection-item">
							<div class="collection-item-img product-remove">
							  <a>
								<img src="<?=$image?>" >
								<button  class="btn btn-product-remove" id="removeCompareProduct1">x</button>
							  </a>
							</div>
							<div class="collection-item-name">
							  <a href="javascript:;"><h3><?=$comparasionProducts[1]->product_name ?></h3></a>
							  <p class="modal-name"><?=$comparasionProducts[1]->model_number ?></p>
							  <span class="price"><?=$comparasionProducts[1]->original_price ?></span>
							  <input type="hidden" name="products[]" id="bensonSearch1" value="<?=$comparasionProducts[1]->slug?>"/>
							</div>
						  </div>
						</td>				
					<?php
					else: ?>	
					<td class="bb-1">
                      <div class="collection-item">
                        <div class="collection-item-img product-remove h-265">
                        </div>
                        <div class="collection-item-name">
							 <input type="hidden" name="products[]" id="bensonSearch1" value=""/>
                                <div class="form-group mb-1">
                                    <select class="select2 form-control brand brand1"   placeholder="Brand Name">
									<option value="">Select Brand</option>
                                    <?php foreach($brandsdata as $brand){ ?>
									<option value="<?php echo $brand['brand_id']; ?>"><?php echo $brand['brand_name']; ?></option>
									<?php } ?>
                                    </select>
                                </div>
								 <div class="form-group mb-1">
								 
                                      <select class="select2 form-control btn-group searchproduct bensonSearch1" id="brand1"  placeholder="Product Name">
                                      
                                      </select>
                                </div>   
                        </div>
                      </div>
                    </td>
					<?php endif;?>
					
					<?php if( !empty($comparasionProducts[2])): 
					$image = \frontend\helpers\NoImage::getProductImageFullPathFromObject($comparasionProducts[2]);?>
						<td class="bb-1">
						  <div class="collection-item">
							<div class="collection-item-img product-remove">
							  <a>
								<img src="<?=$image?>">
								<button  class="btn btn-product-remove" id="removeCompareProduct2">x</button>
								
							  </a>
							</div>
							<input type="hidden" name="products[]" id="bensonSearch2" value="<?=$comparasionProducts[2]->slug?>"/>
							<div class="collection-item-name">
							  <a href="javascript:;"><h3><?=$comparasionProducts[2]->product_name ?></h3></a>
							  <p class="modal-name"><?=$comparasionProducts[2]->model_number ?></p>
							  <span class="price"><?=$comparasionProducts[2]->original_price ?></span>
							</div>
						  </div>
						</td>				
						<?php
					else: ?>
						<td class="bb-1">
                      <div class="collection-item">
                        <div class="collection-item-img product-remove h-265">
                        </div>
                        <div class="collection-item-name">
                        
                                <div class="form-group mb-1">
                                    <select class="select2 form-control brand brand2"   placeholder="Brand Name">
                                     <option value="">Select Brand</option>
                                    <?php foreach($brandsdata as $brands){ ?>
									<option value="<?php echo $brands['brand_id']; ?>"><?php echo $brands['brand_name']; ?></option>
									<?php } ?>
                                    </select>
                                  </div>
									 <div class="form-group mb-1">
									  <input type="hidden" name="products[]" id="bensonSearch2" value=""/>
                                      <select class="select2 form-control searchproduct bensonSearch2" id="brand2"   placeholder="Product Name">
                                      
                                      </select>
                                  </div> 
								
                        </div>
                      </div>
                    </td>
					<?php endif;?>
					
					<?php if(!empty($comparasionProducts[3])): 
						$image = \frontend\helpers\NoImage::getProductImageFullPathFromObject($comparasionProducts[3]);?>
						<td class="bb-1">
						  <div class="collection-item">
						  <input type="hidden" name="products[]" id="bensonSearch3" value="<?=$comparasionProducts[3]->slug?>"/>
							<div class="collection-item-img product-remove">
							  <a>
								<img src="<?=$image?>">
								<button  class="btn btn-product-remove" id="removeCompareProduct3">x</button>
							  </a>
							</div>
							<div class="collection-item-name">
							  <a href="javascript:;"><h3><?=$comparasionProducts[3]->product_name ?></h3></a>
							  <p class="modal-name"><?=$comparasionProducts[3]->model_number ?></p>
							  <span class="price"><?=$comparasionProducts[3]->original_price ?></span>
							</div>
						  </div>
						</td>				
						<?php
					else: ?>
						<td class="bb-1">
                      <div class="collection-item">
                        <div class="collection-item-img product-remove h-265">
                        </div>
                        <div class="collection-item-name">
                    
                                  <div class="form-group mb-1">
                                    <select class="select2 form-control brand brand3" name="products[]" id=""  placeholder="Brand Name">
                                     <option value="">Select Brand</option>
                                    <?php foreach($brandsdata as $brands){ ?>
									<option value="<?php echo $brands['brand_id']; ?>"><?php echo $brands['brand_name']; ?></option>
									<?php } ?>
                                    </select>
                                  </div>
								  <div class="form-group mb-1">
								   <input type="hidden" name="products[]" id="bensonSearch3" value=""/>
                                      <select class="select2 form-control searchproduct bensonSearch3" id="brand3"  placeholder="Product Name">
                                      
                                      </select>
                                  </div>                 
                        </div>
                      </div>
                    </td>
					<?php endif;?>
                  </tr>
				<?php foreach($productFeatureGroup as $featureGroup):
				
				?>
				<tr><th colspan="5" class="bb-1"><?=$featureGroup->name?></th>
				</tr>
					 <?php foreach($featureGroup->features as $feature):
						$firstProductFeature = !isset($productsFeatures[0]) ? '&nbsp;' :  (isset($productsFeatures[0][$feature->id]) ? $productsFeatures[0][$feature->id] :  'Not Available' );
					$secondProductFeature = !isset($productsFeatures[1]) ? '&nbsp;' :  (isset($productsFeatures[1][$feature->id]) ? $productsFeatures[1][$feature->id] :  'Not Available' );
					$thirdProductFeature = !isset($productsFeatures[2]) ? '&nbsp;' :  (isset($productsFeatures[2][$feature->id]) ? $productsFeatures[2][$feature->id] : 'Not Available' );
					$fourthProductFeature = !isset($productsFeatures[3]) ? '&nbsp;' :  (isset($productsFeatures[3][$feature->id]) ? $productsFeatures[3][$feature->id] : 'Not Available');
					
					
					  $featureHTML = <<<FeatureHtml
					  	<tr>
			<td class="bb-1">
			{$feature->name}
			</td>
			<td class="bb-1">
					{$firstProductFeature}
					</td>
			<td class="bb-1">
					{$secondProductFeature}
				</td>
			<td class="bb-1">
					{$thirdProductFeature}
					</td>
			<td class="bb-1">
					{$fourthProductFeature}
					</td>
		
			</tr>
FeatureHtml;
		echo $featureHTML;
					 ?>
					 <?php endforeach; ?>
				 <?php endforeach; ?>
                </table>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- Top Categories end -->
</main>
<?php $redirectUrl = \yii\helpers\Url::to(['product/compare']); ?>
<script src="/js/compare.js"></script>
<script>
 $(function(){
	compareData.callCompare('<?=Yii::$app->request->getCsrfToken()?>','<?=\yii\helpers\Url::to(['product/getproduct']); ?>','<?=\yii\helpers\Url::to(['product/compare']); ?>');
});
</script>