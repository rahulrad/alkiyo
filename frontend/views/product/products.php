<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>
<!-- Top Categories start -->
<section class="bg-white pb-4">
  <div class="container">
    <div class="wapper">
      <div class="wapper-head">
        <div class="row">

          <div class="col-md-12">
            <nav class="breadcrumb-nav">
              <ul class="breadcrumb pl-3">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li> / </li>
                <li class="breadcrumb-item"><a href="<?= \yii\helpers\Url::to(['product/collection']); ?>">Category</a></li>
                <li> / </li>
                <li class="breadcrumb-item active"><?php echo $category['category_name']; ?></li>
              </ul>
            </nav>
          </div>

          <div class="col-lg-6">
            <div class="card-heading">

              <h3>Our Category</h3>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="sort-input">
              <div class="form-group alkiyo-inputs">
                <label>Sort By:</label>
                <select class="form-control" id="sel1">
                  <option value=''>Recommended</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                </select>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="wapper-body">
        <div class="row">
          <div class="col-lg-3 col-md-4">
            <div class="filters">

              <div class="filter-section">
                <div class="clear d-flex justify-content-between">
                  <span class="clear-title">Filters</span>
                  <button type="button" id="clear_all" class="btn clear-link-btn p-0">Clear All</button>
                </div>
              </div>

              <div class="filter-section" id="accordion">
                <a data-toggle="collapse" href="#company">
                  <h4 class="filter-heading">Brands</h4>
                </a>
                <div class="filter-search collapse" id="company" data-parent="#accordion">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search brand" name="txtBrand" id="txtBrand">
                    <div class="input-group-append">
                      <button class="btn filter-search-btn" type="button">
                        <i class="fa fa-search"></i>
                      </button>
                    </div>
                  </div>


                  <div class="filter-checkbox mt-2 showbrand" id="showbrand">
                    <?php
                    if (count($brands) == 1) {
                      $i = 1;
                      $showmore = "false"; ?>
                      <?php foreach ($allBrandsArr as $keyb => $brand) :
                        
                      ?>

                          <label class="alkiyo-checkbox txtBrand"> <?= $brand ?>
                            <input type="checkbox" name="brand_ids[]" value="<?= $keyb ?>" class="brandsValues" <?= in_array($keyb, $arrBrands) ? 'checked="checked"' : '' ?>>
                            <span class="checkmark"></span>
                          </label>
                       
                      <?php endforeach; ?>
                    <?php   } else {
                      $i = 1;
                      $showmore = "false";
                    ?>
                      <?php foreach ($brands as $brand) :
                        
                      ?>
                          <?php if ($brand['count'] > 0) { ?>

                            <label class="alkiyo-checkbox txtBrand"> <?= $brand['name'] ?>
                              <input type="checkbox" name="brand_ids[]" value="<?= $brand['key'] ?>" class="brandsValues" <?= in_array($brand['key'], $arrBrands) ? 'checked="checked"' : '' ?>>
                              <span class="checkmark"></span>
                            </label>

                        <?php }
                         ?>
                      <?php endforeach; ?>
                    <?php } ?>
            
                  </div>




                </div>
              </div>
              <?= frontend\components\SearchFilters::widget(['features' => $features]) ?>
            </div>
          </div>
          <!--display:none 2Contents-->

          <div class="col-lg-9 col-md-8 mt-3" style="display:none" id="2grid">
            <?= $this->render('@frontend/views/search/product2Listing', ['products' => $products, 'category' => $category , 'total' => $total, 'recordPerPage' => $recordPerPage]) ?>
          </div>
          <!-- display:none listViewContents -->
          <!--display:none 3Contents-->

          <div class="col-lg-9 col-md-8 mt-3" style="display:none" id="3grid">
            <?= $this->render('@frontend/views/search/product3Listing', ['products' => $products, 'category' => $category , 'total' => $total, 'recordPerPage' => $recordPerPage]) ?>
          </div>
          <!-- display:none listViewContents -->

          <!--display:block grid4Contents -->

          <div class="col-lg-9 col-md-8 mt-3" id="4grid">
            <?= $this->render('@frontend/views/search/product4Listing', ['products' => $products, 'category' => $category, 'total' => $total, 'recordPerPage' => $recordPerPage]) ?>
          </div>
          <!--display:block gridViewContents-->

      
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Top Categories end -->



<!-- compare-box -->
<div class="compare-box cmpr-pnl sctn sctn--sdbr cmpr-pnl-list cmpr-pnl-list clearfix ui-front cmpr-pnl--bx-shdw" style="display:none;">
  <div class="compare-box-items">
    <div id="div1">
      <img src="<?= Yii::getAlias('@compareImage') ?>" />
      <h3 id="div1name">Enter Product Name</h3>
      <input hidden placeholder="Enter product name" required type="text" name="compproducts[]" placeholder="Search your items.." id="hugoSearch1" class="form-control hugoSearch">
      <button type="button" class="btn btn-compare-close">&times;</button>
    </div>
    <div id="div2">
      <img src="<?= Yii::getAlias('@compareImage') ?>" />
      <h3 id="div2name">Enter Product Name</h3>
      <input hidden placeholder="Enter product name" required type="text" name="compproducts[]" placeholder="Search your items.." id="hugoSearch2" class="form-control hugoSearch">
      <button type="button" class="btn btn-compare-close">&times;</button>
    </div>

    <div id="div3">
      <img src="<?= Yii::getAlias('@compareImage') ?>" />
      <h3 id="div3name">Enter Product Name</h3>
      <input hidden placeholder="Enter product name" required type="text" name="compproducts[]" placeholder="Search your items.." id="hugoSearch3" class="form-control hugoSearch">
      <button type="button" class="btn btn-compare-close">&times;</button>
    </div>
    <div id="div4">
      <img src="<?= Yii::getAlias('@compareImage') ?>" />
      <h3 id="div4name">Enter Product Name</h3>
      <input hidden placeholder="Enter product name" required type="text" name="compproducts[]" placeholder="Search your items.." id="hugoSearch4" class="form-control hugoSearch">
      <button type="button" class="btn btn-compare-close">&times;</button>
    </div>

  </div>
  <div class="compare-box-btn text-right">
    <button type="button" id="clearall" class="btn btn-border">Clear All</button>
    <button class="btn border-blue" id="compareproducts">Compare </button>
  </div>
</div>
<input type="hidden" name="pageNumber" id="pageNumber" value="1" />
<!--<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet">
</link>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>-->
<script src="/js/productFilter.js"></script>
<script>
  $(function() {
    // var pro = Object.create(product);
    // pro.seturlChildCollection('<?php echo \Yii::$app->getUrlManager()->createUrl('product/childcollection/' . \Yii::$app->request->get('slug')) ?>');
    // pro.setDataurlChildCollection('<?= \Yii::$app->getUrlManager()->createUrl('product/childcollection/' . \Yii::$app->request->get('slug')) ?>');
    // pro.setCmpImgUrl('<?= Yii::getAlias('@compareImage') ?>');
    // pro.init();

    const urlChild = '<?php echo \Yii::$app->getUrlManager()->createUrl('product/childcollection/' . \Yii::$app->request->get('slug')) ?>';
    const urlDataChild = '<?= \Yii::$app->getUrlManager()->createUrl('product/childcollection/' . \Yii::$app->request->get('slug')) ?>';
    const urlCmpImg = '<?= Yii::getAlias('@compareImage') ?>';
    const proFilter = new ProductFilter(urlChild, urlDataChild, urlCmpImg);
  });
</script>