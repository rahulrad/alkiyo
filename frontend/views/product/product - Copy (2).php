
<?php
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\helpers\NoImage;
use yii\widgets\ActiveForm;
$brandSlug = $product->brand->folder_slug;
$categorySlug = $product->category->folder_slug;
$imagePreset = $product->category->image_scrap_preset;
$this->title = $product->product_name;
?>
<main>
<!-- Top Categories start -->
<section class="bg-white pb-4">
    <div class="container-fluid">
      <div class="wapper">
        <div class="wapper-head border-0">
          <div class="row">

            <div class="col-md-12">
              <nav class="breadcrumb-nav">
                  <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li> / </li>
                    <li class="breadcrumb-item"><a href="<?=\yii\helpers\Url::to(['product/collection']);?>">Category</a></li>
                    <li> / </li>
                    <li class="breadcrumb-item"><a href="<?=\yii\helpers\Url::to(['product/childcollection','slug'=>$product->category->slug])?>"><?php echo $product->category->category_name; ?></a></li>
                    <li> / </li>
                    <li class="breadcrumb-item active"><?php echo $product['product_name']; ?></li>
                  </ul>
                </nav>
            </div>
          </div>
        </div>
        <div class="wapper-body mt-3">
          <div class="alkiyo-product-detail">
            <div class="row">

              <div class="col-lg-6">
			  <div id="home_main_image"></div>
				<div>
					<ul id="home_demo" class="home-thumbs">
						<li> <a href="1.jpg"><img src="1_thumb.jpg" alt="Alt 1" data-caption="Caption 1" data-href="demo.html"></a> </li>
						<li> <a href="2.jpg"><img src="2_thumb.jpg" alt="Alt 2" data-caption="Caption 1" data-href="README.md"></a> </li>
						<li> <a href="3.jpg"><img src="3_thumb.jpg" alt="Alt 3" data-caption="Caption 1" data-href="LICENSE.md"></a> </li>
						<li> <a href="4.jpg"><img src="4_thumb.jpg" alt="Alt 4" data-caption="Caption 4"></a> </li>
					</ul>
				</div>

			  
			  
			  
                <div class="alkiyo-product-image">
                    <img src="<?php  echo Url::base()."/".$product['image']; ?>" alt="" height="551" width="450" />
                </div>
                <nav class="product-image-list">
                  <ul>
                       <?php 
							foreach($product->productImages as $product_image):
                        ?>
					     <li><img   title="<?=$product->product_name; ?>" alt="<?=$product->product_name; ?> online price in India" src="<?=NoImage::		getProductImageFullPathFromBrandCategorySlug($brandSlug,$categorySlug, $product_image, 'full',$imagePreset,'product')?>" height="84" width="84"> </li>
					  <?php endforeach;?>
                  </ul>
                </nav>
				
				
				
              </div>

              <div class="col-lg-5">
                <div class="product-overviewdetail">
                  <div class="overviewdetail-heading">
				        	<input type="hidden" id="userid" value="<?php echo $userId; ?>">
				        	<input type="hidden" id="prodid" value="<?php echo $product['product_id']; ?>">
				        	<input type="hidden" id="catid" value="<?php echo $product['categories_category_id']; ?>">
				        	<input type="hidden" id="brandid" value="<?php echo $product['brands_brand_id']; ?>">
                    <span class="company-name"><?php echo $product['brand']['brand_name']; ?></span>
                    <h2><?php echo $product['product_name']; ?></h2>
                    <span class="modal-name">(<?php echo $product['model_number']; ?>)</span>
                  </div>
                  <div class="overviewdetail-price">
                    <p><i class="fa fa-inr"></i> <?php echo $product['original_price']; ?> MRP</p>
                    <span class="info tooltip">
                      <a href="javascript:;">
                        <i class="fa fa-info" aria-hidden="true"></i>
                      </a>
                    <span class="tooltiptext"><?php echo $product['price']; ?></span>
                    </span>
                  </div>
                  <div class="select-color">
                    <h5>Color</h5>
                    <nav>
                      <ul>
					  <?php   
							$arrColor = array();
							if(!empty($product['colors']))
							{
								$arrColor = explode('##',$product['colors']);
							}
							foreach($arrColor as $colorval){
						?>
						 <li class="active">
                          <a href="#" class="active">
                            <img src="<?php echo Url::base()."/".$product['product_id']."-".$colorval; ?>" alt="">
                            <span>Chrome</span>
                          </a>
                        </li>
						<?php } ?>
                      </ul>
                    </nav>
                  </div>
                  <div class="overviewdetail-content">
                    <p><?php echo substr($product['product_description'],0, 200);  ?> 
                      <a href="#" id="desc"> See More</a></p>
                  </div>
                  <div class="search-dealer">
                    <h5>Find Dealer</h5>
                    <div class="row">
                      <div class="col-lg-5">
                        <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="City/Pincode" id="pinCode">
                          <div class="input-group-append">
                            <button class="btn" type="submit" id="findShowrooms"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="overviewdetail-btn">
                    <button  class="btn border-blue btn-lg mb-2" id="whislist">Add to wish list</button>
                    <a href="<?=yii\helpers\Url::to(['product/compare',]).'?products[]=' . $product->slug; ?>"  class="btn btn-border btn-lg mb-2">Add to compare</a>
                  </div>
                </div>
              </div>
            </div>

          <div class="container" id="desc">  
            <div class="row">
              <div class="col-lg-12" >
                <div class="product-description" name="product-description">
                  <h2>Product Description</h2>
                  <p><?php echo $product['product_description']; ?></p>
                </div>
              </div>
			<?php if(!empty($product['care_instruction'])){ ?>
              <div class="col-lg-12">
                <div class="product-description">
                  <h2>Care Instruction</h2>
                  <p><?php echo $product['care_instruction']; ?></p> 
                </div>
              </div>
		  <?php } ?>
            </div>
			
			<?php if(!empty($similarProducts)){ ?>
			
              <div class="row pb-3 pt-3">
                <div class="col-lg-6">
                  <div class="card-heading p-0">                   
                    <h3>Similar Product</h3>
                  </div>
                </div>    
                <div class="col-lg-6 text-right">
                    <a href="<?=\yii\helpers\Url::to(['product/childcollection','slug'=>$product->category->slug])?>" class="btn btn-border btn-lg view-more">View More</a>
                </div>   
              </div>

            <div class="row">
			<?php  foreach($similarProducts as $smi){ ?>
                <div class="col-lg-2 col-md-4 col-sm-6 col-12 mb-3">
                  <div class="collection-item">
                      <div class="collection-item-img">
                        <a href="<?=\yii\helpers\Url::to([ 'product/index', 'slug' => $smi->slug, 'category' => $smi->category->slug ])?>">
                          <img src="<?php  echo Url::base()."/".$smi->image; ?>">
                        </a>
                      </div>
                      <div class="collection-item-name">
                        <a href="<?=\yii\helpers\Url::to([ 'product/index', 'slug' => $smi->slug, 'category' => $smi->category->slug ])?>"><h3><?php echo $smi->product_name; ?></h3></a>
                        <p class="modal-name"><?php echo $smi->model_number; ?></p>
                        <span class="price">₹ <?php echo $smi->original_price; ?></span>
                      </div>
                      
                    </div>
                  </div>
					<?php } ?>
             </div>
			<?php } ?>

          </div>

          </div>  
        </div>

      </div>
    </div>
  </section>
<!-- Top Categories end -->

</main>


<!-- product-detail modal -->
<div class="modal" id="showroom-location">
    <div class="modal-dialog max-w-480">
      <div class="modal-content modal-forgot-password">
        <!-- Modal Header -->
        <div class="modal-header border-bottom-0 pb-0">
          <button type="button" class="close change-password-remove" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <h2>Fill information for get showroom<br> location</h2>
          <form>
            <div class="row">
              <div class="col-lg-12">
                  <div class="form-group">
                      <input type="text" class="form-control login-input" id="name" placeholder="Name">
                    </div>
              </div>
              <div class="col-lg-12">
                  <div class="form-group resend-btn-position">
                      <input type="text" class="form-control login-input" maxlength="10" id="mobileno" placeholder="Mobile No.">
                      <button type="button" id="sendOtp" class="resend-btn">Done</button>
                  </div>
              </div>
              <div class="col-lg-6 resend" style="display:none;">
                  <div class="form-group resend-btn-position resend-otp">
                      <input type="text"  id="otp" class="form-control login-input" placeholder="Please enter OTP" >
                      <button type="button" id="resendOtp" class="resend-btn">Resend</button>
                  </div>
              </div>
            </div>
            <button type="button" id="findShowroom" class="btn btn-grey">Find Showroom</button>  
          </form>
        </div>
      </div>
    </div>
</div>

<!-- showerm location password modal -->
<div class="modal" id="fill-location">
    <div class="modal-dialog modal-md">
      <div class="modal-content modal-forgot-password">
        <!-- Modal Header -->
        <div class="modal-header border-bottom-0 pb-0 ">
          <button type="button" class="close change-password-remove" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <h2>Fill information for get showroom<br> location</h2>
         <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action' => Yii::$app->urlManager->createUrl(['showroom/findShowrooms'])]); ?>
            <div class="row">
              <div class="col-lg-12">
                  <div class="form-group resend-btn-position">
                      <input type="text" id="pincodeshow" name="pincode" class="form-control login-input" placeholder="Pin Code" readonly >
                      <!--<button type="button" class="resend-btn">Edit</button>-->
                  </div>
              </div>
              <div class="col-lg-12">
                  <div class="form-group">
                      <input type="text" id="nameshow" name="name" class="form-control login-input" placeholder="Name" readonly >
                    </div>
              </div>
              <div class="col-lg-12">
                  <div class="form-group resend-btn-position">
                      <input type="text"  id="mobileshow" name="mobile" class="form-control login-input" placeholder="Mobile No." readonly >
                      <!--<button type="button" class="resend-btn">Edit</button>-->
                  </div>
              </div>

              <div class="col-lg-12">
                  <div class="form-group">
                      <h3 class="blue-title mb-2">Construction status?</h3>
                      <div class="d-flex">
                        <label class="alkiyo-radio mr-5">Going on
                          <input type="radio" value="1" class="checkboxClass" checked="checked" name="radio">
                          <span class="checkmark"></span>
                        </label>
                        <label class="alkiyo-radio">Planning
                          <input type="radio" class="checkboxClass" value="2" name="radio">
                          <span class="checkmark"></span>
                        </label>
                      </div>
                  </div>
              </div>

              <div class="col-lg-5">
                  <div class="form-group">
                      <h3 class="blue-title mb-2">How many bathroom ?</h3>
                      <div>
                          <div class="form-group">
                              <select class="form-control" name="constru" id="sel1">
                                <option value=""></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
								
                              </select>
                            </div>
                      </div>
                  </div>
              </div>
				<input type="hidden" id="idshow" name="id">
            </div>
            <button type="submit"  class="btn btn-orange">Find Showroom</button>  
          <?php ActiveForm::end(); ?>
        </div>
      </div>
    </div>
</div>
<script src="/js/product.js"></script>
<script type="text/javascript">
$(function(){
	productData.callProduct('<?=Yii::$app->request->getCsrfToken()?>','<?=\Yii::$app->getUrlManager()->createUrl('wishlist/save'); ?>','<?=\Yii::$app->getUrlManager()->createUrl('showroom/findshowroom'); ?>','<?=\Yii::$app->getUrlManager()->createUrl('showroom/resendotp'); ?>','<?=\Yii::$app->getUrlManager()->createUrl('showroom/otpverify'); ?>');
});

$(function() {
$('#home_demo').desoSlide({
  mainImage:'#home_main_image',
  insertion:'replace'
});
});
</script>