<?php 
use yii\helpers\Html;
use yii\helpers\Url;
?>
<main>
<!-- Top Categories start -->
<section class="featured-products mb-4 mt-4">
    <div class="container">
      <div class="card pt-4">
        <div class="card-heading">

          <nav class="breadcrumb-nav">
            <ul class="breadcrumb">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li> / </li>
              <li class="breadcrumb-item active">Category</li>
            </ul>
          </nav>

          <h3>Our Category</h3>
        </div>
          <div class="card-body">
            <div class="row">
			<?php foreach($categories as $category){ ?>
              <div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-3">
                <a class="category-products-link" href="<?=\yii\helpers\Url::to(['product/childcollection','slug'=>$category->slug])?>">
                  <div class="category-products-detail">
                    <div class="category-products-img">
                      <img src="<?php echo Url::base(); ?>/<?php echo $category['image']; ?>" draggable="false" >
                    </div>
                    <div class="category-products-name">
                      <h3><?php echo $category['category_name']?></h3>
                    </div>
                  </div>
                </a>
              </div>
			<?php } ?>

            </div>
          </div>
      </div>
    </div>
  </section>
<!-- Top Categories end -->

</main>

