<?php  //print_r($showrooms); die(); ?>

<main>

<!-- Top Categories start -->
<section class="bg-white pb-4 pt-4">
    <div class="container-fluid">
      <div class="wapper">
        <div class="wapper-body">
          <div class="container">
            
            <div class="find-heading">
              <h2>FIND A SHOWROOOM</h2>
            </div>

            <div class="find-btns">
              <div class="row">
                <div class="col-lg-6">
                  <h3>Below is the authorised dealer for <?php $i=1; foreach($showrooms as $showval){ if($i !=1 && !empty($showval['brand_name'])){ echo ",".$showval['brand_name']; }else if(!empty($showval['brand_name'])){ echo $showval['brand_name']; } $i++;} ?>.</h3>
                  <p>Total number of results: <?php echo count($showrooms); ?></p>
                </div>
                <div class="col-lg-6">
                  <div class="text-right">
                    <button type="button" class="btn btn-border" data-toggle="modal" data-target="#mail">Mail Me</button>
                    <button type="button" class="btn border-blue" id="print">Print</button>
                  </div>
                </div>
              </div>
            </div>

            <div class="showers-list">
              <div class="row">
               <?php $i=0; foreach($showrooms as $showroomdetails){ $i++; ?>
                <div class="col-lg-5 mb-3">
                    <div class="showers-list-item">
                      <span class="showers-count"><?php echo $i; ?></span>
                      <div class="showers-address">
                        <h4><?php echo $showroomdetails['name']; ?></h4>
                        <p><?php echo $showroomdetails['address']; ?><br><?php echo $showroomdetails['pincode']." "; ?><?php echo $showroomdetails['city']; ?></p>
                        <span><i class="fa fa-phone"></i><?php echo $showroomdetails['phone_no']; ?></span>
                        <span><i class="fa fa-envelope-o"></i> <?php echo $showroomdetails['email']; ?></span>
                      </div>
                    </div>
                </div>   
				<?php } ?>
              </div>
            </div>
			
			<div class="showers-list" id="showers-list" style="display:none;">
              
			  <table style="width:100%;padding: 20px; text-align:left; font-size:12px; no-repeat " border="solid">
				<tr><td colspan="4">
						<h3>FIND A SHOWROOOM</h3>
						<p>Total number of results: <?php echo count($showrooms); ?></p>
					</td>
				</tr>
			 <?php $i=0; foreach($showrooms as $showroomdetails){ $i++; 
			   
			   if($i%2 != 0){
			   ?>
                    <tr>
						<?php } ?><td><?php echo $i; ?></td>
                     
						<td> <h4><?php echo $showroomdetails['name']; ?></h4>
                        <p><?php echo $showroomdetails['address']; ?><br><?php echo $showroomdetails['pincode']." "; ?><?php echo $showroomdetails['city']; ?></p>
                        <span><i class="fa fa-phone"></i><?php echo $showroomdetails['phone_no']; ?></span>
                        <span><i class="fa fa-envelope-o"></i> <?php echo $showroomdetails['email']; ?></span>
						</td>
                     
					 <?php if($i%2 == 0){ ?>
					 </tr>
				<?php  } } ?>
				</table>
            
            </div>
			
			
			
			
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- Top Categories end -->

</main>
<script src="/js/findshowroom.js"></script>
<script>
$(function(){
	findShowroom.callFindShowroom('<?=Yii::$app->request->getCsrfToken()?>','<?=\Yii::$app->getUrlManager()->createUrl('showroom/sendmail'); ?>');
});
</script>

<div class="modal" id="mail">
    <div class="modal-dialog">
      <div class="modal-content modal-forgot-password">
  
        <!-- Modal Header -->
        <div class="modal-header border-bottom-0 pb-0 ">
          <button type="button" class="close change-password-remove" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- Modal body -->
        <div class="modal-body pl-4 pr-4">
          <form>
              <div class="row mt-4 mb-3">
                <div class="col-lg-12">
                      <p class="modal-heading text-blue">Email to</p>
                    </div>
                  <div class="col-lg-12">
                      <div class="form-group mb-0">
                          <label class="mb-0" for="exampleInputEmail1">Email id</label>
                          <input type="email" id="mailId" class="form-control login-input" placeholder="test@maaruji,com">
						  <span id="sendMailError" style="color:red;"></span>
                        </div>
                  </div>
                </div>
				<div class="text-right">
				  <button type="button" class="btn btn-orange btn-login" id="mailSend">Send</button>  
				</div>
          </form>
        </div>
      </div>
    </div>
  </div>