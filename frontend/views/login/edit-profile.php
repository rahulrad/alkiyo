<?php $this->title = "Edit Profile"; ?>
<main>
<!-- Top Categories start -->
<section class="bg-white pb-4 pt-4">
    <div class="container-fluid">
      <div class="wapper">
        <div class="wapper-body">
          <div class="container">
              <div class="login-detail d-block">
                  <div>
                    <div class="login-head mb-4">
                      <h2>Edit Profile</h2>
                    </div>
                    <div class="login-form">
                      <form>
                      
                        <div class="row">

                          <div class="col-lg-12">
                            <p class="modal-heading">Personal Information</p>
                          </div>

                          <div class="col-lg-4">
                              <div class="form-group mb-0">
                                  <label class="label-light" for="exampleInputEmail1">First Name</label>
                                  <input type="text" class="form-control login-input" value="<?php echo $user[0]['first_name']; ?>" readonly >
                                </div>
                          </div><div class="col-lg-4">
                              <div class="form-group mb-0">
                                  <label class="label-light" for="exampleInputEmail1">Last Name</label>
                                  <input type="text" class="form-control login-input" value="<?php echo $user[0]['last_name']; ?>" readonly >
                                </div>
                          </div>
                        </div>

                        <div class="row">

                          <div class="col-lg-4">
                              <div class="form-group mb-0">
                                  <label class="label-light" for="exampleInputEmail1">City</label>
                                  <input type="text" class="form-control login-input" value="<?php echo $user[0]['city']; ?>" readonly >
                              </div>
                          </div>
                        </div>

                        <div class="row mt-4">
                          <div class="col-lg-12">
                            <p class="modal-heading">Mobile Number</p>
                          </div>
                          <div class="col-lg-4">
                              <div class="form-group verify-btn-position mb-0 edit-number">
                                  <label class="label-light" for="exampleInputEmail1">Mobile No.</label>
                                  <input type="text" id="mobile" value="<?php echo $user[0]['mobile']; ?>"  maxlength="10" class="form-control login-input" readonly >
                                  <button type="button" class="verify-btn" id="editMobile">Edit</button>
                                  <button type="button" class="verify-btn d-none" data-toggle="modal" data-target="#verify-password">Verify</button>
                              </div>
                          </div>
                        </div>

                        <div class="row mt-4">
                          <div class="col-lg-12">
                              <p class="modal-heading">Email ID</p>
                            </div>
                          <div class="col-lg-4">
                              <div class="form-group mb-0">
                                  <label class="label-light" for="exampleInputEmail1">Email ID</label>
                                  <input type="email" value="<?php echo $user[0]['email']; ?>" class="form-control login-input" readonly >
                                </div>
                          </div>
                        </div>
                        <div class="d-flex justify-content-between btn-back-login">
                          <a  class="btn  pl-0" id="changePassword" data-toggle="modal" data-target="#forgotpassword" >Change Password</a>
                          <button type="button" id="updateProfile" class="btn btn-orange btn-login">Update Profile</button>  
                        </div>
                      
                      </form>
                    </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- Top Categories end -->

<script src="/js/profile.js"></script>
<script>
	  $(function(){
		profileData.callProfile('<?=Yii::$app->request->getCsrfToken()?>','<?=\Yii::$app->getUrlManager()->createUrl('login/mobileVerification'); ?>','<?=\Yii::$app->getUrlManager()->createUrl('login/profile-update')?>','<?=\Yii::$app->getUrlManager()->createUrl('login/sendOtp'); ?>','<?=\Yii::$app->getUrlManager()->createUrl('login/updatePassword'); ?>');
	  });
</script>
</main>


<!-- forgot password modal -->
<div class="modal" id="forgotpassword">
    <div class="modal-dialog">
      <div class="modal-content modal-forgot-password">
  
        <!-- Modal Header -->
        <div class="modal-header border-bottom-0 pb-0 ">
          <button type="button" class="close change-password-remove" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- Modal body -->
        <div class="modal-body">
          <h2>Change Password</h2>
          <form>
            <div class="form-group">
                <input type="password" id="txtNewPassword" class="form-control login-input" placeholder="Type new password">
             </div>
            <div class="form-group">
                <input type="password" id="txtConfirmPassword" class="form-control login-input" placeholder="Retype new password">
				<div id="divCheckPasswordMatch"></div>
            </div>
            <div class="form-group">
              <input type="text" id="mob" class="form-control login-input" maxlength="10" placeholder="Enter your mobile number">
            </div>
            <div class="form-group resend-btn-position">
                <input type="text" id="otp" class="form-control login-input" placeholder="">
                <button type="button" id="resend" class="resend-btn">Resend</button>
            </div>
            <button type="button" id="updatePassword" class="btn btn-orange btn-login" onclick="return Validate();">Submit</button>  
          </form>
        </div>
      </div>
    </div>
  </div>

