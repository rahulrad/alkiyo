<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div role="tabpanel" class="tab-pane" id="signup">
    <div class="main">
        <?php $form = ActiveForm::begin(['id' => 'registration-form']); ?>
        <div class="form-group">
            <?php echo $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => 'Enter Name', 'id' => 'inputUsername']) ?>
        </div>
        <div class="form-group">
            <?php echo $form->field($model, 'email')->textInput(['placeholder' => 'Enter  Email Id', 'id' => 'inputEmail']) ?>
        </div>
        <div class="form-group">
            <?php echo $form->field($model, 'password')->passwordInput(['placeholder' => 'Enter  Password', 'id' => 'inputPassword']) ?>
        </div>
        <div class="form-group">
            <?php echo Html::submitButton('Signup', ['class' => 'btn btn btn-primary btn-submit', 'name' => 'signup-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>