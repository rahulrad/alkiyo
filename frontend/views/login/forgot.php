<?php

use yii\helpers\Url;
$this->title = "Forgot Password";

?>

<main>

<!-- Top Categories start -->
<section class="bg-white pb-4 pt-4">
    <div class="container-fluid">
      <div class="wapper">
        <div class="wapper-body">
          <div class="container">
            
            <div class="login-detail">
              <div class="row">
                <div class="col-lg-6">
                  <div class="login-head">
                    <h2>Forgot Password</h2>
                  </div>
                  <div class="login-form bb-1 pb-3 mb-3">
                    <!--<form>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Username</label>
                          <input type="email" class="form-control login-input" placeholder="Email or Mobile Number">
                        </div>
                      <div class="form-group mb-0">
                          <label for="exampleInputPassword1">Password</label>
                          <input type="password" class="form-control login-input">
                      </div>
                      <div class="form-group">
                        <a href="#" data-toggle="modal" data-target="#forgot-password">Forgot password?</a>
                      </div>
                      <button type="button" class="btn btn-orange btn-login">login</button>  
                    </form>-->

                    <?php
					use dektrium\user\widgets\Login;
					use yii\helpers\Html;
					use yii\widgets\ActiveForm;
					use dektrium\user\widgets\Connect;
					use dektrium\user\models\LoginForm;
					$model = \Yii::createObject(LoginForm::className());
					if (Yii::$app->user->isGuest): ?>
				<?php 
				
					$form = ActiveForm::begin([
					'id'                     => 'password-recovery-form',
					'action'                 => \yii\helpers\Url::to(['/auth/forgot']),
					'enableAjaxValidation'   => true,
					'enableClientValidation' => false,
					'validateOnBlur'         => false,
					'validateOnType'         => false,
					'validateOnChange'       => false,
				]) ?>
				
				<div class="form-group">
                      <label for="exampleInputEmail1">Email</label>
                     <input type="email" name= "email" class="form-control login-input" placeholder="Email or Mobile Number">
                 </div>
		

			<?= Html::submitButton(Yii::t('user', 'Sign in'), ['class' => 'btn btn-primary btn-block']) ?>

			<?php ActiveForm::end(); ?>
			
			<?php else: ?>
			<?= Html::a(Yii::t('user', 'Logout'), ['/user/security/logout'], [
				'class'       => 'btn btn-danger btn-block no-radius',
				'data-method' => 'post'
			]) ?>
		<?php endif ?>

                  </div>

                  <div class="not-account">
                    <h2>Don�t have an account yet?</h2>
                    <p>Create a profile to make browsing, discovering and shopping in store and online quicker. Save shopping lists, favorites and planners, then access them from any computer, or device.</p>
                    <a href="<?=\yii\helpers\Url::to(['login/profile']);?>" class="btn btn-border btn-lg btn-login-border">Create Profile</a>
                  </div>
                </div>

                <div class="col-md-6 bl-1">
                  <div class="login-aside">
                    <div class="login-aside-logo">
                      <img src="images/login/login-logo.png" alt=""/>
                    </div>

                    <div class="login-aside-detail">
                        <div class="login-aside-heading">
                          <h2>Understated <br>
                            <span>Elegance</span>
                          </h2>
                          <p>Grohe essence</p>
                        </div>

                        <div class="login-aside-body">
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed ddidunt ut labore et dolore magna aliqua.</p>
                          <img src="images/login/login-image.png" alt=""/>
                        </div>

                    </div>

                  </div>
                </div>


              </div>
            </div>
         

          </div>
        </div>
      </div>
    </div>
  </section>
<!-- Top Categories end -->

</main>
