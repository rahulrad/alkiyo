<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\Brands;
use frontend\models\SignupForm;
$this->title = "Sign Up";
?>
<main>
<!-- Top Categories start -->
<section class="bg-white pb-4 pt-4">
    <div class="container-fluid">
      <div class="wapper">
        <div class="wapper-body">
          <div class="container">
            
            <div class="login-detail">
              <div class="row">
                <div class="col-lg-6">
                  <div class="login-head">
                    <h2>Create an Alkiyo Profile</h2>
                  </div>
					<?php if (Yii::$app->session->hasFlash('success')): ?>
						<div class="alert alert-success alert-dismissable">
							 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
							 <?= Yii::$app->session->getFlash('success') ?>
						</div>
				<?php endif; ?>
				<?php if (Yii::$app->session->hasFlash('error')): ?>
					<div class="alert alert-danger alert-dismissable">
						 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
						 <?= Yii::$app->session->getFlash('error') ?>
					</div>
				<?php endif; ?>
				
				
                  <div class="login-form">
					<?php $form = ActiveForm::begin([
					'action' => Yii::$app->urlManager->createUrl(['login/profilecreate']),
					
					]); ?>
                    <?php 
					$model = new SignupForm();
					/*  $form = ActiveForm::begin([
					  //'action'    => ['brand/formdata']
					  Yii::$app->urlManager->createUrl(['brand/formdata'])
					  ,'options' => ['method' => 'post']
					]);  */ ?>
					 
                      <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group mb-0">
                                <!--<label for="exampleInputEmail1">First Name</label>
                                <input type="text" name="first_name" class="form-control login-input">-->
								 <?php echo $form->field($model, 'first_name')->textInput(['id' => 'inputFirstname' ,'class' => 'form-control login-input']) ?>
								 
                              </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group mb-0">
                               <!-- <label for="exampleInputEmail1">Last Name</label>
                                <input type="text" name="last_name" class="form-control login-input">-->
								 <?php echo $form->field($model, 'last_name')->textInput(['id' => 'inputLasttname' ,'class' => 'form-control login-input']) ?>
                              </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group mb-0">
                                <!--<label for="exampleInputEmail1">City</label>-->
                                <?php echo $form->field($model, 'city')->textInput([ 'id' => 'inputCity' ,'class' => 'form-control login-input']) ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group verify-btn-position mb-0">
                               <!-- <label for="exampleInputEmail1">Mobile No.</label>
                                <input type="text"  name="mobile" class="form-control login-input">-->
								  <?php echo $form->field($model, 'mobile')->textInput(['id' => 'inputMobile' ,'class' => 'form-control login-input']) ?>
                                <button type="button"  class="verify-btn" id="mobileVerify">Verify</button>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group mb-0">
                               <!-- <label for="exampleInputEmail1">Email ID</label>
                                <input type="email" name="email" class="form-control login-input">-->
								
								 <?php echo $form->field($model, 'email')->textInput(['id' => 'email' ,'class' => 'form-control login-input']) ?>
                              </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group mb-0">
                            
                                <?php echo $form->field($model, 'password')->passwordInput(['id' => 'inputPassword' ,'class' => 'form-control login-input']) ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                 <?php echo $form->field($model, 'password_repeat')->passwordInput([ 'id' => 'inputRePassword' ,'class' => 'form-control login-input']) ?>
                            </div>
                        </div>

                        <div class="col-lg-12 mb-2">
                            <label class="alkiyo-checkbox login-checkbox">I have read & accept <a class="terms" href="#">terms of use.</a> 
                                <input type="checkbox" name="conditions" value="1">
                                <span class="checkmark"></span>
                            </label>
                            <label class="alkiyo-checkbox login-checkbox">Sign up me for updates, newsletters and offers. 
                                <input type="checkbox" name="offers" value="1">
                                <span class="checkmark"></span>
                            </label>
                        </div>

                      </div>
                      
                      <!--<a href="index-user.html" class="btn btn-orange btn-login">Create Profile</a> 
					  <input type="submit" value="submit" class="btn btn-orange btn-login">-->
					  <?php echo Html::submitButton('Create Profile', ['class' => 'btn btn-orange btn-login', 'name' => 'signup-button','id'=>'profileSubmit']) ?>
					  	
                      <a href="<?=\yii\helpers\Url::to(['login/login-redirect']);?>" class="btn btn-back-login">Back to login</a>  
                    <?php ActiveForm::end(); ?>
                  </div>
                </div>

                <div class="col-md-6 bl-1">
                  <div class="login-aside">
                    <div class="login-aside-logo">
                      <img src="images/login/login-logo.png" alt=""/>
                    </div>

                    <div class="login-aside-detail">
                        <div class="login-aside-heading">
                          <h2>Understated <br>
                            <span>Elegance</span>
                          </h2>
                          <p>Grohe essence</p>
                        </div>

                        <div class="login-aside-body">
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed ddidunt ut labore et dolore magna aliqua.</p>
                          <img src="<?php echo Url::base(); ?>/images/login/login-image.png" alt=""/>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- Top Categories end -->

</main>


<div class="modal" id="verify-password">
	<span class="close" data-dismiss="modal"></span>
  <div class="modal-dialog">
    <div class="modal-content modal-verify pt-3">


      <!-- Modal body -->
      <div class="modal-body">
        <h2>Mobile Verification</h2>
        <p id="otpmessage"></p>
     
          <div class="form-group verification-inputs mb-4">
              <input type="text" id="otp1" maxlength="1" class="form-control login-input inputs">
              <input type="text" id="otp2" maxlength="1" class="form-control login-input inputs">
              <input type="text" id="otp3" maxlength="1" class="form-control login-input inputs">
              <input type="text" id="otp4" maxlength="1" class="form-control login-input inputs">
          </div>
          <div class="text-center modal-verify-bottom">
			<button type="button" class="btn btn-orange btn-login mb-2" id="mobileVerifyOtp">Submit</button>  <br>
			<span id="timmer"></span><br>
			<a href="#">Entreated a wrong mobile number ?</a>
         </div>
    
      </div>
    </div>
  </div>
</div>
<script src="/js/profile.js"></script>
<script>
  $(function(){
    profileData.callProfile('<?=Yii::$app->request->getCsrfToken()?>','<?=\Yii::$app->getUrlManager()->createUrl('login/mobileVerification'); ?>','<?=\Yii::$app->getUrlManager()->createUrl('login/profile-update'); ?>','<?=\Yii::$app->getUrlManager()->createUrl('login/sendOtp'); ?>');
  });
	
</script>