<?php

use yii\helpers\Url;
$this->title = 'Alkiyo';
?>
<main>
<section class="bg-white pb-4 pt-4">
    <div class="container">
      <div class="wapper">
        <div class="wapper-body">
          <div class="container">
            <div class="row">
              <div class="col-lg-9">

                <div class="brand-detail bb-1 pb-4 mb-4">
                  <div class="product-brand-name">
                    
                    <ul>
                      <li><img src="<?php echo Url::base(); ?>/<?= $brand[0]->image;?>"></li>
                      <li>
                        <h2><?= $brand[0]->brand_name; ?></h2>
                        <p>(<?=  $brand[0]->brand_h2; ?>)</p>
                        <span>Established in <?php if(!empty($histroy[0]->year)){ $year = explode('-',$histroy[0]->year); echo $year[0]; } ?></span>
                      </li>
                    </ul>

                    <div class="product-brand-detail">
                      <?=  $brand[0]->brand_description; ?>
                    </div>
                  </div>
                </div>

                <!-- brand-histry -->
                <div class="brand-histry bb-1 pb-4 mb-4">
                  <div class="brand-histry-heading">
                    <h2>History</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                  </div>

                  <div class="brand-histry-detail">
                    <div class="tree-grid">  
                      <div class="cos-about-timeline">
                        <div class="row">
						<?php $i=1; foreach($histroy as $his){ 
							if($i%2!=0)
							{
								$v = 'offset-1 p-0';
								$v1 = 'cos-about-timeline-item cos-about-timeline-item-left';
							}
							else
							{
								$v = 'p-0 right-border-space';
								$v1 = 'cos-about-timeline-item cos-about-timeline-item-right';
								
							}
							$i++;
						?>
					
                            <div class="col-5 <?=  $v; ?>">
                              <div class="<?= $v1; ?>">
                                <p class="mb-1"><?= $his->year; ?></p>
                                <div class="timeline-box">
                                  <p><?= $his->content; ?></p>
                                  <div class="circle"></div>
                                </div>

                              </div>
                            </div>
                          <?php } ?>
							
                          </div>
						  </div>
                      <!-- dgjk;ljdfsg -->
                    </div>
                  </div>
                </div>
                <!-- glimpses-ditail -->
                <div class="glimpses-ditail bb-1 pb-4 mb-4">
                    <div class="brand-histry-heading">
                      <h2>Glimpses</h2>
                    </div>
                    <div class="glimpses-slider">
                        <ul id="glimpses-viewed-slide" class="glimpses-viewed-slide">
						<?php foreach($glimpise as $value){ ?>
                            <li>
                              <div class="glimpses-slide-item">
                                <img src="<?php echo Url::base(); ?>/<?php echo $value->image; ?>"  height="164" width="261" />
                                <h2><?php echo $value->name; ?></h2>
                                <p><?php echo $value->description; ?></p>
                              </div>
                            </li>
						<?php } ?>
                          </ul>
                    </div>
                </div>

                 <!-- brands-items -->
                 <div class="brands-items bb-1 pb-4 mb-4">
                    <div class="brand-histry-heading">
                      <h2>Products</h2>
                      <p>Complete bathroom solution</p>
                    </div>
                    <div class="brands-items-detail">
                      <div class="row">
						<?php foreach($products as $prod){  ?>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="brands-item-content">
								<?php $imgArray = explode(".",$prod['image']); ?>
                                <img src="<?php echo Url::base(); ?>/<?php echo "uploads/products/".$prod['catName']."/".$prod['brandName']."/".$imgArray[0]."/".$prod['image']; ?>" />
                                <h2><?= $prod['product_name'];?></h2>
                              </div>
                        </div>
						<?php } ?>
                      </div>
                    </div>
                </div>

                <!-- brands-items -->
                <div class="find-dealer pb-4 mb-4">
                    <div class="brand-histry-heading">
                      <h2>Find Dealer</h2>
                    </div>
                    <div class="find-dealer-detail">
                      <div class="row">
						<?php foreach($showrooms as $showroom) { ?>
                        <div class="col-lg-4">
                          <div class="dealer-address">
                            <img src="<?php echo Url::base(); ?>/<?= $brand[0]->image;?>" alt=""/>
                            <h2><?php echo $showroom['name']; ?></h2>
                            <ul>
                              <li><i class="fa fa-phone"></i> <span><?php echo $showroom['phone_no']; ?></span></li>
                              <li><i class="fa fa-envelope-o"></i> <span><?php echo $showroom['email']; ?></span></li>
                            </ul>
                          </div>					  
                        </div>
						<?php } ?>
					
                        <div class="col-lg-8">
                          <div class="dealer-map">
                              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d227748.38256195097!2d75.6504695008812!3d26.885447918213917!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396c4adf4c57e281%3A0xce1c63a0cf22e09!2sJaipur%2C%20Rajasthan!5e0!3m2!1sen!2sin!4v1569045826335!5m2!1sen!2sin" width="100%" height="357" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                          </div>
                        </div>

                      </div>
                    </div>
                </div>

              </div>

            <div class="col-lg-3">
              <div class="aside-produdts">
                  <div class="brand-histry-heading mb-4">
                      <h2>Popular Product</h2>
                  </div>

			<?php foreach($popularProduct as $prodValue){ ?>	  
                <div class="aside-items">
                  <a href="<?=\yii\helpers\Url::to(['product/index','slug'=>$prodValue['slug'], 'category' => $prodValue['Category']])?>">
                    <div class="aside-items-img">
						<?php $imgArray = explode(".",$prodValue['image']); ?>
                        <img src="<?php echo Url::base(); ?>/<?php echo "uploads/products/".$prodValue['catName']."/".$prodValue['brandName']."/".$imgArray[0]."/".$prodValue['image']; ?>">
                    </div>
                    <div class="aside-items-content">
                        <h4><?php echo $prodValue['product_name']; ?></h4>
                        <p><?php echo $prodValue['store_product_name']; ?></p>
                        <span><i class="fa fa-inr"></i><?php echo $prodValue['lowest_price']; ?></span>
                    </div>
                  </a>
                </div>
			<?php } ?>

                <div class="aside-items-btn">
                    <a href="<?=\yii\helpers\Url::to(['product/collection']);?>" class="btn btn-orange w-100">View All</a>
                </div>

              </div>
            </div>
          
          </div>
        
        </div>
        </div>
      </div>
    </div>
  </section>
<!-- Top Categories end -->

</main>
