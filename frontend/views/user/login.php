<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div role="tabpanel" class="tab-pane active" id="login">
    <div class="main">
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'action' => '/index.php/site/login',
            'enableAjaxValidation' => true,
            'validationUrl' => '/index.php/site/login']); ?>
        <div class="form-group">
            <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'placeholder' => 'Enter Email', 'id' => 'inputUsernameEmail']) ?>
        </div>
        <div class="form-group">
            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Enter Password', 'id' => 'inputPassword']) ?>
        </div>
        <div class="form-group">
            <div class="checkbox pull-right"> <a class="pull-right" href="#">Forgot password?</a> </div>
            <?= Html::button('Log In', ['class' => 'btn btn btn-primary btn-submit', 'name' => 'login-button', 'type' => 'submit']) ?>
        </div>
        <?php ActiveForm::end(); ?>
        <div class="form-group">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <p> Login with your social account </p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"> <a href="#" class="btn btn-lg btn-primary btn-block no-radius"> <i class="fa fa-facebook"></i> Facebook</a> </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"> <a href="#" class="btn btn-lg btn-danger btn-block no-radius"> <i class="fa fa-google-plus"></i> Google</a> </div>
            </div>
        </div>
    </div>
</div>