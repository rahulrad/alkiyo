<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Sliders;
?>
  <?= frontend\components\Slider::widget(); ?>
<?php // echo Yii::$app->modules->articles->Productpath;
//die(); 
$this->title = 'Alkiyo - Indias Comparison Shopping Engine';
?>
<main>
<!-- popular-brands start -->
  <section class="popular-brands mb-4">
    <div class="container">
      <div class="card">
        <div class="card-heading pt-4">
			<h3>Popular Brands</h3>
          </div>
          <div class="card-body">
            <ul id="popular-brands-slide" class="popular-brands-slide">
				<?php  foreach($brands as $brand){
						if(!empty($brand->image))
						{ ?>
						  <li>
							<a href="<?=\yii\helpers\Url::to(['brand/index','slug'=>$brand->slug]);?>">
							  <img src="<?php echo  Url::base(true)."/".$brand->image; ?>" />
							</a>
						  </li>
				 <?php }} ?>
            </ul>   
        </div>
      </div>
    </div>
  </section>
<!-- popular-brands end -->  

<!-- Top Categories start -->
  <section class="top-categories mb-4">
    <div class="container">
      <div class="card pt-4 pb-4">
        <div class="card-heading">
          <h3>Top Categories</h3>
        </div>
          <div class="card-body">
            <ul id="top-categories-slide" class="top-categories-slide">
				<?php  						
					foreach($categories as $cat){ ?>
                    <li>
                      <a class="product-categorie-link" href="<?=\yii\helpers\Url::to(['product/collection','slug'=>$cat->slug]);?>">
                          <div class="product-categorie">
                            <div class="product-categorie-img">
                                <img src="<?php echo  Url::base(true)."/".$cat->image; ?>" />
                            </div>
                            <div class="product-categorie-name">
                              <h3><?= $cat->category_name; ?></h3>
                            </div>
                          </div>
                        </a>
                    </li>
				<?php } ?>
               </ul>
          </div>
      </div>
    </div>
  </section>
<!-- Top Categories end -->

<!-- New Arrivals start -->
  <section class="new-arrivals mb-4">
    <div class="container">
      <div class="card pt-4 pb-4">
        <div class="card-heading">
          <h3>New Arrivals</h3>
        </div>
          <div class="card-body">
             <div class="row">
				
			<?php  foreach($newarrival as $new){
			?>
				  <div class="col-lg-4 col-md-6 col-sm-12 col-12 mb-2">
					<div class="new-arrivals-detail">
					  <div class="new-arrivals-img">
						<?php $imageArray = explode('.',$new['image']); ?>
						<img src="<?php echo "/uploads/products/".$new['catName']."/".$new['brandName']."/".$imageArray[0]."/".$new['image']; ?>" alt=""   />
					  </div>
					  <div class="new-arrivals-name">
						<h3><?= $new['product_name']; ?></h3>
						<?= substr($new['product_description'],0,100); ?>
						
						<a href="<?=\yii\helpers\Url::to(['product/index','slug'=>$new['slug'], 'category' => $new['Category']])?>">+ Explore</a>
						
					  </div>
					</div>
				  </div>
			<?php } ?>

             </div>
          </div>
      </div>
    </div>
  </section>
<!-- New Arrivals end -->

<!-- prodect-add start -->
 <section class="prodect-add mb-4">
    <div class="container">
      <div class="card pt-2 pb-2">
          <div class="card-body">
              <div id="prodect-add" class="carousel slide" data-ride="carousel">

                  <!-- Indicators -->
                  <ul class="carousel-indicators">
					<?php 
					$index = $i = 0;
					foreach($secondslider as $slider){
					?>
                    <li data-target="#prodect-add" data-slide-to="<?= $i;?>"  <?= $i==0 ? 'class="active"' : '' ?>></li>
					<?php $i++;
						$index++;
					} ?>
                  </ul>
                
                  <!-- The slideshow -->
                  <div class="carousel-inner">

                    <!-- slide 1 -->
				
			<?php 
				$index = $i = 0;
				foreach($secondslider as $sliders):
				 ?> 
                    <div class="carousel-item <?= $i==0 ? 'active' : '' ?>">
                      <div class="prodect-add-detail">
                        <div class="row">
                          <div class="col-lg-5 mb-3">
                            <div class="prodect-add-brand">
                              <div class="brand-logo">
                                <img src="/<?php echo $slider['brandimage']; ?>" alt=""/>
                              </div>
                              <div class="prodect-add-head">
                                <h3><?php echo $slider['h1']; ?> <br>
                                  <span><?php echo $slider['h2']; ?></span>
                                </h3>
                                <span><?php echo $slider['h3']; ?></span>
                              </div>
                              <p><?php echo $slider['description']; ?></p>
                               <a href="https://google.com"  target="_blank" class="btn btn-orange">View More</a>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <div class="prodect-add-img">
                              <img src="<?php echo  Url::base(true)."/".$slider['image']; ?>" alt=""/>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                 <?php 
						$i++; 
						$index++;
						endforeach; ?>
                  </div>
                
                </div>
          </div>
      </div>
    </div>
  </section>
<!-- prodect-add end -->

<!-- featured-products start -->
<section class="featured-products mb-4">
    <div class="container">
      <div class="card pt-4 pb-4">
        <div class="card-heading">
          <h3>Featured Products</h3>
        </div>
          <div class="card-body">
            <div class="row">
			<?php foreach($featuredproduct as $feature){ ?>
              <div class="col-lg-3 col-md-6 col-sm-12 col-12">
               <a class="featured-products-link" href="<?=\yii\helpers\Url::to(['product/index','slug'=>$feature['slug'], 'category' => $feature['Category']])?>">
                  <div class="featured-products-detail">
                    <div class="featured-products-img">
					<?php $imageArray = explode('.',$feature['image']); 
							if(empty($imageArray)){ $imageArray = '';}
					?>
                      <img src="<?php echo "/uploads/products/".$feature['catName']."/".$feature['brandName']."/".$imageArray[0]."/".$feature['image']; ?>" draggable="false">
                    </div>
                    <div class="featured-products-name">
                      <h3><?php echo $feature['product_name']; ?></h3>
                      <p><?php echo substr($feature['product_description'],0,100); ?></p>
                    </div>
                  </div>
                </a>
              </div>
			  
			<?php } ?>
			  
            </div>
          </div>
      </div>
    </div>
  </section>
<!-- featured-products end -->

<section class="recently-viewed mb-4">
    <div class="container">
      <div class="card">
        <div class="card-heading pt-4">
          <h3>Recently Viewed Products</h3>
        </div>
          <div class="card-body">

                  <ul id="recently-viewed-slide" class="recently-viewed-slide">
				    <?php foreach($receientview as $val){ ?>
                    <li>
					<?php $imagesArray = explode('.',$val['image']); ?>
                      <a href="<?=\yii\helpers\Url::to(['product/index','slug'=>$val['slug'], 'category' =>$val['Category']])?>">
                        <img src="<?php echo "/uploads/products/".$val['catName']."/".$val['brandName']."/".$imagesArray[0]."/".$val['image']; ?>"  height="116"  width="128"/>
                      </a>
                    </li>
					  <?php } ?>
                  </ul>

          </div>
      </div>
    </div>
</section>
<!-- recently-viewed end -->

</main>

   

