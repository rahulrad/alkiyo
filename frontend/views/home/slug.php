<div class="container">
  <div class="row">
    <p> <span itemprop="description"> <?=$data->descritption?> </span></p>
  </div>
</div>
<?php 
$this->title = $data->meta_title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $data->meta_desc,
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $data->meta_keyword,
]);
