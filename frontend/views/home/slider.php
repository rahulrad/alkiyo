<?php 
use common\models\Sliders;
use frontend\helpers\NoImage;
use common\models\Brands;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

	$homeslider = Sliders::getHomePageSlider(0); 
	$brand      = Brands::find()->all();
	
	?>
<?php 

if(count($homeslider)):?>
	 
     
<!-- baner slider -->
<section class="banner-slider mb-4">
  <div id="demo1" class="carousel slide" data-ride="carousel">
    <ul class="carousel-indicators">
	<?php 
          $index = $i = 0;
          foreach($homeslider as $slider):
          	?>
			<li data-target="#demo1" data-slide-to="<?=$i;?>" <?=($i==0 ? 'class="active"':'')?>><?= $slider['title'];?></li>
      <?php $i++;
			$index++;
          endforeach;
	?>
    </ul>

   
  <div class="carousel-inner">
   <?php 
          $index = $i = 0;
          foreach($homeslider as $slider):
          ?> 
      <div class="carousel-item <?=($i==0 ? 'active':'')?>">
        <img src="<?php  echo Url::base(true)."/".$slider['image']; ?>" alt="Los Angeles" width="100%" height="500">
        
        <div class="carousel-banner-heading d-lg-block d-none">
		
          <a href="<?=\yii\helpers\Url::to(['brand/index','slug'=>$slider['bransslug']]);?>"><img src="<?php echo $slider['brandimage']; ?>" alt=""/></a>
        </div>
        <div class="carousel-caption d-lg-block d-none">
            <div class="banner-contant-detail"> 
                <h3><?= $slider['h1']; ?></h3>
                <h4><?= $slider['h2']; ?></h4>
                <p><?= $slider['h3']; ?>
                </p>
            </div>
        </div>   
      </div>
	  <?php $i++;
			$index++;
          endforeach;
	?>
    </div>

    <div class="lg-hidden d-lg-none d-sm-none">
      <a class="carousel-control-prev" href="#demo1" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
      </a>
      <a class="carousel-control-next" href="#demo1" data-slide="next">
        <span class="carousel-control-next-icon"></span>
      </a>
    </div>

  </div>

  <div class="banner-search" style="width:317px;">
      <div class="search-products">
          <div class="search-heading text-center">
            <h2>Search Product</h2>
          </div>
          <div class="search-tabs">
              <ul class="nav nav-tabs">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#home">By Brand</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#menu1">By Room</a>
                  </li>
                </ul>
  
            
                <div class="tab-content">
                  <div id="home" class="container tab-pane active"><br>
                  	<?php $form = ActiveForm::begin(['id' => 'sliderproduct','method' => 'get']); ?>
					<input type="hidden" name="category" value="mobiles">
                        <div class="form-group">
                            <select class="select2 form-control" id="brand" name="brandname"  placeholder="Brand Name">
								<option value="">Select Brand</option>
								 <?php foreach($brand as $brandval){ ?>
								 <option value="<?php echo $brandval->brand_id;?>"><?php echo $brandval->brand_name; ?></option>
								 <?php } ?>
                            </select>
                          </div>
                          <div class="form-group">
                              <select class="select2 form-control productname" id="productByBrand"  name="slug" placeholder="Product Name">                        
                              </select>
                          </div>
                          <button type="button" class="btn btn-orange w-100" id="searchByBrand">Search</button>
						  <?php ActiveForm::end(); ?>
                     
                  </div>
                  <div id="menu1" class="container tab-pane fade"><br>
                    	<?php $form = ActiveForm::begin(['id' => 'sliderproduct1','method' => 'get']); ?>
                      
                        <div class="form-group">
                            <select class="select2 form-control"   id="productCategory" name="category" placeholder="Room Name">
                              <option value="">Select Room</option>
                              <option value="bathroom">Bathroom</option>
                              <option value="kitchen">Kitchen</option>
                            </select>
                          </div>
						  
                          <div class="form-group">
                              <select class="select2 form-control productname" name="slug" id="productnamebycat"  placeholder="Product Name">
                              
                              </select>
                          </div>
                          <button type="button" class="btn btn-orange w-100" id="productSearcchBycat">Search</button>
                 
					<?php ActiveForm::end(); ?>
                    
                  </div>
                </div>
          </div>
        </div>
  </div>

</section>

<?php endif; ?>

<script src="/js/sliderdata.js"></script>
<script>
  $(function(){
    sliderData.callSlider('<?=Yii::$app->request->getCsrfToken()?>','<?php echo \Yii::$app->getUrlManager()->createUrl("brand/get-product"); ?>','<?=\Yii::$app->getUrlManager()->createUrl('brand/getProductByCat'); ?>','<?=\yii\helpers\Url::to(['product/index','slug' =>'productSlug', 'category' => 'categorySlug'])?>');
  });

 /* function search(id)
 {
	
	var valOfSlug = $("#"+id).val();
	window.location.href = "<?=\yii\helpers\Url::to(['product/index','slug' => 'google-pixel-128-gb-very-silver', 'category' => 'mobiles'])?>";
	
 } */
 
/* 
function getProduct(proval)
{
     if(proval != '')
     {
         data = {
             brand_id : proval,
         '<?=Yii::$app->request->csrfParam?>': '<?=Yii::$app->request->getCsrfToken()?>'
         };
         $("#productnamebybrand").html('');
         
          $.ajax({
                 url: '<?=\Yii::$app->getUrlManager()->createUrl("brand/get-product"); ?>',
                 type: 'POST',
                 data: data,
                 success: function(response){
                     $("#productnamebybrand").append(response);
                 }	
             });
     }
  }

	function getCategoryproduct(catslug)
	{
		if(catslug != '')
		{
			data = {
				cat_slug: catslug,
			'<?=Yii::$app->request->csrfParam?>': '<?=Yii::$app->request->getCsrfToken()?>'
			};
			$("#productnamebycat").html('');
			
			 $.ajax({
					url: '<?=\Yii::$app->getUrlManager()->createUrl('brand/getProductByCat'); ?>',
					type: 'POST',
					data: data,
					success: function(response){
						$("#productnamebycat").append(response);
					}	
				});
		}
		
	}
	
	function getCategoryslug(productslug)
	{
		$("#frontproduct").attr('action','product/search');
	}
	
	function getCategoryslug1(productslug)
	{
		$("#frontproduct1").attr('action','product/search');
	} */
</script>