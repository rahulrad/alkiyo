<?php 
use yii\helpers\Html;
use yii\helpers\Url;
?>
<!-- Top Categories start -->
<section class="mid-banner">
    <div class="my-container">
      <div class="wapper">
       

        <div class="wapper-body">
          <div class="inner-container">

          <div class="wishlist-heading mar-b-3 b-1">
            <div class="ro pad-b-3 pad-t-3  ">
    
              <div class="col-lg-6">
                <div class="card-heading pad-0">
                  
                  <h3 class="wish-10">Wishlist <?php echo $totalwishlist; ?> items</h3>
                </div>
              </div>
  
              <div class="col-lg-6 text-al-right">
                  <button type="button" class="bt border-blu btn-lrg"></button>
              </div>
  
          </div>
          </div>
            
		<?php foreach($wishlistbrand as $brand){
			?>	

          <div class="wishlist-items b-1 mar-b-3">
            <div class="ro">
              <div class="col-md-12">
                <div class="product-company-na">
                  <ul>
                    <li class="li-style-none li-img"><img src="images/brands/jaquar.png" class="b-r"></li>
                    <li class="li-style-none li-pr-name"><h2 class="cera-product">CERA Products</h2></li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="ro">
			<?php  foreach($productdetails['branddata_'.$brand['brand_id']] as $pro){
					$imgArray = explode(".",$pro['image']);
				?>
                  <div class="col-lg-2 col-md-4 col-sm-6 col-12 mar-b-3 col-20">
                    <div class="collection-item">
                        <div class="collection-itm-img product-remov">
                          <a>
                            <img src="<?php  echo Url::base()."/uploads/products/".$pro['catfolslug']."/".$pro['brandfolslug']."/".$imgArray[0]."/".$pro['image']; ?>">
        
                          </a>
                        </div>
                        <div class="collection-itm-name">
                          <a href="product-detail.html"><h3 class="product-top-name"><?php echo $pro['product_name']; ?></h3></a>
                          <p class="modal-nam"><?php echo $pro['model_number']; ?></p>
                          <span class="price">₹ <?php echo $pro['original_price']; ?></span>
                        </div>
                        
                      </div>
                  </div>
				<?php } ?>

            </div>
          </div>


		<?php } ?>
          

          </div>
        </div>
       </div>
    </div>
  </section>
<!-- Top Categories end -->
