<?php 
use yii\helpers\Html;
use yii\helpers\Url;
?>
<main>
<!-- Top Categories start -->
<section class="bg-white pb-4 pt-4">
    <div class="container-fluid">
      <div class="wapper">
        <div class="wapper-body">
          <div class="container">
          <div class="wishlist-heading bb-1 mb-3">
            <div class="row pb-3 pt-3">
              <div class="col-lg-6">
                <div class="card-heading p-0">
                  <h3>Wishlist <?php echo $totalwishlist; ?> items</h3>
                </div>
              </div>
              <div class="col-lg-6 text-right">
                  <button   class="btn border-blue btn-lg">Generate PDF</button>
              </div>
          </div>
          </div>
            
	<?php foreach($wishlistbrand as $brand){
	?>
          <div class="wishlist-items bb-1 mb-3">
            <div class="row">
              <div class="col-md-12">
                <div class="product-company-name">
                  <ul>
                    <li><img src="<?php echo Url::base()."/".$productdetails['branddata_'.$brand['brand_id']][0]['brandimg'] ?>" heght="100" width="80"></li>
                    <li><h2><?php echo $productdetails['branddata_'.$brand['brand_id']][0]['brand_name'] ?> Products</h2></li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="row">		
				<?php  foreach($productdetails['branddata_'.$brand['brand_id']] as $pro){ ?>
                  <div class="col-lg-2 col-md-4 col-sm-6 col-12 mb-3 col-20">
                    <div class="collection-item">
                        <div class="collection-item-img product-remove">
                          <a>
                            <img src="<?php  echo Url::base()."/".$pro['image']; ?>" height="150">
                            <button id="<?php echo $pro['id']; ?>"   class="btn btn-product-remove">×</button>
                          </a>
                        </div>
                        <div class="collection-item-name">
                          <a href="<?=\yii\helpers\Url::to(['product/index','slug'=>$pro['slug'], 'category' => $pro['catslug']])?>"><h3><?php echo $pro['product_name']; ?></h3></a>
                          <p class="modal-name"><?php echo $pro['model_number']; ?></p>
                          <span class="price">₹ <?php echo $pro['original_price']; ?></span>
                        </div>                   
                      </div>
                  </div>
				<?php } ?>
            </div>
          </div>

         <?php } ?>

          </div>
        </div>
      </div>
    </div>
  </section>
<!-- Top Categories end -->

</main>
<script src="/js/wishlist.js"></script>
<script>
 $(function(){
    wishlistData.callWishlist('<?=Yii::$app->request->getCsrfToken()?>','<?=\Yii::$app->getUrlManager()->createUrl('wishlist/del'); ?>');
  });
</script>
