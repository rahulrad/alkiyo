<?php
use yii\widgets\ActiveForm;
?>
<footer class="footer">
  <div class="footer-top pt-5 pb-5">
    <div class="container">
      <div class="row">

        <div class="col-lg-3">
          <div class="footer-about">
              <img src="images/logo.png" alt="">
              <h5>At vero eos et accusamus et</h5>
              <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum ddeleniti atque corrupti.</p>
          </div>
        </div>

        <div class="col-lg-3">
          <div class="footer-link">
            <div class="footer-heading">
              <h5>Help</h5>
            </div>
            <nav>
              <ul>
                <li><a href="javascript:;">FAQ's</a></li>
                <li><a href="javascript:;">Contact us</a></li>
                <li><a href="javascript:;">Terms & Condition</a></li>
                <li><a href="javascript:;">Privacy Policy</a></li>
              </ul>
            </nav>
          </div>
        </div>

        <div class="col-lg-3">
          <div class="footer-link">
            <div class="footer-heading">
              <h5>About Alkiyo</h5>
            </div>
            <nav>
              <ul>
                <li><a href="javascript:;">This is Alkiyo</a></li>
                <li><a href="javascript:;">Working at Alkiyo</a></li>
                <li><a href="javascript:;">Newsroom</a></li>
                <li><a href="javascript:;">Gallery</a></li>
                <li><a href="javascript:;">Blog</a></li>
              </ul>
            </nav>
          </div>
        </div>

        <div class="col-lg-3">
          <div class="footer-link">

            <div>
              <div class="footer-heading">
                <h5>Lets Talk</h5>
              </div>
              <nav>
                <ul>
                  <li>+91-8239199199</li>
                  <li>sales@alkiyo.com</li>
                </ul>
              </nav>
            </div>

            <div>
              <div class="footer-heading">
                <h5>Follow us</h5>
                <nav class="social-links">
                  <ul>
                    <li><a href="javascript:;"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-pinterest"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-instagram"></i></a></li>
                  </ul>
                </nav>
              </div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="copyright pb-3">
    <div class="container">
      <p>Copyright ©Alcodes mobility pvt ltd 2019 Right Scale. All Rights Reserved</p>
    </div>
  </div>
  
  <div class="footer-bottom pt-5 pb-4">
    <div class="container">
            <div>
              <h5>POPULAR SEARCHES</h5>
              <p>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a><span>|</span>
                <a href="javascript:;"> Lorem </a>
              </p>
            </div>

            <div>
              <h5>At vero eos et accusamus et iusto odio dignissimos</h5>
              <p>
                  At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti 
                  quos dolores et quas molestias excepturi sint occaecati cupiditate non providentAt vero eos et accusamus et iusto odio dignissimos
                  ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati
                  cupiditate non provident
              </p>
            </div>

            <div>
              <h5>POPULAR SEARCHES</h5>
              <p>
                  At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti 
                  quos dolores et quas molestias excepturi sint occaecati cupiditate non providentAt vero eos et accusamus et iusto odio dignissimos
                  ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati
                  cupiditate non provident
              </p>
            </div>

            <div>
              <h5>POPULAR SEARCHES</h5>
              <p>
                  At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti 
                  quos dolores et quas molestias excepturi sint occaecati cupiditate non providentAt vero eos et accusamus et iusto odio dignissimos
                  ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati
                  cupiditate non provident
              </p>
            </div>
        </div>
  </div>

</footer>



<!-- forgot password modal -->
<div class="modal" id="forgot-password">
  <div class="modal-dialog">
    <div class="modal-content modal-forgot-password">

      <!-- Modal Header -->
      <div class="modal-header border-bottom-0 pb-0 ">
        <button type="button" class="close change-password-remove" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <h2>Forgot Password</h2>
       <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action' => Yii::$app->urlManager->createUrl(['/auth/forgot'])]); ?>
			<div class="form-group">
               <input type="email" name="email" class="form-control login-input" placeholder="Type email">
            </div>
			
          <button type="submit" class="btn btn-orange btn-login">Submit</button>  
        <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
</div>


