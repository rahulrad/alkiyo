<?php 
use yii\helpers\Html;
use yii\helpers\Url;
?>
   <header class="header">
    <div class="container-fluid">
      <div class="top-header">
        <div class="row">

          <div class="col-lg-2">
            <div class="header-logo mb-1">
              <a href="/">
                <img src="<?php echo Url::base(); ?>/images/logo.png" alt=""/>
              </a>
            </div>
          </div>

          <div class="col-lg-8 col-md-8">
            <div class="header-search mb-1">
                <div class="input-group md-form form-sm form-1 pl-0">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-text1">
                        <i class="fa fa-search"></i></span>
                    </div>
                    <input class="form-control globalSearch" id="SearchText" type="text" placeholder="Search For Categories">
					<!--<input class="form-control globalSearch" id="search-box" type="text" placeholder="Search For Categories">-->
                </div>
            </div>
          </div>

          <div class="col-lg-2 col-md-4">
            <div class="right-bar-link">
              <nav>
                <ul>
				<?php
                  
                  if(Yii::$app->user->isGuest):?>
                  <li><a href="<?=\yii\helpers\Url::to(['/login/login-redirect']);?>"><i class="fa fa-sign-out"></i> <span>Login/Sign up</span></a></li>
				   <?php  else:?>
						 <li class="user-dropdown"><a href="#" data-toggle="dropdown"><i class="fa fa-user-o"></i> <span><?= Yii::$app->user->identity->username ?></span></a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="<?=\yii\helpers\Url::to(['/login/edit-profile']);?>">Edit Profile</a>
								<?=
									Html::beginForm(['/user/security/logout'], 'post')
								.    Html::submitButton(
									'Logout',
										['class' => 'dropdown-item logout']
									)
									. Html::endForm();?>                       
							  </div>
							</li>
                         <?php
                      endif; ?> 
						   <?php   Yii::$app->WishlistComponent->wishlist(); ?>
                </ul>
              </nav>
            </div>
          </div>

        </div>
      </div>

     <?php   Yii::$app->MenuComponent->menu(); ?>

    </div>
  </header>























