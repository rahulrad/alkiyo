<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\Alert;


AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	 <meta charset="utf-8"> 
	  <title><?= Html::encode($this->title) ?></title>
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i&display=swap" rel="stylesheet">
    <?php $this->head() ?>

</head>
<?php $this->beginBody() ?> 
<?php $this->beginContent('@app/views/layouts/header.php'); ?>
    <!-- You may need to put some content here -->
<?php $this->endContent(); ?>
<?php //$this->beginBody() ?>
<?= $content ?>		

<?php $this->beginContent('@app/views/layouts/footer.php'); ?>
<?php $this->endContent(); ?>
<?php $this->endBody() ?>


</body>
<?php //Yii::$app->MenuComponent->mobileMenu(); ?>

<?php $this->endPage() ?>
</html>

<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>

<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('.dropdown-menu-child  a:not(a[href="#"])').on('click', function() {
    	self.location = jQuery(this).attr('href');
	});
	jQuery('#search-box').on('click', function(){
		var searchTerm = jQuery('#globalSearch').val();
		return false;
		if(searchTerm.length > 0)
			window.location = '<?=\Yii::$app->getUrlManager()->createUrl('search/index').'?term=' ?>'+searchTerm;
	});
	jQuery('#m-search-box').on('click', function(){
		return false;
		var searchTerm = jQuery('#m-globalSearch').val();
		if(searchTerm.length > 0)
			window.location = '<?=\Yii::$app->getUrlManager()->createUrl('search/index').'?term=' ?>'+searchTerm;
	});
	jQuery('#search-box').on('keypress',function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
	    	return false;
	        jQuery('#search-box').trigger('click');
	    }
	});
	jQuery('#m-globalSearch').on('keypress',function(event){
		
		var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
	    	return false;
	        jQuery('#m-submitSearch').trigger('click');
	    }
	});
<?php 
$url =  \yii\helpers\Url::to(['search/products']);

$source = <<< Source
source: function( request, response ) {
    $.ajax( {
      url: "{$url}",
      dataType: "json",
      data: {
        term: request.term
      },
      success: function( data ) {
		// Handle 'no match' indicated by [ "" ] response
        response( data.length === 1 && data[ 0 ].length === 0 ? [] : data );
      }
    } );
},
Source;

$script = <<< JS

jQuery( ".globalSearch" ).autocomplete({
    minLength: 2,
    source: "{$url}",
    focus: function( event, ui ) {
      jQuery( ".globalSearch" ).val( ui.item.label );
      return false;
    },
    select: function( event, ui ) {
      jQuery( ".globalSearch" ).val( ui.item.slug );
	  window.location =  ui.item.url;
      return false;
    }
  })
 $("#search-box").autocomplete().data("uiAutocomplete")._renderItem = function( ul, item ) {
    return $( "<li>" )
      .append( item.label )
      .appendTo( ul );
  };
JS;

echo $script;?>
});
</script>
