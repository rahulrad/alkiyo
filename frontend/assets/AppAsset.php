<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/app';
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [ 
	
		'scss/bootstrap.min.css',
		'scss/font-awesome.min.css',
		'scss/content.css',
		'scss/media.css',
		'scss/lightslider.css',
		'scss/select2.min.css',
		//'scss/jquery.desoslide.css',
		'scss/jssor.slider.css',
		//'scss/lib.css',
		
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
	];
    public $js = [
		'js/jquery.min.js',
		'js/popper.min.js',
      	'js/bootstrap.min.js',
		'js/lightslider.js',
		'js/select2.full.min.js',
		'js/brand-detail.js',
		'js/common.js',
		'js/home.js', 
		//'js/jquery.desoslide.js',
		'js/jssor.slider-28.0.0.min.js'
    ];
    public $depends = [
        
    ];
}
