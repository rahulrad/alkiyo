<?php
namespace frontend\controllers;
use Yii;
use frontend\models\Showroom;
use frontend\models\Showroomdetail;
use yii\web\Controller;
use frontend\helpers\Sendsms;
class ShowroomController extends Controller {
	public function actionIndex()
    {
		
    }

	public function actionFindShowroom()
	{
		$otp = rand(1000,9999);
		$mobile = Yii::$app->request->post('mobile');
		$model = new Showroom();
		$model->name = Yii::$app->request->post('name');
		$model->pincode = Yii::$app->request->post('city');
		$model->phone = $mobile;
		$model->otp = $otp;
		$model->created_at = date('Y-m-d H:i:s');
		$model->save();
		Sendsms::sendsms($mobile,$otp);
        return 1; 
		
	}
	
	public function actionResendOtp()
	{
		$otp = rand(1000,9999);
		$mobile = Yii::$app->request->post('mobile');
		$model = Showroom::find()->where(['phone' => $mobile])->orderBy(['id' => SORT_DESC])->limit(1)->all();
		$id = $model[0]->id;
		 Yii::$app->db->createCommand()
        ->update('user_info_showroom_location', ['otp'=>$otp,'updated_at'=>date('Y-m-d H:i:s')], 'id='.$id )
        ->execute();
		Sendsms::sendsms($mobile,$otp);
        return 1; 
		
	}
	
	public function actionOtpVerify()
	{
		$mobile = 	Yii::$app->request->post('mobile');
		$otp 	= 		Yii::$app->request->post('otp');
		$model  = Showroom::find()->where(['otp' => $otp])->andWhere(['phone' => $mobile])->orderBy(['id' => SORT_DESC])->limit(1)->all();
		if($model)
		{
			$showroom  = Showroomdetail::find()->where(['like','city',$model[0]->pincode])->orWhere(['pincode' => $model[0]->pincode])->all();
			if($showroom)
			{
				echo $showroom[0]->pincode."#".$model[0]->id;
			}
			else
			{
				echo "error";
			}
		}
		else
		{
			echo 'invalid';
		}
		
	}
	
	public function actionFindShowrooms()
	{
		 Yii::$app->db->createCommand()
        ->update('user_info_showroom_location', ['construction_status'=>Yii::$app->request->post('radio'),'updated_at'=>date('Y-m-d H:i:s'),'bathroom'=>Yii::$app->request->post('constru')], 'id='.Yii::$app->request->post('id'))
        ->execute();
		
		$queryes = new  yii\db\Query;
					$queryes->select(['showroom.*', 'brands.brand_name'])  
					->from('showroom')
					->leftJoin('brands', 'showroom.brand_id = brands.brand_id')
					->where(['showroom.pincode'=>Yii::$app->request->post('pincode')])
					->andWhere(['showroom.status' => 'active'])
					->andWhere(['showroom.is_delete' => 0]);
					$commands = $queryes->createCommand(); 
					$showrooms = $commands->queryAll();
		
		/* print_r($showrooms);
		die(); */
		//$showrooms = Showroomdetail::find()->where(['pincode'=>Yii::$app->request->post('pincode')])->all();
		 return $this->render('index', [
                'showrooms' => $showrooms,
            ]);
	}
	
	public function actionSendMail()
	{
			Yii::$app->mailer->compose()
			->setFrom('developer.rtspl@gmail.com')
			->setTo('ashiyadav1006@gmail.com')
			->setSubject('subj')
			->setHtmlBody('hello')
			->send();			
	}
	
}
