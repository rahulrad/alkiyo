<?php
namespace frontend\controllers;

use Yii;
use common\models\Brands;
use common\models\Product;
use common\models\BrandHistory;
use common\models\Glimpses;
use common\models\Categories;
use frontend\models\Showroomdetail;

/**
 * Brand controller
 */
class BrandController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$brandsDetails = \common\models\Brands::find()->where(['slug'=>Yii::$app->request->get('slug')])->andWhere(['status'=>'active'])->andWhere(['!=','is_delete',1])->all();
		//$products  	= 	\common\models\Products::find()->where(['brands_brand_id'=>$brandsDetails[0]->brand_id])->andWhere(['active'=>'active'])->andWhere(['!=','is_delete',1])->limit(6)->all();
		
		$queryProduct = new  yii\db\Query;
		$queryProduct ->select(['products.*', 'categories.slug as Category','brands.folder_slug as brandName','categories.folder_slug as catName'])  
        ->from('products')
        ->leftJoin('categories', 'categories.category_id = products.categories_category_id')
		->leftJoin('brands', 'brands.brand_id = products.brands_brand_id')
		->where(['products.active'=>'active'])->andWhere(['!=','products.is_delete',1])
		->andWhere(['products.brands_brand_id'=>$brandsDetails[0]->brand_id])
		->limit(6);
		$commands = $queryProduct->createCommand();
		$products = $commands->queryAll();
		
		
		$history  	= 	\common\models\BrandHistory::find()->where(['brand_id'=>$brandsDetails[0]->brand_id])->andWhere(['status'=>'active'])->andWhere(['!=','is_delete',1])->orderBy(['id' => SORT_ASC])->all();
		$glimpise 	= 	\common\models\Glimpses::find()->where(['brand_id'=>$brandsDetails[0]->brand_id])->andWhere(['active'=>'active'])->andWhere(['!=','is_delete',1])->all();
		$showrooms  =   \frontend\models\Showroomdetail::find()->where(['brand_id'=>$brandsDetails[0]->brand_id])->andWhere(['status'=>'active'])->andWhere(['!=','is_delete',1])->all();
		
		$query = new  yii\db\Query;
		$query ->select(['products.*', 'categories.slug as Category','brands.folder_slug as brandName','categories.folder_slug as catName'])  
        ->from('products')
        ->leftJoin('categories', 'categories.category_id = products.categories_category_id')
		->leftJoin('brands', 'brands.brand_id = products.brands_brand_id')
		->where(['products.active'=>'active'])->andWhere(['!=','products.is_delete',1])
		->orderBy(['products.product_id' => SORT_DESC])->limit(4);
		$command = $query->createCommand();
		$popularProduct = $command->queryAll();
		
		
	
		//$popularProduct = \common\models\Products::find()->where(['active'=>'active'])->andWhere(['!=','is_delete',1])->orderBy(['product_id' => SORT_ASC])->limit(4)->all();
		
        return $this->render('index',['brand'=>$brandsDetails,'products'=>$products,'histroy'=>$history,'glimpise'=>$glimpise,'showrooms'=>$showrooms,'popularProduct'=>$popularProduct]);
    } 
	
	public function actionGetProduct()
	{
					$this->enableCsrfValidation = false;
					$query1 = new  yii\db\Query;
					$query1	->select(['products.product_name','products.slug as prodslug','categories.slug as catslug'])  
					->from('products')
					->leftJoin('categories','categories.category_id=products.categories_category_id')
					->where(['products.brands_brand_id' => Yii::$app->request->post('brand_id')])
					->andWhere(['!=','products.is_delete',1])
					->andWhere(['products.active'=>'active']);
					$commands = $query1->createCommand(); 			
					$brand = $commands->queryAll();
				$option = '<option value="">Select Product</option>';
				foreach($brand as $value)
				{
					$option .= "<option value='".$value['prodslug']."#".$value['catslug']."'>".$value['product_name']."</option>";	
				}
				return $option;
	}
	
	public function actionGetProductByCat()
	{
				$catid = \common\models\Categories::find()->select(['category_id'])->where(['like','slug',Yii::$app->request->post('cat_slug')])->all();
					$query1 = new  yii\db\Query;
					$query1	->select(['products.product_name','products.slug as prodslug','categories.slug as catslug'])  
					->from('products')
					->leftJoin('categories','categories.category_id=products.categories_category_id')
					->where(['products.categories_category_id' => $catid[0]['category_id']])
					->andWhere(['!=','products.is_delete',1])
					->andWhere(['products.active'=>'active']);
					$commands = $query1->createCommand(); 			
					$brand = $commands->queryAll();
				$option = '<option value="">Select Product</option>';
				foreach($brand as $value)
				{
					//$option .= "<option value='".$value['prodslug']."#".$value['catslug']."'>".$value['product_name']."</option>";	
					$option .= "<option value='".$value['prodslug']."'>".$value['product_name']."</option>";	
				}
				return $option;
	}
	
	public function actionGetCategory()
	{
		$getcat = Yii::$app->request->post('slug');
		$query1 = new  yii\db\Query;
		$query1	->select([''])  
		->from('wishlist')
		->where(['wishlist.ip_add' => Yii::$app->request->userIP ])
		->orWhere(['wishlist.user_id' => $userId]);
		$command1 = $query1->createCommand(); 
		$data1 = $command1->queryAll();
				
	}
	

	
 
}
