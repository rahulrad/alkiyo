<?php

namespace frontend\controllers;
use Yii;
use common\models\Newsletters;
use yii\web\Controller;

class NewsletterController extends Controller{
    public function actionInsert(){
    	$model = new Newsletters();
    	if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->status = 1;
			$model->is_delete = 0;
			$model->created = date('Y-m-d H:i:s');
			$model->save();
    		Yii::$app->session->setFlash('success', 'Thank you!. We welcome you to our nayashoppy newsletter subscription.');
    	
    		return $this->goHome();
    	} else {
    		$emailError = ($model->getErrors());
    		$emailError = $emailError['email'][0];
    		Yii::$app->session->setFlash('error', $emailError);
    		return $this->goHome();
    	}
    }
    
}
