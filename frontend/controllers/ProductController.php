<?php

namespace frontend\controllers;

use common\models\ActivityTracker;
use Yii;
use common\models\Products;
use common\models\FeatureGroups;
use common\models\ProductFeature;
use common\models\ReviewForm;
use common\models\ProductReviews;
use common\models\ProductReviewsRank;
use common\models\PriceUpdate;
use common\models\Menus;
use common\models\ProductsSuppliers;
use common\models\Categories;
use yii\web\Controller;
use common\modules\searchEs\components\ElasticSearchProducts;
class ProductController extends \yii\web\Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays Product detail page.
     *
     * @return mixed
     */
	 public  static function CollectionSortForGroupItem($a, $b)
 {
    if ($a[key($a)]['sort'] == $b[key($b)]['sort'])
    {
        return 0;
    }
    return ($a[key($a)]['sort'] < $b[key($b)]['sort']) ? -1 : 1;
  }

 public  static function CollectionSortForItem($a, $b)
{

 
    if ($a['sort'] == $b['sort'])
    {
        return 0;
    }
    return ($a['sort'] < $b['sort']) ? -1 : 1;
}
	 
	 public function actionCollection()
	{
		$cat = \common\models\Categories::find()->where(['is_delete'=>0,'status'=>'active'])->andWhere(['IS','parent_category_id',NULL])->all();
		 return $this->render('category', ['categories'=>$cat]);
		
	}
	 
	 public function actionChildcollection()
	 {
		
		$cat = \common\models\Categories::find()->where(['slug' => Yii::$app->request->get('slug')])->all();
		$commands	= \common\models\Categories::find()->where(['parent_category_id' =>$cat[0]->category_id])->all();
			if(!empty($commands))
			{
				return $this->render('category', ['categories'=>$commands]);
			}
			else
			{
				  $category = Categories::findOne(['slug' => Yii::$app->request->get('slug')], ['status' => 'active']);
				 if(empty($category))
				 throw new \yii\web\NotFoundHttpException();
				 $breadcrumb = Categories::createCategoryBreadcrumb($category->category_id);
				 $allBrandsArray=Categories::getAvailableBrandsforProductsbyCategory($category->category_id);
        
				$search = new ElasticSearchProducts();
				$supplierArr = $param = [];
				$supplierArr = Yii::$app->request->get('store', []);
				$sort = Yii::$app->request->get('sort', 'new');
				$featureIds = Yii::$app->request->get('feature_ids', []);
				$arrCategory = Yii::$app->request->get('categoryIds', []);
				$arrBrands = Yii::$app->request->get('brands');
				$priceMax = Yii::$app->request->get('max_price');
				$priceMin = Yii::$app->request->get('min_price');
				$defaultMinPrice = 100;
				$defaultMaxPrice = 300000;
				if(!is_array($featureIds)){
					$featureIds = [];
				}
				if(!in_array($sort, $search->allowedSort)){
					$sort = 'popular';
				}       
			 if(!is_array($supplierArr))
				$supplierArr = [];
				
				
		  if(!empty($arrBrands)){
        	$arrBrands = explode(',', $arrBrands);
		}
		else{
        	$arrBrands = [];

		}
			
			
		$totalRecords = 0;
        $param['term'] = null;
        $param['category'] = [$category->category_id]; // numeric array
        $param['brand'] = $arrBrands; // numeric array
        $param['filter_group_item'] = $featureIds; //numeric  array
        $param['page'] = Yii::$app->request->get('page', 1); // numeric
        $param['perpage'] = Yii::$app->request->get('perpage', \Yii::$app->params['listingResults']);
        $param['sort'] = $sort;
        $param['store'] = $supplierArr;
		
			if(!empty($_GET) && count($_GET)>1) {
				$this->view->params['index'] = 'false';
			}		
			$param['facets'] = (Yii::$app->request->isAjax) ? true : true;
			$result = $search->find($param);		
			$brands = $features = $products = [];
			if(count($result['items']) > 0){
				$products = $result['items'];
				$brands   = isset($result['facets']['facet_brand_id']) ? $result['facets']['facet_brand_id']: [];
				$features = [];
				$features = isset($result['facets']['filters']) ? $result['facets']['filters'] : [];
			}
			usort($features, "self::CollectionSortForGroupItem");
			foreach($features as $ky => $val){
				  usort($val[key($val)]['values'],"self::CollectionSortForItem");
			$features[$ky][key($val)]['values']=  $val[key($val)]['values'];
	 
			}
		 if(Yii::$app->request->isAjax){
        	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return [
        		'products' => $products,
        		'brands' => $brands,
        		'features' => $features,
        		'list4Html' => $this->renderPartial('@frontend/views/search/product4Listing', ['products' => $products,'category'=>$category,'recordPerPage' => \Yii::$app->params['listingResults'],'total'=> $result['total'] ]),
        		'list3Html' => $this->renderPartial('@frontend/views/search/product3Listing', ['products' => $products,'category'=>$category,'recordPerPage' => \Yii::$app->params['listingResults'],'total'=>$result['total'] ]),
				'list2Html' => $this->renderPartial('@frontend/views/search/product2Listing', ['products' => $products,'category'=>$category,'recordPerPage' => \Yii::$app->params['listingResults'],'total'=> $result['total'] ]),
        		'total' => $result['total'],
        		'pageNumber' => $param['page'],
        		'params' => $param,
        		'listingResults' => \Yii::$app->params['listingResults'],
				'recordPerPage' => \Yii::$app->params['listingResults'],
        	];
        }
		 if(empty($result['items'])) {
		  $this->view->params['index'] = 'true';
		 }

         return $this->render('products', [
        	'category' => $category,
        	'breadcrumb'=>$breadcrumb,
        	'recordPerPage' => \Yii::$app->params['listingResults'],
        	'products' => $result['items'],
        	'brands' => $brands,
        	'features' => $features,
//         	'subCategories' => $subCategories,
        	'arrBrands' => $arrBrands,
        	//'siblings' => $sameLevelItems,
			//'popular' =>  $popularItems,
        	'total' => $result['total'],
        	'defaultMinPrice' => $defaultMinPrice,
        	'defaultMaxPrice' => $defaultMaxPrice,
        	//'description' => $menuItem->description,
        	'supplierArr' => $supplierArr,
            'allBrandsArr' => $allBrandsArray,
			]);
		}
	}
	 
	 
    public function actionIndex() {
        
        $request        = Yii::$app->request;
        $slug           = $request->get('slug');
        //get product from slug
        $product        = Products::findOne(['slug' => $slug, 'is_delete'=>0], ['product_status' => 'active']);
        if(empty($product))
        	throw new \yii\web\NotFoundHttpException();
        //get product category feature groups
        $featureGroups  = FeatureGroups::getFeatureAttributeByCategory($product->categories_category_id);
        //product features array
        $productFeatures = ProductFeature::getProductFeatureValues($product->product_id);
        //product review form model
       // $reviewmodel    = new ReviewForm();
        //$starRating = new StarRating();
        $userIP         = Yii::$app->request->userIP;

       if(!$product->categoriesCategory->is_index){
			$this->view->params['index'] = 'false';
       }

        //else user id 0
    	if (!\Yii::$app->user->isGuest) {
            $userId = \Yii::$app->user->id;
        }else{
        	$userId = 0;
        }
        $product->trigger(Products::PRODUCT_VIEW);
        $productId      = $product->product_id;
       // $ProductReview  = new ProductReviews();
        //$ProductReviews = $ProductReview->getReviews($userId, $userIP, $productId);
        $PriceUpdate = new PriceUpdate();
        $PriceUpdate    = $PriceUpdate->getUpdate($userId, $productId);
//        $product->queueProductSuppliers();
       // $similarProducts = Products::getProductsInTwoPriceRanges($product->lowest_price, $product->lowest_price, $product->categories_category_id);
		$similarProducts = Products::getSimilarProductsForAPI($product->lowest_price, $product->categories_category_id);
        $menuSlug           = 'laptop-pricelists'; //product-price-menu
        $categoryId         = $product->categories_category_id;
        $menuType           = 'left menu';
        //$menuCategoryTypeList   = Menus::getMenuCategoryTypeList($menuSlug, $categoryId, $menuType);
        Products::metaTags($product);
	        $instock=1;
        if($product->categoriesCategory->compare == 1){
        	$view = 'productwithsupplier';
		}
        else {
        	$view = 'productwithoutsupplier';
			$productsSuppliersModel = new ProductsSuppliers();
            $existProductSuppiler = $productsSuppliersModel::find()->where(['product_id' => $productId])->one();
            if (!is_null($existProductSuppiler)) {
                $instock = $existProductSuppiler->instock;
            }

        }
	
        return $this->render('product', [
       		'product'           => $product,
       		'featureGroups'     => $featureGroups,
       		'productFeatures'   => $productFeatures,
       		//'reviewmodel'       => $reviewmodel, 
       		//'ProductReviews'    => $ProductReviews,
            'PriceUpdate'       => $PriceUpdate,
		    'instock' => $instock,
            'userId'            => $userId,
       		'similarProducts'   => $similarProducts,
        	'featurez'          => \common\models\Features::getIsFilterFeatureOfProduct($product->categories_category_id, $product->product_id),
        ]);
    }
    
    /*
     * 
     */
    public function actionAddreview() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model          = new ReviewForm();
        $request        = \Yii::$app->getRequest();
        $message = [];
        if ($request->isPost && $model->load($request->post())) {
            $message['status'] = 200;
            $message['message'] = 'My message';
        }else{
            $message['status'] = 404;
            $message['message'] = 'Error message';
        }
        return $message;
    }

    public function actionReviewpost() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $ProductReviews = new ProductReviews();
        $userIP = \Yii::$app->request->userIP;
        //if user login user id
        //else user id 0
        
    	if (!\Yii::$app->user->isGuest) {
            $userId = \Yii::$app->user->id;
        }else{
        	$userId = 0;
        }
		
        $request        = \Yii::$app->getRequest();
        $requestt       = $request->post('ReviewForm');
        $message = [];
        
        if ($request->isPost) {
            
            $ProductReviews->title      = $requestt['title'];
            $ProductReviews->description= $requestt['description'];
            $ProductReviews->product_id = $requestt['product_id'];
            $ProductReviews->rating     = $requestt['rating'];
            $ProductReviews->user_id    = $userId;
            $ProductReviews->ip         = $userIP;
            $ProductReviews->status     = "InActive";
            $ProductReviews->created    = date('Y/m/d');
            $ProductReviews->save();
            $message['status'] = 200;
            $message['message'] = 'My message';
            
        }else{
            $message['status'] = 404;
            $message['message'] = 'Error message';
        }
        return $message;
    }
    
    public function actionReviewrank() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $ProductReviews = new ProductReviewsRank();
        $userIP = Yii::$app->request->userIP;
        //if user login user id
        //else user id 0
        $userId = 0;
        
        $request        = \Yii::$app->getRequest();
        $requestt       = $request->post('ReviewForm');
        $message = [];
        
        if ($request->isPost) {
            
            $ProductReviews->product_id = $requestt['product_id'];
            $ProductReviews->review_id  = $requestt['review_id'];
            $ProductReviews->user_id    = $userId;
            $ProductReviews->ip         = $userIP;
            $ProductReviews->rank       = $requestt['rank'];
            $ProductReviews->created    = date('Y/m/d');
            $ProductReviews->save();
            $message['status'] = 200;
            $message['message'] = 'My message';
            
        }else{
            $message['status'] = 404;
            $message['message'] = 'Error message';
        }
        return $message;
    }
    
    public function actionPriceupdate() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $PriceUpdate = new PriceUpdate();
    	if (!\Yii::$app->user->isGuest) {
            $userId = \Yii::$app->user->id;
        }else{
        	$userId = 0;
        }
        
        $request        = \Yii::$app->getRequest();
        $requestt       = $request->post('ReviewForm');
        $message = [];
        
    	if ($request->isPost && $userId > 0) {
        	$product = Products::findOne([ 'product_id' => $requestt['product_id'] ]);
            $PriceUpdate->product_id = $product->product_id;
            $PriceUpdate->user_id    = $userId;
            $PriceUpdate->alert_price = $product->lowest_price;
            $PriceUpdate->created    = date('Y/m/d');
            $PriceUpdate->save();
            $message['status'] = 200;
            $message['message'] = 'My message';
        }else{
            $message['status'] = 404;
            $message['message'] = 'Error message';
        }
        return $message;
    }
    
    public function actionCompare(){
    
 $this->view->params['index'] = 'true';
	 
    	$request = Yii::$app->request;
    	$products = $request->get('products');
    	 
    	if(empty($products) && !is_array($products)){
    		return $this->goHome();
    	}
    	
    	$data = Products::compareProducts($products);
    	 
    	if(empty($data)){
    		return $this->goHome();
    	}
		
    	return $this->render('compare', $data);
    }
    
    public function actionComparesearch() {
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $term = Yii::$app->request->get('term');
        $category = Yii::$app->request->get('category') ? intval(Yii::$app->request->get('category')) : 0;

        $search = new \common\modules\searchEs\components\ElasticSearchProducts();
        
        $param = [];
        $param['term'] = $term; // string
        $param['category'] = $category; // numeric array
        $param['page'] = Yii::$app->request->get('page', 1); // numeric
        $param['perpage'] = Yii::$app->request->get('perpage', 20);
        
        $param['sort'] = Yii::$app->request->get('sort', 'relevance');
        
        // return facets as well
        $param['facets'] = false;
        
        $records = $search->find($param);


        $items = [];
        foreach ($records['items'] as $row) {
        	$image = \frontend\helpers\NoImage::getProductImageFullPathFromElasticResult($row);
        	$items[] = [
                'icon' => $image,
                'label' => $row['product_name'],
                'value' => $row['product_name'],
                'slug' => $row['slug'],
            ];
        }

        return $items;
    }


    public function actionStoreOut($id)
    {
        $model = ProductsSuppliers::find()->where(['track_id' => $id])->one();
        if ($model != null) {
            $this->layout = "track";
            $sessionId = Yii::$app->session->id;
            $activityType = ActivityTracker::Activity_PRODUCT;
            $url = $model->url . $model->supplier->data_url;
            ActivityTracker::saveActivity($url, $sessionId, $activityType, $model->supplier->name);
            return $this->render("send_to_store", [
                'url' => $url
            ]);
        }
        throw new \yii\web\NotFoundHttpException();
    }
	
	public function actionGetProduct()
	{
		$id = Yii::$app->request->post('brand_id');
		$query = new  yii\db\Query;
		$query ->select(['products.*', 'categories.slug as Category'])  
        ->from('products')
        ->leftJoin('categories', 'categories.category_id = products.categories_category_id')
        ->where([ 'products.brands_brand_id' => $id ])
		->andWhere(['products.active'=>'active'])->andWhere(['!=','products.is_delete',1]);
		$command = $query->createCommand();
		$getProduct = $command->queryAll();
		$getProducts = "<option value=''>Selet Product</option>";
		foreach($getProduct as $products)
		{
			$getProducts .= '<option value="'.$products['slug'].'#'.$products['Category'].'>'.$products['product_name'].'</option>';
		}
		print_r($getProducts);
		
	}
	
	public function actionSearch()
	{ 
        $slug           = Yii::$app->request->post('slug');
		$slugs  = explode("#", $slug);
		  $product        = Products::findOne(['slug' => $slugs[0], 'is_delete'=>0], ['product_status' => 'active']);
        if(empty($product))
        	throw new \yii\web\NotFoundHttpException();
        //get product category feature groups
        $featureGroups  = FeatureGroups::getFeatureAttributeByCategory($product->categories_category_id);
        //product features array
        $productFeatures = ProductFeature::getProductFeatureValues($product->product_id);
        //product review form model
       // $reviewmodel    = new ReviewForm();
        //$starRating = new StarRating();
        $userIP         = Yii::$app->request->userIP;

       if(!$product->categoriesCategory->is_index){
			$this->view->params['index'] = 'false';
       }

	
        //else user id 0
    	if (!\Yii::$app->user->isGuest) {
            $userId = \Yii::$app->user->id;
        }else{
        	$userId = 0;
        }
        $product->trigger(Products::PRODUCT_VIEW);
        $productId      = $product->product_id;
       // $ProductReview  = new ProductReviews();
        //$ProductReviews = $ProductReview->getReviews($userId, $userIP, $productId);
        $PriceUpdate = new PriceUpdate();
        $PriceUpdate    = $PriceUpdate->getUpdate($userId, $productId);
//        $product->queueProductSuppliers();
        $similarProducts = Products::getSimilarProductsForAPI($product->lowest_price, $product->categories_category_id);
        /* print_r($similarProducts);
		die(); */
        $menuSlug           = 'laptop-pricelists'; //product-price-menu
        $categoryId         = $product->categories_category_id;
        $menuType           = 'left menu';
        //$menuCategoryTypeList   = Menus::getMenuCategoryTypeList($menuSlug, $categoryId, $menuType);
        Products::metaTags($product);
	        $instock=1;
        if($product->categoriesCategory->compare == 1){
        	$view = 'productwithsupplier';
		}
        else {
        	$view = 'productwithoutsupplier';
			$productsSuppliersModel = new ProductsSuppliers();
            $existProductSuppiler = $productsSuppliersModel::find()->where(['product_id' => $productId])->one();
            if (!is_null($existProductSuppiler)) {
                $instock = $existProductSuppiler->instock;
            }

        }
	
	  return $this->render('product', [
       		'product'           => $product,
       		'featureGroups'     => $featureGroups,
       		'productFeatures'   => $productFeatures,
       		//'reviewmodel'       => $reviewmodel, 
       		//'ProductReviews'    => $ProductReviews,
            'PriceUpdate'       => $PriceUpdate,
		    'instock' => $instock,
            'userId'            => $userId,
       		'similarProducts'   => $similarProducts,
        	'featurez'          => \common\models\Features::getIsFilterFeatureOfProduct($product->categories_category_id, $product->product_id),
        ]); 
		
	}
	
}
