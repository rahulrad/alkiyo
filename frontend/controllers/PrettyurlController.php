<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Categories;
use common\models\Products;
use common\models\Brands;
use common\modules\searchEs\components\ElasticSearchProducts;
use backend\models\FilterGroups;


/**
 * Deals controller
 */
class PrettyurlController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        ];
    }


 public  static function CollectionSortForGroupItem($a, $b)
 {
    if ($a[key($a)]['sort'] == $b[key($b)]['sort'])
    {
        return 0;
    }
    return ($a[key($a)]['sort'] < $b[key($b)]['sort']) ? -1 : 1;
  }

 public  static function CollectionSortForItem($a, $b)
{

 
    if ($a['sort'] == $b['sort'])
    {
        return 0;
    }
    return ($a['sort'] < $b['sort']) ? -1 : 1;
}


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays category landing page.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        $request = Yii::$app->request;
        $slug = $request->get('slug', null);
        
        $menuItem = \common\models\Menus::findOne(['slug' => $slug, 'is_delete'=>0]);
        if(empty($menuItem))
        	throw new \yii\web\NotFoundHttpException();


             if(!$menuItem->field_index) {
                 $this->view->params['index'] = 'false';
                }
       
		 $sameLevelItems = \common\models\Menus::findAll(['parent_id' => $menuItem->parent_id, 'type' => 2, 'is_delete' => 0]);
          $popularItems = \common\models\Menus::findAll(['category_id' => $menuItem->category_id, 'is_popular' => 1, 'is_delete' => 0]);




    //$sameLevelItems = \common\models\Menus::findAll(['parent_id' => $menuItem->parent_id, 'is_delete' => 0, 'is_popular'=>1]);
		$category = Categories::findOne(['category_id' => $menuItem->category_id], ['status' => 'active']);
        
        if(empty($category))
            throw new \yii\web\NotFoundHttpException();
			
        $breadcrumb = Categories::createCategoryBreadcrumb($category->category_id);
		$allBrandsArray=Categories::getAvailableBrandsforProductsbyCategory($category->category_id);
        $subCategories = Categories::find()->where(['parent_category_id' => $category->category_id])->orderBy('sort_order ASC')->all();
        
        $search = new ElasticSearchProducts();
        $supplierArr = $param = [];
        $supplierArr = Yii::$app->request->get('store', []);
        $sort = Yii::$app->request->get('sort', 'new');
        $featureIds = Yii::$app->request->get('feature_ids', []);
        $arrCategory = Yii::$app->request->get('categoryIds', []);
        $arrBrands = Yii::$app->request->get('brands');
        $priceMax = Yii::$app->request->get('max_price');
        $priceMin = Yii::$app->request->get('min_price');
	
        
    	if(!Yii::$app->request->isAjax){
        	$brandIdOfMenu = $menuItem->brand_id;
        	$featuresIdsOfMenu = unserialize($menuItem->others);
        	if(!empty($featuresIdsOfMenu)){
        		foreach($featuresIdsOfMenu as $menuFeatureId){
        	//		$featureIds[] = $menuFeatureId['feature_id'];
        	
if(array_key_exists('feature_value_id',$menuFeatureId)){
                        foreach($menuFeatureId['feature_value_id'] as $abc => $vcd){
if(isset($vcd) && !empty($vcd)){
					$listg=FilterGroups::getFilterGroupValuebyfeaturevalue($vcd);
                            
if(is_array($listg) &&  array_key_exists('list',$listg)){
$featureIds[] =$listg['list'];
}
}
                        }
                    }
	}
        	}
         	if(!empty($brandIdOfMenu))
        		$arrBrands = $brandIdOfMenu;
        }

        
//        $defaultMinPrice = 100;
  //      $defaultMaxPrice = 100000;


if($menuItem->price_min || $menuItem->price_max){

$defaultMinPrice = $menuItem->price_min;
if($menuItem->price_max){
  $defaultMaxPrice=$menuItem->price_max;
}else{
    $defaultMaxPrice = 100000;
}

}else{

        $defaultMinPrice = 100;
        $defaultMaxPrice = 300000;
}


        if(!is_array($featureIds)){
        	$featureIds = [];
        }
        if(!in_array($sort, $search->allowedSort)){
        	$sort = 'popular';
        }
        
        if(!is_array($supplierArr))
        	$supplierArr = [];
        
        if( ( $priceMax > 0 && $priceMin >= 0) && ($priceMin <= $priceMax)){
        	$param['price_max'] = $priceMax;
        	$param['price_min'] = $priceMin;
        
}else {

        	$param['price_min'] = $defaultMinPrice;
        	$param['price_max'] = $defaultMaxPrice;
        }
        
        if(!empty($arrBrands)){
        	$arrBrands = explode(',', $arrBrands);
}
		else{
        	$arrBrands = [];

}
       



if(!empty($menuItem->products)){
 $param['products']=explode(',',$menuItem->products);
}

 
        $totalRecords = 0;
        $param['term'] = null;
        $param['category'] = [$category->category_id]; // numeric array
        $param['brand'] = $arrBrands; // numeric array
//         $param['attribute'] = $featureIds; //numeric  array
        $param['filter_group_item'] = $featureIds; //numeric  array
        $param['page'] = Yii::$app->request->get('page', 1); // numeric
        $param['perpage'] = Yii::$app->request->get('perpage', \Yii::$app->params['listingResults']);
        $param['sort'] = $sort;
        $param['store'] = $supplierArr;
       
		if(!empty($_GET) && count($_GET)>1) {
			$this->view->params['index'] = 'false';
		}


        $param['facets'] = (Yii::$app->request->isAjax) ? true : true;
        $result = $search->find($param);
        $brands = $features = $products = [];
        if(count($result['items']) > 0){
        	$products = $result['items'];
        	$brands   = isset($result['facets']['facet_brand_id']) ? $result['facets']['facet_brand_id']: [];
        	$features = [];
        	$features = isset($result['facets']['filters']) ? $result['facets']['filters'] : [];
        }


        usort($features, "self::CollectionSortForGroupItem");
        foreach($features as $ky => $val){
              usort($val[key($val)]['values'],"self::CollectionSortForItem");
		$features[$ky][key($val)]['values']=  $val[key($val)]['values'];
 
        }
        if(Yii::$app->request->isAjax){
        	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return [
        		'products' => $products,
        		'brands' 	=> $brands,
        		'features' => $features,
        		'list4Html' => $this->renderPartial('@frontend/views/search/product4Listing', ['products' => $products,'menuItem'=>$menuItem,'category'=>$category ,'total'=>$result['total'],'recordPerPage'=>\Yii::$app->params['listingResults']]),
        		'list3Html' => $this->renderPartial('@frontend/views/search/product3Listing', ['products' => $products,'menuItem'=>$menuItem,'category'=>$category ,'total'=>$result['total'],'recordPerPage'=>\Yii::$app->params['listingResults']]),
				'list2Html' => $this->renderPartial('@frontend/views/search/product2Listing', ['products' => $products,'menuItem'=>$menuItem,'category'=>$category ,'total'=>$result['total'],'recordPerPage'=>\Yii::$app->params['listingResults']]),
        		'total' => $result['total'],
        		'pageNumber' => $param['page'],
        		'params' => $param,
        		'listingResults' => \Yii::$app->params['listingResults'],
        	];
        }



 \Yii::$app->view->registerMetaTag([
                'name' => 'keyword',
                'content' => $menuItem->meta_keyword,
        ]);


\Yii::$app->view->registerMetaTag([
                'name' => 'description',
                 'content' => $menuItem->meta_desc,
        ]);


\Yii::$app->view->registerMetaTag([
                'name' => 'title',
                'content' => $menuItem->meta_title,
        ]);


 if(empty($result['items'])) {
  $this->view->params['index'] = 'true';
 }
	
	

        return $this->render('index', [
        	'category' => $category,
        	'breadcrumb'=>$breadcrumb,
        	'recordPerPage' => \Yii::$app->params['listingResults'],
        	'products' => $result['items'],
        	'brands' => $brands,
        	'features' => $features,
            'menuItemObject'=>$menuItem,
			'subCategories' => $subCategories,
        	'arrBrands' => $arrBrands,
        	'siblings' => $sameLevelItems,
			'popular' =>  $popularItems,
        	'total' => $result['total'],
        	'defaultMinPrice' => $defaultMinPrice,
        	'defaultMaxPrice' => $defaultMaxPrice,
        	'description' => $menuItem->description,
        	'supplierArr' => $supplierArr,
            'allBrandsArr' => $allBrandsArray,
        ]);
    }
}
