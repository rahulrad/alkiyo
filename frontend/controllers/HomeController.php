<?php
namespace frontend\controllers;

use Yii;
use common\models\Sliders;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Categories;
use common\models\Products;
use common\models\Brands;
use common\models\CouponCategories;
use common\models\Suppliers;
use common\models\Menus;
use backend\models\CouponsVendors;
use common\models\Coupons;
use common\models\RecentlyViewedProducts;

/**
 * Home controller
 */
class HomeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$categories = Categories::find()->where(['show_home_page' => '1','status'=>'active'])->andWhere(['!=','is_delete',1])->andWhere(['IS','parent_category_id',NULL])->orderBy('sort_order ASC')->limit(20)->all();
        $brands =   Brands::find()->where(['status'=>'active'])->andWhere(['!=','is_delete',1])->limit(20)->all();
	    $secondslider = Sliders::getHomePageSecondSlider(0);
        
		$queryNewArrival = new  yii\db\Query;
		$queryNewArrival->select(['products.*', 'categories.slug as Category','brands.folder_slug as brandName','categories.folder_slug as catName'])  
        ->from('products')
        ->leftJoin('categories', 'categories.category_id = products.categories_category_id')
        ->leftJoin('brands', 'brands.brand_id = products.brands_brand_id')
		->where(['products.active'=>'active'])->andWhere(['!=','products.is_delete',1])->andWhere(['products.show_home_page'=>1])
		->orderBy('products.product_id DESC')
		->limit(3);
		$commandNewArrival = $queryNewArrival->createCommand();
		$newarrival = $commandNewArrival->queryAll();
		
		
		//$newarrival = \common\models\Products::getNewProductsForAPI(3);
		//$featuredproduct  = \common\models\Products::getfeatureProductsForAPI(4);
		
		$queryFeature = new  yii\db\Query;
		$queryFeature->select(['products.*', 'categories.slug as Category','brands.folder_slug as brandName','categories.folder_slug as catName'])  
        ->from('products')
        ->leftJoin('categories', 'categories.category_id = products.categories_category_id')
        ->leftJoin('brands', 'brands.brand_id = products.brands_brand_id')
		->where(['products.active'=>'active'])->andWhere(['!=','products.is_delete',1])->andWhere(['products.show_home_page'=>1])
		->orderBy('products.product_id DESC')
		->limit(4);
		$commandFeature = $queryFeature->createCommand();
		$featuredproduct = $commandFeature->queryAll();
		
		$recenview = RecentlyViewedProducts::getRecentlyViewedProducts();
		
		$users = array();
		foreach($recenview as $re)
		{
			array_push($users,$re['product_id']);
		}
		$query = new  yii\db\Query;
		$query ->select(['products.*', 'categories.slug as Category','brands.folder_slug as brandName','categories.folder_slug as catName'])  
        ->from('products')
        ->leftJoin('categories', 'categories.category_id = products.categories_category_id')
		 ->leftJoin('brands', 'brands.brand_id = products.brands_brand_id')
        ->where(['IN', 'products.product_id', $users ])
		->andWhere(['products.active'=>'active'])->andWhere(['!=','products.is_delete',1]);
		$command = $query->createCommand();
		$data = $command->queryAll();
		$menus = Menus::getTopMenu();
        return $this->render('home' , ['categories' => $categories,
            'brands' => $brands,
           'menus'=>$menus,'secondslider'=>$secondslider,'newarrival'=>$newarrival,'featuredproduct'=>$featuredproduct,'receientview'=>$data]);
    }
    
    public function actionSlug(){
    	$request = Yii::$app->request;
        $slug = $request->get('slug');
    	$data = \backend\models\Pages::findOne(['slug' => $slug]);
    	if($data instanceof \backend\models\Pages)
    	return $this->render('slug', ['data' => $data]);
    	else throw new \yii\web\NotFoundHttpException();
    }
    
 
}
