<?php
namespace frontend\controllers;
use Yii;
use common\models\LoginForm;
use frontend\models\Wishlist;
use yii\web\Controller;
use kartik\mpdf\Pdf;
use Mpdf\Mpdf;
use yii\helpers\Url;
use yii\swiftmailer\Mailer;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;



class WishlistController extends Controller {

	public function getData()
	{
		 if (!\Yii::$app->user->isGuest) {
			$userId = \Yii::$app->user->id;
			
			 $querycount = new  yii\db\Query;
				$querycount	->select(['count(wishlist.id) as totalwishlist'])  
				->from('wishlist')
				->where(['wishlist.ip_add' => Yii::$app->request->userIP])
				->orWhere(['wishlist.user_id' => $userId]);
				$commandqry = $querycount->createCommand(); 
				$countdata = $commandqry->queryAll();
				
				
				$query1 = new  yii\db\Query;
				$query1	->select(['DISTINCT(wishlist.brand_id)'])  
				->from('wishlist')
				->where(['wishlist.ip_add' => Yii::$app->request->userIP ])
				->orWhere(['wishlist.user_id' => $userId]);
				$command1 = $query1->createCommand(); 
				$data1 = $command1->queryAll();
				
				$products = array();
				foreach($data1 as $brandid)
				{
					$queryes = new  yii\db\Query;
					$queryes->select(['wishlist.*', 'products.product_name','products.image','products.original_price','products.model_number','products.model_number','products.slug','categories.slug as catslug','brands.image as brandimg','categories.category_name as catname','brands.brand_name','categories.folder_slug as catfolslug','brands.folder_slug as brandfolslug'])  
					->from('wishlist')
					->leftJoin('products', 'wishlist.product_id = products.product_id')
					->leftJoin('categories','categories.category_id=wishlist.cat_id')
					->leftJoin('brands','brands.brand_id=wishlist.brand_id')
					->where(['wishlist.ip_add' => Yii::$app->request->userIP])
					->orWhere(['wishlist.user_id' => $userId])
					->andWhere(['wishlist.brand_id'=> $brandid['brand_id']]);
					$commands = $queryes->createCommand(); 
					$products['branddata_'.$brandid['brand_id']] = $commands->queryAll();
					
				}
			
        }else{
				 $querycount = new  yii\db\Query;
				$querycount	->select(['count(wishlist.id) as totalwishlist'])  
				->from('wishlist')
				->where(['wishlist.ip_add' => Yii::$app->request->userIP]);
				$commandqry = $querycount->createCommand(); 
				$countdata = $commandqry->queryAll();
				
				
				$query1 = new  yii\db\Query;
				$query1	->select(['DISTINCT(wishlist.brand_id)'])  
				->from('wishlist')
				->where(['wishlist.ip_add' => Yii::$app->request->userIP ]);
				$command1 = $query1->createCommand(); 
				$data1 = $command1->queryAll();
				
				
				
				$products = array();
				foreach($data1 as $brandid)
				{
					$queryes = new  yii\db\Query;
					$queryes->select(['wishlist.*', 'products.product_name','products.image','products.original_price','products.model_number','products.model_number','products.slug','categories.slug as catslug','brands.image as brandimg','categories.category_name as catname','brands.brand_name','categories.folder_slug as catfolslug','brands.folder_slug as brandfolslug'])  
					->from('wishlist')
					->leftJoin('products', 'wishlist.product_id = products.product_id')
					->leftJoin('categories','categories.category_id=wishlist.cat_id')
					->leftJoin('brands','brands.brand_id=wishlist.brand_id')
					->where(['wishlist.ip_add' => Yii::$app->request->userIP])
					->andWhere(['wishlist.brand_id'=> $brandid['brand_id']]);
					$commands = $queryes->createCommand(); 
					$products['branddata_'.$brandid['brand_id']] = $commands->queryAll();
					
				}
			}
			
			
			return ['wishlistbrand'=>$data1,'productdetails'=>$products,'totalwishlist'=>$countdata[0]['totalwishlist']];
			
	}


	public function actionIndex()
    {
	
		$data =  $this->getData();
		
       return $this->render('wishlist',$data);
    }

	public function actionSaveWishList()
	{
		$model = new WishList();
		$model->user_id = Yii::$app->request->post('user_id');
		$model->product_id = Yii::$app->request->post('product_id');
		$model->cat_id = Yii::$app->request->post('cat_id');
		$model->ip_add = Yii::$app->request->userIP;
		$model->brand_id = Yii::$app->request->post('brands_brand_id');
	    $model->save(); 
        return 1;
		
	}
	
	public static function actionDeleteWishList()
	{
		$id = Yii::$app->request->post('id');
		$fanclub = WishList::find()
			->where(['id'=>$id])
			->one()
			->delete();
		
	}
	
	public function actionSendMail()
	{
			/* Yii::$app->mailer->compose()
			->setTo('ashiyadav1006@gmail.com')
			->setFrom('developer@radtechnoservices.com')
			->setSubject('test')
			->setTextBody('testing message')
			->send(); */
			
			 $mail = new PHPMailer(true);
					try {
						//Server settings
						$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
						$mail->isSMTP();                                            // Send using SMTP
						$mail->Host       = 'mail.radtechnoservices.com';                    // Set the SMTP server to send through
						$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
						$mail->Username   = 'developer@radtechnoservices.com';                     // SMTP username
						$mail->Password   = 'developer#123';                               // SMTP password
						$mail->SMTPSecure = 'tls';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
						$mail->Port       = 25;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

						//Recipients
						$mail->setFrom('developer@radtechnoservices.com', 'Mailer');
						$mail->addAddress('ashiyadav1006@gmail.com', 'Joe User');     // Add a recipient
					/* 	$mail->addAddress('ellen@example.com');               // Name is optional
						$mail->addReplyTo('info@example.com', 'Information');
						$mail->addCC('cc@example.com');
						$mail->addBCC('bcc@example.com'); */

						// Attachments
						/* $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
						$mail->addAttachment('/tmp/image.jpg', 'new.jpg');  */   // Optional name

						// Content
						$mail->isHTML(true);                                  // Set email format to HTML
						$mail->Subject = 'Here is the subject';
						$mail->Body    = 'This is the HTML message body <b>in bold!</b>';
						$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

						$mail->send();
						echo 'Message has been sent';
					} catch (Exception $e) {
						echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
					}
			
			
		/* 	$mail = new PHPMailer();
    	$mail->IsSMTP();
    	$mail->SMTPAuth = true;
    	$mail->Host = 'ssl://smtp.gmail.com';
    	$mail->From     = 'developer.rtspl@gmail.com';
    	$mail->FromName = "IFS";
    	 $mail->Username = 'developer.rtspl@gmail.com';
    	$mail->Password = 'Dev#123@123';
    	$mail->AddAddress('ashiyadav1006@gmail.com', "IFS");
    	$mail->Subject  = 'fghf';
    	$mail->Body     = 'dsgdsg';
    	$mail->IsHTML(true);
    	$mail->Port = 587;
    	$mail->SMTPSecure = 'tls';
    	if(!$mail->Send())
    	{
    		echo "Mailer Error: " . $mail->ErrorInfo;
    	} 
    	else
    	{
    		echo "Message has been sent";
    	}  */
			
			
	}
	
	public function actionDownload()
    {
		
		$data =  $this->getdata();			
		//$content = $this->render('wishlist',$data);
		$mpdf = new mPDF();
		$mpdf->showImageErrors = true;
		$html = $this->render('pdf',$data);	
		$stylesheet = file_get_contents('scss/content.css');
		$content = $this->renderPartial('wishlistpdf',$data);
     // setup kartik\mpdf\Pdf component
		$pdf = new Pdf(['mode' => Pdf::MODE_UTF8, 'format' => Pdf::FORMAT_A4, 'orientation' => Pdf::ORIENT_PORTRAIT, 'destination' => Pdf::DEST_BROWSER, 'content' => $content, 'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css' ,'cssInline' => ' h3 {
			color: #000;
			font-size: 16px;
			line-height: 19px;
			margin-bottom: 2px;
			font-weight: 400;
		}		.mid-banner{background-color: #fff!important;
      padding-bottom: 1.5rem!important;
      padding-top:  1.5rem!important;
		}

    .my-container{width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;}

    .inner-container {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;}

    .b-1 {
    border-bottom: 1px solid rgba(112, 112, 112, 0.13);
}
    .mar-b-3 {
    margin-bottom: 1rem!important;
}

    .pad-b-3 {
    padding-bottom: 1rem!important;}

    .pad-t-3 {
    padding-top:  1rem!important;}

   .pad0{padding: 0!important;} 

   .text-al-right {
    text-align: right!important;
}
.border-blu {
    background-color: #1c385c;
    font-size: 12px;
    line-height: 14px;
    color: #fff;
    border-color: #1c385c;
    border-radius: 2px;
}
.btn-lrg {
    padding: .5rem 1rem;}

.bt{    display: inline-block;
    font-weight: 400;
    text-align: center;
    vertical-align: middle;
    user-select: none;
    border: 1px solid transparent;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
.ro {
  
    display: flex;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}

.product-company-na {
    margin-bottom: 10px;
}

.collection-itm-img {
    background-color: rgba(224, 224, 224, 0.44);
    margin-bottom: 8px;
}
.product-remov {
    position: relative;
}
.btn-product-remov {
    position: absolute;
    top: 7px;
    right: 7px;
    color: #ADABAB;
    background: transparent;
    border: 1px solid #ADABAB;
    border-radius: 100%;
    padding: 0px;
    line-height: normal;
    font-size: 14px;
    width: 19px;
    height: 19px;
}
.collection-itm-name {
    margin-bottom: 7px;
}

.modal-nam {
    color: #ADABAB;
    font-size: 14px;
    line-height: 16px;
    margin-bottom: 0px;
}

.product-top-name{color: #000;
    font-size: 16px;
    line-height: 19px;
    margin-bottom: 2px;
    font-weight: 400;}

    .wish-10{
    font-size: 20px;
    line-height: 24px;
    color: #000;
    font-weight: 400;
    margin-bottom: 0px;
}

.cera-product{font-size: 16px;
    line-height: 19px;
    font-weight: 400;
    color: #000;}
.b-r{border-right: 1px solid rgba(112, 112, 112, 0.14);}

.li-pr-name{width: 60%; 

}
.li-img{ width: 10%; float: left; padding-right:20px;}

.li-style-none{list-style: none;}

.col-lg-2 {
float:left;
width:20%;
}
ul {
padding-left : 0px;
}
.b-r {
border-right:1px solid #ccc;
}
   
   ', 'options' => ['title' => 'Krajee Report Title'], 'methods' => ['SetHeader' => [''], 'SetFooter' => ['{PAGENO}']]]);
     // return the pdf output as per the destination setting
		return $pdf->render();		
        exit; 
    }
	
	
}
