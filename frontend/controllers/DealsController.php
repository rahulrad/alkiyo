<?php
namespace frontend\controllers;

use common\models\ActivityTracker;
use Yii;
use yii\web\Controller;
use backend\models\CouponsSearch;
use common\models\Deals;
use backend\models\CouponsVendors;
use common\models\Coupons;
use yii\web\NotFoundHttpException;

/**
 * Deal controller
 */
class DealsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays deal listing page.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$list=CouponsVendors::getVendorsList();
        return $this->render('coupon',['list'=>$list]);
    }


	 public function actionMerchant()
    {

		 $merchant = Yii::$app->request->get('merchant');
		$list=CouponsVendors::getCouponsMerchant($merchant);
        return $this->render('coupon',['list'=>$list]);
    }


	 /**
     * Displays deal listing page.
     *
     * @return mixed
     */
    public function actionDeals()
    {
	
  $suppliers = Yii::$app->request->get('merchant');
$couponVendor=CouponsVendors::getVendorObj(  $suppliers );

 if(empty($couponVendor) ||  $couponVendor===null)
   	throw new \yii\web\NotFoundHttpException();

				$typeArray=array(
					
					'1'=>'Coupons',
					'2'=>'Cashback',
						'3'=>'Deals',
					
					);

 		$suppliers = $cats =$typeParam="";
  		$supplierArr = $catArr = $typeParamArr=[];
      
		$cats= Yii::$app->request->get('category');
		$typeParam= '1';
			   
		if(isset($cats)){
					$catArr = explode('-', $cats);
		}

		if(isset($typeParam)){
			$typeParamArr = explode('-', $typeParam);
		}

	$popupalarList=CouponsVendors::getVendorsList();			

	$categoryList=	Coupons::getCategoryList($couponVendor[0]['id'],$typeParamArr);
	$storeList=CouponsVendors::getListswithSlug();
	$type=Coupons::getOfferType();

$uds= Yii::$app->request->get('id');
if(isset($uds)){
	 $this->view->params['index'] = 'false';
}

	 if(!$couponVendor[0]['google_index']) {
                 $this->view->params['index'] = 'false';
        }

if(isset($cats)){
	 $this->view->params['index'] = 'false';
}
	
    	
    	   $searchModel  = new CouponsSearch();
		   $searchModel -> moved=0;
		   $searchModel -> type=0;
		   $paramsdata=Yii::$app->request->queryParams;
		    $paramsdata['id_merchant']=$couponVendor[0]['id'];
		   $dataProvider = $searchModel->search($paramsdata);




 \Yii::$app->view->registerMetaTag([
        	'name' => 'description',
        	'content' => $couponVendor[0]['meta_descrption'] ,
        ]);
        \Yii::$app->view->registerMetaTag([
        	'name' => 'keyword',
        	'content' => $couponVendor[0]['meta_keyword'] ,
        ]);

			return $this->render('index', [
				'searchModel'  => $searchModel,
				'dataProvider' => $dataProvider,
				'category'=>$categoryList,
				'storeList'=>$storeList,
				'catArr'=> $catArr,
				'merchantArray' =>  $couponVendor[0]['id'] ,
				'type'=>$type,
					'popular'=>$popupalarList,
				'typeArray'=>$typeArray,
				'typeParam'=>$typeParamArr,
				'couponVendor'=>$couponVendor,
			]);
        return $this->render('index',['category'=>$categoryList]);
    }
    
    public function actionFilter(){
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	
    	/*
    	current = 1 (+0 = (current-1)*2 ) = 7 =>  1-3,  4-7
    	current = 2 (+2 = (current-1)*2 ) = 14 => 4-6,  7-11
    	current = 3 (+4 = (current-1)*2 ) = 21 => 7-9, 10-13
    	current = 4 (+6 = (current-1)*2 ) = 28 => 10-12, 13-16
    	current = 5 (+8 = (current-1)*2 ) = 35 => 13-15, 16-19
    	*/
    	
    	$pageNumber = (int)\Yii::$app->request->get('page');
    	$supplierId = (int)\Yii::$app->request->get('id');
    	$dealSuppliers = Suppliers::topDiscountedSuppliers();
    	if($pageNumber <= 1)
    		$pageSize = 7;
    	else 
    		$pageSize = ($pageNumber - 1) * 2;
    	
    	$moreDealImages = []; $supplerData = $supplierName = null;
    	foreach($dealSuppliers as $supplier){
    		if($supplierId === $supplier->id){
    			$supplierData = $supplier;
    			$supplierName = $supplier->name;
	    		$query = Deals::getDealsBySupplierQuery($supplierId, (($pageSize+$pageNumber)-1), 8);
// 	    		$countQuery = clone $query;
// 	    		$pages = new \yii\data\Pagination(['defaultPageSize' => $pageSize, 'totalCount' => $countQuery->count()]);
// 	    		$models = $query->offset()
// 	    			->limit(7);
	    		
	    		$moreDealImages = $query->all();
    		}
    	}
    	
    	return [
    		'meta' => ['status' => 200, 'message' => 'All good'], 
    		'data' => ['html' => $this->renderPartial('dealsData',['dealsData' => $moreDealImages, 
    					'supplier' => $supplierData, 'pageNumber' => $pageNumber]), 'id' => $supplierName,
    		]
    	];
    }

    private function findCouponById($id)
    {
        if (($model = Coupons::find()->where(['track_id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionStoreOut($id)
    {
        $this->layout = "track";
        $coupon = $this->findCouponById($id);
        $url = $coupon->link;
        $sessionId = Yii::$app->session->id;
        $activityType = ActivityTracker::Activity_COUPON;
        $storeName = $coupon->vendors->name;
        ActivityTracker::saveActivity($url, $sessionId, $activityType, $storeName);
        return $this->render("//product/send_to_store", [
            'url' => $url
        ]);
    }
}
