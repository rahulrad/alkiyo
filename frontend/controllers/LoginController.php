<?php
namespace frontend\controllers;
use Yii;
use common\models\LoginForm;
use frontend\models\SignupForm;
use yii\web\Controller;
use frontend\helpers\Sendsms;
class LoginController extends Controller {
	public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLoginRedirect()
    {
		
        return $this->render('login');   
    }
	
	public function actionProfile()
    {
		
        return $this->render('profile');   
    }
	
	public function actionForgot()
    {
		
       
    }
	
	public function actionProfileCreate()
    {
		
		$model = new SignupForm();
		$model->load(Yii::$app->request->post());
		$data = Yii::$app->request->post();  
		$model->first_name = $data['SignupForm']['first_name'];	
		$model->last_name = $data['SignupForm']['last_name'];
		$model->city = $data['SignupForm']['city'];
		$model->mobile = $data['SignupForm']['mobile'];
		$model->email = $data['SignupForm']['email'];
		$model->password_hash = $data['SignupForm']['password'];
		if(isset($data['conditions']))
		{
			$conditons = $data['conditions'];
		}
		else{
			$conditons = 0;
		}
	
		if(isset($data['offers']))
		{
			$offer = $data['offers'];
		}
		else{
			$offer = 0;
		}
		
		
		if (!$model->validate()) {
			
			Yii::$app->session->setFlash('error', "Your email and username not unique.");
        }
		else
		{ 
			$sql = "insert into user(`username`,`first_name`,`last_name`,`city`,`mobile`,`email`,`password_hash`,`created_at`,`term_conditions`,`newsletter_offer`)values('".$data['SignupForm']['first_name']."','". $data['SignupForm']['first_name']."','".$data['SignupForm']['last_name']."','".$data['SignupForm']['city']."','".$data['SignupForm']['mobile']."','".$data['SignupForm']['email']."','".Yii::$app->security->generatePasswordHash($data['SignupForm']['password'])."','".date("Y-m-d H:i:s")."',".$conditons.",".$offer.")";
			Yii::$app->db->createCommand($sql)->execute();
			Yii::$app->session->setFlash('success', "Profile create successfully");
		}
		return $this->render('profile');  
    }
	
	public function actionMobileVerification()
	{
		$otp = rand(1000,9999);
		$mobile	=  Yii::$app->request->post('mobile');
		Yii::$app->session->set("mobile_random",$otp);
		Yii::$app->session->set("mobile_status","fase");
		//Sendsms::sendsms($mobile,$otp);
		return $otp;
	}
	
	public function actionEditProfile()
	{
		$userId = Yii::$app->user->id;
		$queryes = new  yii\db\Query;
					$queryes->select(['user.*'])->from('user')->where(['user.id' => $userId]);
					$commands = $queryes->createCommand(); 
					$userdeatil = $commands->queryAll();
		return $this->render('edit-profile',['user'=>$userdeatil]);  
	}
	
	public function actionProfileUpdate()
	{
		$mobile	=  Yii::$app->request->post('mobile');
		\Yii::$app->db->createCommand("UPDATE user SET mobile=:column1 WHERE id=:id")
		->bindValue(':id', \Yii::$app->user->id)
		->bindValue(':column1', $mobile)
		->execute();
		
	}
	
	public function actionSendOtp()
	{
		$otp = rand(1000,9999);
		$mobile	=  Yii::$app->request->post('mobile');
		Sendsms::sendsms($mobile,$otp);
		return $otp;
	}
	
	public function actionUpdatePassword()
	{
		$password	=  Yii::$app->request->post('password');
		\Yii::$app->db->createCommand("UPDATE user SET password_hash =:column1 WHERE id=:id")
		->bindValue(':id', \Yii::$app->user->id)
		->bindValue(':column1', Yii::$app->security->generatePasswordHash($password))
		->execute();
	}
	
}
