<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
	'defaultRoute' => 'home',
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['admin'],
            'urlPrefix' => 'auth',
        ],
    ],
	 
    'components' => [
        'authClientCollection' => [
            'class'   => \yii\authclient\Collection::className(),
            'clients' => [
                'facebook' => [
                    'class'        => 'dektrium\user\clients\Facebook',
                    'clientId'     => '1685079011532659',
                    'clientSecret' => 'ff8ef90d7e94c20d9e754b259e72bacf',
                ],
                'google' => [
                    'class'        => 'dektrium\user\clients\Google',
                    'clientId'     => '277129216237-68a71p437in4plcj4okqeaoomkf2j5pb.apps.googleusercontent.com',
                    'clientSecret' => 'ZhV2s49M7L9tW2-dsj6PkVtU',
                ],
            ],
        ],
	
	'view' => [
			'class' => '\rmrevin\yii\minify\View',
			'enableMinify' => false,
			'concatCss' => false, // concatenate css
			'minifyCss' =>false, // minificate css
			'concatJs' => false, // concatenate js
			'minifyJs' => false, // minificate js
			'minifyOutput' => true, // minificate result html page
			'webPath' => '@web', // path alias to web base
			'basePath' => '@webroot', // path alias to web base
			'minifyPath' => '@webroot/minify', // path alias to save minify result
			'jsPosition' => [ \yii\web\View::POS_END,\yii\web\View::POS_HEAD ], // positions of js files to be minified
			'forceCharset' => 'UTF-8', // charset forcibly assign, otherwise will use all of the files found charset
			'expandImports' => true, // whether to change @import on content
			'compressOptions' => ['extra' => true], // options for compress
		],

        'LoginComponent' => [
            'class' => 'frontend\components\LoginComponent',
            ],
        'NewsletterComponent' => [
            'class' => 'frontend\components\NewsletterComponent',
            ],
	   'WishlistComponent' => [
            'class' => 'frontend\components\WishlistComponent',
            ],
	    'MenuComponent' => ['class' => 'frontend\components\MenuComponent',],
        'session' => [
            'name' => 'PHPFRONTSESSID',
            'savePath' => sys_get_temp_dir(),
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '9743088770',
            'csrfParam' => '_frontendCSRF',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request'=>[
                'class' => 'common\components\Request',
                'web'=> '/frontend/web'
        ],
        'urlManager' => [
			
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
				'wishlist/sendmail' => 'wishlist/send-mail',
				'login/sendOtp' => 'login/send-otp',
				'login/updatePassword' => 'login/update-password',
				'showroom/sendmail' => 'showroom/send-mail',
				'product/search' => 'product/search',
				'brand/getProductByCat' => 'brand/get-product-by-cat',
				'brand/getcategory' => 'brand/get-category',
				'brand/get-product' => 'brand/get-product',
				'wishlist/download' => 'wishlist/download',
				'showroom/findShowrooms' => 'showroom/find-showrooms',
				'showroom/otpverify' => 'showroom/otp-verify',
				'showroom/findshowroom' => 'showroom/find-showroom',
				'showroom/resendotp' => 'showroom/resend-otp',
				'category/<slug>' => '/category/index',
				'wishlist/save' => 'wishlist/save-wish-list',
				'product/getproduct' => 'product/get-product',
				'wishlist/items' => 'wishlist/index',
				'wishlist/del' => 'wishlist/delete-wish-list',
				'/product/category' =>'/product/collection',
				'product/childcollection/<slug>' =>'/product/childcollection',
				'brand/<slug>'=>'/brand/index',
				'login' =>'login/login-redirect', 
				'login/forgot'=>'/login/forgot',
				'login/profile'=>'login/profile',
				'login/profilecreate'=>'/login/profile-create',
				'login/editprofile' => 'login/edit-profile',
				'login/profileupdate' => 'login/profile-update',
				'sitemap.xml' => 'site/sitemap',
				'<category>-listing-sitemap.xml' => 'site/sitemaplist',
				'<category>-product-sitemap.xml' => 'site/sitemapproduct',
				'coupons' => '/deals/index',
				'<merchant>.html' => '/deals/deals',
				 'coupons/<merchant>' => '/deals/deals',
				'product-out-<id>' => '/product/store-out',
				'coupon-out-<id>' => '/deals/store-out',
				'engage' => '/articles',
				'engage/<slug>-<type>' => '/articles/items/category',
				'engage/<slug>' => '/articles/items/category',
				'engage/<type>/<alias>' => '/articles/items/view',
				'content/<slug>' => 'home/slug',
				'<title>-us' => 'site/contact',
				'site/captcha' => 'site/captcha',
				'home' => 'home/index',
				'product/insertorder' => '/product/insertorder',
				'search/products' => 'search/products',
				'search/index' => 'search/index',
				'product/comparesearch' => '/product/comparesearch',
				'product/compare' => '/product/compare',
				'product/addreview' => '/product/addreview',
				'product/reviewpost' => 'product/reviewpost',
				'product/reviewrank' => 'product/reviewrank',
				'product/priceupdate' => 'product/priceupdate',   
				'login/mobileVerification' => 'login/mobile-verification',
				'<category>/<slug>' => '/product/index',
				//'<slug>' => '/category/index',	
				 '<slug>' => '/prettyurl/index',
			],
        ],
    	'assetManager' => [
    		'bundles' => [
    			'yii\web\JqueryAsset' => [
    				'sourcePath' => null,   // do not publish the bundle
    				'js' => []
    			],
    		],
    	],
		'mailer' => [
		'class' => 'yii\swiftmailer\Mailer',
		'useFileTransport' => false,
		'transport' => [
			'class' => 'Swift_SmtpTransport',
			'host' => 'mail.radtechnoservices.com',  
			'username' => 'developer@radtechnoservices.com',
			'password' => 'developer#123',
			'port' => '25',
			'encryption' => 'tls',
			'streamOptions' => [
        'ssl' => [
            'verify_peer' => false,
            'verify_peer_name' => false,
        ],
    ],
			
		   ]
		],
		
    ],
	'timeZone'=>'Asia/Kolkata',
    'params' => $params,
];
