var findShowroom = (function(){
	var mailId,setCsrfToken,sendMailUrl;
	function startFindShowroom(csrf,emailSendUrl)
	{
		setCsrfToken = csrf;
		sendMailUrl = emailSendUrl;
		bindEvent();
	}
	function bindEvent()
	{
		$("#mailSend").bind('click',sendMail);
		$("#print").bind('click',printDiv);
	}
	
	function sendMail()
	{
		 mailId = $("#mailId").val();
		if(mailId == '')
		{	
			$("#sendMailError").html("Enter Email id");
		}
		else
		{
			 var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

			if (reg.test(mailId) == false) 
			{
				$("#sendMailError").html('Invalid Email Address');
				return false;
			}
			data = {
			mail : mailId,
			 _csrf : setCsrfToken
			};
			
			 $.ajax({
				url: sendMailUrl,
				type: 'POST',
				data: data,
				success: function(response){
					location.reload();
				 }
			 });

		}
	 }
	
		function printDiv() {
			var divToPrint = document.getElementById('showers-list');
			var popupWin = window.open('', '_blank', 'width=300,height=300');
			popupWin.document.open();
			popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
			popupWin.document.close();
		}
		
		return {
		callFindShowroom : startFindShowroom
		};
		
})();