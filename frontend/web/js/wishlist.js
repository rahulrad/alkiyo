var wishlistData = (function() {
	var urlWishlist , csrfToken;
	function startWishlist(csrfTokenVal,getWishlistUrl)
	{
		csrfToken = csrfTokenVal;
		urlWishlist = getWishlistUrl;
		bindEvent();
	}	
	
	function bindEvent()
	{
		$(".btn-product-remove").bind('click',wishlistDataDelete);	
	}
	
	function wishlistDataDelete(event)
	{
		//alert($(event.target.attr('id')));
		data = {
		id : $(event.currentTarget).attr('id'),
		 _csrf : csrfToken,
		};
		
		 $.ajax({
            url: urlWishlist,
            type: 'POST',
            data: data,
            success: function(response){
				location.reload();
             }
         });
	}
	
	return {
		callWishlist : startWishlist, 
	
	};
	
	
})();