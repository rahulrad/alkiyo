var profileData = (function() { 
	var $mobileNo,$csrfToken,$mobileVerifyUrl,$mobileOtp,$updateProfileUrl,$sendOtpUrl,$forgotOtp,$passwordUpdate,$forgotOtpVerify;
	var downloadTimer,setVerifyOtp;  
	function startProfile(csrfValue,verifyUrl,urlOfUpdateProfile,urlSendOtp,urlChangePassword)
	{
		$csrfToken = csrfValue;
		$mobileVerifyUrl = verifyUrl;
		$updateProfileUrl = urlOfUpdateProfile;
		$sendOtpUrl = urlSendOtp;
	    $passwordUpdate = urlChangePassword;
		bindEvent();
	}
	
	function bindEvent()
	{
		$("#mobileVerify").bind('click',profileData);
		$("#mobileVerifyOtp").bind('click',verifyOtp);
		$("#profileSubmit").bind('click',profleSubmit);
		$("#editMobile").bind('click',editMobileNo);
		$("#changePassword").bind('click',changePassword);
		$("#updateProfile").bind('click',updateProfile);
		$("#mob").bind('blur',sendOtp);
		$("#resend").bind('click',resendOtp);
		$("#updatePassword").bind('click',validate);
		$(".inputs").bind('keyup',nextFocus.bind(this));
	}
	
	
	function nextFocus(event) {
		$(event.target).next().focus();
	};
	
	function profileData()
	{
		$mobileNo = $("#inputMobile").val();
		data = {
					mobile : $mobileNo,
					_csrf : $csrfToken,
			};
		if($mobileNo != '')
		{
			 $.ajax({
				url: $mobileVerifyUrl,
				type: 'POST',
				data: data,
				success: function(response){
					 	$mobileOtp = response;
						$("#otpmessage").html('OTP has been sent to mobile no ******'+$mobileNo %100+'., pleas enter the same here to verify it');
						$('#verify-password').modal('show'); 
						clearInterval(downloadTimer);
						timer(); 
				 }
			 });
		}
		else
		{
			alert('Please enter mobile no');
		}
	}
	
	function timer()
	{
		var timeleft = 59;
		$("#Resendotpsubmit").prop('disabled', true);
		downloadTimer = setInterval(function()
		{
			var minutes = Math.floor(timeleft / 60);
			var seconds = 0;
			if(minutes > 0){
				minutes = '0' + minutes.toString();
				seconds = timeleft - (minutes * 60);
			}
			else{
				minutes = '00';
				seconds = timeleft;
			}
			
			if(seconds < 10){
				seconds = '0' + seconds.toString();
			}

			$("#timmer").html('Get OTP (' + minutes + ':' + seconds + ' minutes)');
			timeleft -= 1;
			if(timeleft <= 0){
				clearInterval(downloadTimer);
				$("#timmer").html('');
				
				setotp();
			}
		}, 1000);
	}
	
	
	function verifyOtp(){
		var oldotp = '';
		var otp1 = $("#otp1").val();
		var otp2 = $("#otp2").val();
		var otp3 = $("#otp3").val();
		var otp4 = $("#otp4").val();
		var otp = otp1 + otp2 + otp3 + otp4;	
		if(otp == $mobileOtp && $mobileOtp != '')
		{
			setVerifyOtp =1 ;
			$("#verify-password .close").click();
			
		}
		else
		{
			alert("Wrong otp");
		}
	}
	
	function profleSubmit(){
		if(setVerifyOtp == 1)
		{
			return true;
		}
		else
		{
			alert("Mobile verification First");
			return false;
		}
	}
	
	function editMobileNo()
	{
		$("#mobile").removeAttr("readonly");
	}
	
	
	function updateProfile()
	{
		data = {
		mobile : $("#mobile").val(),
		_csrf : $csrfToken
		};
		
		 $.ajax({
            url: $updateProfileUrl,
            type: 'POST',
            data: data,
            success: function(response){
				      location.reload();
             }
         });
		
	}
	function setotp()
	{
		$mobileOtp = '';
		
	}
	
	
	function changePassword()
	{
		$("#otp").attr("placeholder", "Enter OTP sent to +91 "+$("#mobile").val().substring(0, 2)+"********"+($("#mobile").val()%100));
	}
	
	function sendOtp()
	{
		$mobileNo = $("#mob").val();
	   if($mobileNo != '' && $mobileNo.length == 10 )
	   {
			data = {
				mobile : $mobileNo,
				_csrf : $csrfToken,
			};
			 $.ajax({
				url: $sendOtpUrl,
				type: 'POST',
				data: data,
				success: function(response){
					$forgotOtp = response;
				 }
			 });
	  }
	  else
	  {
		alert('Please enter valid mobile number');
	  }
	}
	
	function resendOtp()
	{
		$mobileNo = $("#mob").val();
	   if($mobileNo != '' && $mobileNo.length == 10 )
	   {
			data = {
				mobile : $mobileNo,
				_csrf : $csrfToken,
			};
			 $.ajax({
				url: $sendOtpUrl,
				type: 'POST',
				data: data,
				success: function(response){
					$forgotOtp = response;
				 }
			 });
	  }
	  else
	  {
		alert('Please enter valid mobile number');
	  }
	}
	
	 function validate() {
        var password = $("#txtNewPassword").val();
        var confirmPassword = $("#txtConfirmPassword").val();
        if (password != confirmPassword ) {
            alert("Passwords does not match.");
            return false;
        }
		
		if(password.length < 8)
		{
			alert("Password length must be 8 character");
			return false;
		}
		
		if($forgotOtp != $("#otp").val() || $("#otp").val() == '')
		{
			alert("Invalid otp");
			return false
		}
		
		data = {
		password : $("#txtNewPassword").val(),
		_csrf : $csrfToken,
		};
		
		 $.ajax({
				url: $passwordUpdate,
				type: 'POST',
				data: data,
				success: function(response){
					location.reload();
				 }
			 });
        
    }
	
	return {
		callProfile : startProfile,
	};
	
})();