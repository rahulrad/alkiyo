var layoutPageRender = (function(){
    var searchIndex = 0; 
    var searchVals = ['Products','Brands','Categories'];
    setInterval(function(){
        //$('#SearchText').attr('placeholder','Search For '+ searchVals[searchIndex%searchVals.length]);
         $('#SearchText').attr('placeholder','Search For '+ searchVals[searchIndex%searchVals.length]);
		searchIndex++;
    },2000); 
})();


//select 2
var select2 = (function(){
    $('.select2').each(function(){
        var ph = $(this).attr('placeholder');
        $(this).select2({
            placeholder: ph,
            minimumResultsForSearch: -1
        })
    });
    
})();

var homePageRender = (function() {
    var popularBrandsSlide = function() {
        $('#popular-brands-slide').lightSlider({
            item: 6,
            loop: false,
            slideMove: 2,
            easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
            speed: 600,
            responsive: [{
                    breakpoint: 991,
                    settings: {
                        item: 5,
                        slideMove: 1
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        item: 4,
                        slideMove: 1,
                        slideMargin: 6,
                    }
                },

                {
                    breakpoint: 480,
                    settings: {
                        item: 2,
                        slideMove: 1
                    }
                }
            ]
        });
    };

    var topCategoriesSlide = function() {
        $('#top-categories-slide').lightSlider({
            item: 5,
            loop: false,
            slideMove: 2,
            easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
            speed: 600,
            responsive: [{
                    breakpoint: 991,
                    settings: {
                        item: 3,
                        slideMove: 1
                    }
                },

                {
                    breakpoint: 768,
                    settings: {
                        item: 2,
                        slideMove: 1,
                        slideMargin: 6,
                    }
                },
                {
                    breakpoint: 420,
                    settings: {
                        item: 1,
                        slideMove: 1
                    }
                }
            ]
        });
    };

    var recentlyViewedSlide = function() {
        $('#recently-viewed-slide').lightSlider({
            item: 8,
            loop: false,
            slideMove: 2,
            easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
            speed: 600,
            responsive: [{
                    breakpoint: 1200,
                    settings: {
                        item: 6,
                        slideMove: 1
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        item: 4,
                        slideMove: 1,
                        slideMargin: 6,
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        item: 4,
                        slideMove: 1
                    }
                },
                {
                    breakpoint: 701,
                    settings: {
                        item: 3,
                        slideMove: 1
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        item: 2,
                        slideMove: 1
                    }
                }
            ]
        });
    };

    return {
        popularBrandsSlide: popularBrandsSlide,
        topCategoriesSlide: topCategoriesSlide,
        recentlyViewedSlide: recentlyViewedSlide
    }
})();

var brandDetailRender = (function() {
    var glimpsesViewedSlide = function() {
        $('#glimpses-viewed-slide').lightSlider({
            item: 3,
            loop: false,
            slideMove: 2,
            easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
            speed: 600,
            responsive: [{
                    breakpoint: 991,
                    settings: {
                        item: 3,
                        slideMove: 1
                    }
                },

                {
                    breakpoint: 768,
                    settings: {
                        item: 2,
                        slideMove: 1,
                        slideMargin: 6,
                    }
                },
                {
                    breakpoint: 420,
                    settings: {
                        item: 1,
                        slideMove: 1
                    }
                }
            ]
        });
    };
    return {
        glimpsesViewedSlide: glimpsesViewedSlide
    }
})();

