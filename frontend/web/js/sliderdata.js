var sliderData = (function() {
	var $urlGetProductData,$urlGetProductByCat,$csrfToken,$setSearchUrlProduct;
	var  $getProductByBrand 				= $('#brand');
	var  $setProductBrand 				= $("#productByBrand");
	var  $setFormUrlProduct 			    = $("#sliderproduct");
	var  $setFormUrlProductByCat 	    = $("#sliderproduct1");
	var  $getProductCat 					= $("#productCategory");
	var $setCatProduct                   =  $("#productnamebycat");
	
	function sliderStart(csrfTokenValue,setUrlGetProduct,setUrlProductByCat,setUrlSearchProd)
	{
		$csrfToken = csrfTokenValue;
		$urlGetProductData = setUrlGetProduct;
		$urlGetProductByCat = setUrlProductByCat;
		$setSearchUrlProduct = setUrlSearchProd;
		bindEvent();
	}
	
   function bindEvent()
   {
		 $getProductByBrand.bind('change', productByBrand); 
		 $setProductBrand.bind('change', setFormUrl);
		 $getProductCat.bind('change', getProductByCat);
		 $setCatProduct.bind('change', setFormUrlByCat);
		 $("#searchByBrand").bind('click',productSearchByBrand);
		 $("#productSearcchBycat").bind('click',productSearchByCat);
		 
   }
   
	function productByBrand(event)
	{

		if($(event.currentTarget).val() != '')
		 {
			
			 data = {
				 brand_id : $(event.currentTarget).val(),
				_csrf : $csrfToken,
			 };
			 $setProductBrand.html('');
			  $.ajax({
					 url: $urlGetProductData,
					 type: 'POST',
					 data: data,
					 success: function(response){
						 $setProductBrand.html(response);
					 }	
				 });
		 }
	}
	function getProductByCat(event)
	{
		if($(event.currentTarget).val() != '')
		{
			data = {
				cat_slug: $(event.currentTarget).val(),
				_csrf: $csrfToken,
			};
			$setCatProduct.html('');
			 $.ajax({
					url:  $urlGetProductByCat,
					type: 'POST',
					data: data,
					success: function(response){
						$setCatProduct.html(response);
					}	
				});
		}
	}
	
	function setFormUrlByCat()
	{
		$setFormUrlProductByCat.attr('action','product/index');
	}
	
	function setFormUrl()
	{
		$setFormUrlProduct.attr('action','product/index');
	}
	
	function productSearchByBrand()
	{
		var valOfSlug = $("#productByBrand").val();
		var getProductCat = valOfSlug.split("#");
		var newUrlProduct = $setSearchUrlProduct.replace('productSlug',getProductCat[0]);
		var newUrlpro = newUrlProduct.replace('categorySlug',getProductCat[1]);
		window.location.href = newUrlpro;
	}
	
	function productSearchByCat()
	{
		var valOfSlug = $("#productnamebycat").val();
		var valOfCat  = $("#productCategory").val();
		var newUrlProduct = $setSearchUrlProduct.replace('productSlug',valOfSlug);
		var newUrlpro = newUrlProduct.replace('categorySlug',valOfCat);
		window.location.href = newUrlpro;
	}
	
	return {
		callSlider : sliderStart,
	};
		
})();