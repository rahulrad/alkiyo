var compareData = (function(){
	var id,prodId,setCsrfToken,setBrandUrl,setCompareUrl,number;
	
	function startCompare(csrfToken,brandUrl,compareUrl)
	{
		setCsrfToken = csrfToken;
		setBrandUrl = brandUrl;
		setCompareUrl = compareUrl;
		bindEvent();
	}
	
	function bindEvent()
	{
		$(".brand").bind("change",getBrandProducts);
		$(".searchproduct").bind("change",getProducts);
		$(".btn-product-remove").bind("click",productRemove);
	}
	
	
	function getBrandProducts(event)
	{		
		prodId = $(event.currentTarget).attr('class').split(" ");
		 if($(event.currentTarget).val() != '' || $(event.currentTarget).val() >0)
		  {
			data = {
			brand_id : $(event.currentTarget).val(),
			_csrf : setCsrfToken
			};
			 $.ajax({
				url: setBrandUrl,
				type: 'POST',
				data: data,
				success: function(response){
					//$("#"+prodid).append(response);
					$("#"+prodId[prodId.length - 1]).append(response);
				 }
			 });	
		}
	}
	
 	function getProducts(event)
	{
		prodId = $(event.currentTarget).attr("class").split(" ");
		$("#"+prodId[prodId.length-1]).val($(event.currentTarget).val());
		var queryString = getCompareQueryString();
	    var location = setCompareUrl + queryString;
		window.location.href = location; 
	}
	
	 function getCompareQueryString(){
    	var queryString = '?';
		jQuery('input[name^="products"]').each(function() {
	        console.log(jQuery(this).val());
			if(jQuery.trim(jQuery(this).val()).length != 0){
				queryString = queryString.concat('products[]=' +jQuery.trim(jQuery(this).val()) + '&');
			}
		});
    	queryString += 'category_id='+jQuery('input[name="category_id"]').val();
		return queryString;
	}
	

    function productRemove(event)
	{
		id = $(event.currentTarget).prop('id');
    	number = id.charAt(id.length - 1);
    	number = '#bensonSearch' + number;
    	jQuery(number).val('');
    	queryString = getCompareQueryString();
    	var location = setCompareUrl + queryString;
		window.location.href = location;
	}
	
	
	return {
		callCompare : startCompare,
	};
})();