// class ProductFilter{
//     constructor(){
//         alert('constuctor called');
//     }
//     GetCheck() {
//         alert('getcheck called')    
//     }
// }
// var ProductFilter = (function(){
//     function GetCheck() {
//         alert('getcheck called')    
//     }
//     return{
//         check=GetCheck
//     }
// })();

class ProductFilter {

  constructor(urlChild, urlDataChild, urlCmpImg) {
    this.urlChildCollection = urlChild;
    this.urlDataChildCollection = urlDataChild;
    this.cmpImgUrl = urlCmpImg;

    this.currentView = '4grid';
    this.sort = 'popular';
    this.page = 1;
    this.selectedchec = 4;
    //this.urlChildCollection = '';
    //this.urlDataChildCollection = '';
    this.str_brand_ids = '';
    //this.cmpImgUrl = '';

    this.cacheDom();
    this.setBrandCheckBoxes();
    this.bindEvents();
    //this.ajaxSearch();
  }

  cacheDom() {
    this.$btnCompare = $('#compareproducts');
    this.$btnCompareClose = $('.btn-compare-close');
    this.$btnClearAll = $('#clearall');
    this.$btnFilterClearAll = $('#clear_all');
    this.$selectRecomended = $('#sel1');
    this.$txtBrand = $('#txtBrand');
    this.$brandsValues = $('.brandsValues');
    this.$suppliersValues = $('.suppliersValues');
    this.$featureValues = $('.featureValues');
    this.$checkBoxAddToCompare = $('.js-add-to-cmpr');
    this.$btnviewMore = $('.viewMore');
    this.$more = $(".more");
    //alert(this.$btnviewMore.length);
  }

  bindEvents() {
    this.$btnCompare.on('click', this.addToCompareBox.bind(this));
    this.$more.on('click', this.showMore.bind(this));
    this.$btnCompareClose.on('click', this.compareClose.bind(this));
    this.$btnClearAll.on('click', this.clearAll.bind(this));
    this.$selectRecomended.on('change', this.setRecomended.bind(this));
    this.$txtBrand.on('keyup', this.selectBrand.bind(this));
    this.$brandsValues.on('click', this.ajaxSearch.bind(this));
    this.$suppliersValues.on('click', this.ajaxSearch.bind(this));
    this.$featureValues.on('click', this.ajaxSearch.bind(this));
    this.$btnFilterClearAll.on('click', this.clearFilterAll.bind(this));
    this.$btnviewMore.on('click', this.viewMore.bind(this));
    this.$checkBoxAddToCompare.on("change", this.setCompareItem.bind(this));
  }
  // Getter

  addToCompareBox() {
    var notemptyTextBoxes = $('.cmpr-pnl-list').find('.hugoSearch').filter(
      function (e) {
        return $('.cmpr-pnl-list').find('.hugoSearch').eq(e).val() !== "";
      });

    if (notemptyTextBoxes.length > 0) {
      var string = "";
      notemptyTextBoxes.each(function (e) {
        string += "&products[]=" + $("#" + $(notemptyTextBoxes).eq(e).attr('id')).attr('slug');
      });
      var params = string.substring(1, string.length);
      location.href = '/product/compare?' + params;
    }
  }
  getCompareItems = () => {
    let count = 0;
    // alert($('.compare-box-items').find('.hugoSearch').length);
    // $('.compare-box-items').find('.hugoSearch').each((h3item) => {
    //   alert($(h3item).html());
    //   if ($(h3item).attr('slug') != undefined) {
    //     count++;
    //   }
    // });
    // alert($("#div1").find('.hugoSearch').val());
    // alert($("#div2").find('.hugoSearch').val());
    // alert($("#div3").find('.hugoSearch').val());
    // alert($("#div4").find('.hugoSearch').val());
    if ($("#div1").find('.hugoSearch').val() != '') {
      count++;
    }
    if ($("#div2").find('.hugoSearch').val() != '') {
      count++;
    }
    if ($("#div3").find('.hugoSearch').val() != '') {
      count++;
    }
    if ($("#div4").find('.hugoSearch').val() != '') {
      count++;
    }
    return count;
  }
  setCompareItem = (event) => {
    //alert('add to compare called.')
    $(".compare-box").show();
    let $checkboxInstance = $(event.target);
    if ($('.cmpr-pnl-wrpr').hasClass('add-cmp-ml')) {
      $('.cmpr-pnl-wrpr').removeClass('add-cmp-ml');
    }

    var emptyTextBoxes = $('.cmpr-pnl-list').find('.hugoSearch').filter(
      function (e) {
        return $('.cmpr-pnl-list').find('.hugoSearch').eq(e).val() == "";
      }
    );
    // alert(selectedPro);
    // alert(this.selectedchec);
    if ($checkboxInstance.prop("checked") == true) {
      let selectedPro = this.getCompareItems();
      if (selectedPro < this.selectedchec) {
        var targetElement = $("#" + emptyTextBoxes[0].id);
        var targetElementImage = targetElement.parents('.comparerow').find('.compare-product img').offset();
        var slug = $checkboxInstance.attr('slug');
        var name = $checkboxInstance.attr('name');
        var imageOffset = $checkboxInstance.parents('.collection-item').closest('div').find('img').prop('src');
        var cart = $('nav .navbar-right strong').offset();
        if ($("#div1").find('.hugoSearch').val() == '') {
          $("#div1name").text(name);
          $("#div1").find('.hugoSearch').val(name);
          $("#div1").find('.hugoSearch').attr('slug', slug);
          $("#div1").find('img').attr('src', imageOffset);
          $("#div1").find('button').attr('id', slug);
        }
        else if ($("#div2").find('.hugoSearch').val() == '') {
          $("#div2name").text(name);
          $("#div2").find('.hugoSearch').val(name);
          $("#div2").find('.hugoSearch').attr('slug', slug);
          $("#div2").find('img').attr('src', imageOffset);
          $("#div2").find('button').attr('id', slug);
        }
        else if ($("#div3").find('.hugoSearch').val() == '') {
          $("#div3name").text(name);
          $("#div3").find('.hugoSearch').val(name);
          $("#div3").find('.hugoSearch').attr('slug', slug);
          $("#div3").find('img').attr('src', imageOffset);
          $("#div3").find('button').attr('id', slug);
        }
        else if ($("#div4").find('.hugoSearch').val() == '') {
          $("#div4name").text(name);
          $("#div4").find('.hugoSearch').val(name);
          $("#div4").find('.hugoSearch').attr('slug', slug);
          $("#div4").find('img').attr('src', imageOffset);
          $("#div4").find('button').attr('id', slug);
        }

      } else {
        $checkboxInstance.prop('checked', false);
        if (this.selectedchec > 0) {
          alert("Maximum " + this.selectedchec + " products compare at a time");
        }
      }
    } else {
      var slug = $checkboxInstance.attr('slug');
      var objectInput = $(".cmpr-pnl-list input[slug=" + slug + "]");
      objectInput.val("");
      objectInput.attr('slug', '');
      objectInput.siblings('img').attr('src', this.cmpImgUrl);
      objectInput.siblings('h3').text('Enter product name');
    }
    $("#compareproducts").text('Compare (' + (this.getCompareItems()) + ')');
  }
  compareClose = (event) => {
    var slug = $(event.target).attr('id');
    if (slug != '') {
      var objectInput = $(".cmpr-pnl-list input[slug=" + slug + "]");
      objectInput.val("");
      objectInput.attr('slug', '');
      objectInput.siblings('img').attr('src', this.cmpImgUrl);
      objectInput.siblings('h3').text('Enter product name');
      $("input[slug='" + slug + "']:checkbox").prop('checked', false);
      $("#compareproducts").text('Compare (' + this.getCompareItems() + ')');
    }
  }
  clearAll = () => {
    $("input[class='js-add-to-cmpr']:checkbox").prop('checked', false);
    var objectInput = $(".cmpr-pnl-list input");
    objectInput.val("");
    objectInput.attr('slug', '');
    objectInput.siblings('img').attr('src', this.cmpImgUrl);
    objectInput.siblings('h3').text('Enter product name');
    $("#compareproducts").text('Compare (0)');
  }
  setRecomended = () => {
    $("#2grid").hide();
    $("#3grid").hide();
    $("#4grid").hide();
    //alert(this.$selectRecomended.val());
    if (this.$selectRecomended.val() > 0) {
      $("#" + this.$selectRecomended.val() + "grid").show();
    }
    else {
      $("#4grid").show();
    }
  }
  selectBrand = () => {
    var value = this.$txtBrand.val();
    $('.filter-checkbox label.txtBrand').hide().each(function (e) {

      if ($('.filter-checkbox label.txtBrand').eq(e).text().search(new RegExp(value, "i")) > -1) {
        $('.filter-checkbox label.txtBrand').eq(e).show();
      } else {
        $('.filter-checkbox label.txtBrand').eq(e).hide();
      }
    });
  }
  getCommaSepratedBrandValues = () => {
    var values = [];
    $(".brandsValues").each(function (e) {
      if ($(".brandsValues").eq(e).prop('checked') == true && !values.includes($(".brandsValues").eq(e).val()))
        values.push($(".brandsValues").eq(e).val());
    });

    if (values.length > 0)
      return values.join(',');
    return '';
  }
  getFeaturesIdsArray() {
    var values = [];
    $(".featureValues").each(function (e) {
      if ($(".featureValues").eq(e).prop('checked') == true)
        values.push($(".featureValues").eq(e).val());
    });
    return values;
  }
  getFeatureValuesGetString = () => {
    let features = this.getFeaturesIdsArray();
    let getString = '';
    if (features.length > 0) {
      for (var i = 0; i < features.length; i++) {
        getString += '&feature_ids[]=' + features[i];
      }
    }
    return getString;
  }
  getSuppliersValues() {
    var values = [];
    $(".suppliersValues").each(function (e) {
      if ($(".suppliersValues").eq(e).prop('checked') == true)
        values.push($(".suppliersValues").eq(e).val());
    });
    return values;
  }
  getSupplierValuesForQueryString = () => {
    var values = this.getSuppliersValues();
    let supplierQueryString = '';
    if (values.length > 0) {
      for (var i = 0; i < values.length; i++) {
        supplierQueryString += '&store[]=' + values[i];
      }
    }
    return supplierQueryString;
  }
  ajaxSearch = (event, pageNumber = 1) => {
    this.page = pageNumber;
    this.str_brand_ids = "";
    this.str_brand_ids = this.getCommaSepratedBrandValues();
    let data = {
      /* 	'store[]': getSuppliersValues(),
          'feature_ids[]' : getFeaturesIdsArray(),
          min_price : min_price,
          max_price : max_price, */
      'feature_ids[]': this.getFeaturesIdsArray(),
      page: this.page,
      currentView: this.currentView,
      brands: this.str_brand_ids,
      sort: this.sort
    };
    const selfObj = this;
    window.history.pushState('object or string', 'Title', this.urlChildCollection + '?brands=' + this.str_brand_ids + this.getFeatureValuesGetString() + this.getSupplierValuesForQueryString());
    $.ajax({
      url: this.urlDataChildCollection,
      type: 'GET',
      data: data,
      success: (response) => {
        //alert('post fired !');
        $("#2grid").html('');
        $("#3grid").html('');
        $("#4grid").html('');
        $("#pageNumber").val(1);
        $("#2grid").html(response.list2Html);
        $("#3grid").html(response.list3Html);
        $("#4grid").html(response.list4Html);
        var currentlyShowing = response.listingResults * response.pageNumber;
        if (currentlyShowing > response.total) {
          currentlyShowing = response.total;
        }
        //$('.viewMore').parent().show();
        $('h3.pull-left').html('<span style="font-size:12px">' + (currentlyShowing) + ' out of ' + response.total + '</span>');
        // product.init();
        //alert($('.js-add-to-cmpr').length);
        // $this = product;
        // this.$checkBoxAddToCompare = $('.js-add-to-cmpr');
        // alert(this.$checkBoxAddToCompare.length);
        // this.$btnCompare = $('#compareproducts');
        //alert($('.viewMore').length);
        $('.viewMore').on('click', this.viewMore.bind(this));
        $('.js-add-to-cmpr').on("change", this.setCompareItem);
      }
    });
  }

  clearFilterAll = () => {
    this.$txtBrand.val('');
    this.$txtBrand.keyup();
    $('.searchfeatures').val('');
    $('.searchfeatures').keyup();
    $('.featureValues').prop('checked', false);
    $('.brandsValues').prop('checked', false);
    //this.clearAll();
    this.ajaxSearch();
  }
  viewMore(e) {
    $('.viewMore').remove();
    //alert('view more');
    var pagenum = parseInt($("#pageNumber").val()) + 1;
    this.addResults(pagenum);
  }
  getCategoryIdsArray() {
    var values = [];
    $(".categoryValues").each(function () {
      if ($(this).prop('checked') == true)
        values.push($(this).val());
    });
    return values;
  }
  getCommaSepratedCategoryIds = () => {
    var values = this.getCategoryIdsArray();
    if (values.length > 0)
      return values.join(',');
    return '';
  }
  addResults = (pageNumber = 1) => {

    let categoryIds = this.getCommaSepratedCategoryIds();
    /*if(str_brand_ids.length > 0){
        str_brand_ids = str_brand_ids.substr(0, (str_brand_ids.length-1)  )
    }*/
    this.str_brand_ids = this.getCommaSepratedBrandValues();

    let data = {
      'feature_ids[]': this.getFeaturesIdsArray(),
      page: pageNumber,
      currentView: this.currentView,
      brands: this.str_brand_ids,
      sort: this.sort,
    };
    $.ajax({
      url: this.urlChildCollection,
      type: 'GET',
      data: data,
      success: (response) => {
        $("#2grid").append(response.list2Html);
        $("#3grid").append(response.list3Html);
        $("#4grid").append(response.list4Html);
        // alert(listingResults);
        // alert(total);
        var currentlyShowing = response.listingResults * response.pageNumber;
        if (currentlyShowing > response.total) {
          currentlyShowing = response.total;
        }
        //$('.viewMore').parent().hide();
        $('h3.pull-left').html('<span style="font-size:12px">' + (currentlyShowing) + ' out of ' + response.total + '</span>');
        $("#pageNumber").val((parseInt(response.pageNumber)));
        $('.viewMore').on('click', this.viewMore.bind(this));
        $('.js-add-to-cmpr').on("change", this.setCompareItem);

      }
    });
  }
  showMore(event) {

    var id = $(event.target).attr('id');
    $(".show" + id).hide();
    $(".more" + id).show();
  }

  getParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
      var urlparam = url[i].split('=');
      if (urlparam[0] == param) {
        return urlparam[1];
      }
    }
  }

  setBrandCheckBoxes = () => {
    var strBrands = this.getParameterValues('brands');
    if (strBrands != undefined && strBrands.length > 0) {
      var brandArray = strBrands.split(',');
      $(".brandsValues").each(function (e) {
        if (brandArray.includes($(".brandsValues").eq(e).val())) {
          $(".brandsValues").eq(e).prop('checked', 'checked');
        }
      });
    }
  }
}