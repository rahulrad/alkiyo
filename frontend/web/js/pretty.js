var productData = (function(){
	var setCsrfToken,mobile,name,cityName,userId,prod,catId,brandId,setWishlistUrl,mobileNo,sendOtpUrl,sendResendOtpUrl,otp,otpVerifyUrl;
	function startProduct(csrfToken,wishlistUrl,otpUrl,resendOtpUrl,otpUrl)
	{
		setCsrfToken = csrfToken;
		setWishlistUrl = wishlistUrl;
		sendOtpUrl = otpUrl;
		sendResendOtpUrl = resendOtpUrl;
		otpVerifyUrl = otpUrl;
		eventBind();
	}
		
	function eventBind()
	{
		$("#whislist").bind('click',addToWishlist);
		$("#desc").bind('click',showDescription);
		$("#findShowrooms").bind('click',findShowroomModel);
		$("#sendOtp").bind('click',sendOtp);
		$("#resendOtp").bind('click',resendOtp);
		$("#findShowroom").bind('click',findShowroom);
	}
	
	function addToWishlist() {
		 userId = $("#userid").val();
		 prod  = $("#prodid").val();
		 catId = $("#catid").val();
		 brandId = $("#brandid").val();
		data = {
			user_id : userId,
			product_id : prod,
			cat_id : catId,
			brands_brand_id : brandId,
			_csrf : setCsrfToken,
		};
		 $.ajax({
            url: setWishlistUrl,
            type: 'POST',
            data: data,
            success: function(response){
				        location.reload();
             }
         });
	}
	
	function showDescription(){
        $('html, body').animate({scrollTop:$('#desc').offset().top}, 2000);
	}
	
	function findShowroomModel() {
		if($("#pinCode").val()){
		$('#showroom-location').modal('show'); }
	}
	
	function sendOtp() {
		mobileNo = $("#mobileno").val();
		if(mobileNo && mobileNo.length == 10)
		{
			name = $("#name").val();
			city = $("#pinCode").val();
			datas = {
				name : name,
				mobile : mobileNo,
				city : city,
				_csrf: setCsrfToken
			};
			 $.ajax({
            url: sendOtpUrl,
            type: 'POST',
            data: datas,
            success: function(response){
				     $(".resend").show();   
				}
			}); }
		else {
				alert('Invalid mobile no'); }
	}
	
	function resendOtp() {
		if(mobileNo && mobileNo.length == 10)
		{
			data = {
				mobile : mobileNo,
				_csrf: setCsrfToken
			};
			 $.ajax({
            url: sendResendOtpUrl,
            type: 'POST',
            data: data,
            success: function(response){
				     
				}
			});
			
		}
		else
		{
			alert('Invalid Otp');
		}	
	}
	
	
	function findShowroom(){
			  otp = $("#otp").val();
			if(otp && otp.length>0)
			{
				data = {
					mobile : mobileNo,
					otp : otp,
					_csrf : setCsrfToken
				};
				 $.ajax({
					url: otpVerifyUrl,
					type: 'POST',
					data: data,
					success: function(response){
						if(response != 'invalid')
						{
							if(response != 'error')
							{
								var responsedata = response.split("#");
								$("#pincodeshow").val(responsedata[0]);
								$("#nameshow").val(name);
								$("#mobileshow").val(mobileNo);
								$("#idshow").val(responsedata[1]);
								$('#fill-location').modal('show'); 
								
							}
						}
						else
						{	
							alert('invalid');
						}							
					}	
				});
			}
			else {
				alert("Please enter otp");
			}
			
	}
	return {
		callProduct : startProduct,
	};
	

})();