<?php
namespace frontend\models;

use Yii;


/**
 * Signup form
 */
class SignupForm extends \yii\db\ActiveRecord   
{
    public $username;
	public $first_name;
	public $last_name;
    public $email;
    public $password;
	public $password_repeat;
	public $city;
	public $_csrf;
	public $mobile; 
    /**
     * @inheritdoc
     */
	 public static function tableName()   
    {   
        return 'user';   
    }
	 
    public function rules()
    {
        return [
			['first_name', 'filter', 'filter' => 'trim'],
            ['first_name', 'required'],
            //['first_name', 'unique', 'message' => 'This username has already been taken'],
            ['first_name', 'string', 'min' => 1, 'max' => 50],			
			['last_name', 'required'],
			['last_name', 'string', 'min' => 1, 'max' => 50],		
			['city', 'required'],
			['city', 'string', 'min' => 3, 'max' => 20],	
			['mobile', 'required'],
			['mobile', 'number'],
			['mobile','string', 'min' => 10 , 'max' => 10],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
           // ['email', 'unique','targetAttribute'=>'email', 'message' => 'This email address has already been taken'],
			[['first_name', 'email'], 'unique','message' => 'This username has already been taken'],
            ['password', 'required'],
            ['password', 'string', 'min' => 8],  
			['password_repeat', 'required'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Password does n't match" ]			
        ];
    }
		public function attributeLabels() {
        return [
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'city' => 'City',
			'mobile' => 'Mobile',
           'email' => 'Email',
		   'password' => 'Password',	
		   'password_repeat' => 'Re-type password',
        ];
    }
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
      
       /*  $user = new User();
        $user->first_name = $this->first_name;
		$user->last_name = $this->last_name;
	    $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->mobile = $this->mobile;
		if (!$this->validate()) {
           
			$errores = $this->getErrors();
			 return $errores;

        }
		if($user->save())
		{
			return 'success';
		}
		else
		{
			echo 'success';
		} */
		
    }
}
