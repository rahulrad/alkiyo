<?php
namespace frontend\models;
use Yii;


/**
 * This is the model class for table "categories".
 *
 * @property integer $category_id
 * @property string $category_name
 * @property integer $parent_category_id
 * @property string $category_description
 * @property string $category_status
 * @property string $created
 * @property int $is_index
 */


class WishList extends \yii\db\ActiveRecord   
{
   /*  public $user_id;
	public $product_id;
	public $cat_id; 
	public $brands_brand_id;
	*/

	
	public static function tableName()   
    {   
        return 'wishlist';   
    }  

	public function rules()   
    {   
        return [   
            [['user_id', 'product_id', 'cat_id','brand_id'], 'required'],   
            
        ];   
    }  

  public function attributeLabels() {
        return [
            'user_id' => 'user_id',
            'product_id' => 'product_id',
            'cat_id' => 'cat_id',
			'brand_id' => 'brand_id',
           
        ];
    }

    public static function countwishlist()
	{
		if (!\Yii::$app->user->isGuest) {
            $userId = \Yii::$app->user->id;
			$query = new  yii\db\Query;
		$query	->select(['wishlist.*'])  
        ->from('wishlist')
        ->where(['user_id' => $userId])
         		->orWhere(['ip_add' => Yii::$app->request->userIP]);
		$command = $query->createCommand();
		$data = $command->queryAll();
			
        }else{	
			$query = new  yii\db\Query;
			$query	->select(['wishlist.*'])  
			->from('wishlist')
			->Where(['ip_add' => Yii::$app->request->userIP]);
			$command = $query->createCommand();
			$data = $command->queryAll();
			
        }	
         return $data;		
	
	}
	
	

   
}
