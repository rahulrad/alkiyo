<?php
namespace frontend\models;
use Yii;


/**
 * This is the model class for table "categories".
 *
 * @property integer $category_id
 * @property string $category_name
 * @property integer $parent_category_id
 * @property string $category_description
 * @property string $category_status
 * @property string $created
 * @property int $is_index
 */


class Showroomdetail extends \yii\db\ActiveRecord   
{
   /*  public $user_id;
	public $product_id;
	public $cat_id; 
	public $brands_brand_id;
	*/

	
	public static function tableName()   
    {   
        return 'showroom';   
    }  

	public function rules()   
    {   
        return [   
            [['phone'], 'required'],   
            
        ];   
    }
	
	public function attributeLabels() {
        return [
            'name' => 'name',
            'address' => 'address',
			'city' => 'city',
            'pincode' => 'pincode',
			'phone_no' => 'phone_no',
			'email' => 'email',
			'status' => 'status',
           
        ];
    }
	
}
