<?php
namespace frontend\models;
use Yii;


/**
 * This is the model class for table "categories".
 *
 * @property integer $category_id
 * @property string $category_name
 * @property integer $parent_category_id
 * @property string $category_description
 * @property string $category_status
 * @property string $created
 * @property int $is_index
 */


class Showroom extends \yii\db\ActiveRecord   
{
   /*  public $user_id;
	public $product_id;
	public $cat_id; 
	public $brands_brand_id;
	*/

	
	public static function tableName()   
    {   
        return 'user_info_showroom_location';   
    }  

	public function rules()   
    {   
        return [   
            [['phone'], 'required'],   
            
        ];   
    }
	
	public function attributeLabels() {
        return [
            'phone' => 'phone',
            'name' => 'name',
            'pincode' => 'pincode',
			'construction_statu' => 'construction_statu',
			'bathroom' => 'bathroom',
			'otp' => 'otp',
           
        ];
    }
	
}
